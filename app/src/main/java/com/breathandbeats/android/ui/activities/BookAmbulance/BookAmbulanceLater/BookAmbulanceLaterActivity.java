package com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceLater;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.BookLater.BookAmbulanceLaterResponce;
import com.breathandbeats.android.pojos.BookLater.BookAmbulanceLater_Input;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.breathandbeats.android.ui.fragments.CalendarPickerFragment;
import com.breathandbeats.android.ui.fragments.TimePickerFragment;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rakesh on 09-10-2017.
 */

public class BookAmbulanceLaterActivity extends AppCompatActivity implements  TimePickerFragment.TimeDialogListener ,
        CalendarPickerFragment.DateDialogListener{


    private final static String TAG = "BookAmbulanceLaterfile";
    private static final String DIALOG_DATE = "BookAmbulanceLaterActivity.DateDialog";
    private static final String DIALOG_TIME = "BookAmbulanceLaterActivity.TimeDialog";
    int PLACE_AUTOCOMPLETE_REQUEST_CODE=1;
    int PLACE_PICKER_REQUEST = 1;
    private static  double Search_Latitude;
    private static  double Search_Longitude;

   @BindView(R.id.btnBookAmbulanceNow)
    Button btnBookAmbulanceNow;

    @BindView(R.id.btnSelectCalender)
    RelativeLayout btnSelectCalender;

    @BindView(R.id.btnSelectTime)
    RelativeLayout btnSelectTime;

    @BindView(R.id.rlBackarrow)
    RelativeLayout rlBackarrow;

    @BindView(R.id.tvAddressField)
    TextView tvAddressField;

     String emergencydata=null;
    @BindView(R.id.btnSearchAddress)
    Button btnSearchAddress;

    @BindView(R.id.tvpickupdate)
    TextView tvpickupdate;
    @BindView(R.id.tvpickuptime)
    TextView tvpickuptime;

    Dialog dialogForConfirmation;
    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    long difference_inseconds;

    BookAmbulanceLaterResponce bookAmbulanceLaterResponce_v=new BookAmbulanceLaterResponce();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bookambulancelater);

        ButterKnife.bind(this);

        application = (BnBApp) getApplication();
        sharedPrefHelper = application.getSharedPrefHelper();
        bnBService = application.getRestClient().getApiSerivice();// for API Call


        btnBookAmbulanceNow.setBackground(getResources().getDrawable(R.drawable.round_blue_corner));
        btnBookAmbulanceNow.setTextColor(getResources().getColor(R.color.black));

        Intent emergencydataintent=getIntent();
        if(emergencydataintent.hasExtra("EmergencyData")) {
            emergencydata = emergencydataintent.getStringExtra("EmergencyData");
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.btnSelectCalender)
    public  void  btnSelectCalenderClicked(){
        getDatePicker();
    }

    @OnClick(R.id.btnSelectTime)
    public  void  OnbtnSelectTimeClicked(){

      getTimePicker();
    }

    @OnClick(R.id.btnBookAmbulanceNow)
    public  void  onbtnBookAmbulanceNowClicked(){

        if(NetworkUtil.getConnectivityStatusBoolen(this)) {

            getTheVerification();
        }else{
            BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",BookAmbulanceLaterActivity.this);
        }


    }

    private String convertDate(String date) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");
            Date d = format.parse(date);
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            return serverFormat.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    @OnClick(R.id.btnSearchAddress)
    public  void onbtnSearchAddressClicked(){

        if(NetworkUtil.getConnectivityStatusBoolen(BookAmbulanceLaterActivity.this)){

            getLocationPicker();
        }else{
            BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",application);
        }


    }

    private void getLocationPicker() {


        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);


                Search_Latitude=place.getLatLng().latitude;
                Search_Longitude=place.getLatLng().longitude;
                tvAddressField.setText(place.getAddress());

                gettextverification();
            }
        }
    }

    private void getTheVerification() {
        if(tvpickupdate.getText()!=null){

            if(tvpickupdate.getText().toString().length()!=0 ){

                if(tvpickuptime.getText()!=null){

                    if(tvpickuptime.getText().toString().length()!=0){


                        if(tvAddressField.getText().toString()!=null){

                            if(tvAddressField.getText().toString().length()>0){

//                                Toast.makeText(this, "Thank you for booking.", Toast.LENGTH_SHORT).show();


                                getVerifiactionDialogBox();



                            }else{
                               BnBUserUtility.toastMessage_info( "Please search for pickup address",this);
                            }
                        }else{

                        }


                    }else{
                        BnBUserUtility.toastMessage_info("Please select your booking time and try again. ",this);
                    }
                }else{
                    BnBUserUtility.toastMessage_info( "Please select your booking time and try again. ",this);
                }


            }else{
                BnBUserUtility.toastMessage_info("Please select your booking date and time and try again. ",this);
            }


        }else{
            BnBUserUtility.toastMessage_info( "Please select your booking date and try again. ",this);
        }
    }

    @OnClick(R.id.rlBackarrow)
    public  void  OnrlBackarrowClicked(){
        onBackPressed();
    }

    private void getTimePicker() {

        TimePickerFragment dialog = new TimePickerFragment();
        dialog.show(getSupportFragmentManager(), DIALOG_TIME);
    }

    private void gettextverification() {

        if(tvpickupdate.getText()!=null){

            if(tvpickupdate.getText().toString().length()!=0 ){

                if(tvpickuptime.getText()!=null){

                    if(tvpickuptime.getText().toString().length()!=0){

                        if(tvAddressField.getText().toString()!=null){

                            if(tvAddressField.getText().toString().length()>0){

                                btnBookAmbulanceNow.setBackground(getResources().getDrawable(R.drawable.rounded_blue_filled));
                                btnBookAmbulanceNow.setTextColor(getResources().getColor(R.color.white));

                            }
                        }
                    }
                }
            }
        }

    }


    private void getDatePicker() {

        CalendarPickerFragment dialog = new CalendarPickerFragment();
        dialog.show(getSupportFragmentManager(), DIALOG_DATE);
    }

    @Override
    public void onFinishDialog(String time) {

        String timevalue = time;
        String RequestTime = tvpickupdate.getText().toString() + " " + timevalue.toString();

        int time_difference=date_comparison(convertDate(RequestTime));
        Log.d(TAG, " time diff:  " + date_comparison(convertDate(RequestTime)));

      if (time_difference >= 6)
        {
            tvpickuptime.setText(timevalue);
            gettextverification();

        }
        else {
            BnBUserUtility.toastMessage_info("You can't request for ambulance for selected time,Booking time must be more than 6 hours from current time.", BookAmbulanceLaterActivity.this);
        }

    }

    @Override
    public void onFinishDialog(Date date) {

        String datavalue=formatDate(date);

        tvpickupdate.setText(datavalue);
        tvpickuptime.setText("");

        gettextverification();
    }

    public String formatDate(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String hireDate = sdf.format(date);
        return hireDate;
    }

//-------------------------------------------------------
    private void getVerifiactionDialogBox() {

    dialogForConfirmation = new Dialog(this);
    dialogForConfirmation.setContentView(R.layout.dialog_booklater);
    dialogForConfirmation.show();

    Button Done_Btn = (Button) dialogForConfirmation.findViewById(R.id.Done_Btn);
    TextView tvTripMessage = (TextView) dialogForConfirmation.findViewById(R.id.tvTripMessage);

//    tvTripMessage.setText("Please make a booking payment within 15 minute, otherwise your booking request will be cancelled automatically.");
        tvTripMessage.setText("Are you sure, You want to book ambulance ?");
    Done_Btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            dialogForConfirmation.dismiss();

            if(NetworkUtil.getConnectivityStatusBoolen(BookAmbulanceLaterActivity.this)){
                BnBUserUtility.showProgress(BookAmbulanceLaterActivity.this,"Please wait...");
                RequestForHospital();
            }else{
                BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",application);
            }

        }
    });

}

    private void RequestForHospital() {
        String token="Bearer "+sharedPrefHelper.getAuthToken();

        String RequestTime = tvpickupdate.getText().toString()+" "+tvpickuptime.getText().toString();
        Log.d(TAG," DATE 0 : "+convertDate(RequestTime));
        Log.d(TAG," emergencydata : "+emergencydata);
        BookAmbulanceLater_Input bookAmbulanceLater_input=new BookAmbulanceLater_Input();
        bookAmbulanceLater_input.setEmergencyType(emergencydata);
        bookAmbulanceLater_input.setUserId(sharedPrefHelper.getUserID());
        bookAmbulanceLater_input.setScheduledOnAppTime(convertDate(RequestTime));
        bookAmbulanceLater_input.setUserAddress(tvAddressField.getText().toString());

        List<String> LatLong = new ArrayList<String>();

        LatLong.add(String.valueOf(Search_Longitude));
        LatLong.add(String.valueOf(Search_Latitude));

        bookAmbulanceLater_input.setUserCurrentlocation(LatLong);

        Call<BookAmbulanceLaterResponce> bookAmbulanceLaterResponce=bnBService.BookAmbulanceLater(token,bookAmbulanceLater_input);

        bookAmbulanceLaterResponce.enqueue(new Callback<BookAmbulanceLaterResponce>() {
            @Override
            public void onResponse(Call<BookAmbulanceLaterResponce> call, Response<BookAmbulanceLaterResponce> response) {

                if(response!=null) {
                    BnBUserUtility.dismissProgressDialog();
                    if (response.isSuccessful()) {

                        bookAmbulanceLaterResponce_v=response.body();

                        if(bookAmbulanceLaterResponce_v.getSuccess().equals(true))
                        {
                            Log.d(TAG, " success  true : ");
                            SharedPreferenceBnBUtility.setNotificationTripID(BookAmbulanceLaterActivity.this,bookAmbulanceLaterResponce_v.getData().getId().toString());
                            BnBUserUtility.toastMessage_success("Request has been sent successfully to hospital.", getApplicationContext());
                            Intent intent=new Intent(BookAmbulanceLaterActivity.this,RequestConfirmationActivity.class);
                            intent.putExtra("Latitude", String.valueOf(Search_Latitude));
                            intent.putExtra("Longitude", String.valueOf(Search_Longitude));
                            startActivity(intent);


                        }else if(bookAmbulanceLaterResponce_v.getSuccess().equals(false)){
                            Log.d(TAG, " success  false : ");
                            BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                    " reach our service at the moment.Please try again.", getApplicationContext());
                        }


                    } else {
                        Log.d(TAG, " not success : ");
                        BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                " reach our service at the moment.Please try again.", getApplicationContext());
                    }
                }else{
                    Log.d(TAG, " responce null : ");
                    BnBUserUtility.dismissProgressDialog();
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                            " reach our service at the moment.Please try again.",getApplicationContext());
                }
            }

            @Override
            public void onFailure(Call<BookAmbulanceLaterResponce> call, Throwable t) {
                BnBUserUtility.dismissProgressDialog();
                Log.d(TAG," failure  : "+t);
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                        " reach our service at the moment.Please try again.",getApplicationContext());
            }
        });


    }

    private int date_comparison(String dateofevent)
    {
        // TODO Auto-generated method stub
        int difference_hr = 0;
        String toyBornTime = dateofevent;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");


        Date oldDate = null;
        try {
            oldDate = dateFormat.parse(toyBornTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Date currentDate = new Date();
        if(oldDate!=null && currentDate!=null){
        difference_inseconds = oldDate.getTime() - currentDate.getTime();

        difference_inseconds = (difference_inseconds / 1000);//--getting total difference in seconds

        //--splitting inti sec, min, hour,and days----------
        /*
        long diffSeconds = difference_inseconds / 1000 % 60;
        long diffMinutes = difference_inseconds / (60 * 1000) % 60;
        long diffHours = difference_inseconds / (60 * 60 * 1000) % 24;
        long diffDays = difference_inseconds / (24 * 60 * 60 *  1000);
        */

        long diffHours = difference_inseconds / (60 * 60);

         difference_hr=Integer.parseInt(String.valueOf(diffHours));
        }
        return  difference_hr;
    }

}

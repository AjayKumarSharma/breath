package com.breathandbeats.android.ui.activities.SaveaLife;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.RV.SaveALifeAdapter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.RV.SaveALifeItem;
import com.breathandbeats.android.pojos.api.SaveALifeCategoriesResponse;
import com.breathandbeats.android.pojos.api.SaveALifeCategoryData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaveALifeActivity extends AppCompatActivity {

    @BindView(R.id.activity_save_a_life_RV)
    RecyclerView activitySaveALifeRV;

    ArrayList<SaveALifeItem> saveALifeItems = new ArrayList<>();
    BnBApp app;
    SharedPrefHelper sharedPrefHelper;
    BnBService bnBService;
    String authToken;
    SaveALifeAdapter saveALifeAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_a_life);
        ButterKnife.bind(this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_save_a_life_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Save A life");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);
        app = BnBApp.getInstance();
        sharedPrefHelper = app.getSharedPrefHelper();
        bnBService = app.getRestClient().getApiSerivice();
        authToken = "Bearer " + sharedPrefHelper.getAuthToken();
        saveALifeAdapter = new SaveALifeAdapter(saveALifeItems, this);
        activitySaveALifeRV.setAdapter(saveALifeAdapter);
        activitySaveALifeRV.setLayoutManager(new LinearLayoutManager(this));
        bnBService.getSaveALifeCategories(authToken).enqueue(new Callback<SaveALifeCategoriesResponse>() {
            @Override
            public void onResponse(Call<SaveALifeCategoriesResponse> call, Response<SaveALifeCategoriesResponse> response) {
                if (response.body().getSuccess()) {
                    SaveALifeCategoryData saveALifeCategoryData = response.body().getSaveALifeCategoryData();
                    List<String> categories = saveALifeCategoryData.getCategories();
                    saveALifeItems.clear();
                    for (int i = 0; i < categories.size(); i++) {
                        String categoryName = categories.get(i);
                        SaveALifeItem saveALifeItem = new SaveALifeItem(categoryName);
                        saveALifeItems.add(saveALifeItem);
                    }
                    saveALifeAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<SaveALifeCategoriesResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}



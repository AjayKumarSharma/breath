package com.breathandbeats.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import com.breathandbeats.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by lavanya on 05-11-2017.
 */

public class NotificationActivity extends AppCompatActivity {

    String TAG="Notification_file";

    @BindView(R.id.rlCloseSrc)
    RelativeLayout rlCloseSrc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);


        ButterKnife.bind(this);


    }

    @OnClick(R.id.rlCloseSrc)
    public  void onrlCloseSrcClicked(){

        Intent intent=new Intent(this,MainActivity.class);
        //Clear all activities and start new task
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}

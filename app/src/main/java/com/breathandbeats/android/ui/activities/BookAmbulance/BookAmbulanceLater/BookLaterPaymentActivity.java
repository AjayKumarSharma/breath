package com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceLater;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.breathandbeats.android.BookingRemainder.MyReceiver;
import com.breathandbeats.android.DBHelper.TripCollection;
import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.CancelTheTripRequested;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Canceltrip;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.NotificationBasePojo;
import com.breathandbeats.android.pojos.TripPaymentpojo.TripPaymentInput;
import com.breathandbeats.android.pojos.TripPaymentpojo.TripPaymentResponce;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.MainActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lavanya on 13-11-2017.
 */
public class BookLaterPaymentActivity extends AppCompatActivity implements PaymentResultListener {

    TextView tvTimer;
    Dialog dialogTimerAlert;
    SharedPrefHelper sharedPrefHelper;
    BnBApp application;
    private  String TAG="payment_activity";

    @BindView(R.id.tvTimertext)
    TextView tvTimertext;

    @BindView(R.id.btnContinuePayment_later)
    Button btnContinuePayment_later;

    @BindView(R.id.rlBackarrow)
    RelativeLayout rlBackarrow;

    @BindView(R.id.tvPaymnetMode)
    TextView tvPaymnetMode;
    @BindView(R.id.tvTotalCost)
    TextView tvTotalCost;
    @BindView(R.id.tvgstpercentage)
    TextView tvgstpercentage;
    @BindView(R.id.tvCgst)
    TextView Cgst;
    @BindView(R.id.tvOverAllCost)
    TextView tvOverallTotal;
    @BindView(R.id.tvminimumFareCost)
    TextView tvminimumFareCost;

    @BindView(R.id.btnCancelReq)
    Button btnCancelReq;

   static double TotalPaymentAmount=100*1;
    private  String PaymentType;
    long difference_inseconds;
    Gson gson;
    BnBService bnBService;
    CountDownTimer CountDownTimer;
    NotificationBasePojo NotificationBasePojo_V=new NotificationBasePojo();
    CancelTheTripRequested cancelTheTripRequested_V=new CancelTheTripRequested();
    private Realm myRealm;
    private static int id = 1;
    private static ArrayList<TripCollection> personDetailsModelArrayList = new ArrayList<>();
    TripPaymentResponce tripPaymentResponce_v=new TripPaymentResponce();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paymentlater);

        application = (BnBApp) getApplication();
        sharedPrefHelper = application.getSharedPrefHelper();
        tvTimer=(TextView)findViewById(R.id.tvTimer);

        bnBService = application.getRestClient().getApiSerivice();// for API Call
        ButterKnife.bind(this);
        getBillingData();
        getTimerFunction();

        Checkout.preload(getApplicationContext());
        myRealm = Realm.getInstance(BookLaterPaymentActivity.this);

    }

    private void getBillingData() {

        gson = new Gson();
        String notifData = SharedPreferenceBnBUtility.getAdvancePaymentInfo(BookLaterPaymentActivity.this);
        Log.d(TAG," notifi: "+notifData);
        NotificationBasePojo_V= gson.fromJson(notifData, NotificationBasePojo.class);
        if(NotificationBasePojo_V.getTrip().getTotalFare()!=null) {
            tvTimertext.setText("Make a a payment of Rs " + NotificationBasePojo_V.getTrip().getTotalFare().toString() + "  with in 15 minute.");
        }
        if(NotificationBasePojo_V.getTrip().getPaymentMode()!=null) {
            tvPaymnetMode.setText(NotificationBasePojo_V.getTrip().getPaymentMode().toString());
        }
        if(NotificationBasePojo_V.getTrip().getRange()!=null) {
            tvminimumFareCost.setText("( " + NotificationBasePojo_V.getTrip().getRange().toString() + " ) km");
        }
        if(NotificationBasePojo_V.getTrip().getEstimatedFare()!=null) {
            tvTotalCost.setText(NotificationBasePojo_V.getTrip().getEstimatedFare().toString());
        }
        if(NotificationBasePojo_V.getTrip().getGstPercentage()!=null) {
            tvgstpercentage.setText(String.valueOf(NotificationBasePojo_V.getTrip().getGstPercentage().toString()) + "%");

        }
        if(NotificationBasePojo_V.getTrip().getGstCost()!=null){
            Cgst.setText("Rs "+String.valueOf(NotificationBasePojo_V.getTrip().getGstCost().toString()));
        }
         if(NotificationBasePojo_V.getTrip().getTotalFare()!=null) {
               tvOverallTotal.setText(NotificationBasePojo_V.getTrip().getTotalFare().toString());
         }
        if(NotificationBasePojo_V.getTrip().getTotalFare()!=null) {
            float totalamt=(NotificationBasePojo_V.getTrip().getTotalFare());
            int Total_amt = (int)Math.round(totalamt);
            TotalPaymentAmount=100*Total_amt;
        }
    }


    @Override
    public void onBackPressed() {
//       super.onBackPressed();
    }

    @OnClick(R.id.rlBackarrow)
    public void onrlBackarrowClicked(){

        onBackPressed();
    }
    @OnClick(R.id.btnCancelReq)
    public  void  onbtnCancelReqClicked(){

        getTheCancelRequestFunction();

    }
    private void getTimerFunction() {


         CountDownTimer = new CountDownTimer(900000, 1000) { // adjust the milli seconds here ,15min time set

            public void onTick(long millisUntilFinished) {
                tvTimer.setText("Time Remaining : "+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {

                tvTimer.setText("Time expired!");
                getAlertBox("time");
            }
        }.start();
    }

    private void getAlertBox(String condition) {

        dialogTimerAlert = new Dialog(this);
        dialogTimerAlert.setContentView(R.layout.dialog_timeralert);
        dialogTimerAlert.show();
        dialogTimerAlert.setCancelable(false);

        Button Done_Btn = (Button) dialogTimerAlert.findViewById(R.id.Done_Btn);

        TextView tvpayment=(TextView)dialogTimerAlert.findViewById(R.id.tvpayment);

        if(condition.equals("payment")){
            tvpayment.setText("Trip payment completed successfully.You can get details of booking information in the trip information page. Thank you for choosing the Breath and Beats.");
        }else if(condition.equals("time")){
            tvpayment.setText("Make Payment time has been expired, Please make a new request.");
        }
        SharedPreferenceBnBUtility.setAdvacneBookedReq(BookLaterPaymentActivity.this,"false");

        Done_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTimerAlert.dismiss();
                /**/

                if(condition.equals("payment")){
                    BnBUserUtility.toastMessage_success("Please wait...",application);
                    BnBUserUtility.Clear_notification(BookLaterPaymentActivity.this);
                    SharedPreferenceBnBUtility.setAdvacneBookedReq(BookLaterPaymentActivity.this,"false");
                    SharedPreferenceBnBUtility.setAdvanceTrip(BookLaterPaymentActivity .this,"false");
                    BnBUserUtility.Clear_SharedPreferenceData(BookLaterPaymentActivity.this);
                    Intent intent=new Intent(BookLaterPaymentActivity.this,MainActivity.class);
                    //Clear all activities and start new task
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);

                }else if(condition.equals("time")){

                    getTheCancelRequestFunction();
                }


            }
        });

    }
    @OnClick(R.id.btnContinuePayment_later)
    public  void  onbtnContinuePayment_laterClikced(){

       startPayment();
    }
//--------------------------------------

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;
        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Breath&Beats");
            options.put("description", "Ambulance Trip Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://rzp-mobile.s3.amazonaws.com/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", String.valueOf(TotalPaymentAmount));

            JSONObject preFill = new JSONObject();
            preFill.put("email", sharedPrefHelper.getUserData().getEmail());
            preFill.put("contact",sharedPrefHelper.getUserData().getPhoneNumber());

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {

            BnBUserUtility.toastMessage_error("Error in payment: " + e.getMessage(),application);
            e.printStackTrace();
        }
    }
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {

        try {
            BnBUserUtility.toastMessage_success("Payment Successful: " + razorpayPaymentID,application);
            Log.d(TAG,"razorpayPaymentID: "+razorpayPaymentID);
            CountDownTimer.cancel();
            UpdatePaymnetStatusToHospital(razorpayPaymentID);


        } catch (Exception e) {
            Log.d(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int code, String response) {

        try {
            BnBUserUtility.toastMessage_error("Payment failed, Please make a payment again.",this);

        } catch (Exception e) {
            Log.d(TAG, "Exception in onPaymentError", e);
        }
    }
//-----------------------------
private void UpdatePaymnetStatusToHospital(String razorpayPaymentID) {


    String token="Bearer "+sharedPrefHelper.getAuthToken();

    Log.d(TAG," trip id : "+SharedPreferenceBnBUtility.getNotificationTripID(this));
    Log.d(TAG," time : "+BnBUserUtility.getCurrenttime());
    String Payment_time=BnBUserUtility.getCurrenttime();
    TripPaymentInput tripPaymentInput=new TripPaymentInput();
    tripPaymentInput.setTripId(SharedPreferenceBnBUtility.getNotificationTripID(this));
    tripPaymentInput.setPaymentStatus("success");
    tripPaymentInput.setPaid(true);
    tripPaymentInput.setCapturedOnApp(Payment_time);
    tripPaymentInput.setCaptured(true);
    tripPaymentInput.setRazorpayPaymentId(razorpayPaymentID);

    Call<TripPaymentResponce> tripPaymentResponceCall=bnBService.Trippayment(token,tripPaymentInput);

    tripPaymentResponceCall.enqueue(new Callback<TripPaymentResponce>() {
        @Override
        public void onResponse(Call<TripPaymentResponce> call, Response<TripPaymentResponce> response) {

            if(response!=null){

                if(response.isSuccessful()){

                    tripPaymentResponce_v=response.body();


                    if(tripPaymentResponce_v!=null){

                        if(tripPaymentResponce_v.getSuccess().equals(true)){

                            if(tripPaymentResponce_v.getData()!=null){

                                Log.d(TAG," id : "+tripPaymentResponce_v.getData().getId());
                                Log.d(TAG," name  : "+tripPaymentResponce_v.getData().getHospital().getName());
                                addDataToRealm(tripPaymentResponce_v.getData().getId(),"pending");
                                getAlramTimerFunction(tripPaymentResponce_v);
                                BnBUserUtility.Clear_notification(BookLaterPaymentActivity.this);


                            }
                        }else{
                            if(tripPaymentResponce_v.getMessage()!=null) {
                                BnBUserUtility.toastMessage_info(tripPaymentResponce_v.getMessage().toString(),BookLaterPaymentActivity.this);
                            }
                        }
                    }
                }else{
                    Log.d(TAG," responce not  success  : ");
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                            " reach our service at the moment.Please try again.",getApplicationContext());
                }

            }else{

                Log.d(TAG," responce null  : ");
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                        " reach our service at the moment.Please try again.",getApplicationContext());
            }
        }

        @Override
        public void onFailure(Call<TripPaymentResponce> call, Throwable t) {
            Log.d(TAG," failure : "+t);
            BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                    " reach our service at the moment.Please try again.",getApplicationContext());
        }
    });


}
//-------------------------------------------------

   private void getAlramTimerFunction(TripPaymentResponce notificationBasePojo_V) {

        date_comparison(notificationBasePojo_V.getData().getScheduledOnAppTime().toString());
        long time_in_sec = (difference_inseconds-3600);
       Log.d(TAG,"time : "+time_in_sec);
        int totaltime = (int) time_in_sec;
        alarmmanager(totaltime,notificationBasePojo_V);
    }


    private void alarmmanager(int time_in_sec, TripPaymentResponce notificationBasePojo_V)
    {
        // TODO Auto-generated method stub
        if (time_in_sec > 0)
        {
            AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
            Intent notificationIntent = new Intent(BookLaterPaymentActivity.this, MyReceiver.class);
            notificationIntent.addCategory("android.intent.category.DEFAULT");
            //---------------------------------------------------

            Log.d("my_receiver"," trip id @bookalter : "+notificationBasePojo_V.getData().getId());
            Log.d("my_receiver"," driver_name @bookalter : "+notificationBasePojo_V.getData().getDriver().getUsername());
            Log.d("my_receiver"," driver_number @bookalter : "+notificationBasePojo_V.getData().getDriver().getPhoneNumber());

             notificationIntent.putExtra("trip_id", notificationBasePojo_V.getData().getId());

            notificationIntent.putExtra("driver_name", notificationBasePojo_V.getData().getDriver().getUsername());
            notificationIntent.putExtra("driver_number", notificationBasePojo_V.getData().getDriver().getPhoneNumber());

            notificationIntent.putExtra("hospital_name", notificationBasePojo_V.getData().getHospital().getName());
            notificationIntent.putExtra("hospital_number", notificationBasePojo_V.getData().getHospital().getPhoneNumber());
            notificationIntent.putExtra("hospital_address", notificationBasePojo_V.getData().getHospital().getAddress());
            notificationIntent.putExtra("hospital_ID",notificationBasePojo_V.getData().getHospital().getId());

            notificationIntent.putExtra("patient_lat", notificationBasePojo_V.getData().getLocation().getCoordinates().get(1).toString());
            notificationIntent.putExtra("patient_long",notificationBasePojo_V.getData().getLocation().getCoordinates().get(0).toString());

            notificationIntent.putExtra("driver_lat", notificationBasePojo_V.getData().getDriver().getDriverCurrentCoordinates().get(1).toString());
            notificationIntent.putExtra("driver_long", notificationBasePojo_V.getData().getDriver().getDriverCurrentCoordinates().get(0).toString());

            notificationIntent.putExtra("notification_received","true");
            //-------------------------------------------------------------------
            PendingIntent broadcast = PendingIntent.getBroadcast(getApplicationContext(), time_in_sec, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            java.util.Calendar cal = java.util.Calendar.getInstance();
            cal.add(java.util.Calendar.SECOND, time_in_sec);//setting alarm to particular time.
            //	alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT)
            {
                // only for kitkat and newer versions
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);
                Log.d(TAG,"KITKAT ");
            }
            else
            {
                Log.d(TAG,"NOT KITKAT ");
                alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), broadcast);

            }

        }
        getAlertBox("payment");
    }



    private long date_comparison(String dateofevent)
    {
        // TODO Auto-generated method stub

        String toyBornTime = dateofevent;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");


            Date oldDate = null;
            try {
                oldDate = dateFormat.parse(toyBornTime);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Date currentDate = new Date();
            difference_inseconds = oldDate.getTime() - currentDate.getTime();

            difference_inseconds = (difference_inseconds / 1000);//--getting total difference in seconds

            //--splitting inti sec, min, hour,and days----------
            long diffSeconds = difference_inseconds / 1000 % 60;
            long diffMinutes = difference_inseconds / (60 * 1000) % 60;
            long diffHours = difference_inseconds / (60 * 60 * 1000) % 24;
            long diffDays = difference_inseconds / (24 * 60 * 60 *  1000);


        return  difference_inseconds;
    }



    //--------------------------------------------------

    private void getTheCancelRequestFunction() {

        if(NetworkUtil.getConnectivityStatusBoolen(this)) {

            updateTheRequestToHospital();
        }else{
            BnBUserUtility.toastMessage_default("Please note that you are not connected to ineternet.",application);
        }
    }
    private void updateTheRequestToHospital() {
        BnBUserUtility.showProgress(this,"Requesting, Please wait...");

        String token="Bearer "+sharedPrefHelper.getAuthToken();

        Canceltrip canceltrip=new Canceltrip();

        canceltrip.setTripId(SharedPreferenceBnBUtility.getNotificationTripID(this));
        Call<CancelTheTripRequested> cancelTheTripRequestedCall=bnBService.CancelTripRequested(token,canceltrip);

        cancelTheTripRequestedCall.enqueue(new Callback<CancelTheTripRequested>() {
            @Override
            public void onResponse(Call<CancelTheTripRequested> call, Response<CancelTheTripRequested> response) {

                if(response!=null) {
                    BnBUserUtility.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.d(TAG, "Responce: success " + response.body());

                        cancelTheTripRequested_V=response.body();

                        if(cancelTheTripRequested_V.getSuccess().equals(true)){

//                            BnBUserUtility.toastMessage_success("Trip request cancelled successfully.",BookLaterPaymentActivity.this);
                            CountDownTimer.cancel();
                            SharedPreferenceBnBUtility.setAdvacneBookedReq(BookLaterPaymentActivity.this,"false");
                            Intent CallAmbulanceIntent=new Intent(BookLaterPaymentActivity.this,MainActivity.class);
                            //Clear all activities and start new task
                            CallAmbulanceIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(CallAmbulanceIntent);

                        } else if (cancelTheTripRequested_V.getSuccess().equals(false)) {

//                            BnBUserUtility.toastMessage_info("Unable to cancel Trip request ,Please try again.",BookLaterPaymentActivity.this);
                        }

                    } else {
                        Log.d(TAG, "responce not successfull : ");
                        BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                " reach our service at the moment.Please try again.", getApplicationContext());
                    }
                }else{
                    BnBUserUtility.dismissProgressDialog();
                    Log.d(TAG, "Responce: null" );
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                            " reach our service at the moment.Please try again.", getApplicationContext());
                }
            }

            @Override
            public void onFailure(Call<CancelTheTripRequested> call, Throwable t) {
                Log.d(TAG,"FAILURE : "+ t);
                BnBUserUtility.dismissProgressDialog();
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                        " reach our service at the moment.Please try again.",getApplicationContext());
            }
        });
    }


    private void addDataToRealm(String TripID,String status) {
        myRealm.beginTransaction();
        RealmResults<TripCollection> results = myRealm.where(TripCollection.class).findAll();

        if(results.size()>0) {
            id = myRealm.where(TripCollection.class).max("id").intValue() + 1;
        }
        TripCollection tripDetailsModel = myRealm.createObject(TripCollection.class);
        tripDetailsModel.setId(id);
        tripDetailsModel.setTrip_id(TripID);
        tripDetailsModel.setStatus(status);
        personDetailsModelArrayList.add(tripDetailsModel);
        myRealm.commitTransaction();
        id++;
    }

}

package com.breathandbeats.android.ui.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.breathandbeats.android.BuildConfig;
import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.viewpager.MainAdapter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.infrastructure.Constants;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.api.PulseRequest;
import com.breathandbeats.android.pojos.api.SignupResponse;
import com.breathandbeats.android.ui.fragments.DoctorConsultionFragment;
import com.breathandbeats.android.ui.fragments.FragmentSearchDoctor;
import com.breathandbeats.android.ui.fragments.FragmentSpeciality;
import com.breathandbeats.android.ui.fragments.HomeFragment;
import com.breathandbeats.android.ui.fragments.RequestFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

@RuntimePermissions
public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "Home_Activity";

    @BindView(R.id.activity_main_viewPager)
    ViewPager viewPager;
    /*@BindView(R.id.activity_main_sliding_tabs)
    TabLayout tabLayout;*/
    MainAdapter mainAdapter;

    @BindView(R.id.layone)
    LinearLayout layone;
    @BindView(R.id.laytwo)
    LinearLayout laytwo;
    @BindView(R.id.laythree)
    LinearLayout laythree;
    @BindView(R.id.tvprofile)
    TextView tvprofile;
    @BindView(R.id.tvhome)
    TextView tvhome;
    @BindView(R.id.tvdoctconstult)
    TextView tvdoctconstult;

    @BindView(R.id.ivprofileicon)
    ImageView ivprofileicon;
    @BindView(R.id.ivhomeicon)
    ImageView ivhomeicon;
    @BindView(R.id.ivdoctconstult)
    ImageView ivdoctconstult;

    GoogleApiClient googleApiClient;
    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    private ArrayList<Double> location = new ArrayList<>();
    String authToken;
    static  int CALL=100;
    static  int LOCATION=102;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        createViewPager(viewPager);

        onlayoneClicked();

        Intent position_intent=getIntent();
        if(position_intent.hasExtra("position")){

            if(position_intent.getStringExtra("position").equals("1")){
//                viewPager.setCurrentItem(1);
                onlaytwoClicked();
            }else if(position_intent.getStringExtra("position").equals("0")){
//                viewPager.setCurrentItem(0);
                onlayoneClicked();
            }else if(position_intent.getStringExtra("position").equals("2")){
//                viewPager.setCurrentItem(0);
                onlaylaythreeClicked();
            }
            else{
//                viewPager.setCurrentItem(0);
                onlayoneClicked();
            }

        }else{
//            viewPager.setCurrentItem(0);
            onlayoneClicked();
        }
//        viewPager.setCurrentItem(1);
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        application = (BnBApp) getApplication();
        bnBService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();
        authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                // Check if this is the page you want.
                Log.d(TAG," position: "+position);
                if(position==0){
                    onlayoneClicked();
                }else if(position==1){
                    onlaytwoClicked();
                }else if(position==2){
                    onlaylaythreeClicked();
                }

            }
        });


        askForPermission(Manifest.permission.CALL_PHONE,CALL);
        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,LOCATION);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        MainActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        DispalyAlerForExit("Are you sure, you want to exit the application ?",false);
    }


    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void getLocation() {
        Log.i(TAG, "getLocation: permitted");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationManager locateManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean locationEnabled = isLocationEnabled(locateManager);
        if (locationEnabled) {
            //you will be here when the you get the permission and the location is enable, so get the location of the device and send the pulse
            Log.i(TAG, "sendPulse: location is enabled");
            Log.i(TAG, "getLocation: isconnected" + googleApiClient.isConnected());
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            ArrayList<Double> location = new ArrayList<>();
            location.clear();
            if (lastLocation != null) {
                location.add(0, lastLocation.getLongitude());
                location.add(1, lastLocation.getLatitude());
                updateLocationInSP(location);
                Log.i(TAG, "sendPulse: location is " + location);
                sendPulse(authToken, location);
            }
        } else {
            //you will be here when the you get the permission and the location is disable, but you
            // cant ask user to turn on the location (this is pulse) so get the location from SP
            //if the location is null (not updated even once, may be first time) then send default
            // location to the server in the pulse
            Log.i(TAG, "getLocation: locaiton not enabled");
            ArrayList<Double> location = sharedPrefHelper.getLastLocation();
            if (location == null) {
                location = getDefaultLocation();
            }
            Log.i(TAG, "sendPulse: location is " + location);
            sendPulse(authToken, location);
        }

    }

    public void sendPulse(String authToken, ArrayList<Double> location) {
        bnBService.pulse(authToken, getPulseRequest(location)).enqueue(new Callback<SignupResponse>() {
            @Override
            public void onResponse(Call<SignupResponse> call, Response<SignupResponse> response) {
                if(response.body()!=null) {
                    if (response.body().getSuccess() != null) {
                        if (response.body().getSuccess()) {
                            sharedPrefHelper.saveUserData(response.body().getData());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<SignupResponse> call, Throwable t) {
                Log.i(TAG, "onFailure: ");
            }
        });


    }

    private void updateLocationInSP(ArrayList<Double> location) {
        sharedPrefHelper.setLastLocation(location);
    }

    private boolean isLocationEnabled(LocationManager locateManager) {
        boolean locationEnabled = locateManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return locationEnabled;
    }

    private PulseRequest getPulseRequest(ArrayList<Double> lastLocation) {
        if (lastLocation.isEmpty()) {
            lastLocation.clear();
            lastLocation.add(0, 0.0);
            lastLocation.add(1, 0.0);

        }
        PulseRequest pulseRequest = new PulseRequest();
        pulseRequest.setRegistrationId(sharedPrefHelper.getFCMToken());
        pulseRequest.setPlatform(Constants.PLATFORM);
        pulseRequest.setAppVersion(String.valueOf(BuildConfig.VERSION_CODE));
        pulseRequest.setLastLocation(lastLocation);
        return pulseRequest;
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForGetLocation(PermissionRequest request) {
        Log.i(TAG, "showRationaleForGetLocation: ");
        new AlertDialog.Builder(this)
                .setMessage("App needs location to get you updated about the requests")
                .setPositiveButton("Allow", (dialog, button) -> request.proceed())
                .setNegativeButton("Deny", (dialog, button) -> request.cancel())
                .setCancelable(false)
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForGetLocation() {
        Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "showDeniedForgetLocation: permission denied");
        ArrayList<Double> location = sharedPrefHelper.getLastLocation();
        if (location == null) {
            location = getDefaultLocation();
        }
        Log.i(TAG, "sendPulse: location is " + location);
        sendPulse(authToken, location);

    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    void showNeverAskForGetLocation() {
        Toast.makeText(this, "You can give location permission from the settings", Toast.LENGTH_SHORT).show();
        ArrayList<Double> location = sharedPrefHelper.getLastLocation();
        if (location == null) {
            location = getDefaultLocation();
        }
        Log.i(TAG, "sendPulse: location is " + location);
        sendPulse(authToken, location);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "onConnected: about to ask permissoin");
        MainActivityPermissionsDispatcher.getLocationWithCheck(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed: ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart: about to connect");
        googleApiClient.connect();

    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();

    }

    private ArrayList<Double> getDefaultLocation() {
        ArrayList<Double> location = new ArrayList<>();
        location.add(0, 0.0);
        location.add(1, 0.0);
        return location;
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


    //-----------------------------------

    private void createViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new HomeFragment(), "HOME");
        adapter.addFrag(new FragmentSearchDoctor(), "DOCTORCONSULT");
        adapter.addFrag(new RequestFragment(), "PROFILE");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void DispalyAlerForExit(String message,final boolean condition)//dialog box----
    {
        new android.app.AlertDialog.Builder(this).setMessage(message).setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {

               if(condition==false){  // closing the application

                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
               }
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {

            }
        }).show();
    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(MainActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(MainActivity.this, new String[]{permission}, requestCode);
            }
        } else {
//            Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }
    @OnClick(R.id.layone)
    public  void onlayoneClicked(){
        viewPager.setCurrentItem(0);
        ivhomeicon.setImageDrawable(getResources().getDrawable(R.mipmap.home_iconred));
        ivprofileicon.setImageDrawable(getResources().getDrawable(R.mipmap.usericon_grey));
        tvhome.setTextColor(getResources().getColor(R.color.selectdtab));
        tvprofile.setTextColor(getResources().getColor(R.color.unselecttab));
        layone.setBackground(getResources().getDrawable(R.drawable.tab_background_selected));
        laytwo.setBackground(getResources().getDrawable(R.drawable.tab_background_unselected));
        laythree.setBackground(getResources().getDrawable(R.drawable.tab_background_unselected));
        tvdoctconstult.setTextColor(getResources().getColor(R.color.unselecttab));
        ivdoctconstult.setImageDrawable(getResources().getDrawable(R.mipmap.usericon_grey));
    }
    @OnClick(R.id.laythree)
    public  void onlaytwoClicked(){
        viewPager.setCurrentItem(1);
        ivhomeicon.setImageDrawable(getResources().getDrawable(R.mipmap.home_iconred));
        ivdoctconstult.setImageDrawable(getResources().getDrawable(R.mipmap.user_iconred));
        tvdoctconstult.setTextColor(getResources().getColor(R.color.selectdtab));
        tvprofile.setTextColor(getResources().getColor(R.color.unselecttab));
        tvhome.setTextColor(getResources().getColor(R.color.unselecttab));
        layone.setBackground(getResources().getDrawable(R.drawable.tab_background_unselected));
        laytwo.setBackground(getResources().getDrawable(R.drawable.tab_background_unselected));
        laythree.setBackground(getResources().getDrawable(R.drawable.tab_background_selected));
        ivprofileicon.setImageDrawable(getResources().getDrawable(R.mipmap.usericon_grey));
        ivhomeicon.setImageDrawable(getResources().getDrawable(R.mipmap.hone_icongrey));

    }

    @OnClick(R.id.laytwo)
    public  void onlaylaythreeClicked(){
        viewPager.setCurrentItem(2);
        ivhomeicon.setImageDrawable(getResources().getDrawable(R.mipmap.hone_icongrey));
        ivprofileicon.setImageDrawable(getResources().getDrawable(R.mipmap.user_iconred));
        tvhome.setTextColor(getResources().getColor(R.color.unselecttab));
        tvprofile.setTextColor(getResources().getColor(R.color.selectdtab));
        laytwo.setBackground(getResources().getDrawable(R.drawable.tab_background_selected));
        layone.setBackground(getResources().getDrawable(R.drawable.tab_background_unselected));
        laythree.setBackground(getResources().getDrawable(R.drawable.tab_background_unselected));
        tvdoctconstult.setTextColor(getResources().getColor(R.color.unselecttab));
        ivdoctconstult.setImageDrawable(getResources().getDrawable(R.mipmap.usericon_grey));
    }
}

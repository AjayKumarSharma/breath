package com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.RelativeLayout;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.HistoryTripListPojo.CompletedTripListData;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.fragments.TripFragment;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Rakesh on 31-10-2017.
 */

public class TripHistoryAcitivity extends AppCompatActivity{


    String TAG="Notification_file";
    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    String authToken;
    CompletedTripListData completedTripListData_v=new CompletedTripListData();
    Gson gson;
    @BindView(R.id.rlCloseSrc)
    RelativeLayout rlCloseSrc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_triphistory);

        ButterKnife.bind(this);

        application = (BnBApp)getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Cal

        if(NetworkUtil.getConnectivityStatusBoolen(TripHistoryAcitivity.this)){

            getthefunction();

        }else{
            BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",application);
        }
    }


    private void getthefunction() {

        TripFragment fragment= new TripFragment();

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, fragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @OnClick(R.id.rlCloseSrc)
    public  void onrlCloseSrcClicked(){

        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

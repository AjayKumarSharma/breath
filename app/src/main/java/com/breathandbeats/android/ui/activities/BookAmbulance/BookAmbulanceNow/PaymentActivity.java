package com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow;

import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.NotificationBasePojo;
import com.breathandbeats.android.pojos.TripPaymentpojo.TripPaymentInput;
import com.breathandbeats.android.pojos.TripPaymentpojo.TripPaymentResponce;
import com.breathandbeats.android.services.BnBFirebaseMessagingService;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.MainActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.google.gson.Gson;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rakesh on 26-10-2017.
 */

public class PaymentActivity extends AppCompatActivity implements PaymentResultListener {


    TextView tvTimer;
    Dialog dialogTimerAlert;
    SharedPrefHelper sharedPrefHelper;

    private  String TAG="payment_activity";

    @BindView(R.id.btnContinuePayment_now)
    Button btnContinuePayment_now;

    @BindView(R.id.tvPaymnetMode)
     TextView tvPaymnetMode;

    @BindView(R.id.tvTotalCost)
     TextView tvTotalCost;

    @BindView(R.id.tvgstpercentage)
    TextView tvgstpercentage;

    @BindView(R.id.tvCgst)
    TextView Cgst;

    @BindView(R.id.tvOverAllCost)
    TextView tvOverallTotal;

    @BindView(R.id.btncardcollection)
     Button btncardcollection;

    BnBApp application;
    BnBService bnBService;

    @BindView(R.id.tvminimumFareCost)
    TextView tvminimumFareCost;

    static double TotalPaymentAmount;
    private  String PaymentType;
    private  static   float totalamt=1;
    int Total_amt=1;
    Gson gson;
    NotificationBasePojo NotificationBasePojo_V=new NotificationBasePojo();
    TripPaymentResponce tripPaymentResponce_v=new TripPaymentResponce();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        application = (BnBApp) getApplication();
        sharedPrefHelper = application.getSharedPrefHelper();
        tvTimer=(TextView)findViewById(R.id.tvTimer);

        ButterKnife.bind(this);
        bnBService = application.getRestClient().getApiSerivice();// for API Call
        Checkout.preload(getApplicationContext());

        getBillingData();

       getverificationFunction();

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastReceiver, new IntentFilter(BnBFirebaseMessagingService.BROADCAST_ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        registerReceiver(broadcastReceiver, new IntentFilter(BnBFirebaseMessagingService.BROADCAST_ACTION));
        unregisterReceiver(broadcastReceiver);
    }

    private void getverificationFunction() {

        String paymnetmode="online";

        if(paymnetmode.equals("cash")){

            btncardcollection.setVisibility(View.VISIBLE);
            btnContinuePayment_now.setVisibility(View.GONE);

        }else if(paymnetmode.equals("online")){

            btncardcollection.setVisibility(View.GONE);
            btnContinuePayment_now.setVisibility(View.VISIBLE);

        }
    }

    private void getBillingData() {

        gson = new Gson();
        String notifData = SharedPreferenceBnBUtility.getPaymentInfo(PaymentActivity.this);
        Log.d(TAG, " notifi: " + notifData);
        NotificationBasePojo_V = gson.fromJson(notifData, NotificationBasePojo.class);

        Log.d(TAG, " mode : " + NotificationBasePojo_V.getTrip().getPaymentMode());
        Log.d(TAG, " gate way : " + NotificationBasePojo_V.getTrip().getPaymentGateway());

        if (NotificationBasePojo_V.getTrip().getPaymentMode() != null) {
            tvPaymnetMode.setText(NotificationBasePojo_V.getTrip().getPaymentMode().toString());
        }
        if (NotificationBasePojo_V.getTrip().getRange() != null) {
            tvminimumFareCost.setText("( " + NotificationBasePojo_V.getTrip().getRange().toString() + " ) km");
        }
        if (NotificationBasePojo_V.getTrip().getEstimatedFare() != null) {
            tvTotalCost.setText(NotificationBasePojo_V.getTrip().getEstimatedFare().toString());
        }
        if (NotificationBasePojo_V.getTrip().getGstPercentage() != null){
            tvgstpercentage.setText(String.valueOf(NotificationBasePojo_V.getTrip().getGstPercentage().toString()) + "%");
        }
        if(NotificationBasePojo_V.getTrip().getGstCost()!=null) {
            Cgst.setText("Rs " + String.valueOf(NotificationBasePojo_V.getTrip().getGstCost().toString()));
        }
        if(NotificationBasePojo_V.getTrip().getTotalFare()!=null) {
            tvOverallTotal.setText(NotificationBasePojo_V.getTrip().getTotalFare().toString());
            totalamt=(NotificationBasePojo_V.getTrip().getTotalFare());
        }


     Total_amt = (int)Math.round(totalamt);
        TotalPaymentAmount=100*Total_amt;
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }


    @OnClick(R.id.btnContinuePayment_now)
    public  void  onbtnContinuePayment_nowClikced(){

        if(NetworkUtil.getConnectivityStatusBoolen(this)) {

            startPayment();

        }else{
            BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",application);
        }

    }
    private void getAlertBox(String condition) {

        dialogTimerAlert = new Dialog(this);
        dialogTimerAlert.setContentView(R.layout.dialog_timeralert);
        dialogTimerAlert.show();
        dialogTimerAlert.setCancelable(false);

        Button Done_Btn = (Button) dialogTimerAlert.findViewById(R.id.Done_Btn);

        TextView tvpayment=(TextView)dialogTimerAlert.findViewById(R.id.tvpayment);

        if(condition.equals("payment")){
            tvpayment.setText("Trip payment completed successfully. Thank you for choosing the Breath and Beats.");
        }else if(condition.equals("CASH")){
            tvpayment.setText("Trip payment has been recived by CASH  successfully. Thank you for choosing the Breath and Beats.");
        }

        Done_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTimerAlert.dismiss();

                BnBUserUtility.Clear_notification(PaymentActivity.this);
                SharedPreferenceBnBUtility.setAdvanceTrip(PaymentActivity.this,"false");
                BnBUserUtility.toastMessage_success("Please wait...",application);
                Intent intent=new Intent(PaymentActivity.this,MainActivity.class);
                //Clear all activities and start new task
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

    }

//--------------------------------------

    public void startPayment() {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        final Activity activity = this;

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Breath&Beats");
            options.put("description", "Ambulance Trip Charges");
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://rzp-mobile.s3.amazonaws.com/images/rzp.png");
            options.put("currency", "INR");
            options.put("amount", String.valueOf(TotalPaymentAmount));

            JSONObject preFill = new JSONObject();
            preFill.put("email", sharedPrefHelper.getUserData().getEmail());
            preFill.put("contact",sharedPrefHelper.getUserData().getPhoneNumber());

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            BnBUserUtility.toastMessage_error("Error in payment: " + e.getMessage(),application);
            e.printStackTrace();
        }
    }
    @Override
    public void onPaymentSuccess(String razorpayPaymentID) {

        try {
            BnBUserUtility.toastMessage_success("Payment Successful: " + razorpayPaymentID,application);
            Log.d(TAG,"razorpayPaymentID: "+razorpayPaymentID);


            SharedPreferenceBnBUtility.setPaymentID(PaymentActivity.this,razorpayPaymentID);

           /* if(SharedPreferenceBnBUtility.getUserPaidMoney(PaymentActivity.this).equals("true")){

            }else if(SharedPreferenceBnBUtility.getUserPaidMoney(PaymentActivity.this).equals("false")){

            }else{

            }*/
            UpdatePaymnetStatusToHospital(razorpayPaymentID);

        } catch (Exception e) {
            Log.d(TAG, "Exception in onPaymentSuccess", e);
        }
    }

    private void UpdatePaymnetStatusToHospital(String razorpayPaymentID) {


        String token="Bearer "+sharedPrefHelper.getAuthToken();
        Log.d(TAG," trip id : "+SharedPreferenceBnBUtility.getNotificationTripID(this));

        TripPaymentInput tripPaymentInput=new TripPaymentInput();
        tripPaymentInput.setTripId(SharedPreferenceBnBUtility.getNotificationTripID(this));
        tripPaymentInput.setPaymentStatus("success");
        tripPaymentInput.setPaid(true);
        tripPaymentInput.setCapturedOnApp(BnBUserUtility.getCurrenttime());
        tripPaymentInput.setCaptured(true);
        tripPaymentInput.setRazorpayPaymentId(razorpayPaymentID);

        Call<TripPaymentResponce> tripPaymentResponceCall=bnBService.Trippayment(token,tripPaymentInput);

        tripPaymentResponceCall.enqueue(new Callback<TripPaymentResponce>() {
            @Override
            public void onResponse(Call<TripPaymentResponce> call, Response<TripPaymentResponce> response) {

                if(response!=null){

                    if(response.isSuccessful()){


                        tripPaymentResponce_v=response.body();
                        Log.d(TAG," responce success  : "+tripPaymentResponce_v.getSuccess().toString());
                        if(tripPaymentResponce_v.getSuccess().toString().equals("true")) {

                            SharedPreferenceBnBUtility.setUserPaidMoney(PaymentActivity.this, "true");
                            SharedPreferenceBnBUtility.setPaymentID(PaymentActivity.this, "null");

                            BnBUserUtility.Clear_SharedPreferenceData(PaymentActivity.this);

                            getAlertBox("payment");
                        }else if(tripPaymentResponce_v.getSuccess().toString().equals("false")){
                            if(tripPaymentResponce_v.getMessage()!=null){
                                BnBUserUtility.toastMessage_info(tripPaymentResponce_v.getMessage().toString(),PaymentActivity.this);
                            }
                        }

                    }else{
                        Log.d(TAG," responce not  success  : ");
                        BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                " reach our service at the moment.Please try again.",getApplicationContext());
                    }

                }else{

                    Log.d(TAG," responce null  : ");
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                            " reach our service at the moment.Please try again.",getApplicationContext());
                }
            }

            @Override
            public void onFailure(Call<TripPaymentResponce> call, Throwable t) {
                Log.d(TAG," failure : "+t);
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                        " reach our service at the moment. Please try again.",getApplicationContext());
            }
        });

    }

    @Override
    public void onPaymentError(int code, String response) {

        try {

            BnBUserUtility.toastMessage_error("Payment failed,Please make a payment again.",PaymentActivity.this);

        } catch (Exception e) {
            Log.d(TAG, "Exception in onPaymentError", e);
        }
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            intent.getAction();
            if(intent.hasExtra("paymentmode")) {
//
            }else{

            }
        }
    };
}

package com.breathandbeats.android.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.RelativeLayout;

import com.breathandbeats.android.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MakeANoiseReceiveActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "MakeANoiseReceiveActivi";
    @BindView(R.id.activity_make_anoise_receive)
    RelativeLayout activityMakeAnoiseReceive;
    GoogleMap googleMap;
    boolean isMapReady;

    double lat;
    double lng;
    String subject;
    String reason;
    String message;
    String distance;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_anoise_receive);
        ButterKnife.bind(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.activity_make_anoise_receive_map);
        mapFragment.getMapAsync(this);
        Bundle makeANoiseReceiveData = getIntent().getExtras();
        subject = makeANoiseReceiveData.getString("subject");
        reason = makeANoiseReceiveData.getString("reason");
        message = makeANoiseReceiveData.getString("message");
        distance = makeANoiseReceiveData.getString("distance");
        lat = makeANoiseReceiveData.getDouble("lat");
        lng = makeANoiseReceiveData.getDouble("lng");
        Log.i(TAG, "onCreate: sub" + subject);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        isMapReady = true;
        this.googleMap = googleMap;

        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latLng = new LatLng(lat, lng);
        markerOptions.position(latLng);
        Marker m = googleMap.addMarker(markerOptions);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
    }
}

package com.breathandbeats.android.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import com.astuetz.PagerSlidingTabStrip;
import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.RV.RequestLogAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RequestLogActivity extends AppCompatActivity {
    private static final String TAG = "RequestLogActivity";

    @BindView(R.id.activity_request_log_vp)
    ViewPager activityRequestLogVp;
    @BindView(R.id.activity_request_log)
    LinearLayout activityRequestLog;

    RequestLogAdapter requestLogAdapter;
    @BindView(R.id.request_toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_request_log_tabs)
    PagerSlidingTabStrip activityRequestLogTabs;
    double lat;
    double lng;
    String subject;
    String reason;
    String message;
    String distance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_log);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        requestLogAdapter = new RequestLogAdapter(getSupportFragmentManager());
        activityRequestLogVp.setAdapter(requestLogAdapter);
        activityRequestLogTabs.setViewPager(activityRequestLogVp);

//        Bundle makeANoiseReceiveData = getIntent().getExtras();
//        subject = makeANoiseReceiveData.getString("subject");
//        reason = makeANoiseReceiveData.getString("reason");
//        message = makeANoiseReceiveData.getString("message");
//        distance = makeANoiseReceiveData.getString("distance");
//        lat = makeANoiseReceiveData.getDouble("lat");
//        lng = makeANoiseReceiveData.getDouble("lng");
//        Log.i(TAG, "onCreate: sub" + subject);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}

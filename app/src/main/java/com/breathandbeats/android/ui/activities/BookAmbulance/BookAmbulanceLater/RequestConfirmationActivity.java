package com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceLater;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.CancelTheTripRequested;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Canceltrip;
import com.breathandbeats.android.services.BnBFirebaseMessagingService;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.MainActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.razorpay.Checkout;
import com.skyfishjy.library.RippleBackground;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lavanya on 15-11-2017.
 */
public class RequestConfirmationActivity  extends AppCompatActivity {


    @BindView(R.id.btnCancelBooking)
    Button btnCancelBooking;
    @BindView(R.id.tvTimer)
    TextView tvTimer;

    CountDownTimer CountDownTimer;
    Dialog dialogTripAlert;
    BnBApp application;
    BnBService bnBService;
    private String TAG="DriverSearch_Activity";
    SharedPrefHelper sharedPrefHelper;
    CancelTheTripRequested cancelTheTripRequested_V=new CancelTheTripRequested();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requestconfirmation);

        final RippleBackground rippleBackground=(RippleBackground)findViewById(R.id.content);
        rippleBackground.startRippleAnimation();

        ButterKnife.bind(this);

        application = (BnBApp) getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Call
        sharedPrefHelper = application.getSharedPrefHelper();

        getTimerFunction();
        Checkout.preload(getApplicationContext());
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastReceiver, new IntentFilter(BnBFirebaseMessagingService.BROADCAST_ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        registerReceiver(broadcastReceiver, new IntentFilter(BnBFirebaseMessagingService.BROADCAST_ACTION));
        unregisterReceiver(broadcastReceiver);
    }

    @OnClick(R.id.btnCancelBooking)
    public  void onbtnCancelBookingClicked() {

        getTheCancelRequestFunction();
    }

    private void getTheCancelRequestFunction() {

        if(NetworkUtil.getConnectivityStatusBoolen(this)) {

            updateTheRequestToHospital();
        }else{
            BnBUserUtility.toastMessage_default("Please note that you are not connected to ineternet.",application);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
//        CountDownTimer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(SharedPreferenceBnBUtility.getNotificationRecived(RequestConfirmationActivity.this).equals("false")){ // no trip assigned to user.
            //do nothing
            if(SharedPreferenceBnBUtility.getAdvacneBookedReq(RequestConfirmationActivity.this).equals("true")){

                getVerifiactionDialogBox("AdvanceBookConfirmed");

            }else if(SharedPreferenceBnBUtility.getAdvacneBookedReq(RequestConfirmationActivity.this).equals("false")){

            }else{

            }
        }
    }

    private void updateTheRequestToHospital() {
        BnBUserUtility.showProgress(this,"Requesting, Please wait...");

        String token="Bearer "+sharedPrefHelper.getAuthToken();

        Canceltrip canceltrip=new Canceltrip();

        canceltrip.setTripId(SharedPreferenceBnBUtility.getNotificationTripID(this));
        Call<CancelTheTripRequested> cancelTheTripRequestedCall=bnBService.CancelTripRequested(token,canceltrip);

        cancelTheTripRequestedCall.enqueue(new Callback<CancelTheTripRequested>() {
            @Override
            public void onResponse(Call<CancelTheTripRequested> call, Response<CancelTheTripRequested> response) {

                if(response!=null) {
                    BnBUserUtility.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.d(TAG, "Responce: success " + response.body());

                        cancelTheTripRequested_V=response.body();

                        if(cancelTheTripRequested_V.getSuccess().equals(true)){

                            BnBUserUtility.toastMessage_success("Trip request cancelled successfully.",RequestConfirmationActivity.this);

                            BnBUserUtility.Clear_notification(RequestConfirmationActivity.this);
                            BnBUserUtility.Clear_SharedPreferenceData(RequestConfirmationActivity.this);
                            SharedPreferenceBnBUtility.setAdvacneBookedReq(RequestConfirmationActivity.this,"false");
                            Intent CallAmbulanceIntent=new Intent(RequestConfirmationActivity.this,MainActivity.class);
                            //Clear all activities and start new task
                            CallAmbulanceIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(CallAmbulanceIntent);

                        } else if (cancelTheTripRequested_V.getSuccess().equals(false)) {

                            BnBUserUtility.toastMessage_info("Unable to cancel Trip request ,Please try again.",RequestConfirmationActivity.this);
                        }

                    } else {
                        Log.d(TAG, "responce not successfull : ");
                        BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                " reach our service at the moment.Please try again.", getApplicationContext());
                    }
                }else{
                    BnBUserUtility.dismissProgressDialog();
                    Log.d(TAG, "Responce: null" );
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                            " reach our service at the moment.Please try again.", getApplicationContext());
                }
            }

            @Override
            public void onFailure(Call<CancelTheTripRequested> call, Throwable t) {
                Log.d(TAG,"FAILURE : "+ t);
                BnBUserUtility.dismissProgressDialog();
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                        " reach our service at the moment.Please try again.",getApplicationContext());
            }
        });
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            intent.getAction();
            if(intent.hasExtra("AdvanceBookConfirmed")) {

                if (intent.getStringExtra("AdvanceBookConfirmed").equals("true")) {
                    getVerifiactionDialogBox("AdvanceBookConfirmed");
                }
            }
            if(NetworkUtil.getConnectivityStatusBoolen(context)){
                //do nothing
            }else{
                BnBUserUtility.toastMessage_warning(getResources().getString(R.string.noInternet),RequestConfirmationActivity.this);
            }

        }
    };

    private void getVerifiactionDialogBox(String condition) {

        dialogTripAlert = new Dialog(this);
        dialogTripAlert.setContentView(R.layout.dialog_tripalert);

        dialogTripAlert.setCancelable(false);
        dialogTripAlert.show();
        Button Done_Btn = (Button) dialogTripAlert.findViewById(R.id.Done_Btn);
        TextView tvTripMessage = (TextView) dialogTripAlert.findViewById(R.id.tvTripMessage);

        if(condition.equals("AdvanceBookConfirmed")){
            CountDownTimer.cancel();
            tvTripMessage.setText("Ambulance successfully assigned to you.make advance booking charges.");
        }else if(condition.equals("time")){
            CountDownTimer.cancel();
            tvTripMessage.setText("Trip request time has been expired.Please make a new request.");
        }


        Done_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTripAlert.dismiss();

                if(condition.equals("AdvanceBookConfirmed")){

                    BnBUserUtility.Clear_notification(RequestConfirmationActivity.this);
                    Intent driverUserIntent=new Intent(RequestConfirmationActivity.this,BookLaterPaymentActivity.class);
                    startActivity(driverUserIntent);

                }else if(condition.equals("time")){
                    getTheCancelRequestFunction();

                }
            }
        });




    }

    private void getTimerFunction() {


        CountDownTimer = new CountDownTimer(600000, 1000) { // 10 min adjust the milli seconds here ,10min time set

            public void onTick(long millisUntilFinished) {
                tvTimer.setText("Time Remaining : "+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {

                tvTimer.setText("Time expired!");
                getVerifiactionDialogBox("time");
            }
        }.start();
    }
}

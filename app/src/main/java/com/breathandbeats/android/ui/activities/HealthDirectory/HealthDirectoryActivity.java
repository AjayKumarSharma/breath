package com.breathandbeats.android.ui.activities.HealthDirectory;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.infrastructure.RetrofitMaps;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.api.SignedUpDoctorsResponse;
import com.breathandbeats.android.pojos.api.SignedUpDoctorsResponseData;
import com.breathandbeats.android.pojos.api.maps.Example;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HealthDirectoryActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, GoogleMap.OnInfoWindowClickListener {
    private static final String TAG = "HealthDirectoryActivity";
    GoogleMap googleMap;
    boolean isMapReady;
    GoogleApiClient mGoogleApiClient;
    Location lastLocation;
    double longitude;
    double latitude;
    ProgressDialog pd;

    HttpLoggingInterceptor logging;
    OkHttpClient client;
    @BindView(R.id.activity_health_dir_hospital_sidemenu)
    LinearLayout activityHealthDirHospitalSidemenu;
    @BindView(R.id.activity_health_dir_trainer_sidemenu)
    LinearLayout activityHealthDirTrainerSidemenu;
    @BindView(R.id.activity_health_dir_clinic_sidemenu)
    LinearLayout activityHealthDirClinicSidemenu;
    @BindView(R.id.activity_health_dir_doctor_sidemenu)
    LinearLayout activityHealthDirDoctorSidemenu;


    BnBApp bnbApp;
    BnBService bnbService;
    SharedPrefHelper sharedPrefHelper;
    String authToken;
    @BindView(R.id.activity_health_directory_sideMenu_LL)
    LinearLayout sideMenuLL;
    @BindView(R.id.activity_health_directory_optionsTV)
    TextView optionsTV;
    @BindView(R.id.activity_health_directory_closeOptions_TV)
    ImageView closeOptionsTV;

    Map<String, SignedUpDoctorsResponseData> mMarkers = new HashMap<String, SignedUpDoctorsResponseData>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_directory);
        ButterKnife.bind(this);
        bnbApp = (BnBApp) getApplication();
        bnbService = bnbApp.getRestClient().getApiSerivice();
        sharedPrefHelper = bnbApp.getSharedPrefHelper();
        authToken = sharedPrefHelper.getAuthToken();
        pd = new ProgressDialog(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.activity_health_directory_map);
        mapFragment.getMapAsync(this);
//        btnHospital.setOnClickListener(v->build_retrofit_and_get_response("Hospital"));
        optionsTV.setVisibility(View.GONE);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        isMapReady = true;
        this.googleMap = googleMap;
        googleMap.clear();
        Log.i(TAG, "onMapReady: " + isMapReady);
        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                googleMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            googleMap.setMyLocationEnabled(true);

        }
        googleMap.setOnInfoWindowClickListener(this);

    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "buildGoogleApiClient: creating client... ");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .enableAutoManage(this, this)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        pd.setTitle("Please Wait");
        pd.setMessage("Setting up Map");
        pd.show();
        Log.i(TAG, "onConnected: ");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        lastLocation = LocationServices.FusedLocationApi
                .getLastLocation(mGoogleApiClient);
        build_retrofit_and_get_response("hospital");
        getSignedUpDoctors("doctor");
    }

    private void getSignedUpHospitals(String entity) {
        BnBUserUtility.toastMessage_info("No clinics found. ",HealthDirectoryActivity.this);
        pd.dismiss();
    }

    private void getSignedUpDoctors(String entity) {
        bnbService.getSignupDoctors("Bearer " + authToken, entity).enqueue(new Callback<SignedUpDoctorsResponse>() {
            @Override
            public void onResponse(Call<SignedUpDoctorsResponse> call, Response<SignedUpDoctorsResponse> response) {
                Log.i(TAG, "onResponse: ");
                if (response.body().getSuccess()) {
                    int numOfDoctors = response.body().getData().size();
                    Log.i(TAG, "onResponse: num" + numOfDoctors);
                    for (int i = 0; i < numOfDoctors; i++) {
                        SignedUpDoctorsResponseData signedUpDoctorsResponseData = response.body().getData().get(i);
                        Double lng = signedUpDoctorsResponseData.getLastLocation().getCoordinates().get(0);
                        Double lat = signedUpDoctorsResponseData.getLastLocation().getCoordinates().get(1);
                        String placeName = signedUpDoctorsResponseData.getName();
                        String vicinity = signedUpDoctorsResponseData.getName();

                        MarkerOptions markerOptions = new MarkerOptions();
                        LatLng latLng = new LatLng(lat, lng);
                        markerOptions.position(latLng);
                        markerOptions.title(placeName + " : " + vicinity);
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                        Marker m = googleMap.addMarker(markerOptions);
                        mMarkers.put(m.getId(), signedUpDoctorsResponseData);
                        Log.i(TAG, "onResponse: signed" + placeName);
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
                    }
                    if(numOfDoctors==0){
                        if(entity.equals("trainer")) {
                            BnBUserUtility.toastMessage_info("No trainers found.", HealthDirectoryActivity.this);
                        }else if(entity.equals("doctor")){
                            BnBUserUtility.toastMessage_info("No doctors found.", HealthDirectoryActivity.this);
                        }
                    }
                    pd.dismiss();
                }
            }

            @Override
            public void onFailure(Call<SignedUpDoctorsResponse> call, Throwable t) {
                pd.dismiss();
                Log.d(TAG," @failure : "+t);
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed: ");
    }


    public OkHttpClient getClient() {
        logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        client = new OkHttpClient.Builder().addInterceptor(logging)
                .build();

        return client;
    }

    private void build_retrofit_and_get_response(String type) {
        Log.i(TAG, "build_retrofit_and_get_response: starting...");
        String url = "https://maps.googleapis.com/maps/";


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getClient())
                .build();

        RetrofitMaps service = retrofit.create(RetrofitMaps.class);
        if(lastLocation!=null) {
            latitude = lastLocation.getLatitude();
            longitude = lastLocation.getLongitude();
        }


        Call<Example> call = service.getNearbyPlaces(type, latitude + "," + longitude, 10000);

        call.enqueue(new Callback<Example>() {
            @Override
            public void onResponse(Call<Example> call, Response<Example> response) {
                try {
                    Log.i(TAG, "onResponse: " + response.body());
                    for (int i = 0; i < response.body().getResults().size(); i++) {
                        Double lat = response.body().getResults().get(i).getGeometry().getLocation().getLat();
                        Double lng = response.body().getResults().get(i).getGeometry().getLocation().getLng();
                        String placeName = response.body().getResults().get(i).getName();
                        String vicinity = response.body().getResults().get(i).getVicinity();
                        MarkerOptions markerOptions = new MarkerOptions();
                        LatLng latLng = new LatLng(lat, lng);
                        markerOptions.position(latLng);
                        markerOptions.title(placeName + " : " + vicinity);

                        Marker m = googleMap.addMarker(markerOptions);
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        googleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
                    }
                    if(response.body().getResults().size()==0){

                        if(type.equals("hospital")){
                           BnBUserUtility.toastMessage_info("No hospitals found. ",HealthDirectoryActivity.this);
                        }else if(type.equals("clinic")){
                            BnBUserUtility.toastMessage_info("No clinics found. ",HealthDirectoryActivity.this);
                        }
                    }
                } catch (Exception e) {
                    Log.i(TAG, "onResponse: error");

                }
                pd.dismiss();
            }

            @Override
            public void onFailure(Call<Example> call, Throwable t) {
                Log.i(TAG, "onFailure: " + t.toString());
                pd.dismiss();
            }
        });
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        googleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }


    }

    @OnClick(R.id.activity_health_dir_hospital_sidemenu)
    public void menu1click() {
        googleMap.clear();
        Log.i(TAG, "onRequestPermissionsResult: ");
        pd.setMessage("Getting Hospitals");
        pd.show();

        build_retrofit_and_get_response("hospital");
//        getSignedUpHospitals("hospital");
    }

    @OnClick(R.id.activity_health_dir_trainer_sidemenu)
    public void menu2click() {
        googleMap.clear();
        pd.setMessage("Getting Trainers");
        pd.show();

//        build_retrofit_and_get_response("pharmacy");
        getSignedUpDoctors("trainer");

    }

    @OnClick(R.id.activity_health_dir_clinic_sidemenu)
    public void menu3click() {
        googleMap.clear();
        pd.setMessage("Getting Clinics");
        pd.show();
//        build_retrofit_and_get_response("fire_station");
        build_retrofit_and_get_response("doctor");// will get clinic under this api.
//        getSignedUpHospitals("clinic");

    }

    @OnClick(R.id.activity_health_dir_doctor_sidemenu)
    public void menu4click() {
        googleMap.clear();
        pd.setMessage("Getting Doctors");
        pd.show();
//        build_retrofit_and_get_response("doctor");
        getSignedUpDoctors("doctor");
    }

    @OnClick({R.id.activity_health_directory_optionsTV, R.id.activity_health_directory_closeOptions_TV})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_health_directory_optionsTV:
                showOptions();
                break;
            case R.id.activity_health_directory_closeOptions_TV:
                hideOptions();
                break;
        }
    }

    private void showOptions() {
        sideMenuLL.setVisibility(View.VISIBLE);
        optionsTV.setVisibility(View.GONE);
        closeOptionsTV.setVisibility(View.VISIBLE);
    }

    private void hideOptions() {
        closeOptionsTV.setVisibility(View.GONE);
        sideMenuLL.setVisibility(View.GONE);
        optionsTV.setVisibility(View.VISIBLE);
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Log.i(TAG, "onInfoWindowClick: id" + marker.getId());
        Log.i(TAG, "onInfoWindowClick: data" + mMarkers.get(marker.getId()));
        SignedUpDoctorsResponseData signedUpDoctorsResponseData = mMarkers.get(marker.getId());
        Intent intent = new Intent(this, HealthDirectoryDetailActivity.class);
        String name="";
        if(signedUpDoctorsResponseData!=null) {
            if (signedUpDoctorsResponseData.getName() != null) {
                name = signedUpDoctorsResponseData.getName();
            }
            List<String> workingDays = signedUpDoctorsResponseData.getWorkingDays();
            List<String> workingTime = signedUpDoctorsResponseData.getWorkingTime();
            String primaryContact = signedUpDoctorsResponseData.getPrimaryContact();
            List<String> otherContactNumber = signedUpDoctorsResponseData.getOtherContactNumber();
            Log.i(TAG, "onInfoWindowClick: name" + name);
            Log.i(TAG, "onInfoWindowClick: workingdayus" + workingDays);
            Log.i(TAG, "onInfoWindowClick: workingTime" + workingTime);
            Log.i(TAG, "onInfoWindowClick: primaryContact" + primaryContact);
            startActivity(intent);
            Log.i(TAG, "onInfoWindowClick: ");
        }
    }
}



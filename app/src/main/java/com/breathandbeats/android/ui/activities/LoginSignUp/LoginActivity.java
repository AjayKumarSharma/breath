package com.breathandbeats.android.ui.activities.LoginSignUp;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.infrastructure.Constants;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.api.DeviceInfo;
import com.breathandbeats.android.pojos.api.ForgotPasswordRequest;
import com.breathandbeats.android.pojos.api.ForgotPasswordResponse;
import com.breathandbeats.android.pojos.api.GenericResponse;
import com.breathandbeats.android.pojos.api.LoginRequest;
import com.breathandbeats.android.pojos.api.LoginResponse;
import com.breathandbeats.android.pojos.api.UserData;
import com.breathandbeats.android.pojos.api.VerifyForgotPassRequest;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.MainActivity;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.breathandbeats.android.utils.Validators;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "Login_Activity";
    EditText passwordET;
    EditText mobileNumberET;
    Button loginBtn;
    TextView signUpTV;
    Dialog forgotDialog;
    Dialog resetDialog;

    String phoneNumber;
    String password;
    BnBApp application;
    BnBService apiService;
    SharedPrefHelper sharedPrefHelper;
    ForgotPasswordResponse forgotPasswordResponse_v=new ForgotPasswordResponse();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        passwordET = (EditText) findViewById(R.id.activity_login_password_ET);
        mobileNumberET = (EditText) findViewById(R.id.activity_login_mobileNumber_ET);
        loginBtn = (Button) findViewById(R.id.activity_login_login_BTN);
        signUpTV = (TextView) findViewById(R.id.activity_login_signUp_TV);
        TextView forgotPasswordTV = (TextView) findViewById(R.id.activity_login_forgot_password_TV);
        application = (BnBApp) getApplication();

        apiService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();
        loginBtn.setOnClickListener(view -> onLoginBtnClicked());
        signUpTV.setOnClickListener(view -> goToSignUpScreen());

        forgotPasswordTV.setOnClickListener(view -> launchForgotPasswordDialog());

    }

    private void goToSignUpScreen() {
        startActivity(new Intent(this, SignUpActivity.class));
    }


    private boolean validateCredentials(String phoneNumber, String password) {
        Log.i(TAG, "validateCredentials: ");
        if (!Validators.isMobileValid(phoneNumber)) {
            Log.i(TAG, "validateCredentials: mobileinvalid");
            mobileNumberET.setError(getResources().getString(R.string.phoneNumberError));
            return false;
        }
        if (password.isEmpty()) {
            Log.i(TAG, "validateCredentials: pass invalid");
            passwordET.setError(getResources().getString(R.string.passwordError));
            return false;
        }
        return true;
    }

    public void onLoginBtnClicked() {
        phoneNumber = mobileNumberET.getText().toString();
        String pass_v=phoneNumber;
        password = passwordET.getText().toString();
//        password = Shared.encryptPassword(phoneNumber, password);
        Log.i(TAG, "onLoginBtnClicked: " + phoneNumber + " " + password);
        boolean areCredentialValid = validateCredentials(phoneNumber, password);
        if (areCredentialValid) {
            BnBUserUtility.showProgress(LoginActivity.this,"Please wait...");
            apiService.login(getLoginRequest(phoneNumber, password)).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                    BnBUserUtility.dismissProgressDialog();
                    if (response.body() != null && response.body().getSuccess()) {
                        storeUserInSP(response.body().getData());
                        sharedPrefHelper.setAuthToken(response.body().getData().getToken());
                        sharedPrefHelper.setLoggedIn(true);
                        sharedPrefHelper.setPhoneVerified(response.body().getData().getPhoneVerified());
                        sharedPrefHelper.setUserID(response.body().getData().getId());

                        Log.d(TAG, " name : " + response.body().getData().getName());
                        Log.d(TAG, " blood group : " + response.body().getData().getBloodGroup());
                        if (response.body().getData() != null) {

                            boolean isPhoneVerified = response.body().getData().getPhoneVerified();
                            Log.d(TAG, "token : " + response.body().getData().getToken());
                            SharedPreferenceBnBUtility.setUserName(LoginActivity.this, response.body().getData().getName());
                            SharedPreferenceBnBUtility.setUserNumber(LoginActivity.this, response.body().getData().getPhoneNumber());

                            SharedPreferenceBnBUtility.setUserEmail(LoginActivity.this, response.body().getData().getEmail());
                            if (response.body().getData().getBloodGroup() != null) {
                                SharedPreferenceBnBUtility.setUserBloodgroup(LoginActivity.this, response.body().getData().getBloodGroup());
                            }

                            SharedPreferenceBnBUtility.setUserPassword(LoginActivity.this, pass_v);

                        }

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    } else {
                        BnBUserUtility.dismissProgressDialog();
                        if (response.body().getMessage() != null){
                            Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.i(TAG, "onFailure: ");
                    BnBUserUtility.dismissProgressDialog();
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't reach our service at the moment.Please try again.",LoginActivity.this);
                }
            });
        }
    }

    private LoginRequest getLoginRequest(String phoneNumber, String password) {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setPhoneNumber(phoneNumber);
        loginRequest.setAppVersion("1");
        loginRequest.setPassword(password);
        loginRequest.setPlatform(Constants.PLATFORM);
        loginRequest.setDeviceInfo(new DeviceInfo("abcd", "efgh",sharedPrefHelper.getFCMToken()));
        return loginRequest;
    }

    private void storeUserInSP(UserData data) {
        sharedPrefHelper.saveUserData(data);
    }


    public void launchForgotPasswordDialog() {
        forgotDialog = new Dialog(this);
        forgotDialog.setContentView(R.layout.fragment_forgot_password);
        Button confirmBtn = (Button) forgotDialog.findViewById(R.id.fragment_forgot_password_confirmBtn);
        Button cancelBtn = (Button) forgotDialog.findViewById(R.id.fragment_forgot_password_cancelBtn);
        final EditText phoneNumberET = (EditText) forgotDialog.findViewById(R.id.fragment_forgot_password_phoneNumberET);
        confirmBtn.setOnClickListener(view -> {
            if (!Validators.isMobileValid(phoneNumberET.getText().toString())) {
                phoneNumberET.setError("Invalid Number");
                return;
            }
            final String phoneNumber1 = phoneNumberET.getText().toString();
            apiService.initiateForgotPassword(getForgotPasswordRequest(phoneNumber1)).enqueue(new Callback<ForgotPasswordResponse>() {
                @Override
                public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                    if (response.body().getSuccess()) {
                        forgotDialog.dismiss();
                        Log.d(TAG, "responce : ");
                        if(response.body()!=null) {

                            forgotPasswordResponse_v=response.body();
                            Log.d(TAG, "responce body not null : "+response.body().getSuccess());

                            if(response.body().getSuccess()==true){

                                launchResetPassDialog( phoneNumber1);
                                Log.d(TAG, "dtaa : " + response.body().getData());
                            }else if(response.body().getSuccess()==false){

                            }

                        }else{
                            Log.d(TAG, "responce null : ");
                        }
                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        forgotDialog.dismiss();
                        Log.d(TAG, "responce failure : ");
                    }

                }

                @Override
                public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                    Log.d(TAG, "responce failure exsep : "+t);
                    forgotDialog.dismiss();
                }
            });

        });
        cancelBtn.setOnClickListener(view -> forgotDialog.dismiss());
        forgotDialog.show();
    }

    private ForgotPasswordRequest getForgotPasswordRequest(String phoneNumber) {
        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest(phoneNumber);
        return forgotPasswordRequest;
    }


    public void launchResetPassDialog( final String phoneNumber) {
        resetDialog = new Dialog(LoginActivity.this);
        resetDialog.setContentView(R.layout.fragment_reset_password);
        resetDialog.show();
        forgotDialog.dismiss();
        TextView emailTV = (TextView) resetDialog.findViewById(R.id.fragment_reset_password_emailTV);
        final EditText passwordET = (EditText) resetDialog.findViewById(R.id.fragment_reset_password_passwordET);
        final EditText resetCodeET = (EditText) resetDialog.findViewById(R.id.fragment_reset_password_restCodeET);
        String displayText = emailTV.getText().toString();
       /* displayText = displayText + "registered mobile number.";
        emailTV.setText(displayText);*/
        Button resetDialogConfirmBtn = (Button) resetDialog.findViewById(R.id.fragment_reset_password_confirmBtn);
        Button resetDialogCancelBtn = (Button) resetDialog.findViewById(R.id.fragment_reset_password_cancelBtn);

        resetDialogCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetDialog.dismiss();
            }
        });

        resetDialogConfirmBtn.setOnClickListener(view -> {
            if (resetCodeET.getText().toString().isEmpty()) {
                resetCodeET.setError("Code cannot be empty");
                return;
            }
            if (passwordET.getText().toString().isEmpty()) {
                passwordET.setError("password cannot be empty");
                return;
            }
            final String newPass = passwordET.getText().toString();
            final String resetCode = resetCodeET.getText().toString();
            VerifyForgotPassRequest verifyForgotPassword = new VerifyForgotPassRequest();
            verifyForgotPassword.setPhoneNumber(phoneNumber);
            verifyForgotPassword.setNewPassword(newPass);
            verifyForgotPassword.setVerificationCode(resetCode);

            apiService.verifyForgotPass(verifyForgotPassword).enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                    if (response.body().getSuccess()) {
                        Toast.makeText(LoginActivity.this, "Password Reset Successfull", Toast.LENGTH_SHORT).show();
                        resetDialog.dismiss();
                        startActivity(new Intent(LoginActivity.this, LoginActivity.class));
                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    Toast.makeText(LoginActivity.this, Constants.ERRORMSG, Toast.LENGTH_SHORT).show();
                }
            });

        });
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

package com.breathandbeats.android.ui.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.Logout;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow.TripHistoryAcitivity;
import com.breathandbeats.android.ui.activities.EmergencyContact.EmergencyContactListActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.ProfileActivity;
import com.breathandbeats.android.ui.activities.RequestLogActivity;
import com.breathandbeats.android.ui.activities.SOSLogActivity;
import com.breathandbeats.android.ui.activities.SplashActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rakesh on 09-11-2017.
 */

public class RequestFragment extends Fragment {

    @BindView(R.id.fragment_profile_emergencyContacts_LL)
    LinearLayout emergencyContactsLL;
    @BindView(R.id.fragment_profile_request_LL)
    LinearLayout requestLL;
    @BindView(R.id.fragment_profile_sosLogs)
    LinearLayout sosLogsLL;

    @BindView(R.id.tripInformationlayout)
    LinearLayout tripInformationlayout;
    @BindView(R.id.profilelayout)
    LinearLayout profilelayout;
    @BindView(R.id.logout)
    LinearLayout logout;

    BnBApp application;
    BnBService apiSerivice;
    SharedPrefHelper sharedPrefHelper;
    BnBService bnBService;
    static  String authToken;
    Logout logout_v=new Logout();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_request, container, false);
        ButterKnife.bind(this, v);
        application = (BnBApp) getActivity().getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Call
        sharedPrefHelper = application.getSharedPrefHelper();
        authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();
        return v;
    }

    @OnClick(R.id.fragment_profile_request_LL)
    public void goToRequestLogScreen() {
        startActivity(new Intent(getActivity(), RequestLogActivity.class));
    }

    @OnClick(R.id.fragment_profile_emergencyContacts_LL)
    public void goToEmergencyContactScreen() {
        startActivity(new Intent(getActivity(), EmergencyContactListActivity.class));
    }

    @OnClick(R.id.fragment_profile_sosLogs)
    public void goToSOSLogsScreen() {
        startActivity(new Intent(getActivity(), SOSLogActivity.class));
    }

    @OnClick(R.id.tripInformationlayout)
    public void ontripInformationlayoutClicked() {
        startActivity(new Intent(getActivity(), TripHistoryAcitivity.class));
    }
    @OnClick(R.id.profilelayout)
    public void onprofilelayoutClicked() {
        startActivity(new Intent(getActivity(), ProfileActivity.class));
    }
    @OnClick(R.id.logout)
    public void onlogoutClicked() {

        DispalyAlerForExit("Are you sure, You want to logout the application ? ");

    }

    private void DispalyAlerForExit(String message)//dialog box----
    {
        new android.app.AlertDialog.Builder(getContext()).setMessage(message).setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)            {

                updateTOAdmin();
                }
        }).setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {

            }
        }).show();
    }

    private void updateTOAdmin() {

        if(NetworkUtil.getConnectivityStatusBoolen(getContext())){

            updatestatus();
        }else{
            BnBUserUtility.toastMessage_info("Please note that you are not connected to internet",getContext());
        }
    }

    private void updatestatus() {

        BnBUserUtility.showProgress(getContext(),"Please wait...");
        Call<Logout> completedTripListDataCall=bnBService.LogoutInput(authToken);

        completedTripListDataCall.enqueue(new Callback<Logout>() {
            @Override
            public void onResponse(Call<Logout> call, Response<Logout> response) {

                if(response.isSuccessful()){
                    BnBUserUtility.dismissProgressDialog();
                    logout_v=response.body();

                    if(logout_v.getSuccess().equals(true)){

                        sharedPrefHelper.setAuthToken(null);
                        Intent intent=new Intent(getContext(),SplashActivity.class);
                        startActivity(intent);

                    }else{

                        BnBUserUtility.toastMessage_info(logout_v.getMessage(),getContext());
                    }
                }else{
                    BnBUserUtility.dismissProgressDialog();
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't reach our " +
                            "service at the moment.Please try again.",getContext());
                }
            }

            @Override
            public void onFailure(Call<Logout> call, Throwable t) {
                BnBUserUtility.dismissProgressDialog();
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't reach our " +
                        "service at the moment.Please try again.",getContext());
            }
        });

    }
}

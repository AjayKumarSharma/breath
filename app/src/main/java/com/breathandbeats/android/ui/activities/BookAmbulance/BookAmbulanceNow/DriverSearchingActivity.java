package com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow;

import android.Manifest;
import android.animation.IntEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.CancelTheTripRequested;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Canceltrip;
import com.breathandbeats.android.services.BnBFirebaseMessagingService;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.BookAmbulance.CallAmbulanceActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.razorpay.Checkout;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rakesh on 05-10-2017.
 */

public class DriverSearchingActivity  extends AppCompatActivity  implements OnMapReadyCallback{

    private GoogleMap mMap;
    @BindView(R.id.btnCancelBooking)
    Button btnCancelBooking;

    @BindView(R.id.tvtimer)
     TextView tvtimer;

    Dialog dialogTripAlert;
    private  double UserLatitude;
    private  double UserLongitude;
    BnBApp application;
    BnBService bnBService;
    private String TAG="DriverSearch_Activity";
    SharedPrefHelper sharedPrefHelper;
    CountDownTimer CountDownTimer;
    CancelTheTripRequested cancelTheTripRequested_V=new CancelTheTripRequested();
    private GoogleMap googleMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchingdriver);

        ButterKnife.bind(this);

        application = (BnBApp) getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Call
        sharedPrefHelper = application.getSharedPrefHelper();

//         Intent intent=getIntent();
        /*if(intent.hasExtra("Latitude") && intent.hasExtra("Longitude")){

            if(intent.getStringExtra("Latitude")!=null && intent.getStringExtra("Longitude")!=null) {
                UserLatitude = Double.parseDouble(intent.getStringExtra("Latitude"));
                UserLongitude = Double.parseDouble(intent.getStringExtra("Longitude"));

                Log.d(TAG, "LATLONG: " + UserLatitude + ":  " + UserLongitude);
            }
        }*/

        UserLatitude=Double.parseDouble(SharedPreferenceBnBUtility.getuserRequestedPointLatitude(DriverSearchingActivity.this));
        UserLongitude=Double.parseDouble(SharedPreferenceBnBUtility.getuserRequestedPointLongitude(DriverSearchingActivity.this));

        SupportMapFragment mapFragment = (SupportMapFragment ) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);

        registerReceiver(broadcastReceiver, new IntentFilter(BnBFirebaseMessagingService.BROADCAST_ACTION));

        getTimerFunction();
        Checkout.preload(getApplicationContext());

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        CountDownTimer.cancel();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap=googleMap;


            LatLng source = new LatLng(UserLatitude, UserLongitude);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        if(source!=null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(source, 13));

//        googleMap.setTrafficEnabled(true);

            setUpMap(source, mMap);

            startSearchingDriver(source);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastReceiver, new IntentFilter(BnBFirebaseMessagingService.BROADCAST_ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        registerReceiver(broadcastReceiver, new IntentFilter(BnBFirebaseMessagingService.BROADCAST_ACTION));
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(SharedPreferenceBnBUtility.getNotificationRecived(DriverSearchingActivity.this).equals("true")) {//trip notification for ride
            //
            /*Intent intent=new Intent(DriverSearchingActivity.this, MainActivity.class);
            //Clear all activities and start new task
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
            getVerifiactionDialogBox("AmbulanceAssigned");

        }else{
            //do nothing
        }
    }

    @OnClick(R.id.btnCancelBooking)
    public  void  btnCancelBookingClicked(){

        if(SharedPreferenceBnBUtility.getNotificationRecived(DriverSearchingActivity.this).equals("true")) {//trip notification for ride
       //
            /*Intent intent=new Intent(DriverSearchingActivity.this, MainActivity.class);
            //Clear all activities and start new task
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
            getVerifiactionDialogBox("AmbulanceAssigned");

        }else if(SharedPreferenceBnBUtility.getNotificationRecived(DriverSearchingActivity.this).equals("false")){
            getTheCancelRequestFunction("true");
        }else{
            getTheCancelRequestFunction("true");
        }

    }

    private void getTheCancelRequestFunction(String condition_v) {

        if(NetworkUtil.getConnectivityStatusBoolen(this)) {

            updateTheRequestToHospital(condition_v);
        }else{
            BnBUserUtility.toastMessage_default("Please note that you are not connected to ineternet.",application);
        }
    }

    private void updateTheRequestToHospital(String condition_v) {
//        BnBUserUtility.showProgress(this,"Requesting, Please wait...");

        String token="Bearer "+sharedPrefHelper.getAuthToken();

        Canceltrip  canceltrip=new Canceltrip();

        canceltrip.setTripId(SharedPreferenceBnBUtility.getNotificationTripID(this));
        Call<CancelTheTripRequested> cancelTheTripRequestedCall=bnBService.CancelTripRequested(token,canceltrip);

        cancelTheTripRequestedCall.enqueue(new Callback<CancelTheTripRequested>() {
            @Override
            public void onResponse(Call<CancelTheTripRequested> call, Response<CancelTheTripRequested> response) {

                if(response!=null) {
                    BnBUserUtility.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.d(TAG, "Responce: success " + response.body());

                        cancelTheTripRequested_V=response.body();

                        if(cancelTheTripRequested_V.getSuccess().equals(true)){

                            if(condition_v.equals("false")){
                               //don't do anything
                                SharedPreferenceBnBUtility.setrequestedforhospital(DriverSearchingActivity.this,"false");
                            }else if(condition_v.equals("true")){
                                BnBUserUtility.toastMessage_success("Trip request cancelled successfully.",DriverSearchingActivity.this);
                                SharedPreferenceBnBUtility.setrequestedforhospital(DriverSearchingActivity.this,"false");
                                BnBUserUtility.Clear_notification(DriverSearchingActivity.this);
                                BnBUserUtility.Clear_SharedPreferenceData(DriverSearchingActivity.this);
                                Intent CallAmbulanceIntent=new Intent(DriverSearchingActivity.this,CallAmbulanceActivity.class);
                                startActivity(CallAmbulanceIntent);
                            }


                        } else if (cancelTheTripRequested_V.getSuccess().equals(false)) {

//                              BnBUserUtility.toastMessage_info("Unable to cancel Trip request ,Please try again.",DriverSearchingActivity.this);
                        }

                    } else {
                        Log.d(TAG, "responce not successfull : ");
                        BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                " reach our service at the moment.Please try again.", getApplicationContext());
                    }
                }else{
                    BnBUserUtility.dismissProgressDialog();
                    Log.d(TAG, "Responce: null" );
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                            " reach our service at the moment.Please try again.", getApplicationContext());
                }
            }

            @Override
            public void onFailure(Call<CancelTheTripRequested> call, Throwable t) {
                Log.d(TAG,"FAILURE : "+ t);
                BnBUserUtility.dismissProgressDialog();
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                        " reach our service at the moment.Please try again.",getApplicationContext());
            }
        });
    }

    private void setUpMap(final LatLng  markerLatLng,final GoogleMap mMap) {

        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);


        mMap.addMarker(new MarkerOptions()
                .position(markerLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));

        final View mapView = getSupportFragmentManager().findFragmentById(R.id.map).getView();
        if (mapView.getViewTreeObserver().isAlive()) {
            mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressLint("NewApi")
                // We check which build version we are using.
                @Override
                public void onGlobalLayout() {
                    LatLngBounds bounds = new LatLngBounds.Builder().include(markerLatLng).build();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
//                   mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 13));
                }
            });
        }
    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    private void startSearchingDriver(LatLng source) {

        final Circle circle = mMap.addCircle(new CircleOptions().center(source)
                .strokeWidth(4)
                .strokeColor(getResources().getColor(R.color.green)).radius(100));

        ValueAnimator vAnimator = new ValueAnimator();
        vAnimator.setRepeatCount(ValueAnimator.INFINITE);
        vAnimator.setRepeatMode(ValueAnimator.RESTART);  /* PULSE */
        vAnimator.setIntValues(0, 100);
        vAnimator.setDuration(2000);
        vAnimator.setEvaluator(new IntEvaluator());
        vAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        vAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float animatedFraction = valueAnimator.getAnimatedFraction();
                // Log.e("", "" + animatedFraction);
                circle.setRadius(animatedFraction * 1500);
            }
        });
        vAnimator.start();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            intent.getAction();
            if(intent.hasExtra("AmbulanceAssigned")) {

                if (intent.getStringExtra("AmbulanceAssigned").equals("true")) {
                    CountDownTimer.cancel();
                    getVerifiactionDialogBox("AmbulanceAssigned");
                } else {
                }

            }else{

            }
            if(NetworkUtil.getConnectivityStatusBoolen(context)){
            }else{
                BnBUserUtility.toastMessage_warning(getResources().getString(R.string.noInternet),DriverSearchingActivity.this);
            }

        }
    };

    private void getVerifiactionDialogBox(String condition) {


                dialogTripAlert = new Dialog(DriverSearchingActivity.this);
                dialogTripAlert.setContentView(R.layout.dialog_tripalert);

                dialogTripAlert.setCancelable(false);

                Button Done_Btn = (Button) dialogTripAlert.findViewById(R.id.Done_Btn);
                TextView tvTripMessage = (TextView) dialogTripAlert.findViewById(R. id.tvTripMessage);

                if(condition.equals("AmbulanceAssigned")) {
                    CountDownTimer.cancel();
                    tvTripMessage.setText("Ambulance successfully assigned to you.Please click on OK to get details.");
                }else if(condition.equals("time")){
                    tvTripMessage.setText("Trip request time has been expired.Please make a new request.");
                }

                Done_Btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogTripAlert.dismiss();
                        SharedPreferenceBnBUtility.setrequestedforhospital(DriverSearchingActivity.this,"false");
                        if(condition.equals("AmbulanceAssigned")) {
                            BnBUserUtility.Clear_notification(DriverSearchingActivity.this);
                            SharedPreferenceBnBUtility.setrequestedforhospital(DriverSearchingActivity.this,"false");
                            Intent driverUserIntent=new Intent(DriverSearchingActivity.this,DriverUserMapViewActivity.class);
                            startActivity(driverUserIntent);
                        }else if(condition.equals("time")){

                            Intent CallAmbulanceIntent=new Intent(DriverSearchingActivity.this,CallAmbulanceActivity.class);
                            startActivity(CallAmbulanceIntent);
                        }

                    }
                });
        try {
            dialogTripAlert.show();
        } catch(Exception e){
            // WindowManager$BadTokenException will be caught and the app would not display
            // the 'Force Close' message
        }

    }

    private void getTimerFunction() {


        CountDownTimer = new CountDownTimer(60000, 1000) { // adjust the milli seconds here ,1min time set

            public void onTick(long millisUntilFinished) {
                tvtimer.setText("Time Remaining : "+String.format("%d min, %d sec",
                        TimeUnit.MILLISECONDS.toMinutes( millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) -
                                TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
            }

            public void onFinish() {

                tvtimer.setText("Time expired!");
                if(SharedPreferenceBnBUtility.getNotificationTripStatus(DriverSearchingActivity.this).equals("true")){
                  //do nothing
                    CountDownTimer.cancel();
                    tvtimer.setText("Ambulance assigned to you.Please check the notificaiton!");

                    getVerifiactionDialogBox("AmbulanceAssigned");

                }else if(SharedPreferenceBnBUtility.getNotificationTripStatus(DriverSearchingActivity.this).equals("false")){
                    getVerifiactionDialogBox("time");
                    getTheCancelRequestFunction("false");
                }else{
                    getVerifiactionDialogBox("time");
                    getTheCancelRequestFunction("false");
                }
            }
        }.start();
    }
}

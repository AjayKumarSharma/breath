package com.breathandbeats.android.ui.Adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.HistoryTripListPojo.AdvannceBookedTripData;
import com.breathandbeats.android.ui.activities.BnBUserUtility;

/**
 * Created by Rakesh on 02-11-2017.
 */

public class AdvanceBookedTripAdapter extends RecyclerView.Adapter<AdvanceBookedTripAdapter.ViewHolder>{


    Context context;
    AdvannceBookedTripData advancebookdata =new AdvannceBookedTripData();
    String TAG="advance_booked";
    public AdvanceBookedTripAdapter(Context context, AdvannceBookedTripData historyTripData) {

       this.advancebookdata =historyTripData;
        this.context=context;
    }

    @Override
    public AdvanceBookedTripAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()) .inflate(R.layout.advancebooktrip_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdvanceBookedTripAdapter.ViewHolder holder, int position) {

        if (position % 2 == 1) {
            holder.rlMainlayout.setBackgroundColor(context.getResources().getColor(R.color.backgroundcolor));
        } else {
            holder.rlMainlayout.setBackgroundColor(context.getResources().getColor(R.color.white));
        }
            String datevalue= advancebookdata.getData().get(position).getScheduledOnAppTime().toString();
            holder.tvDate.setText(BnBUserUtility.getTheCorrectFormateDate(datevalue));

                Log.d(TAG,"name : "+advancebookdata.getData().get(position).getHospital().getName().toString());
            holder.tvhospitalname.setText(advancebookdata.getData().get(position).getHospital().getName().toString());
            holder.tvambulancetype.setText(advancebookdata.getData().get(position).getAmbulance().getAmbulanceCategory().toString());
            holder.tvpaymentstatus.setText("Completed");
//            holder.tvpaymnetstatus.setText(advancebookdata.getData().get(position).);


        holder.callhospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String HospitalNo =advancebookdata.getData().get(position).getHospital().getPhoneNumber().toString();
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + HospitalNo));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                context.startActivity(intent);
            }
        });

        holder.calldriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String DriverNo =advancebookdata.getData().get(position).getDriver().getPhoneNumber().toString();
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setData(Uri.parse("tel:" + DriverNo));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                context.startActivity(intent);

            }
        });
    }



    @Override
    public int getItemCount() {
        return advancebookdata.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate;
        TextView tvDeatils;
        TextView tvhospitalname,tvambulancetype, tvpaymentstatus,tvpaymnetstatus;
        RelativeLayout rlMainlayout;
        LinearLayout calldriver,callhospital;


        public ViewHolder(View itemView) {
            super(itemView);
            tvDate=(TextView)itemView.findViewById(R.id.tvDate);
            tvhospitalname=(TextView)itemView.findViewById(R.id.tvhospitalname);
            tvambulancetype=(TextView)itemView.findViewById(R.id.tvambulancetype);
            tvpaymentstatus =(TextView)itemView.findViewById(R.id.tvpaymentstatus);
            rlMainlayout=(RelativeLayout)itemView.findViewById(R.id.rlMainlayout);
            callhospital=(LinearLayout) itemView.findViewById(R.id.callhospital);
            calldriver=(LinearLayout)itemView.findViewById(R.id.calldriver);
        }
    }
}

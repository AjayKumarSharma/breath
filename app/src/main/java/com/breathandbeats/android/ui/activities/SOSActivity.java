package com.breathandbeats.android.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.api.GenericResponse;
import com.breathandbeats.android.pojos.api.SOSRequest;
import com.breathandbeats.android.pojos.orm.EmergencyContact;
import com.breathandbeats.android.ui.activities.EmergencyContact.EmergencyContactActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RuntimePermissions
public class SOSActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = "SOSActivity";
    @BindView(R.id.activity_sos_callPCRVan_LL)
    LinearLayout activitySosCallPCRVanLL;
    @BindView(R.id.activity_sos_callFireBrigade_LL)
    LinearLayout activitySosCallFireBrigadeLL;
    @BindView(R.id.activity_sos_addEmergencyContact_LL)
    LinearLayout activitySosAddEmergencyContactLL;
    @BindView(R.id.activity_sos)
    LinearLayout activitySos;

    GoogleApiClient googleApiClient;

    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    DBHelper dbHelper;

    Location lastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos);
        ButterKnife.bind(this);
        application = (BnBApp) getApplication();
        bnBService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();
        dbHelper = application.getDBHelper();
        Log.i(TAG, "onCreate: number" + dbHelper.getNumberOfEmergencyContacts());
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        if (dbHelper.getNumberOfEmergencyContacts() == 0) {
            Log.i(TAG, "onCreate: zero emergency contact");
            startActivity(new Intent(this, EmergencyContactActivity.class));
        }

    }

    private void sendSOS(String sosType) {
        String authHeader = "Bearer " + sharedPrefHelper.getUserData().getToken();
        Log.d(TAG," toen:  "+authHeader);
        bnBService.sos(authHeader, getSOSRequest(sosType)).enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                Log.i(TAG, "onResponse: "+response.body());
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                Log.i(TAG, "onFailure: ");
            }
        });
    }

    private SOSRequest getSOSRequest(String sosType) {
        SOSRequest sosRequest = new SOSRequest();
        sosRequest.setSosType(sosType);
        List<EmergencyContact> emergencyContacts = dbHelper.getEmergencyContacts();
        ArrayList<String> emergencyContactNumbers = new ArrayList<>();
        for (int i = 0; i < emergencyContacts.size(); i++) {
            emergencyContactNumbers.add(emergencyContacts.get(i).getContactNumber());
        }
        sosRequest.setEmergencyContacts(emergencyContactNumbers);
        sosRequest.setLocation(getLocation());
        return sosRequest;

    }

    private ArrayList<Double> getLocation() {
        ArrayList<Double> coordinates = new ArrayList<>();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return coordinates;
        }
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        double lat = lastLocation.getLongitude();
        double lng = lastLocation.getLatitude();
        coordinates.add(lat);
        coordinates.add(lng);
        return coordinates;

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "onConnected: ");
        sendSOS("emergencyContact");
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "onConnectionFailed: ");
    }

    @Override
    protected void onStart() {
        super.onStart();

        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        googleApiClient.disconnect();
    }

    @OnClick(R.id.activity_sos_addEmergencyContact_LL)
    public void goToAddEmergencyContactScreen() {
        startActivity(new Intent(this, EmergencyContactActivity.class));
    }

    @OnClick(R.id.activity_sos_callFireBrigade_LL)
    public void callFireBrigade() {
        Log.i(TAG, "callFireBrigade: ");
        sendSOS("fireBrigade");
        SOSActivityPermissionsDispatcher.openDialerWithCheck(this, Uri.parse("tel:101"));
    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    public void openDialer(Uri uri) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(uri);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    @OnClick(R.id.activity_sos_callPCRVan_LL)
    public void callPCRVan() {
        Log.i(TAG, "callPCRVan: ");
        sendSOS("pcrVan");
        SOSActivityPermissionsDispatcher.openDialerWithCheck(this, Uri.parse("tel:100"));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        SOSActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.CALL_PHONE)
    void showRationaleForPhoneCall(PermissionRequest request) {
        new AlertDialog.Builder(this)
                .setMessage("App needs to make phone call")
                .setPositiveButton("Allow", (dialog, button) -> request.proceed())
                .setNegativeButton("Deny", (dialog, button) -> request.cancel())
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(Manifest.permission.CALL_PHONE)
    void showDeniedForPhoneCall() {
        Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(Manifest.permission.CALL_PHONE)
    void showNeverAskForPhoneCall() {
        Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
    }


}

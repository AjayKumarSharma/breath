package com.breathandbeats.android.ui.activities;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.ChatAdapter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.Constants;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.RV.MessageItem;
import com.breathandbeats.android.pojos.events.ChatListUpdateEvent;
import com.breathandbeats.android.pojos.orm.MessageORM;
import com.breathandbeats.android.pojos.socket.AuthEvent;
import com.breathandbeats.android.pojos.socket.ChatMessage;
import com.breathandbeats.android.pojos.socket.GeneralChatMessage;
import com.breathandbeats.android.pojos.socket.MessageData;
import com.breathandbeats.android.pojos.socket.OnMessageSuccessEvent;
import com.breathandbeats.android.utils.Mappers;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatActivity extends AppCompatActivity {
    private static final String TAG = "ChatActivity";

    @BindView(R.id.activity_chat_RV)
    RecyclerView chatRV;
    @BindView(R.id.activity_chat_message_ET)
    EditText chatMessageET;
    @BindView(R.id.activity_chat_sendBtn_IV)
    ImageView chatSendBtnIV;
    @BindView(R.id.chat_toolbar)
    Toolbar toolbar;


    String textMessage;
    Socket mSocket;
    BnBApp app;
    ChatAdapter chatAdapter;
    DBHelper dbHelper;
    SharedPrefHelper sharedPrefHelper;
    private EventBus bus = EventBus.getDefault();

    ArrayList<MessageItem> messageItems = new ArrayList<>();
    List<MessageORM> messages;
    private String authToken;
    Gson gson;

    private static final String EMIT_EVENT_AUTHENTICATION = "authentication";
    private static final String EVENT_AUTHENTICATED = "authenticated";
    private static final String EVENT_UNAUTHORIZED = "unauthorized";
    private static final String EVENT_MSG_SUCCESS = "success";
    private static final String EVENT_MSG_ERROR = "error";
    private static final String EMIT_EVENT_MESSAGE = "message";
    private String requestId;
    private String messageId;
    private String chatId;
    private String createdAt;
    private String sendByName;
    private String sendById;
    private String messageDataText;
    private String otherUserName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        otherUserName = extras.getString("name");
        Toolbar myToolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(otherUserName);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);
        app = (BnBApp) getApplication();

        mSocket = app.getSocket();
        dbHelper = app.getDBHelper();
        sharedPrefHelper = app.getSharedPrefHelper();
//        this.chatAdapter = new ChatAdapter(this, getMessageItems());
//        chatRV.setAdapter(this.chatAdapter);
//        chatRV.setLayoutManager(new LinearLayoutManager(this));
        authToken = sharedPrefHelper.getAuthToken();
        try {

            mSocket = IO.socket(Constants.BASE_URL_STRING);
            BnBApp.getInstance().setSocket(mSocket);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }

        requestId = extras.getString("requestId");
        messageId = extras.getString("messageId");
        chatId = extras.getString("chatId");
        createdAt = extras.getString("createdAt");
        sendByName = extras.getString("sendByName");
        otherUserName = extras.getString("name");
        sendById = extras.getString("sendById");
        messageDataText = extras.getString("messageDataText");


        gson = new Gson();

        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(EVENT_AUTHENTICATED, onAuthenticated);
        mSocket.on(EVENT_UNAUTHORIZED, onUnauthorized);
        mSocket.on(EVENT_MSG_SUCCESS, onMsgSuccess);
        mSocket.on(EVENT_MSG_ERROR, onMsgError);
        mSocket.connect();
        sharedPrefHelper.setCurrentChatId(chatId);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setTitle(otherUserName);
        String currentChatId = sharedPrefHelper.getCurrentChatId();

    }

    private ArrayList<MessageItem> getMessageItems() {
        messages = dbHelper.getMessageItems(chatId);
        messageItems = Mappers.getMessageItems(messages);
        return messageItems;
    }

    @OnClick(R.id.activity_chat_sendBtn_IV)
    public void onClick() {
        if (isNetworkConnected()) {
            textMessage = chatMessageET.getText().toString();
            if (!textMessage.isEmpty()) {
                chatMessageET.setText("");
                Date date = new Date();
                updateRV(date);
                sendMessage(textMessage, chatId);
                // TODO: 19/05/17 update message id
                updateDB(date);
            }
        } else {
            Toast.makeText(this, "You are not connected to the internet", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    private void updateRV(Date date) {
        if (mSocket.connected()) {
            MessageItem messageItem = new MessageItem(textMessage, true, date);
            messageItems.add(messageItem);
            chatAdapter.notifyDataSetChanged();
        }
    }

    private void sendMessage(String textMessage, String chatId) {
        MessageData messageData = new MessageData(textMessage);
        GeneralChatMessage generalChatMessage = new GeneralChatMessage();
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setChatId(chatId);
        chatMessage.setMessageData(new MessageData(textMessage));
        generalChatMessage.setChatMessage(chatMessage);
        mSocket.emit(EMIT_EVENT_MESSAGE, gson.toJson(generalChatMessage));

    }

    private void updateDB(Date date) {
        dbHelper.createMessage(textMessage, chatId, date);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMessages(chatId);
    }

    private void setUpMessages(String chatId) {
        List<MessageORM> messageItems = getMessages(chatId);
        populateRV(messageItems);

    }

    private void populateRV(List<MessageORM> messages) {
        messageItems.clear();
        for (int i = 0; i < messages.size(); i++) {
            MessageORM messageORM = messages.get(i);
            String textMessage = messageORM.getMessage();
            boolean createdBySelf = messageORM.isCreatedBySelf();
            Date createdAt = messageORM.getCreatedAt();
            MessageItem messageItem = new MessageItem(textMessage, createdBySelf, createdAt);
            messageItems.add(messageItem);
        }
        chatAdapter = new ChatAdapter(this, messageItems);
        chatRV.setAdapter(chatAdapter);
        chatRV.setLayoutManager(new LinearLayoutManager(this));
        chatAdapter.notifyDataSetChanged();
    }

    private List<MessageORM> getMessages(String chatId) {
        return dbHelper.getMessageItems(chatId);
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            AuthEvent event = new AuthEvent();
            event.setToken(authToken);
            Gson gson = new Gson();
            Log.v("Token json", gson.toJson(event));
            mSocket.emit(EMIT_EVENT_AUTHENTICATION, gson.toJson(event));
        }
    };

    private Emitter.Listener onAuthenticated = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.v("App", "Authenticated");
            if (args.length > 0) {
//                bus.post(new OnAuthEvent(args[0]));
            }
        }
    };

    private Emitter.Listener onMsgSuccess = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (args.length > 0) {
                JSONObject data = (JSONObject) args[0];
                bus.post(new OnMessageSuccessEvent(args[0]));
            }
        }
    };

    private Emitter.Listener onMsgError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (args.length > 0) {
//                bus.post(new OnMessageErrorEvent(args[0]));
            }
        }
    };

    private Emitter.Listener onUnauthorized = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (args.length > 0) {
//                bus.post(new OnUnAuthEvent(args[0]));
            }
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.v("App", "Disconnected");
            if (args.length > 0) {
//                bus.post(new OnDisconnectEvent(args[0]));
            }
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (args.length > 0) {
//                bus.post(new OnConnectErrorEvent(args[0]));
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        bus.register(this);
    }

    @Override
    protected void onStop() {
        bus.unregister(this);
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sharedPrefHelper.setCurrentChatId("");
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateChatRV(ChatListUpdateEvent event) {
        setUpMessages(chatId);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}




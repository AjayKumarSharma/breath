package com.breathandbeats.android.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.HistoryTripListPojo.HistoryTripData;
import com.breathandbeats.android.pojos.HistoryTripListPojo.Rejectedtripdata;
import com.breathandbeats.android.ui.Adapter.RejectedTripAdapter;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lavanya on 14-12-2017.
 */
public class RejectedTripFragment extends Fragment implements Serializable {

    Context context;
    private  String TAG="RejectedTripFragment";
    @BindView(R.id.rvCompletedTrip)
    RecyclerView rvCompletedTrip;
    Gson gson;

    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    String authToken;
    public static transient List<HistoryTripData> historyTripData;
    Rejectedtripdata rejectedtripdata =new Rejectedtripdata();

    public  RejectedTripFragment(){
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.completedtrip_layout, container, false);
        context = getActivity();
        ButterKnife.bind((Activity) context);

        application = (BnBApp) getActivity().getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Call
        sharedPrefHelper = application.getSharedPrefHelper();

        rvCompletedTrip=(RecyclerView)rootView.findViewById(R.id.rvCompletedTrip);

        rvCompletedTrip.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvCompletedTrip.setLayoutManager(layoutManager);

        sharedPrefHelper = application.getSharedPrefHelper();
        authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();
        Log.d(TAG,"authtoken : "+authToken);

        if(NetworkUtil.getConnectivityStatusBoolen(context)){
            BnBUserUtility.showProgress(context,"Please wait, getting data...");
            getTripCompletedData(authToken);
        }else{
            Toast.makeText(context, "Please note that you are not connected to internet.", Toast.LENGTH_SHORT).show();
        }



        return rootView;
    }

    private void getTripCompletedData(String authToken) {

        if(NetworkUtil.getConnectivityStatusBoolen(context)){

            Call<Rejectedtripdata> completedTripListDataCall=bnBService.RejectedTripListData(authToken);

            completedTripListDataCall.enqueue(new Callback<Rejectedtripdata>() {
                @Override
                public void onResponse(Call<Rejectedtripdata> call, Response<Rejectedtripdata> response) {

                    if(response!=null){
                        BnBUserUtility.dismissProgressDialog();
                        Log.d(TAG,"responce  : "+response);
                        if(response.isSuccessful()){
                            Log.d(TAG,"responce success : ");
                            rejectedtripdata =response.body();

                            /*JSONObject jsonObject= null;
                            try {
                                jsonObject = new JSONObject(String.valueOf(new Gson().toJson(response)));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d(TAG,"jsonObject : "+jsonObject);*/

                            if(rejectedtripdata.getSuccess().equals(true)){
                                Log.d(TAG,"responce true : ");
                                if(rejectedtripdata.getData()!=null){

                                    if(rejectedtripdata.getData().size()>0) {
                                        RejectedTripAdapter RejectedTripAdapter = new RejectedTripAdapter(context, rejectedtripdata);
                                        rvCompletedTrip.setAdapter(RejectedTripAdapter);
                                    }
                                }
                            }else if(rejectedtripdata.getSuccess().equals(false)){
                                Log.d(TAG,"responce false : ");
                            }

                        }else{
                            Log.d(TAG,"responce not  success : ");
                        }

                    }else{
                        Log.d(TAG,"responce null : ");
                        BnBUserUtility.dismissProgressDialog();
                        Toast.makeText(context, "Sorry,something went wrong,we can't reach our " +
                                "service at the moment.Please try again.", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<Rejectedtripdata> call, Throwable t) {
                    BnBUserUtility.dismissProgressDialog();
                    Log.d(TAG,"failure : "+t);
                    Toast.makeText(context, "Sorry,something went wrong,we can't reach our " +
                            "service at the moment.Please try again.", Toast.LENGTH_SHORT).show();
                }
            });


        }else{
            Toast.makeText(context, "Please note that you are not connected to internet.", Toast.LENGTH_SHORT).show();
        }

    }
}

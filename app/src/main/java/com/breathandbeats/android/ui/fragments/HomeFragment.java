package com.breathandbeats.android.ui.fragments;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceLater.BookLaterPaymentActivity;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow.DriverSearchingActivity;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow.DriverUserMapViewActivity;
import com.breathandbeats.android.ui.activities.BookAmbulance.CallAmbulanceActivity;
import com.breathandbeats.android.ui.activities.HealthDirectory.HealthDirectoryActivity;
import com.breathandbeats.android.ui.activities.MakeANoiseActivity;
import com.breathandbeats.android.ui.activities.SOSActivity;
import com.breathandbeats.android.ui.activities.SaveaLife.SaveALifeActivity;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * A simple {@link Fragment} subclass.
 */
@RuntimePermissions
public class HomeFragment extends Fragment {
    private  String TAG = "Home_Fragment";


    @BindView(R.id.fragment_home_callAmbulance_LL)
    LinearLayout fragmentHomeCallAmbulanceLL;
    @BindView(R.id.fragment_home_makeANoise_LL)
    LinearLayout fragmentHomeMakeANoiseLL;
    @BindView(R.id.fragment_home_healthDir_LL)
    LinearLayout fragmentHomeHealthDirLL;
    @BindView(R.id.fragment_home_saveALife_LL)
    LinearLayout fragmentHomeSaveALifeLL;
    @BindView(R.id.llCall_Emergency)
    LinearLayout fragmentHomeSOSBtn;

    @BindView(R.id.tvname)
    TextView tvname;
    @BindView(R.id.tvnote)
    TextView tvnote;

    @BindView(R.id.llCall_108)
    LinearLayout llCall_108;

    private BnBApp application;
    private BnBService apiService;
    private SharedPrefHelper sharedPrefHelper;
    private String authToken;

    Handler Handler_time = new Handler();
    int delay = 15000; //15 seconds
    Runnable runnable;
    Dialog dialogTripAlert;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (BnBApp) getActivity().getApplication();
        apiService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, v);

        getTheTripVerificationFunction(); // checking trip status,

        return v;
    }

    private void getTheTripVerificationFunction() {

        if(SharedPreferenceBnBUtility.getNotificationRecived(getContext()).equals("true")){
            //trip notification for ride .

            if(SharedPreferenceBnBUtility.getTripPaymentRecived(getContext()).equals("true")){//payment confirmation
                //do nothing
            }else if(SharedPreferenceBnBUtility.getTripPaymentRecived(getContext()).equals("false")){

                getVerification();

            }else{
                getVerification();
            }

        }else if(SharedPreferenceBnBUtility.getNotificationRecived(getContext()).equals("false")){ // no trip assigned to user.
            //do nothing
            if(SharedPreferenceBnBUtility.getAdvacneBookedReq(getContext()).equals("true")){

                getAdvanceDialogBox();

            }else if(SharedPreferenceBnBUtility.getAdvacneBookedReq(getContext()).equals("false")){

            }else{

            }
        }else{
            // do nothing
        }

        tvname.setText(SharedPreferenceBnBUtility.getUserName(getContext()));
//        tvnote.setText(getTimeMessage());

        if(SharedPreferenceBnBUtility.getNotificationRecived(getContext()).equals("true")){
            //trip notification for ride .

         // do nothing
        }else if(SharedPreferenceBnBUtility.getNotificationRecived(getContext()).equals("false")){

                if(SharedPreferenceBnBUtility.getrequestedforhospital(getContext()).equals("true")){ // to check user requested for ambulance but not get confirmation

                    Intent loadrclass = new Intent(getContext(), DriverSearchingActivity.class);
                    loadrclass.putExtra("Latitude", String.valueOf(SharedPreferenceBnBUtility.getNotificationPatientLatitude(getContext())));
                    loadrclass.putExtra("Longitude", String.valueOf(SharedPreferenceBnBUtility.getNotificationPatientLongitude(getContext())));
                    startActivity(loadrclass);

                }else if(SharedPreferenceBnBUtility.getrequestedforhospital(getContext()).equals("false")){
                    //do nothing
                }

            }else{

            if(SharedPreferenceBnBUtility.getrequestedforhospital(getContext()).equals("true")){ // to check user requested for ambulance but not get confirmation

                Intent loadrclass = new Intent(getContext(), DriverSearchingActivity.class);
                loadrclass.putExtra("Latitude", String.valueOf(SharedPreferenceBnBUtility.getNotificationPatientLatitude(getContext())));
                loadrclass.putExtra("Longitude", String.valueOf(SharedPreferenceBnBUtility.getNotificationPatientLongitude(getContext())));
                startActivity(loadrclass);

            }else if(SharedPreferenceBnBUtility.getrequestedforhospital(getContext()).equals("false")){
                //do nothing
            }
        }

    }

    private void getVerification() {

        if(SharedPreferenceBnBUtility.getNotificationTripCompleted(getContext()).equals("true")){

            BnBUserUtility.Clear_notification(getContext());

            Intent driverUserIntent=new Intent(getContext(),DriverUserMapViewActivity.class);
            startActivity(driverUserIntent);

        }else if(SharedPreferenceBnBUtility.getNotificationTripCompleted(getContext()).equals("false")){
            getVerifiactionDialogBox();
        }else{
            getVerifiactionDialogBox();
        }
    }


    private String  getTimeMessage() { // to get time status

        SimpleDateFormat sdf = new SimpleDateFormat("kk");
        String str = sdf.format(new Date());
        int timeOfDay=Integer.parseInt(str);
        String ReturnValue="";

        if(timeOfDay >= 0 && timeOfDay < 12){
            ReturnValue="Good Morning,  ";
        }else if(timeOfDay >= 12 && timeOfDay < 16){
            ReturnValue="Good Afternoon,  ";
        }else if(timeOfDay >= 16 && timeOfDay < 21){
            ReturnValue="Good Evening,  ";
        }else if(timeOfDay >= 21 && timeOfDay < 24){
            ReturnValue="Good Night,  ";
        }
        return ReturnValue;
    }

    @Override
    public void onPause() {
        super.onPause();
        Handler_time.removeCallbacks(runnable); //stop handler when activity not visible
    }

    @OnClick(R.id.fragment_home_callAmbulance_LL)
    public void OnCallAmbulanceClick() {
        startActivity(new Intent(getActivity(), CallAmbulanceActivity.class));
    }

    @OnClick(R.id.fragment_home_makeANoise_LL)
    public void OnMakeANoiseClick() {
        startActivity(new Intent(getActivity(), MakeANoiseActivity.class));
    }

    @OnClick(R.id.fragment_home_healthDir_LL)
    public void OnhealthDirClick() {
        startActivity(new Intent(getActivity(), HealthDirectoryActivity.class));
    }

    @OnClick(R.id.fragment_home_saveALife_LL)
    public void OnsaveALifeClick() {
        startActivity(new Intent(getActivity(), SaveALifeActivity.class));
    }

    @OnClick(R.id.llCall_Emergency)
    public void OnSOSClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure to send the SOS ?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }

    DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                goToSOSActivity();
                break;

            case DialogInterface.BUTTON_NEGATIVE:
                break;
        }
    };

    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    public void goToSOSActivity() {
        boolean locationEnabled = isLocationEnabled();
        if (locationEnabled) {
//            progress.show();
            startActivity(new Intent(getActivity(), SOSActivity.class));
        } else {
            showLocationDialog();
        }

    }
  @OnClick(R.id.llCall_108)
  public void onclick_Dial108_LL() {

      getAlertBoxforDial();

   }

    private void getAlertBoxforDial() {

        new android.app.AlertDialog.Builder(getContext()).setMessage("Are you sure,You want to dial ?").setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {
                String Number = "100";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + Number));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {

            }
        }).show();

    }

    private boolean isLocationEnabled() {
        LocationManager locateManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        boolean locationEnabled = locateManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return locationEnabled;
    }

    private void showLocationDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder
                .setMessage("GPS is disabled in your device. Enable it?")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        (dialog, id) -> {
                            Intent callGPSSettingIntent = new Intent(
                                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(callGPSSettingIntent);
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                (dialog, id) -> dialog.cancel());
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        HomeFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForGetLocation(PermissionRequest request) {
        Log.i(TAG, "showRationaleForGetLocation: ");
        new AlertDialog.Builder(getActivity())
                .setMessage("App needs location to send SOS")
                .setPositiveButton("Allow", (dialog, button) -> request.proceed())
                .setNegativeButton("Deny", (dialog, button) -> request.cancel())
                .setCancelable(false)
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForGetLocation() {
        Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "showDeniedForGetLocation: permission denied");
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    void showNeverAskForGetLocation() {
        Log.i(TAG, "showNeverAskForGetLocation:");
        Toast.makeText(getActivity(), "Permission denied", Toast.LENGTH_SHORT).show();
    }

    private void getVerifiactionDialogBox() {

        /*dialogTripAlert = new Dialog(getContext());
        dialogTripAlert.setContentView(R.layout.dialog_booklater);
        dialogTripAlert.show();

        dialogTripAlert.setCancelable(false);

        Button Done_Btn = (Button) dialogTripAlert.findViewById(R.id.Done_Btn);
        TextView tvTripMessage = (TextView) dialogTripAlert.findViewById(R.id.tvTripMessage);

        tvTripMessage.setText("You booked an ambulance, Please click on OK to get details.");

        Done_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogTripAlert.dismiss();

                BnBUserUtility.Clear_notification(getContext());

                Intent driverUserIntent=new Intent(getContext(),DriverUserMapViewActivity.class);
                startActivity(driverUserIntent);
            }
        });*/

        BnBUserUtility.toastMessage_success("Booking has been done \n re-directing you to the details page.",getContext());
        BnBUserUtility.Clear_notification(getContext());

        Intent driverUserIntent=new Intent(getContext(),DriverUserMapViewActivity.class);
        startActivity(driverUserIntent);
    }

    private void getAdvanceDialogBox() {

        dialogTripAlert = new Dialog(getContext());
        dialogTripAlert.setContentView(R.layout.dialog_tripalert);
        dialogTripAlert.show();

        dialogTripAlert.setCancelable(false);

        Button Done_Btn = (Button) dialogTripAlert.findViewById(R.id.Done_Btn);
        TextView tvTripMessage = (TextView) dialogTripAlert.findViewById(R.id.tvTripMessage);

        tvTripMessage.setText("Ambulance successfully assigned to you.make advance booking charges.");

        Done_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTripAlert.dismiss();

                BnBUserUtility.Clear_notification(getContext());
                Intent driverUserIntent=new Intent(getContext(),BookLaterPaymentActivity.class);
                startActivity(driverUserIntent);
            }
        });
    }

}

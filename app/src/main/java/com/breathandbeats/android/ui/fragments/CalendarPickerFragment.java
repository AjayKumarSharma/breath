package com.breathandbeats.android.ui.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.breathandbeats.android.R;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by asish on 4/24/16.
 */
public class CalendarPickerFragment extends DialogFragment
{

    private DatePicker datePicker;

    public interface DateDialogListener {
        void onFinishDialog(Date date);
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){

        View v = LayoutInflater.from(getActivity())
                .inflate(R.layout.calendar_dialog,null);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, 0);
        Date result = cal.getTime();

        Calendar calmaxdate = Calendar.getInstance();
        calmaxdate.add(Calendar.DAY_OF_MONTH, +2);
        Date resultdate = calmaxdate.getTime();


        datePicker = (DatePicker) v.findViewById(R.id.dialog_date_date_picker);
        datePicker.setMinDate(result.getTime());
        datePicker.setMaxDate(resultdate.getTime());


        return new android.support.v7.app.AlertDialog.Builder(getActivity())
                .setView(v)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                int year = datePicker.getYear();
                                int mon = datePicker.getMonth();
                                int day = datePicker.getDayOfMonth();
                                Date date = new GregorianCalendar(year,mon,day).getTime();
                                DateDialogListener activity = (DateDialogListener) getActivity();
                                activity.onFinishDialog(date);
                                dismiss();
                            }
                        })
                .create();
    }
}


package com.breathandbeats.android.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.RepliesAdapter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.pojos.RV.RepliesItem;
import com.breathandbeats.android.pojos.orm.Chat;
import com.breathandbeats.android.utils.Mappers;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class RepliesActivity extends AppCompatActivity {
    private static final String TAG = "RepliesActivity";

    @BindView(R.id.activity_replies_RV)
    RecyclerView repliesRV;
    @BindView(R.id.replies_toolbar)
    Toolbar toolbar;
    RepliesAdapter repliesAdapter;

    DBHelper dbHelper;
    BnBApp app;
    String requestId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_replies);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        app = (BnBApp) getApplication();
        dbHelper = app.getDBHelper();
        requestId = getIntent().getExtras().getString("requestId");
        Log.i(TAG, "onCreate: request id" + requestId);

        repliesAdapter = new RepliesAdapter(this, getReplies(requestId));
        Log.i(TAG," replay size : "+getReplies(requestId).size());
        repliesRV.setAdapter(repliesAdapter);
        repliesRV.setLayoutManager(new LinearLayoutManager(this));
    }

    private ArrayList<RepliesItem> getReplies(String requestId) {
        List<Chat> replies = dbHelper.getReplies(requestId);
        Log.i(TAG, "getReplies: " + replies.size());
        return Mappers.getRepliesItems(replies);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

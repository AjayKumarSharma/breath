package com.breathandbeats.android.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.pojos.events.DeleteContactEvent;
import com.breathandbeats.android.ui.activities.EmergencyContact.EmergencyContactListActivity;
import com.breathandbeats.android.utils.Validators;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateEmergencyContactActivity extends AppCompatActivity {
    String oldName;
    String oldEmail;
    String oldRelationship;
    String oldNumber;
    @BindView(R.id.activity_update_emergency_contact_name_ET)
    EditText contactNameET;
    @BindView(R.id.activity_update_emergency_contact_relationship_ET)
    EditText contactRelationshipET;
    @BindView(R.id.activity_update_emergency_contact_number_ET)
    EditText contactNumberET;
    @BindView(R.id.activity_update_emergency_contact_email_ET)
    EditText contactEmailET;
    @BindView(R.id.activity_update_emergency_contact_addContact_Btn)
    Button contactAddContactBtn;
    @BindView(R.id.activity_emergency_contact)
    LinearLayout activityEmergencyContact;

    BnBApp bnBApp;
    DBHelper dbHelper;
    EventBus bus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_emergency_contact);
        ButterKnife.bind(this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_update_emergency_contact_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Update Emergency Contact");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);
        Bundle extras = getIntent().getExtras();
        oldName = extras.getString("name");
        oldEmail = extras.getString("email");
        oldNumber = extras.getString("number");
        oldRelationship = extras.getString("relationship");
        contactEmailET.setText(oldEmail);
        contactRelationshipET.setText(oldRelationship);
        contactNameET.setText(oldName);
        contactNumberET.setText(oldNumber);
        bnBApp = (BnBApp) getApplication();
        dbHelper = bnBApp.getDBHelper();
        bus = EventBus.getDefault();
    }

    @OnClick(R.id.activity_update_emergency_contact_addContact_Btn)
    public void updateContact() {
        String name = contactNameET.getText().toString();
        String number = contactNumberET.getText().toString();
        String relationship = contactRelationshipET.getText().toString();
        String email = contactEmailET.getText().toString();
        boolean areCredentialValid = validateCredentials(name, number, relationship, email);
        if (areCredentialValid) {
            dbHelper.updateEmergencyContact(name, relationship, number, email, oldNumber);
            bus.post(new DeleteContactEvent());
//            finish();
            Intent intent=new Intent(UpdateEmergencyContactActivity.this,EmergencyContactListActivity.class);
            startActivity(intent);
        }
    }

    private boolean validateCredentials(String name, String number, String relationship, String email) {
        if (name.isEmpty()) {
            contactNameET.setError(getResources().getString(R.string.ETEmptyError));
            return false;
        }
        if (!Validators.isMobileValid(number)) {
            contactNumberET.setError(getResources().getString(R.string.phoneNumberError));
            return false;
        }
        if (relationship.isEmpty()) {
            contactRelationshipET.setError(getResources().getString(R.string.ETEmptyError));
            return false;
        }
        if (!Validators.isEmailValid(email)) {
            contactEmailET.setError(getResources().getString(R.string.emailError));
            return false;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}

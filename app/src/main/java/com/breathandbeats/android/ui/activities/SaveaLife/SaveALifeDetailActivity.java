package com.breathandbeats.android.ui.activities.SaveaLife;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.Constants;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SaveALifeDetailActivity extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener {
    private static final String TAG = "SaveALifeDetailActivity";

    @BindView(R.id.activity_save_a_life_detail_title_TV)
    TextView titleTV;
    @BindView(R.id.activity_save_a_life_detail_ytp)
    YouTubePlayerView ytp;
    @BindView(R.id.activity_save_alife_detail_favBtn)
    Button favBtn;
    @BindView(R.id.activity_save_alife_detail_desc_TV)
    TextView descTV;
    @BindView(R.id.activity_save_alife_detail)
    LinearLayout activitySaveAlifeDetail;
    private String title;
    String description;
    String videoId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_a_life_detail);
        ButterKnife.bind(this);
        Log.i(TAG, "onCreate: ");
        Bundle extras = getIntent().getExtras();
        title = extras.getString("title");
        Log.i(TAG, "onCreate: " + title);
        description = extras.getString("description");
        videoId = extras.getString("videoId");
        titleTV.setText(title);
        descTV.setText(description);
        ytp.initialize(Constants.DEV_KEY, this);

    }

    @OnClick(R.id.activity_save_alife_detail_favBtn)
    public void onClick() {
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        Log.i(TAG, "onInitializationSuccess: ");
        youTubePlayer.loadVideo(videoId);
        youTubePlayer.play();
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

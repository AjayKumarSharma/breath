package com.breathandbeats.android.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.HistoryTripListPojo.CompletedTripListData;
import com.breathandbeats.android.pojos.HistoryTripListPojo.HistoryTripData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rakesh on 02-11-2017.
 */

public class TripFragment extends Fragment {


    Context context;
    String TAG="TripFragment";
    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    String authToken;
    CompletedTripListData completedTripListData_v=new CompletedTripListData();

    private List<HistoryTripData> historyTripData;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        context = getActivity();

        application = (BnBApp) getActivity().getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Call


      /*  Bundle bundle = this.getArguments();
        if(bundle.containsKey("TripData")) {

            historyTripData = (List<HistoryTripData>) bundle.getSerializable("TripData");
//            SharedPreferenceBnBUtility.setTriphistory(context,String.valueOf(historyTripData));

            Log.d(TAG," data : "+historyTripData);
        }*/
        sharedPrefHelper = application.getSharedPrefHelper();
        authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();
        Log.d(TAG,"authtoken : "+authToken);


        // Setting ViewPager for each Tabs
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        // Set Tabs inside Toolbar
        TabLayout tabs = (TabLayout) view.findViewById(R.id.result_tabs);
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        tabs.setupWithViewPager(viewPager);
//        tabs.getTabAt(0).select();
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {

        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new RejectedTripFragment(), "Rejected ");
        adapter.addFragment(new CompletedTripFragment(), "Completed ");
       adapter.addFragment(new AdvanceBookFragment(), "Advance ");
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

package com.breathandbeats.android.ui.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.breathandbeats.android.Models.DiscussionModel;
import com.breathandbeats.android.Models.DiscussionModelGegerSetter;
import com.breathandbeats.android.Models.DoctorSearchModel;
import com.breathandbeats.android.Models.SearchDataModel;
import com.breathandbeats.android.NewUtils.AppController;
import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.RV.DoctorSearchCardAdpter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.ui.activities.ManageConsult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.MODE_PRIVATE;
import static com.breathandbeats.android.NewUtils.Const.ServiceType.SearchDoctorInfor;

/**
 * Created by Rocky on 3/3/2018.
 */

public class FragmentSearchDoctor extends Fragment{
    RecyclerView mRecyclerView;
    private static ArrayList<DoctorSearchModel> arraylist;
    Activity mActivity;
    private RecyclerView recyclerView;
    private static RecyclerView.Adapter activityAdapter;
    BnBApp application;
    BnBService apiSerivice;
    SharedPrefHelper sharedPrefHelper;
    BnBService bnBService;
    static String authToken;
    TabHost host;
    private static ArrayList<String> array;
    RecyclerView rv;
    LinearLayoutManager linearLayoutManager;
    RecyclerView.Adapter adapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private final int count = 7;
    EditText EditLocation;
    Button BtnSearchDoc;
    ProgressDialog progressDialog;
    static final String REQ_TAG = "VACTIVITY";
    RequestQueue requestQueue;
    String Location;
    ArrayList<SearchDataModel> keyconsult;
    public static final String POST_REQUEST_TAG = "JSON_OBJECT_POST_REQUEST_TAG";


    public FragmentSearchDoctor() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();

        arraylist = new ArrayList<DoctorSearchModel>();

        for (int i = 0; i < DiscussionModel.ids.length; i++) {
            arraylist.add(new DoctorSearchModel(
                    DiscussionModel.ids[i],
                    DiscussionModel.SearchDoctors[i].toString(),
                    false

            ));
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);

        final View view = inflater.inflate(R.layout.fragment_searchdoctor, container, false);
        ButterKnife.bind(this, view);

        mActivity = getActivity();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerSearchDoctor);
        EditLocation=(EditText) view.findViewById(R.id.EditLocation);
        BtnSearchDoc=(Button) view.findViewById(R.id.btnSearchDoc);
        DoctorSearchCardAdpter adapter = new DoctorSearchCardAdpter(mActivity, arraylist);
        mRecyclerView.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, OrientationHelper.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        BtnSearchDoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                keyconsult = new ArrayList<SearchDataModel>();
                JSONArray list = new JSONArray();
                String ConcatString="";
                for(int i=0; i < arraylist.size();i++){

                    if(arraylist.get(i).isActive()==true){
                        //arraylist.get(i).getTxtSpeciality().toString()
                        keyconsult.add(new SearchDataModel(
                                arraylist.get(i).getTxtSpeciality().toString()

                        ));
                        list.put(arraylist.get(i).getTxtSpeciality().toString());
                        ConcatString=ConcatString+","+arraylist.get(i).getTxtSpeciality().toString();
                    }

                }

                SharedPreferences.Editor editor = mActivity.getSharedPreferences("PrefDoctorSearchKey", MODE_PRIVATE).edit();
                editor.putString("specialities", ConcatString.substring(1));
                editor.putString("location", EditLocation.getText().toString().trim());
                editor.commit();

                String array[]=new String[50];

                for(int i=0;i< keyconsult.size();i++){
                    array[i]=keyconsult.get(i).getMsginfo().toString();
                }

                //getdata();
                //GetSearchDoctorData(EditLocation.getText().toString().trim(), ConcatString);
                Intent intent=new Intent(mActivity,ManageConsult.class);
                //intent.putExtra("position","1");
                //Clear all activities and start new task
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

        return view;

    }

    public void GetSearchDoctorData(String location,String keyarray){
        JSONObject json = new JSONObject();
        try {


            json.put("specialities", keyarray);
            json.put("location", location);

            //String url = getResources().getString(R.string.json_get_url);
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, SearchDoctorInfor, json,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try{
                                String message=response.get("message").toString();
                                if(response.get("success").toString().trim()=="true"){

                                   /* Toast.makeText(getApplicationContext(), "SignUp successfully..", Toast.LENGTH_SHORT).show();
                                    Intent i = new Intent(SignUpActivity.this, SignInActivity.class);
                                    startActivity(i);*/
                                }else{

                                    Toast.makeText(mActivity, message, Toast.LENGTH_SHORT).show();

                                }
                            }catch (Exception ex){
                                ex.printStackTrace();
                            }


                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    try {
                        String errormsg = error.toString();
                        Log.d("Error", errormsg);
                        if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }

                }
            });
            jsonObjectRequest.setTag(REQ_TAG);
            requestQueue.add(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void getdata(){
        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i <= 0; i++) {
            jsonArray.put("Ortho");
        }
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, SearchDoctorInfor,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
//                      VolleyLog.d(TAG, "Error: " + error.getMessage());
                    }
                }) {

            // You can send parameters as header with POST request....
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("specialities", jsonArray.toString());
                headers.put("location", "");
                return headers;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq, POST_REQUEST_TAG);

    }
}
package com.breathandbeats.android.ui.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.api.MakeANoiseRequest;
import com.breathandbeats.android.pojos.api.MakeANoiseResponse;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@RuntimePermissions
public class MakeANoiseActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final String TAG = "MakeANoise_Activity";

    /*@BindView(R.id.activity_make_anoise_subject_ET)
    EditText activityMakeAnoiseSubjectET;*/

    @BindView(R.id.spMakeaNoiseReason)
    Spinner activityMakeAnoiseReasonSP;
    @BindView(R.id.btnMakeANoise)
    Button btnMakeANoise;

    @BindView(R.id.onekm)
    RelativeLayout onekm;
    @BindView(R.id.threekm)
    RelativeLayout threekm;
    @BindView(R.id.fivekm)
    RelativeLayout fivekm;
    @BindView(R.id.tenkm)
    RelativeLayout tenkm;
    @BindView(R.id.twentykm)
    RelativeLayout twentykm;

    @BindView(R.id.tvonekm)
    TextView tvonekm;
    @BindView(R.id.tvthreekm)
    TextView tvthreekm;
    @BindView(R.id.tvfivekm)
    TextView tvfivekm;
    @BindView(R.id.tvtenkm)
    TextView tvtenkm;
    @BindView(R.id.tvtwentykm)
    TextView tvtwentykm;

    @BindView(R.id.rlBackarrow)
     RelativeLayout rlBackarrow;

    /*@BindView(R.id.activity_make_anoise)
    LinearLayout activityMakeAnoise;*/
  /*  @BindView(R.id.activity_make_anoise_distanceRange_SB)
    DiscreteSeekBar activityMakeAnoiseDistanceRangeSB;*/
   /* @BindView(R.id.activity_make_anoise_message_ET)
    EditText activityMakeAnoiseMessageET;*/

    GoogleApiClient googleApiClient;
    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    DBHelper dbHelper;
    ProgressDialog progress;


    private String subject;
    private Location lastLocation;
    private String reason;
    private String message;
    private int distance=0;
    private ArrayList<Double> location = new ArrayList<>();
    String[] reasonList = { "Blood Donor Required A+",
    "Blood Donor Required A-","Blood Donor Required B+ ",
    "Blood Donor Required B- ","Blood Donor Required AB+ ",
    "Blood Donor Required AB- ","Blood Donor Required O+ ",
    "Blood Donor Required O- ","Blood Donor Required (Any Blood Group) "};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makeanoise);
        ButterKnife.bind(this);
//        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_make_a_noise_toolbar);
      /*  setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Make a Noise");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);*/
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        application = (BnBApp) getApplication();
        bnBService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();

        dbHelper = application.getDBHelper();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, reasonList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityMakeAnoiseReasonSP.setAdapter(dataAdapter);
        activityMakeAnoiseReasonSP.setPrompt("Title");
        btnMakeANoise.setOnClickListener(view -> OnMakeANoiseBtnClick());
        progress = new ProgressDialog(this);
        progress.setTitle("Please Wait...");
        progress.setMessage("We are Making a Noise for you...");
        progress.setCancelable(false);

        defaultBackupcolor();

    }

    private void defaultBackupcolor() {

        onekm.setBackground(getResources().getDrawable(R.drawable.rectangularcorner));
        threekm.setBackground(getResources().getDrawable(R.drawable.rectangularcorner));
        fivekm.setBackground(getResources().getDrawable(R.drawable.rectangularcorner));
        tenkm.setBackground(getResources().getDrawable(R.drawable.rectangularcorner));
        twentykm.setBackground(getResources().getDrawable(R.drawable.rectangularcorner));

        tvonekm.setTextColor(getResources().getColor(R.color.textblack));
        tvthreekm.setTextColor(getResources().getColor(R.color.textblack));
        tvfivekm.setTextColor(getResources().getColor(R.color.textblack));
        tvtenkm.setTextColor(getResources().getColor(R.color.textblack));
        tvtwentykm.setTextColor(getResources().getColor(R.color.textblack));

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @OnClick(R.id.onekm)
    public void ononekmClicked(){
        defaultBackupcolor();
        onekm.setBackground(getResources().getDrawable(R.drawable.rounded_corner_purple));
        tvonekm.setTextColor(getResources().getColor(R.color.white));
        distance=1;
    }
    @OnClick(R.id.threekm)
    public void onthreekmClicked(){

        defaultBackupcolor();
        threekm.setBackground(getResources().getDrawable(R.drawable.rounded_corner_purple));
        tvthreekm.setTextColor(getResources().getColor(R.color.white));
        distance=3;
    }
    @OnClick(R.id.fivekm)
    public void onfivekmClicked(){
        defaultBackupcolor();
        fivekm.setBackground(getResources().getDrawable(R.drawable.rounded_corner_purple));
        tvfivekm.setTextColor(getResources().getColor(R.color.white));
        distance=5;
    }
    @OnClick(R.id.tenkm)
    public void ontenkmClicked(){
        defaultBackupcolor();
        tenkm.setBackground(getResources().getDrawable(R.drawable.rounded_corner_purple));
        tvtenkm.setTextColor(getResources().getColor(R.color.white));
        distance=10;
    }
    @OnClick(R.id.twentykm)
    public void ontwentykmClicked(){
        defaultBackupcolor();
        twentykm.setBackground(getResources().getDrawable(R.drawable.rounded_corner_purple));
        tvtwentykm.setTextColor(getResources().getColor(R.color.white));
        distance=20;
    }

    @OnClick(R.id.rlBackarrow)
    public  void  onrlBackarrowClicked(){
        onBackPressed();
    }
    @NeedsPermission(Manifest.permission.ACCESS_FINE_LOCATION)
    void getLocation() {
        boolean locationEnabled = isLocationEnabled();
        if (locationEnabled) {
            progress.show();
            requestLocation();
        } else {
            showLocationDialog();
        }
    }

    private void showLocationDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setMessage("GPS is disabled in your device. Enable it?")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        (dialog, id) -> {
                            Intent callGPSSettingIntent = new Intent(
                                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(callGPSSettingIntent);
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                (dialog, id) -> dialog.cancel());
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void requestLocation() {
        LocationRequest locationRequest = getLocationRequest();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, MakeANoiseActivity.this);
    }

    @NonNull
    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(60 * 1000);
        locationRequest.setFastestInterval(15 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }


    private void OnMakeANoiseBtnClick() {

       /* distance = activityMakeAnoiseDistanceRangeSB.getProgress();
        Log.d(TAG," distance : "+distance);*/
        if(distance!=0) {
            MakeANoiseActivityPermissionsDispatcher.getLocationWithCheck(this);
        }else if(distance==0){
            BnBUserUtility.toastMessage_info("Please select the distance range and try again.",MakeANoiseActivity.this);
        }
    }

    public void sendMANRequest() {
        subject ="";
        message = "";
//        distance = activityMakeAnoiseDistanceRangeSB.getProgress();
//        distance=20;
        Log.d(TAG," distance : "+distance);
//        boolean areCredentialValid = validateCredentials(subject);
//        if (areCredentialValid)
        {
            String token = "Bearer " + sharedPrefHelper.getAuthToken();
            Log.d(TAG," token : "+token);
            bnBService.makeANoise(token, getMakeANoiseRequest(subject, reason, message, distance, location)).enqueue(new Callback<MakeANoiseResponse>() {
                @Override
                public void onResponse(Call<MakeANoiseResponse> call, Response<MakeANoiseResponse> response) {
                    if (response.body().getSuccess()) {
                        Log.d(TAG, "Success: ");
                        String requestId = response.body().getMakeANoiseData().getRequestId();
                        addSentRequestInDB(requestId);
                        progress.dismiss();
                        showToast();
                        Intent intent=new Intent(MakeANoiseActivity.this, MainActivity.class);
                        //Clear all activities and start new task
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                        finish();
                    }
                }

                @Override
                public void onFailure(Call<MakeANoiseResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: ");
                    progress.dismiss();
                }
            });
        }
    }

    private void showToast() {
        Toast.makeText(this, "Your Request has been sent.", Toast.LENGTH_LONG).show();
    }

    private void addSentRequestInDB(String requestId) {
        String status = "open";
        Date createdAt = new Date();
        dbHelper.addSentRequest(requestId, status, subject, reason, message, distance, location, createdAt);
    }

    private MakeANoiseRequest getMakeANoiseRequest(String subject, String reason, String message, int distance, ArrayList<Double> location) {
        MakeANoiseRequest makeANoiseRequest = new MakeANoiseRequest();
        makeANoiseRequest.setDistance(distance);
        makeANoiseRequest.setMessage(message);
        makeANoiseRequest.setReason(reason);
        makeANoiseRequest.setSubject(subject);
        makeANoiseRequest.setCurrentLocation(location);
        return makeANoiseRequest;
    }


   /* private boolean validateCredentials(String subject) {
        if (subject.isEmpty()) {
            activityMakeAnoiseSubjectET.setError(getResources().getString(R.string.ETEmptyError));
            progress.dismiss();
            return false;
        }
        return true;
    }*/


    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        MakeANoiseFragmentPermissionsDispatcher.getLocationWithCheck(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart: ");
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // NOTE: delegate the permission handling to generated method
        MakeANoiseActivityPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @OnShowRationale(Manifest.permission.ACCESS_FINE_LOCATION)
    void showRationaleForGetLocation(PermissionRequest request) {
        Log.i(TAG, "showRationaleForGetLocation: ");
        new AlertDialog.Builder(this)
                .setMessage("App needs location to get you updated about the requests")
                .setPositiveButton("Allow", (dialog, button) -> request.proceed())
                .setNegativeButton("Deny", (dialog, button) -> request.cancel())
                .setCancelable(false)
                .show();
    }

    // Annotate a method which is invoked if the user doesn't grant the permissions
    @OnPermissionDenied(Manifest.permission.ACCESS_FINE_LOCATION)
    void showDeniedForGetLocation() {
        Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "showDeniedForGetLocation: permission denied");
    }

    // Annotates a method which is invoked if the user
    // chose to have the device "never ask again" about a permission
    @OnNeverAskAgain(Manifest.permission.ACCESS_FINE_LOCATION)
    void showNeverAskForGetLocation() {
        Log.i(TAG, "showNeverAskForGetLocation:");
        Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
    }

    @OnItemSelected(R.id.spMakeaNoiseReason)
    public void spinnerItemSelected(Spinner spinner, int position) {
        reason = reasonList[position];
    }

    @OnItemSelected(value = R.id.spMakeaNoiseReason, callback = OnItemSelected.Callback.NOTHING_SELECTED)
    void onNothingSelected() {
        Log.i(TAG, "onNothingSelected: ");
    }

    private boolean isLocationEnabled() {
        LocationManager locateManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        boolean locationEnabled = locateManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return locationEnabled;
    }

    private void updateLocationInSP(ArrayList<Double> location) {
        sharedPrefHelper.setLastLocation(location);
    }


    @Override
    public void onLocationChanged(Location lastLocation) {
        Log.i(TAG, "onLocationChanged: ");
        location.clear();
        location.add(lastLocation.getLongitude());
        location.add(lastLocation.getLatitude());
        Log.i(TAG, "onLocationChanged: " + location);
        updateLocationInSP(location);
        sendMANRequest();
    }



}

package com.breathandbeats.android.ui.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.tapadoo.alerter.Alerter;

import java.io.File;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;

/**
 * Created by Rakesh on 12-07-2017.
 */

public class BnBUserUtility {

    static ProgressDialog dialog;
    private static final String ImagaeFolderName = "BNB";
    static String TAG = "BnBUserUtility";





    public static void displayMessageAlert(String Message, Context c) {
       new AlertDialog.Builder(c).setMessage(Message).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {

            }
        }).show();
    }

    public static void toastMessage(String Message, Context con) {
        Toast toast = Toast.makeText(con, Message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, 0);
        toast.show();
    }

    public static void showProgress(Context context, String message) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = ProgressDialog.show(context, null, message, true, false);
    }

    public static void dismissProgressDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

    public static boolean emailValidator(String email) {
        // "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean fullNameValidator(String fullname) {
        //^[a-zA-Z0-9_ ]*$

        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(fullname);
        return matcher.matches();
    }

//*********************************************** Camera ************************************************

    public static boolean isDeviceSupportCamera(Context context) { //Checking device has camera hardware or not
        if (context.getApplicationContext().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }

    public static Uri getOutputMediaFileUri(int type) {
        //Creating file uri to store image/video
        return Uri.fromFile(BnBUserUtility.getOutputMediaFile(type));
    }

    public static File getOutputMediaFile(int type) {
        //returning image / video
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), ImagaeFolderName);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File mediaFile;
        if (type == 1) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        } else if (type == 2) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_" + timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }
    //******************************************** End Camera **********************************************


    public static boolean isValidMobile(String phone2) {
        boolean check;
        if (phone2.length() < 6 || phone2.length() > 13) {
            check = false;

        } else {
            check = true;
            /*if((String.valueOf(phone2.charAt(0))=="9")|| (String.valueOf(phone2.charAt(0))=="8")||  (String.valueOf(phone2.charAt(0))=="7")){
                check = true;
            }else{
                check = false;
            }*/

        }
        return check;
    }


    public static void AlertMessage(String Message, Context c) {
        Alerter.create((Activity) c)
                .setTitle("Alert")
                .setText(Message)
                .setIcon(R.drawable.alerter_ic_notifications)
                .enableProgress(true)
                .enableSwipeToDismiss()
                .setProgressColorRes(R.color.colorPrimary)
                .setBackgroundColorRes(R.color.blue) // or setBackgroundColorInt(Color.CYAN)
                .show();
    }

    public static void AlertMessageDismiss(Context c){

        Alerter.hide();
    }

    public static String getTheCorrectFormateDate(String datevalue) {

        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat destFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm a"); //here 'a' for AM/PM

        Date date = null;
        try {
            date = sourceFormat.parse(datevalue);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = destFormat.format(date);
        return formattedDate;
    }

    public static String getCurrenttime() {

       String date_time= DateFormat.getDateTimeInstance().format(new Date());
        return date_time;
    }
    //-----------------------------------------------------------------------

    public static void toastMessage_warning(String Message, Context con) {
        Toasty.info(con, Message, Toast.LENGTH_SHORT, true).show();
    }

    public static void toastMessage_success(String Message, Context con) {
        Toasty.success(con, Message, Toast.LENGTH_SHORT, true).show();
    }
    public static void toastMessage_error(String Message, Context con) {
        Toasty.error(con, Message, Toast.LENGTH_SHORT, true).show();
    }
    public static void toastMessage_info(String Message, Context con) {
        Toasty.info(con, Message, Toast.LENGTH_SHORT, true).show();
    }

    public static void toastMessage_confusion(String Message, Context con) {
//        TastyToast.makeText(con,Message, TastyToast.LENGTH_LONG, TastyToast.CONFUSING);
    }
    public static void toastMessage_default(String Message, Context con) {
        Toasty.info(con, Message, Toast.LENGTH_SHORT, true).show();
    }
    //--------------------------------------------------------------------

    public static void Clear_SharedPreferenceData(Context con) {

        SharedPreferenceBnBUtility.setNotificationTripID(con,"null");
        SharedPreferenceBnBUtility.setDriverNumber(con,"null");
        SharedPreferenceBnBUtility.setDriverName(con,"null");

        SharedPreferenceBnBUtility.setNotificationHospitalAddress(con,"null");
        SharedPreferenceBnBUtility.setNotificationHospitalName(con,"null");
        SharedPreferenceBnBUtility.setNotificationHospitalNumber(con,"null");

        SharedPreferenceBnBUtility.setNotificationPatientLatitude(con,"null");
        SharedPreferenceBnBUtility.setNotificationPatientLongitude(con,"null");

        SharedPreferenceBnBUtility.setDriverLatitude(con,"null");
        SharedPreferenceBnBUtility.setDriverLongitude(con,"null");

        SharedPreferenceBnBUtility.setNotificationRecived(con,"false");

        SharedPreferenceBnBUtility.setNotificationTripStarted(con,"false");
        SharedPreferenceBnBUtility.setNotificationTripCompleted(con,"false");
        SharedPreferenceBnBUtility.setNotificationTripStatus(con,"false");

        SharedPreferenceBnBUtility.setNotificationTripvalue(con,"Noting to show!");
        SharedPreferenceBnBUtility.setTripPaymentRecived(con,"true");
    }

    public static void Clear_notification(Context con) {
        NotificationManager notificationManager = (NotificationManager)
                con.getSystemService(Context.
                        NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

    }

    public static int time_difference(Context con,String InputTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date date1=null;
        Date  date2 = null;

        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        df.setTimeZone(tz);
        String currentdate = df.format(new Date());

        try {
            date1 = simpleDateFormat.parse(currentdate);
            date2 = simpleDateFormat.parse(InputTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long difference = date2.getTime() - date1.getTime();
        int days = (int) (difference / (1000 * 60 * 60 * 24));
        int  hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
        int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
        hours = (hours < 0 ? -hours : hours);
        Log.i("======= Hours", " :: " + hours);
        return  hours;
    }
    public static String  DateFormatClass(Context con,String Date,String timev) {

        String myTimestamp="2014/02/17 20:49:00";
        String valu="";
        SimpleDateFormat form = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        Date time = null;
        try
        {
            date = form.parse(myTimestamp);
            Log.d("datevalue","date : "+date);
             valu=String.valueOf(date);
//            TastyToast.makeText(con,""+date, TastyToast.LENGTH_LONG, TastyToast.CONFUSING);
            /*time = new Date(myTimestamp);
            SimpleDateFormat postFormater = new SimpleDateFormat("MMM dd");
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            String newDateStr = postFormater.format(date).toUpperCase();
            String newTimeStr = sdf.format(time);
            System.out.println("Date  : "+newDateStr);
            System.out.println("Time  : "+newTimeStr);*/
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return valu;
    }

    public void showCustomAlert()
    {

        /*Context context = getApplicationContext();
        // Create layout inflator object to inflate toast.xml file
        LayoutInflater inflater = getLayoutInflater();

        // Call toast.xml file for toast layout
        View toastRoot = inflater.inflate(R.layout.toast, null);

        Toast toast = new Toast(context);

        // Set layout to toast
        toast.setView(toastRoot);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL,
                0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();*/

    }
}


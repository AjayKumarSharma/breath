package com.breathandbeats.android.ui.activities.EmergencyContact;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.utils.Validators;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EmergencyContactActivity extends AppCompatActivity {

    @BindView(R.id.activity_emergency_contact_name_ET)
    EditText activityEmergencyContactNameET;
    @BindView(R.id.activity_emergency_contact_relationship_ET)
    EditText activityEmergencyContactRelationshipET;
    @BindView(R.id.activity_emergency_contact_number_ET)
    EditText activityEmergencyContactNumberET;
    @BindView(R.id.activity_emergency_contact_email_ET)
    EditText activityEmergencyContactEmailET;
    @BindView(R.id.activity_emergency_contact_addContact_Btn)
    Button activityEmergencyContactAddContactBtn;
    @BindView(R.id.activity_emergency_contact)
    LinearLayout activityEmergencyContact;

    @BindView(R.id.rlBackarrow)
    RelativeLayout rlBackarrow;

    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contact);
        ButterKnife.bind(this);
        /*Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_emergency_contact_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Add Emergency Contact");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);*/

        application = (BnBApp) getApplication();
        bnBService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();
        dbHelper = application.getDBHelper();
    }

    @OnClick(R.id.activity_emergency_contact_addContact_Btn)
    public void addContact() {
        String name = activityEmergencyContactNameET.getText().toString();
        String number = activityEmergencyContactNumberET.getText().toString();
        String relationship = activityEmergencyContactRelationshipET.getText().toString();
        String email = activityEmergencyContactEmailET.getText().toString();
        boolean areCredentialValid = validateCredentials(name, number, relationship, email);
        if (areCredentialValid) {
            dbHelper.setEmergencyContact(name, relationship, number, email);
//            finish();
            Intent intent=new Intent(EmergencyContactActivity.this,EmergencyContactListActivity.class);
            startActivity(intent);
        }
    }


    @OnClick(R.id.rlBackarrow)
    public  void  onrlBackarrowClicked(){
        onBackPressed();
    }

    private boolean validateCredentials(String name, String number, String relationship, String email) {
        if (name.isEmpty()) {
            activityEmergencyContactNameET.setError(getResources().getString(R.string.ETEmptyError));
            return false;
        }
        if (!Validators.isMobileValid(number)) {
            activityEmergencyContactNumberET.setError(getResources().getString(R.string.phoneNumberError));
            return false;
        }
        if (relationship.isEmpty()) {
            activityEmergencyContactRelationshipET.setError(getResources().getString(R.string.ETEmptyError));
            return false;
        }
        if (!Validators.isEmailValid(email)) {
            activityEmergencyContactEmailET.setError(getResources().getString(R.string.emailError));
            return false;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

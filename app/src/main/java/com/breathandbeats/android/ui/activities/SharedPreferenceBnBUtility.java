package com.breathandbeats.android.ui.activities;

import android.content.Context;

/**
 * Created by Rakesh on 31-07-2017.
 */

public class SharedPreferenceBnBUtility {

    private static final String APP_PREF = "BNB_DRIVER_APP";
    private static final String APP_DRIVERNUMBER = "DRIVER_NUMBER";
    private static final String APP_DRIVERNAME = "DRIVER_NAME";
    private static final String APP_DRIVERHOSPITALID = "DRIVERHOSPITAL_ID";
    private static final String APP_DRIVERLATITUDE = "DRIVERLATITUDE";
    private static final String APP_DRIVERLONGITUDE = "DRIVERLONGITUDE";
    private static final String APP_NOTIFICATION_HOSPITALNUMBER = "HOSPITALNUMBER ";
    private static final String APP_NOTIFICATION_HOSPITALADDRESS = "HOSPITALADDRESS ";
    private static final String APP_NOTIFICATION_PATIENTLATITUDE = "PATIENTLATITUDE ";
    private static final String APP_NOTIFICATION_PATIENTLONGITUDE = "PATIENTLONGITUDE ";
    private static final String APP_NOTIFICATION_RECIVED = "RECIVED";
    private static final String APP_NOTIFICATION_PATIENTADDRESS = "PATIENTADDRESS";
    private static final String APP_NOTIFICATION_HOSPITALNAME = "HOSPITALNAME";
    private static final String APP_DBID = "DBID";
    private static final String APP_NOTIFICATION_TRIPID = "TRIPID";
    private static final String APP_NOTIFICATION_TRIPSTATUS = "TRIPSTATUS";
    private static final String APP_NOTIFICATION_TRIPSTARTED = "TRIPSTARTED";
    private static final String APP_NOTIFICATION_TRIPCOMPLETED = "TRIPCOMPLETED";
    private static final String APP_NOTIFICATION_TRIPVALUE= "TRIPVALUE";
    private static final String APP_NOTIFICATION_PAYMENTSTATUS= "PAYMENTSTATUS";
    private static final String APP_TRIPHISTORY= "TRIPHISTORY";
    private static final String APP_NAME= "USERNAME";
    private static final String APP_NUMBER= "USERNUMBER";
    private static final String APP_EMAIL= "EMAIL";
    private static final String APP_BLOODGROUP= "BLOODGROUP";
    private static final String APP_PASSWORD= "PASSWORD";
    private static final String APP_PAYMNET= "PAYMNET_MODE";
    private static final String  APP_TRIPPAYMENTRECEIVED="APP_TRIPPAYMENTRECEIVED";
    private static final String APP_HOSPITALID= "APP_HOSPITALID";
    private static final String  APP_PAYMENT="APP_PAYMENT";
    private static final String  APP_ADVANCE_PAYMENT="APP_ADVANCE_PAYMENT";
    private static final String  APP_ADVANCETRIP="APP_ADVANCETRIP";
    private static final String  APP_PAID="APP_PAID";
    private static final String  APP_PAIDID="APP_PAIDID";
    private static final String  APP_ADBOOKERQ="APP_ADBOOKERQ";
    private static final String  APP_REQHOSPITAL="APP_REQHOSPITAL";
    private static final String  APP_USERREQLAT="APP_USERREQLAT";
    private static final String  APP_USERREQLONG="APP_USERREQLONG";
    //-----------------------------------------------------

    public static void setDriverNumber(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_DRIVERNUMBER, Values).commit();
    }

    public static String getDriverNumber(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_DRIVERNUMBER, "");
    }
    //-----------------------------------------------------

    public static void setDriverName(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_DRIVERNAME, Values).commit();
    }

    public static String getDriverName(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_DRIVERNAME, "");
    }
    //-----------------------------------------------------

    public static void setDriverHospitalID(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_DRIVERHOSPITALID, Values).commit();
    }

    public static String getDriverHospitalID(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_DRIVERHOSPITALID, "");
    }

    //-----------------------------------------------------


    public static void setDriverLatitude(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_DRIVERLATITUDE, Values).commit();
    }

    public static String getDriverLatitude(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_DRIVERLATITUDE, "");
    }
    //-----------------------------------------------------

    public static void setDriverLongitude(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_DRIVERLONGITUDE, Values).commit();
    }

    public static String getDriverLongitude(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_DRIVERLONGITUDE, "");
    }

    //--------------------------------------------------------
    public static void setNotificationHospitalNumber(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_HOSPITALNUMBER, Values).commit();
    }

    public static String getNotificationHospitalNumber(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_HOSPITALNUMBER, "");
    }
    //--------------------------------------------------------
    public static void setNotificationHospitalAddress(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_HOSPITALADDRESS, Values).commit();
    }

    public static String getNotificationHospitalAddress(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_HOSPITALADDRESS, "");
    }

    //--------------------------------------------------------
    public static void setNotificationPatientLatitude(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_PATIENTLATITUDE, Values).commit();
    }

    public static String getNotificationPatientLatitude(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_PATIENTLATITUDE, "");
    }
//----------------------------------------------------------------------------------
    public static void setNotificationPatientLongitude(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_PATIENTLONGITUDE, Values).commit();
    }

    public static String getNotificationPatientLongitude(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_PATIENTLONGITUDE, "");
    }

    //----------------------------------------------------------------------------------
    public static void setNotificationRecived(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_RECIVED, Values).commit();
    }

    public static String getNotificationRecived(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_RECIVED, "");
    }
    //----------------------------------------------------------------------------------
    public static void setNotificationPatientAddress(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_PATIENTADDRESS, Values).commit();
    }

    public static String getNotificationPatientAddress(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_PATIENTADDRESS, "");
    }
    //----------------------------------------------------------------------------------
    public static void setNotificationHospitalName(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_HOSPITALNAME, Values).commit();
    }

    public static String getNotificationHospitalName(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_HOSPITALNAME, "");
    }
    //----------------------------------------------------------------------------------
    public static void setDBID(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_DBID, Values).commit();
    }

    public static String getDBID(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_DBID, "");
    }

    //----------------------------------------------------------------------------------
    public static void setNotificationTripID(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_TRIPID, Values).commit();
    }

    public static String getNotificationTripID(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_TRIPID, "");
    }

    //----------------------------------------------------------------------------------
    public static void setNotificationTripStatus(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_TRIPSTATUS, Values).commit();
    }

    public static String getNotificationTripStatus(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_TRIPSTATUS, "");
    }
    //----------------------------------------------------------------------------------
    public static void setNotificationTripStarted(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_TRIPSTARTED, Values).commit();
    }

    public static String getNotificationTripStarted(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_TRIPSTARTED, "");
    }
    //----------------------------------------------------------------------------------
    public static void setNotificationTripCompleted(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_TRIPCOMPLETED, Values).commit();
    }

    public static String getNotificationTripCompleted(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_TRIPCOMPLETED, "");
    }
    //----------------------------------------------------------------------------------
    public static void setNotificationTripvalue(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_TRIPVALUE, Values).commit();
    }

    public static String getNotificationTripvalue(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_TRIPVALUE, "");
    }
    //----------------------------------------------------------------------------------
    public static void setTripPaymentStatus(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NOTIFICATION_PAYMENTSTATUS, Values).commit();
    }

    public static String getTripPaymentStatus(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NOTIFICATION_PAYMENTSTATUS, "");
    }
    //----------------------------------------------------------------------------------
    public static void setTriphistory(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_TRIPHISTORY, Values).commit();
    }

    public static String getTriphistory(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_TRIPHISTORY, "");
    }
    //----------------------------------------------------------------------------------
    public static void setUserName(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NAME, Values).commit();
    }

    public static String getUserName(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NAME, "");
    }
    //----------------------------------------------------------------------------------
    public static void setUserNumber(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_NUMBER, Values).commit();
    }

    public static String getUserNumber(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_NUMBER, "");
    }

    //----------------------------------------------------------------------------------
    public static void setUserEmail(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_EMAIL, Values).commit();
    }

    public static String getUserEmail(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_EMAIL, "");
    }
    //----------------------------------------------------------------------------------
    public static void setUserBloodgroup(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_BLOODGROUP, Values).commit();
    }

    public static String getUserBloodgroup(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_BLOODGROUP, "");
    }
    //----------------------------------------------------------------------------------
    public static void setUserPassword(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_PASSWORD, Values).commit();
    }

    public static String getUserPassword(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_PASSWORD, "");
    }
    //----------------------------------------------------------------------------------
    public static void setPaymnetmode(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_PAYMNET, Values).commit();
    }

    public static String getPaymnetmode(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_PAYMNET, "");
    }

    //-----------------------------------------------------

    public static void setTripPaymentRecived(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_TRIPPAYMENTRECEIVED, Values).commit();
    }

    public static String getTripPaymentRecived(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_TRIPPAYMENTRECEIVED, "");
    }

    //-----------------------------------------------------

    public static void setHospitalID(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_HOSPITALID, Values).commit();
    }

    public static String getHospitalID(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_HOSPITALID, "");
    }

    //-----------------------------------------------------

    public static void setPaymentInfo(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_PAYMENT, Values).commit();
    }

    public static String getPaymentInfo(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_PAYMENT, "");
    }

    //-----------------------------------------------------

    public static void setAdvancePaymentInfo(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_ADVANCE_PAYMENT, Values).commit();
    }

    public static String getAdvancePaymentInfo(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_ADVANCE_PAYMENT, "");
    }

    //-----------------------------------------------------
    public static void setAdvanceTrip(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_ADVANCETRIP, Values).commit();
    }

    public static String getAdvanceTrip(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_ADVANCETRIP, "");
    }
    //-----------------------------------------------------
    public static void setUserPaidMoney(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_PAID, Values).commit();
    }

    public static String getUserPaidMoney(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_PAID, "");
    }
    //-----------------------------------------------------
    public static void setPaymentID(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_PAIDID, Values).commit();
    }

    public static String getPaymentID(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_PAIDID, "");
    }

    //-----------------------------------------------------
    public static void setAdvacneBookedReq(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_ADBOOKERQ, Values).commit();
    }

    public static String getAdvacneBookedReq(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_ADBOOKERQ, "");
    }
    //-----------------------------------------------------
    public static void setrequestedforhospital(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_REQHOSPITAL, Values).commit();
    }

    public static String getrequestedforhospital(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_REQHOSPITAL, "");
    }
    //-----------------------------------------------------
    public static void setuserRequestedPointLatitude(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_USERREQLAT, Values).commit();
    }

    public static String getuserRequestedPointLatitude(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_USERREQLAT, "");
    }
    //-----------------------------------------------------
    public static void setuserRequestedPointLongitude(Context context, String Values)
    {

        context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).edit().putString(APP_USERREQLONG, Values).commit();
    }

    public static String getuserRequestedPointLongitude(Context context)
    {
        return context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE).getString(APP_USERREQLONG, "");
    }
}

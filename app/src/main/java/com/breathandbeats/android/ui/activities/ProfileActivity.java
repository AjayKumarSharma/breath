package com.breathandbeats.android.ui.activities;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.pojos.api.ForgotPasswordRequest;
import com.breathandbeats.android.pojos.api.ForgotPasswordResponse;
import com.breathandbeats.android.pojos.api.GenericResponse;
import com.breathandbeats.android.pojos.api.VerifyForgotPassRequest;
import com.breathandbeats.android.utils.Validators;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lavanya on 09-11-2017.
 */

public class ProfileActivity  extends AppCompatActivity{

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvNumber)
    TextView tvNumber;
    @BindView(R.id.tvemailId)
    TextView tvemailId;
    @BindView(R.id.tvBloodgroup)
    TextView tvBloodgroup;
    @BindView(R.id.tvpassword)
    TextView tvpassword;

    @BindView(R.id.changepassword)
    RelativeLayout changepassword;

    @BindView(R.id.backbtn)
     RelativeLayout backbtn;

    Dialog resetDialog;
    Dialog forgotDialog;
    BnBApp application;
    BnBService bnBService;
    String TAG="profile_Activity";
    ForgotPasswordResponse forgotPasswordResponse_v=new ForgotPasswordResponse();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ButterKnife.bind(this);

        application = (BnBApp) getApplication();
        bnBService = application.getRestClient().getApiSerivice();

        setdata();
    }

    private void setdata() {


        tvName.setText(SharedPreferenceBnBUtility.getUserName(this));
        tvNumber.setText(SharedPreferenceBnBUtility.getUserNumber(this));
        tvemailId.setText(SharedPreferenceBnBUtility.getUserEmail(this));
        tvBloodgroup.setText(SharedPreferenceBnBUtility.getUserBloodgroup(this));
        tvpassword.setText(SharedPreferenceBnBUtility.getUserPassword(this));


    }

    @OnClick(R.id.changepassword)
    public void onchangepasswordClicked(){

        launchForgotPasswordDialog();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent=new Intent(ProfileActivity.this,MainActivity.class);
        intent.putExtra("position","1");
        //Clear all activities and start new task
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.backbtn)
    public void onbackbtnClicked(){
        onBackPressed();
    }
    public void launchForgotPasswordDialog() {
        forgotDialog = new Dialog(this);
        forgotDialog.setContentView(R.layout.fragment_forgot_password);
        TextView headettext=(TextView)  forgotDialog.findViewById(R.id.headettext);
        Button confirmBtn = (Button) forgotDialog.findViewById(R.id.fragment_forgot_password_confirmBtn);
        Button cancelBtn = (Button) forgotDialog.findViewById(R.id.fragment_forgot_password_cancelBtn);
        final EditText phoneNumberET = (EditText) forgotDialog.findViewById(R.id.fragment_forgot_password_phoneNumberET);

        headettext.setText("Change Password ");

        confirmBtn.setOnClickListener(view -> {
            if (!Validators.isMobileValid(phoneNumberET.getText().toString())) {
                phoneNumberET.setError("Invalid Number");
                return;
            }
            final String phoneNumber1 = phoneNumberET.getText().toString();
            bnBService.initiateForgotPassword(getForgotPasswordRequest(phoneNumber1)).enqueue(new Callback<ForgotPasswordResponse>() {
                @Override
                public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                    if (response.body().getSuccess()) {
                        forgotDialog.dismiss();
                        Log.d(TAG, "responce : ");
                        if(response.body()!=null) {

                            forgotPasswordResponse_v=response.body();
                            Log.d(TAG, "responce body not null : "+response.body().getSuccess());

                            if(response.body().getSuccess()==true){

                                launchResetPassDialog(phoneNumber1);
                                Log.d(TAG, "dtaa : " + response.body().getData());
                            }else if(response.body().getSuccess()==false){

                            }

                        }else{
                            Log.d(TAG, "responce null : ");
                        }
                    } else {
                        Toast.makeText(ProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        forgotDialog.dismiss();
                        Log.d(TAG, "responce failure : ");
                    }

                }

                @Override
                public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                    Log.d(TAG, "responce failure exsep : "+t);
                    forgotDialog.dismiss();
                }
            });

        });
        cancelBtn.setOnClickListener(view -> forgotDialog.dismiss());
        forgotDialog.show();
    }

    private ForgotPasswordRequest getForgotPasswordRequest(String phoneNumber) {
        ForgotPasswordRequest forgotPasswordRequest = new ForgotPasswordRequest(phoneNumber);
        return forgotPasswordRequest;
    }

    public void launchResetPassDialog( final String phoneNumber) {
        resetDialog = new Dialog(this);
        resetDialog.setContentView(R.layout.fragment_reset_password);
        resetDialog.show();
        forgotDialog.dismiss();
        TextView emailTV = (TextView) resetDialog.findViewById(R.id.fragment_reset_password_emailTV);
        final EditText passwordET = (EditText) resetDialog.findViewById(R.id.fragment_reset_password_passwordET);
        final EditText resetCodeET = (EditText) resetDialog.findViewById(R.id.fragment_reset_password_restCodeET);
        String displayText = emailTV.getText().toString();
       /* displayText = displayText + "registered mobile number.";
        emailTV.setText(displayText);*/
        String Password_v=passwordET.getEditableText().toString();
        Button resetDialogConfirmBtn = (Button) resetDialog.findViewById(R.id.fragment_reset_password_confirmBtn);
        Button resetDialogCancelBtn = (Button) resetDialog.findViewById(R.id.fragment_reset_password_cancelBtn);

        resetDialogCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetDialog.dismiss();
            }
        });

        resetDialogConfirmBtn.setOnClickListener(view -> {
            if (resetCodeET.getText().toString().isEmpty()) {
                resetCodeET.setError("Code cannot be empty");
                return;
            }
            if (passwordET.getText().toString().isEmpty()) {
                passwordET.setError("password cannot be empty");
                return;
            }
            final String newPass = passwordET.getText().toString();
            final String resetCode = resetCodeET.getText().toString();
            VerifyForgotPassRequest verifyForgotPassword = new VerifyForgotPassRequest();
            verifyForgotPassword.setPhoneNumber(phoneNumber);
            verifyForgotPassword.setNewPassword(newPass);
            verifyForgotPassword.setVerificationCode(resetCode);

            bnBService.verifyForgotPass(verifyForgotPassword).enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                    if (response.body().getSuccess()) {

                        Log.d(TAG," new pass : "+newPass);
                        BnBUserUtility.toastMessage("Password Reset Successfull",ProfileActivity.this);
                        resetDialog.dismiss();
                        SharedPreferenceBnBUtility.setUserPassword(ProfileActivity.this,newPass);
                        startActivity(new Intent(ProfileActivity.this, ProfileActivity.class));

                    } else {
                        Toast.makeText(ProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    BnBUserUtility.toastMessage("Sorry,something went wrong,we can't reach our service at the moment.Please try again.",ProfileActivity.this);
                    Log.d(TAG,"@FAILURE : "+t);
                }
            });

        });
    }

}

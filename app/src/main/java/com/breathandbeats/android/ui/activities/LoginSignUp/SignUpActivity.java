package com.breathandbeats.android.ui.activities.LoginSignUp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.breathandbeats.android.BuildConfig;
import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.infrastructure.Constants;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.api.DeviceInfo;
import com.breathandbeats.android.pojos.api.LoginResponse;
import com.breathandbeats.android.pojos.api.SignUpRequest;
import com.breathandbeats.android.pojos.api.UserData;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.MainActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.breathandbeats.android.utils.Validators;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUpActivity extends AppCompatActivity {
    private static final String TAG = "SignUpAcitivty";

    @BindView(R.id.activity_signup_mobileNumber_ET)
    EditText mobileNumberET;
    @BindView(R.id.activity_signup_password_ET)
    EditText passwordET;
    @BindView(R.id.activity_signup_emailID_ET)
    EditText emailIDET;
    @BindView(R.id.activity_signup_signup_BTN)
    Button signupBTN;
    @BindView(R.id.activity_signup_text2_TV)
    TextView loginTV;

    String phoneNumber;
    String password;
    String email;
    String BloodGroup;
    BnBApp application;
    BnBService apiSerivice;
    SharedPrefHelper sharedPrefHelper;

    Boolean isLoggedIn;
    Boolean isPhoneVerified;
    String authToken;
    UserData userData;
    @BindView(R.id.activity_signup_name_ET)
    EditText nameET;
    @BindView(R.id.activity_signup)
    RelativeLayout activitySignup;

    @BindView(R.id.etBloodGroup)
    EditText etBloodGroup;
    private String name;
    String  password_v="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_acitivty);
        ButterKnife.bind(this);

        application = (BnBApp) getApplication();
        apiSerivice = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();

        userData = sharedPrefHelper.getUserData();
        isPhoneVerified = sharedPrefHelper.getPhoneVerified();
        authToken = sharedPrefHelper.getAuthToken();
        Log.i(TAG, "onCreate: " + authToken);

        if(!authToken.equals(null)) {
            if (!authToken.isEmpty()) {
                if (!isPhoneVerified)
                    startActivity(new Intent(this, MobileVerifyActivity.class));
                else {
                    Intent intent = new Intent(this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        }

    }

    @OnClick(R.id.activity_signup_signup_BTN)
    public void onSignUpBtnClicked() {


        if(NetworkUtil.getConnectivityStatusBoolen(this)){

            RequestForSignUp();
        }else{

            BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",application);
        }

       /* Intent homeIntent = new Intent(SignUpActivity.this, MainActivity.class);
//        homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(homeIntent);*/
    }

    private void RequestForSignUp() {


        phoneNumber = mobileNumberET.getText().toString();
        password = passwordET.getText().toString();
         password_v=password;
//        password = Shared.encryptPassword(phoneNumber, password);
        Log.d(TAG,"password : "+password);
        email = emailIDET.getText().toString();
        name = nameET.getText().toString();
        BloodGroup=etBloodGroup.getText().toString();
        Log.d(TAG, "onLoginBtnClicked: " + phoneNumber + " " + password + " " + email+" "+BloodGroup );
        Log.d(TAG, " BloodGroup:  " +BloodGroup);
        boolean areCredentialValid = validateCredentials(phoneNumber, password, email, name,password_v);
        if(areCredentialValid){
            if(phoneNumber.length()>0){
                String firstLetter = String.valueOf(phoneNumber.charAt(0)) ;

                if(firstLetter.equalsIgnoreCase("9") || firstLetter.equalsIgnoreCase("8") || firstLetter.equalsIgnoreCase("7")){

                    //------------------------------
                    BnBUserUtility.showProgress(SignUpActivity.this,"Please wait...");
                    apiSerivice.signup(getSignupRequest(phoneNumber, password, email, name,BloodGroup)).enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                            if(response!=null) {
                                BnBUserUtility.dismissProgressDialog();
                                if (response.isSuccessful()) {

                                    Log.d(TAG, "onResponse: success ");
                                    Log.d(TAG, "onResponse:  "+response.body().getData());

                                    if(response.body().getData()!=null) {

                                        boolean isPhoneVerified = response.body().getData().getPhoneVerified();

                                        saveDataInSP(response.body().getData());
                                        saveNameInSP(name,phoneNumber,email,BloodGroup,password_v);

                                        if (isPhoneVerified) {

                                            Intent homeIntent = new Intent(SignUpActivity.this, MainActivity.class);
                                            homeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(homeIntent);

                                        } else {
                                            startActivity(new Intent(SignUpActivity.this, MobileVerifyActivity.class));
                                        }

                                        finish();
                                    }else{

                                        BnBUserUtility.toastMessage_error(response.body().getMessage().toString(),SignUpActivity.this);
                                    }
                                } else {
                                    BnBUserUtility.dismissProgressDialog();
                                    Log.d(TAG, "onResponse: success fail");

                                    BnBUserUtility.toastMessage_error("Unable to sign up the application, please try again.",SignUpActivity.this);
//                            Toast.makeText(SignUpActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                BnBUserUtility.dismissProgressDialog();
                                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't reach our service at the moment.Please try again.",SignUpActivity.this);
                            }

                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            Log.d(TAG, "onFailure: "+t);
                            BnBUserUtility.dismissProgressDialog();
                            BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't reach our service at the moment.Please try again.",SignUpActivity.this);
                        }
                    });

                    //-------------------------------
                }else{
               mobileNumberET.setError(getResources().getString(R.string.phoneNumberError));
//                    BnBUserUtility.toastMessage_error(getResources().getString(R.string.phoneNumberError),SignUpActivity.this);

                }
            }
        }else{

        }

    }

    private void saveNameInSP(String name, String phoneNumber, String email, String bloodGroup, String password_v) {
        sharedPrefHelper.setName(name);
        sharedPrefHelper.setNumber(phoneNumber);

        SharedPreferenceBnBUtility.setUserName(this,name);
        SharedPreferenceBnBUtility.setUserNumber(this,phoneNumber);

        SharedPreferenceBnBUtility.setUserEmail(this,email);
        SharedPreferenceBnBUtility.setUserBloodgroup(this,bloodGroup);

        SharedPreferenceBnBUtility.setUserPassword(this,password_v);
    }

    private void saveDataInSP(UserData data) {
        sharedPrefHelper.saveUserData(data);
        sharedPrefHelper.setPhoneVerified(data.getPhoneVerified());
        sharedPrefHelper.setAuthToken(data.getToken());
        sharedPrefHelper.setUserID(data.getId());
        Log.d(TAG,"token : "+data.getToken());
        Log.d(TAG,"id : "+data.getId());
    }

    private SignUpRequest getSignupRequest(String phoneNumber, String password, String email, String name,String BloodGroup) {
        SignUpRequest signUpRequest = new SignUpRequest();
        signUpRequest.setPhoneNumber(phoneNumber);
        signUpRequest.setPassword(password);
        signUpRequest.setEmail(email);
        signUpRequest.setPlatform(Constants.PLATFORM);
        signUpRequest.setAppVersion(String.valueOf(BuildConfig.VERSION_CODE));
        signUpRequest.setName(name);
        signUpRequest.setBloodGroup(BloodGroup);
//         String deviceId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
//        TelephonyManager tManager = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
//        String deviceIMEI = tManager.getDeviceId();
        signUpRequest.setDeviceInfo(new DeviceInfo("abcd", "efgh",sharedPrefHelper.getFCMToken()));
        Log.d(TAG, "fcm token: " + sharedPrefHelper.getFCMToken());

        return signUpRequest;
    }

    private boolean validateCredentials(String phoneNumber, String password, String email, String name,String pass_V) {
        Log.i(TAG, "validateCredentials: ");

        if (name.isEmpty()) {
            Log.d(TAG, "validateCredentials: pass invalid");
            nameET.setError(getResources().getString(R.string.nameError));
//            BnBUserUtility.toastMessage_error(getResources().getString(R.string.nameError),SignUpActivity.this);
            return false;
        }
        if (!Validators.isMobileValid(phoneNumber)) {
            Log.d(TAG, "validateCredentials: mobileinvalid");
           mobileNumberET.setError(getResources().getString(R.string.phoneNumberError));
//            BnBUserUtility.toastMessage_error(getResources().getString(R.string.phoneNumberError),SignUpActivity.this);
            return false;
        }

        if (password.isEmpty()) {
            Log.d(TAG, "validateCredentials: pass invalid");
           passwordET.setError(getResources().getString(R.string.passwordError));
//            BnBUserUtility.toastMessage_error(getResources().getString(R.string.passwordError),SignUpActivity.this);
            return false;
        }

        if (pass_V.isEmpty()) {
            Log.d(TAG, "validateCredentials: pass invalid");
           passwordET.setError("Please enter password and try again.");
//            BnBUserUtility.toastMessage_error("Please enter password and try again.",SignUpActivity.this);
            return false;
        }
        if (pass_V.length()==0) {
            Log.d(TAG, "validateCredentials: pass invalid");
            passwordET.setError("Please enter password and try again.");
//            BnBUserUtility.toastMessage_error("Please enter password and try again.",SignUpActivity.this);
            return false;
        }
        if (!Validators.isEmailValid(email)) {
            Log.d(TAG, "validateCredentials: email invalid");
          emailIDET.setError(getResources().getString(R.string.emailError));
//            BnBUserUtility.toastMessage_error(getResources().getString(R.string.emailError),SignUpActivity.this);
            return false;
        }
        return true;
    }

    @OnClick(R.id.activity_signup_text2_TV)
    public void goToLoginScreen() {
        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

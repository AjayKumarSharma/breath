package com.breathandbeats.android.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.ui.activities.LoginSignUp.SignUpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "SplashActivity";
    BnBApp application;
    SharedPrefHelper sharedPrefHelper;
    @BindView(R.id.activity_splash_PB)
    ProgressBar activitySplashPB;
    @BindView(R.id.activity_splash_text_TV)
    TextView activitySplashTextTV;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        new Handler().postDelayed(() -> {
            Intent signUpIntent = new Intent(SplashActivity.this, SignUpActivity.class);
            signUpIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(signUpIntent);
            finish();
        }, 3000);

    }


}

package com.breathandbeats.android.ui.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.Toast;


import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import com.android.volley.toolbox.JsonObjectRequest;
import com.breathandbeats.android.Models.ActivityData;
import com.breathandbeats.android.Models.DiscussionModel;
import com.breathandbeats.android.Models.DiscussionModelGegerSetter;
import com.breathandbeats.android.Models.SearchDataModel;
import com.breathandbeats.android.R;
import com.breathandbeats.android.ui.fragments.FragmentSpeciality;
import com.breathandbeats.android.NewUtils.RequestQueueSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.breathandbeats.android.NewUtils.Const.ServiceType.SearchDoctorInfor;

/**
 * Created by Rocky on 1/2/2018.
 */

public class ManageConsult extends AppCompatActivity implements SearchView.OnQueryTextListener {
    ImageView Homes, ClinicalView, ManageConsults, ClinicalEvents, ClassRooms, ivStationSetting;
    TabHost host;
    RecyclerView rv;
    LinearLayoutManager linearLayoutManager;
    RecyclerView.Adapter adapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    Activity mActivity;
    private final int count = 7;
    private static ArrayList<String> array;
    private static ArrayList<DiscussionModelGegerSetter> arraylist;
    String Userid = null;

    ProgressDialog progressDialog;
    static final String REQ_TAG = "VACTIVITYNEW";
    RequestQueue requestQueue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manageconsults);

        progressDialog = new ProgressDialog(ManageConsult.this);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        ClinicalEvents = (ImageView) findViewById(R.id.ivProfile);
        requestQueue = RequestQueueSingleton.getInstance(this.getApplicationContext())
                .getRequestQueue();
        host = (TabHost) findViewById(R.id.tabHostMelodyPacks);
        host.setup();

        /*keyconsult = new ArrayList<SearchDataModel>();
        String arrays[]=new String[50];

        for(int i=0;i< keyconsult.size();i++){
            arrays[i]=keyconsult.get(i).getMsginfo().toString();
        }*/

        array = new ArrayList<String>();
        try {
            for (int i = 0; i < ActivityData.id_.length; i++) {
                array.add(ActivityData.GenereConsult[i].toString());
            }
            for (int i = 0; i < array.size(); i++) {
                //Tab
                TabHost.TabSpec spec = host.newTabSpec(array.get(i).toString());
                spec.setContent(createTabContent());
                spec.setIndicator(array.get(i).toString());
                host.addTab(spec);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        arraylist = new ArrayList<DiscussionModelGegerSetter>();

        for (int i = 0; i < DiscussionModel.id.length; i++) {
            arraylist.add(new DiscussionModelGegerSetter(
                    DiscussionModel.id[i],
                    DiscussionModel.userProfileImage[i].toString(),
                    DiscussionModel.headermsg.toString(),
                    DiscussionModel.middletext[i].toString(),
                    DiscussionModel.subject[i].toString()

            ));
        }

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences("PrefDoctorSearchKey", MODE_PRIVATE);
        String Specialities = sharedPreferences.getString("specialities", null);
        String Locations = sharedPreferences.getString("location", null);
        GetSearchDoctorData(Locations, Specialities);

        FragmentSpeciality fragmentFeed = new FragmentSpeciality();
        getFragmentManager().beginTransaction().replace(R.id.activity_manageconsults, fragmentFeed).commit();

        host.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String arg0) {
                int currentTab = host.getCurrentTab();
                //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), OrientationHelper.VERTICAL, false);
                if (currentTab == 0) {

                    FragmentSpeciality fragmentFeed = new FragmentSpeciality();
                    getFragmentManager().beginTransaction().replace(R.id.activity_manageconsults, fragmentFeed).commit();
                } else if (currentTab == 1) {


                }

            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private TabHost.TabContentFactory createTabContent() {
        return new TabHost.TabContentFactory() {
            @Override
            public View createTabContent(String tag) {
                rv = new RecyclerView(getApplicationContext());
                rv.setHasFixedSize(true);
                linearLayoutManager = new LinearLayoutManager(getApplicationContext());
                rv.setLayoutManager(linearLayoutManager);
                rv.setItemAnimator(new DefaultItemAnimator());
                rv.setAdapter(adapter);
                linearLayoutManager = (LinearLayoutManager) rv.getLayoutManager();


                rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                        super.onScrollStateChanged(recyclerView, newState);
                    }

                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                        super.onScrolled(recyclerView, dx, dy);
                        int visibleItemCount = linearLayoutManager.getChildCount();
                        int totalItemCount = linearLayoutManager.getItemCount();
                        int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                        if (!isLoading && !isLastPage) {

                            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount &&
                                    firstVisibleItemPosition >= 0 && totalItemCount >= count) {
                                //if (AppHelper.checkNetworkConnection(mActivity)) {
                                isLoading = true;
                                // getDataApi();
                                //}
                            }
                        }
                    }
                });

                return rv;
            }
        };
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ManageConsult.this, MainActivity.class);
        intent.putExtra("position", "1");
        //Clear all activities and start new task
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

    public void GetSearchDoctorData(String location, String keyarray) {
        JSONObject json = new JSONObject();


        try {

            json.put("specialities", keyarray);
            json.put("location", location);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, SearchDoctorInfor, json, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {


                               /* JSONObject jsonObject=new JSONObject();
                                jsonObject=response;*/

                        SharedPreferences preferences = getSharedPreferences("PrefDoctorSearchKey", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.commit();

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }


                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            try {
                                String errormsg = error.toString();
                                Log.d("Error", errormsg);
                        /*if (progressDialog != null) {
                            if (progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                        }*/
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                        }
                    });
            jsonObjectRequest.setTag(REQ_TAG);
            requestQueue.add(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //sendPostRequest();


    }
}

package com.breathandbeats.android.ui.activities.SaveaLife;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.LinearLayout;

import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.RV.SaveALifeLevel2Adapter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.RV.SaveALifeDetailItem;
import com.breathandbeats.android.pojos.api.ContentList;
import com.breathandbeats.android.pojos.api.SaveALifeByCategoryResponse;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SaveALifeLevel2Activity extends AppCompatActivity {
    private static final String TAG = "SaveALife_Activity";
    @BindView(R.id.activity_save_a_life_level2_RV)
    RecyclerView activitySaveALifeLevel2RV;
    @BindView(R.id.activity_save_a_life_level2)
    LinearLayout activitySaveALifeLevel2;
    ArrayList<SaveALifeDetailItem> saveALifeDetailItems = new ArrayList<>();
    BnBApp app;
    SharedPrefHelper sharedPrefHelper;
    BnBService bnBService;
    String authToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_a_life_level2);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        String category = extras.getString("categoryTitle");
        Log.i(TAG, "onCreate: categoty" + category);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_save_a_life_level2_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(category);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);
        Log.i(TAG, "onCreate: ");
        app = BnBApp.getInstance();
        sharedPrefHelper = app.getSharedPrefHelper();
        bnBService = app.getRestClient().getApiSerivice();
        authToken = "Bearer " + sharedPrefHelper.getAuthToken();
        SaveALifeLevel2Adapter saveALifeLevel2Adapter = new SaveALifeLevel2Adapter(saveALifeDetailItems, this);
        activitySaveALifeLevel2RV.setAdapter(saveALifeLevel2Adapter);
        activitySaveALifeLevel2RV.setLayoutManager(new LinearLayoutManager(this));


        bnBService.getSaveALifeItemsByCategory(authToken, category).enqueue(new Callback<SaveALifeByCategoryResponse>() {
            @Override
            public void onResponse(Call<SaveALifeByCategoryResponse> call, Response<SaveALifeByCategoryResponse> response) {
                if (response.body().getSuccess()) {

                    Log.d(TAG," responce success  : ");
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(String.valueOf(new Gson().toJson(response)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG,"jsonObject : "+jsonObject);


                    Log.i(TAG, "onResponse: " + response.body());
                    List<ContentList> contentListItems = response.body().getContentList();
                    Log.i(TAG, "onResponse: " + contentListItems);
                    for (int i = 0; i < contentListItems.size(); i++) {
                        ContentList contentList = contentListItems.get(i);
                        String id = contentList.getId();
                        String category1 = contentList.getCategory();
                        String description = contentList.getDescription();
                        String title = contentList.getTitle();
                        String videoUrl = contentList.getVideoUrl();
                        SaveALifeDetailItem saveALifeDetailItem = new SaveALifeDetailItem(id, title, category1, videoUrl, description);
                        saveALifeDetailItems.add(saveALifeDetailItem);
                    }
                    saveALifeLevel2Adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<SaveALifeByCategoryResponse> call, Throwable t) {
                Log.i(TAG, "onFailure: ");
            }
        });


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

package com.breathandbeats.android.ui.fragments;

import android.Manifest;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.HistoryTripListPojo.CompletedTripListData;
import com.breathandbeats.android.pojos.HistoryTripListPojo.HistoryTripData;
import com.breathandbeats.android.pojos.Logout;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceLater.BookLaterPaymentActivity;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow.DriverSearchingActivity;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow.DriverUserMapViewActivity;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow.TripHistoryAcitivity;
import com.breathandbeats.android.ui.activities.BookAmbulance.CallAmbulanceActivity;
import com.breathandbeats.android.ui.activities.EmergencyContact.EmergencyContactListActivity;
import com.breathandbeats.android.ui.activities.HealthDirectory.HealthDirectoryActivity;
import com.breathandbeats.android.ui.activities.MakeANoiseActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.ProfileActivity;
import com.breathandbeats.android.ui.activities.RequestLogActivity;
import com.breathandbeats.android.ui.activities.SOSActivity;
import com.breathandbeats.android.ui.activities.SOSLogActivity;
import com.breathandbeats.android.ui.activities.SaveaLife.SaveALifeActivity;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.breathandbeats.android.ui.activities.SplashActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rocky on 2/21/2018.
 */

public class DoctorConsultionFragment extends Fragment {
    Context context;
    String TAG = "TripFragment";
    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    String authToken;
    CompletedTripListData completedTripListData_v = new CompletedTripListData();

    private List<HistoryTripData> historyTripData;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        context = getActivity();

        application = (BnBApp) getActivity().getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Call


        sharedPrefHelper = application.getSharedPrefHelper();
        authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();
        Log.d(TAG, "authtoken : " + authToken);


        // Setting ViewPager for each Tabs
        ViewPager viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        // Set Tabs inside Toolbar
        TabLayout tabs = (TabLayout) view.findViewById(R.id.result_tabs);
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);
        tabs.setupWithViewPager(viewPager);
//        tabs.getTabAt(0).select();
        return view;
    }

    private void setupViewPager(ViewPager viewPager) {


    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<android.support.v4.app.Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

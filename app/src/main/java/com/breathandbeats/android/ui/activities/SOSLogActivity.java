package com.breathandbeats.android.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.RV.SOSLogAdapter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.RV.SOSLogItem;
import com.breathandbeats.android.pojos.api.SOSLogResponse;
import com.breathandbeats.android.pojos.api.SosLogResponseData;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SOSLogActivity extends AppCompatActivity {
    private static final String TAG = "SOSLogActivity";

    @BindView(R.id.activity_sos_log_RV)
    RecyclerView activitySosLogRV;
    @BindView(R.id.activity_sos_log)
    RelativeLayout activitySosLog;

    ArrayList<SOSLogItem> sosLogItems;
    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    SOSLogAdapter sosLogAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos_log);
        ButterKnife.bind(this);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_sos_log_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("SOS Logs");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);
        application = (BnBApp) getApplication();
        bnBService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();
        String authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();

        BnBUserUtility.showProgress(SOSLogActivity.this,"Please wait, Getting data...");
        bnBService.getSOSLogs(authToken).enqueue(new Callback<SOSLogResponse>() {
            @Override
            public void onResponse(Call<SOSLogResponse> call, Response<SOSLogResponse> response) {
                Log.i(TAG, "onResponse: ");
                BnBUserUtility.dismissProgressDialog();
                if (response.body().getSuccess()) {
                    BnBUserUtility.dismissProgressDialog();
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(String.valueOf(new Gson().toJson(response)));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG,"jsonObject : "+jsonObject);

                    sosLogAdapter = new SOSLogAdapter(getSOSItems(response.body().getSosLogResponseDataList()), SOSLogActivity.this);
                    activitySosLogRV.setAdapter(sosLogAdapter);
                    activitySosLogRV.setLayoutManager(new LinearLayoutManager(SOSLogActivity.this));

                } else {
                    BnBUserUtility.dismissProgressDialog();
                    Toast.makeText(SOSLogActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SOSLogResponse> call, Throwable t) {
                BnBUserUtility.dismissProgressDialog();
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                        " reach our service at the moment.Please try again.",getApplicationContext());
            }
        });

    }

    private ArrayList<SOSLogItem> getSOSItems(List<SosLogResponseData> data) {
        sosLogItems = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            SosLogResponseData sosLogResponseData = data.get(i);
            String updatedAt = sosLogResponseData.getCreatedAt();
            String sosType = sosLogResponseData.getSosType();
            SOSLogItem sosLogItem = new SOSLogItem(updatedAt, sosType);
            sosLogItems.add(sosLogItem);
        }
        Log.i(TAG, "getSOSItems: " + sosLogItems);
        Log.d(TAG," size : "+data.size());
        return sosLogItems;
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent=new Intent(SOSLogActivity.this,MainActivity.class);
        intent.putExtra("position","1");
        //Clear all activities and start new task
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

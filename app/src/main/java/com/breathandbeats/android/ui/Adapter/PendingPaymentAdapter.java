package com.breathandbeats.android.ui.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.HistoryTripListPojo.CompletedTripListData;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow.PaymentActivity;

import java.io.Serializable;

/**
 * Created by lavanya on 03-11-2017.
 */

public class PendingPaymentAdapter extends RecyclerView.Adapter<PendingPaymentAdapter.ViewHolder> implements Serializable{


    Context context;
    CompletedTripListData completedTripListData=new CompletedTripListData();

    public PendingPaymentAdapter(Context context, CompletedTripListData historyTripData) {

        this.completedTripListData=historyTripData;
        this.context=context;
    }

    @Override
    public PendingPaymentAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.pendingpayment_tripadapter_layout, viewGroup, false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PendingPaymentAdapter.ViewHolder holder, int position) {

        if(completedTripListData.getData().get(position).getTripStatus().equals("completed")) {
            String emergencytype = completedTripListData.getData().get(position).getEmergencyType();
            String HospitalName = completedTripListData.getData().get(position).getHospital().getName();
            String tripstatus = completedTripListData.getData().get(position).getTripStatus();
            String Details = "Trip of ambulance emergency type " + emergencytype + " to the hospital " + HospitalName + " has been completed." + "  Please make a payment. ";

            String datevalue = completedTripListData.getData().get(position).getCreatedAt();

            holder.tvDeatils.setText(Details);
            holder.tvDate.setText(BnBUserUtility.getTheCorrectFormateDate(datevalue));

            holder.btnMakepayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(context, PaymentActivity.class);
//                intent.putExtra("var", (Parcelable) historyTripData);
                    context.startActivity(intent);
                }
            });

        }

    }


    @Override
    public int getItemCount() {
        return completedTripListData.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate;
        TextView tvDeatils;
        Button btnMakepayment;
        public ViewHolder(View itemView) {
            super(itemView);

            tvDeatils=(TextView)itemView.findViewById(R.id.tvDeatils);
            tvDate=(TextView)itemView.findViewById(R.id.tvDate);
            btnMakepayment=(Button)itemView.findViewById(R.id.btnMakepayment);

        }
    }
}


package com.breathandbeats.android.ui.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.HistoryTripListPojo.Rejectedtripdata;
import com.breathandbeats.android.ui.fragments.TripData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by lavanya on 14-12-2017.
 */

public class RejectedTripAdapter extends RecyclerView.Adapter<RejectedTripAdapter.ViewHolder>{


    Context context;
    String TAG="RejectedTripAdapter";
    ArrayList<TripData> tripDatas;
    Rejectedtripdata rejectedtripdata =new Rejectedtripdata();

    public RejectedTripAdapter(Context context, Rejectedtripdata historyTripData) {

        this.rejectedtripdata =historyTripData;
        this.context=context;
    }

    @Override
    public RejectedTripAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rejectededtripadapter_layout, viewGroup, false);
        return  new RejectedTripAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RejectedTripAdapter.ViewHolder holder, int position) {


        if (position % 2 == 1) {
            holder.rlMainlayout.setBackgroundColor(context.getResources().getColor(R.color.backgroundcolor));
        } else {
            holder.rlMainlayout.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

//        if(rejectedtripdata.getData().get(position).getTripStatus().equals("completed"))

        {
            String datevalue= rejectedtripdata.getData().get(position).getCreatedAt().toString();
           holder.tvDate.setText( getmethod(datevalue));

//            holder.tvhospitalname.setText(rejectedtripdata.getData().get(position).getHospital().getName().toString());
            holder.tvambulancetype.setText(rejectedtripdata.getData().get(position).getEmergencyType().toString());
            holder.tvtripstatus.setText(rejectedtripdata.getData().get(position).getTripStatus().toString());
//            holder.tvpaymnetstatus.setText(advancebookdata.getData().get(position).);
        }
        /*else{
            //do nothing
        }*/


    }

    private String  getmethod(String currentDateandTime) {

        Log.d(TAG," currentDateandTime 1: "+currentDateandTime);
        SimpleDateFormat sourceFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        SimpleDateFormat destFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm a"); //here 'a' for AM/PM
        String timeone="- ";

        final Date date;
        try {
            date = sourceFormat.parse(currentDateandTime);
            final Calendar calendar = Calendar.getInstance();

            calendar.setTime(date);
            calendar.add(Calendar.HOUR, 5);
            calendar.add(Calendar.MINUTE, 30);
             timeone=destFormat.format(calendar.getTime());
            System.out.println("Time here " + timeone);
            Log.d(TAG," time 1: "+timeone);
            String formattedDate = destFormat.format(date);
            Log.d(TAG," formattedDate :  "+formattedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeone;


    }


    @Override
    public int getItemCount() {
        return rejectedtripdata.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate;
        TextView tvDeatils;
        TextView tvhospitalname,tvambulancetype,tvtripstatus,tvpaymnetstatus;
        RelativeLayout rlMainlayout;

        public ViewHolder(View itemView) {
            super(itemView);

//            tvDeatils=(TextView)itemView.findViewById(R.id.tvDeatils);
            tvDate=(TextView)itemView.findViewById(R.id.tvDate);
            tvhospitalname=(TextView)itemView.findViewById(R.id.tvhospitalname);
            tvambulancetype=(TextView)itemView.findViewById(R.id.tvambulancetype);
            tvtripstatus=(TextView)itemView.findViewById(R.id.tvtripstatus);
            rlMainlayout=(RelativeLayout)itemView.findViewById(R.id.rlMainlayout);
//            tvpaymnetstatus=(TextView)itemView.findViewById(R.id.tvpaymnetstatus);

        }
    }

}

package com.breathandbeats.android.ui.activities.EmergencyContact;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.RV.EmergencyContactListAdapter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.events.DeleteContactEvent;
import com.breathandbeats.android.pojos.orm.EmergencyContact;
import com.breathandbeats.android.ui.activities.MainActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EmergencyContactListActivity extends AppCompatActivity {
    private static final String TAG = "EmergencyContactListAct";

    @BindView(R.id.activity_emergency_contact_list_RV)
    RecyclerView contactListRV;
    @BindView(R.id.activity_emergency_contact_list)
    LinearLayout contactListLL;
    @BindView(R.id.activity_emergency_contact_list_addContactBtn)
    Button addEmergencyContactBtn;
    @BindView(R.id.rlBackarrow)
    RelativeLayout rlBackarrow;
    ArrayList<EmergencyContact> emergencyContacts = new ArrayList<>();
    BnBApp app;
    SharedPrefHelper sharedPrefHelper;
    DBHelper dbHelper;
    EmergencyContactListAdapter emergencyContactListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_contact_list);
        ButterKnife.bind(this);
        app = BnBApp.getInstance();
        dbHelper = app.getDBHelper();
        /*Toolbar myToolbar = (Toolbar) findViewById(R.id.activity_emergency_contact_list_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Emergency Contacts");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        myToolbar.setTitleTextColor(Color.WHITE);*/

        setupContactList();
        addEmergencyContactBtn.setOnClickListener(view -> startActivity(new Intent(EmergencyContactListActivity.this, EmergencyContactActivity.class)));
    }

   /* @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }*/

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent intent=new Intent(EmergencyContactListActivity.this,MainActivity.class);
        intent.putExtra("position","1");
        //Clear all activities and start new task
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.rlBackarrow)
    public  void  onrlBackarrowClicked()
    {
        onBackPressed();
    }

    private void setupContactList() {
        emergencyContactListAdapter = new EmergencyContactListAdapter(getEmergencyContacts(), this);
        contactListRV.setAdapter(emergencyContactListAdapter);
        contactListRV.setLayoutManager(new LinearLayoutManager(this));
        emergencyContactListAdapter.notifyDataSetChanged();
    }

    private ArrayList<EmergencyContact> getEmergencyContacts() {
        return (ArrayList<EmergencyContact>) dbHelper.getEmergencyContacts();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAcceptedEvent(DeleteContactEvent event) {
        Log.i(TAG, "onChatSelectedEvent: ");
        setupContactList();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}

package com.breathandbeats.android.ui.activities;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.breathandbeats.android.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SOSMapActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "SOSMapActivity";

    GoogleMap googleMap;
    boolean isMapReady;
    Location lastLocation;
    double longitude;
    double latitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sosmap);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.activity_sosmap_map);
        mapFragment.getMapAsync(this);
//
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        isMapReady = true;
        this.googleMap = googleMap;
        Log.i(TAG, "onMapReady: " + isMapReady);

    }
}

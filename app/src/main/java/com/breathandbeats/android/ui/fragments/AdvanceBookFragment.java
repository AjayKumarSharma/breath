package com.breathandbeats.android.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.HistoryTripListPojo.AdvannceBookedTripData;
import com.breathandbeats.android.ui.Adapter.AdvanceBookedTripAdapter;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.google.gson.Gson;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rakesh on 02-11-2017.
 */
public class AdvanceBookFragment extends Fragment implements Serializable {

    Context context;
    private  String TAG="AdvanceBookFragment";
    @BindView(R.id.rvAdvanceTrip)
    RecyclerView rvAdvanceTrip;

    @BindView(R.id.tvnothingtoshow)
    TextView tvnothingtoshow;


    Gson gson;
    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    String authToken;

    AdvannceBookedTripData advannceBookedTripData_v=new AdvannceBookedTripData();
    public AdvanceBookFragment(){
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        //Do not call super class method here.
        //super.onSaveInstanceState(outState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.advancetripbook_layout, container, false);
        context = getActivity();
        ButterKnife.bind((Activity)context);

        rvAdvanceTrip=(RecyclerView)rootView.findViewById(R.id.rvAdvanceTrip);

        rvAdvanceTrip.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvAdvanceTrip.setLayoutManager(layoutManager);

        application = (BnBApp) getActivity().getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Cal

        sharedPrefHelper = application.getSharedPrefHelper();
        authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();
        Log.d(TAG,"authtoken : "+authToken);

        if(NetworkUtil.getConnectivityStatusBoolen(context)){

            BnBUserUtility.showProgress(context,"Please wait, getting data...");
            getAdvancedTripdData(authToken);
        }else{
            Toast.makeText(context, "Please note that you are not connected to internet.", Toast.LENGTH_SHORT).show();
        }

        return  rootView;
    }

    private void getAdvancedTripdData(String authToken) {

        if(NetworkUtil.getConnectivityStatusBoolen(context)){

            Call<AdvannceBookedTripData> advancedtripdata=bnBService.AdvancebookedTripListData(authToken);

            advancedtripdata.enqueue(new Callback<AdvannceBookedTripData>() {
                @Override
                public void onResponse(Call<AdvannceBookedTripData> call, Response<AdvannceBookedTripData> response) {
                    BnBUserUtility.dismissProgressDialog();
                    if(response!=null){
                        BnBUserUtility.dismissProgressDialog();
                        if(response.isSuccessful()){
                            Log.d(TAG,"responce success : ");

                            advannceBookedTripData_v=response.body();
                            if(advannceBookedTripData_v.getSuccess().equals(true)){

                                if(advannceBookedTripData_v.getData()!=null){

                                    if(advannceBookedTripData_v.getData().size()>0){

                                        Log.d(TAG," size : "+advannceBookedTripData_v.getData().size());
                                        AdvanceBookedTripAdapter onGoingTripAdapter = new AdvanceBookedTripAdapter(context,advannceBookedTripData_v);
                                        rvAdvanceTrip.setAdapter(onGoingTripAdapter);
                                    }else{

                                    }
                                }

                            }else if(advannceBookedTripData_v.getSuccess().equals(false)){

                            }
                        }else{
                            BnBUserUtility.dismissProgressDialog();
                        }

                    }else{
                        BnBUserUtility.dismissProgressDialog();
                        Log.d(TAG,"responce null : ");
                        Toast.makeText(context, "Sorry,something went wrong,we can't reach our " +
                                "service at the moment.Please try again.", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<AdvannceBookedTripData> call, Throwable t) {
                    Log.d(TAG,"failure : "+t);
                    BnBUserUtility.dismissProgressDialog();
                    Toast.makeText(context, "Sorry,something went wrong,we can't reach our " +
                            "service at the moment.Please try again.", Toast.LENGTH_SHORT).show();
                }
            });


        }else{
            Toast.makeText(context, "Please note that you are not connected to internet.", Toast.LENGTH_SHORT).show();
        }

    }
}

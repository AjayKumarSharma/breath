package com.breathandbeats.android.ui.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.api.GenericResponse;
import com.breathandbeats.android.pojos.api.InitiateEmailOTPRequest;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmailVerifyFragment extends Fragment {
    private static final String TAG = "EmailVerifyFragment";

    @BindView(R.id.fragment_email_verify_otp_ET)
    EditText fragmentEmailVerifyOtpET;
    @BindView(R.id.fragment_email_btn)
    Button fragmentEmailBtn;
    private BnBApp application;
    private BnBService apiService;
    private SharedPrefHelper sharedPrefHelper;
    private String authToken;

    public EmailVerifyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_email_verify, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        application = (BnBApp) getActivity().getApplication();
        apiService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();

    }

    @OnClick(R.id.fragment_email_btn)
    public void initiateEmailVerify(){

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
        sendInitialEmailOTPRequest("false");
    }

    private void sendInitialEmailOTPRequest(String isResend) {
        authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();
        apiService.initiateEmailOTP(authToken, getEmailOtpRequest(isResend)).enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {

            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {

            }
        });
    }

    private InitiateEmailOTPRequest getEmailOtpRequest(String isResend) {

            InitiateEmailOTPRequest initiateOTPRequest = new InitiateEmailOTPRequest();
            initiateOTPRequest.setResend(isResend);
            return initiateOTPRequest;
        }
    }




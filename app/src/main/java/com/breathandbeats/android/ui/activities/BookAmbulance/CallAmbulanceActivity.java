package com.breathandbeats.android.ui.activities.BookAmbulance;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.BookAmbulanceNowresponce;
import com.breathandbeats.android.pojos.api.UsercurrentLocationInput;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceLater.BookAmbulanceLaterActivity;
import com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow.DriverSearchingActivity;
import com.breathandbeats.android.ui.activities.MainActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CallAmbulanceActivity extends AppCompatActivity {


    @BindView(R.id.llInjuryALS)
    LinearLayout llInjuryALS;
    @BindView(R.id.llInjuryBLS)
    LinearLayout llInjuryBLS;
    @BindView(R.id.llTransportation)
    LinearLayout llTransportation;
    @BindView(R.id.llOthers)
    LinearLayout llOthers;

    @BindView(R.id.llDial108)
    LinearLayout llDial108;

    @BindView(R.id.llCallambulanceNow)
    LinearLayout llCallambulanceNow;

    @BindView(R.id.llCallambulanceLater)
    LinearLayout llCallambulanceLater;

    @BindView(R.id.tvCallambulanceNow)
    TextView tvCallambulanceNow;
    @BindView(R.id.tvCallambulanceLater)
    TextView tvCallambulanceLater;

    @BindView(R.id.ivInjuryALS)
    ImageView ivInjuryALS;
    @BindView(R.id.ivInjuryBLS)
    ImageView ivInjuryBLS;
    @BindView(R.id.ivTransportation)
    ImageView ivTransportation;
    @BindView(R.id.ivOthers)
    ImageView ivOthers;

    @BindView(R.id.ivbooklater)
     ImageView ivbooklater;

    @BindView(R.id.ivbooknow)
    ImageView ivbooknow;

    int PLACE_PICKER_REQUEST = 1;
    private String EmergencyData = "null";
    Dialog dialogCallAmbulanceNow;
    BnBApp application;
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;
    private String TAG = "CallAmbulance_Activity";
    BookAmbulanceNowresponce bookAmbulanceNowresponce_v=new BookAmbulanceNowresponce();
    Dialog dialogForConfirmation;
    String paymnetmode="null";
    static  int CALL=100;
    static  int LOCATION=102;
    private Realm myRealm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_call_ambulance);

        application = (BnBApp) getApplication();

        sharedPrefHelper = application.getSharedPrefHelper();

        ButterKnife.bind(this);
        bnBService = application.getRestClient().getApiSerivice();// for API Call

        getGPSverification();

        defaultEmergencyIcon();
        disableCallambulanceFunction();

        myRealm = Realm.getInstance(CallAmbulanceActivity.this);

    }

    private void getGPSverification() {
        if(isLocationEnabled(this)){
            // do nothing
        }else{

            showTheAlertmessage();
        }
    }

    private void showTheAlertmessage() {

        dialogForConfirmation = new Dialog(this);
        dialogForConfirmation.setContentView(R.layout.dialog_searchaddress);
        dialogForConfirmation.show();

        Button Done_Btn = (Button) dialogForConfirmation.findViewById(R.id.Done_Btn);
        TextView tvTripMessage = (TextView) dialogForConfirmation.findViewById(R.id.tvTripMessage);

        tvTripMessage.setText("Please enable the GPS Service in mobile and try again for booking .");
        dialogForConfirmation.setCancelable(false);

        Done_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogForConfirmation.dismiss();

                Intent callGPSSettingIntent = new Intent(
                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(callGPSSettingIntent);

            }
        });
    }


    private void defaultEmergencyIcon() {

        ivInjuryALS.setImageDrawable(getResources().getDrawable(R.drawable.injuries_icon));
        ivInjuryBLS.setImageDrawable(getResources().getDrawable(R.drawable.medical_services_icon));
        ivTransportation.setImageDrawable(getResources().getDrawable(R.drawable.transporation_icon));
        ivOthers.setImageDrawable(getResources().getDrawable(R.drawable.other_icon));
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent home_intent=new Intent(CallAmbulanceActivity.this,MainActivity.class);
        //Clear all activities and start new task
        home_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home_intent);
    }

    @OnClick(R.id.rlBackarrow)
    public  void  onrlBackarrowclicked(){
//        onBackPressed();
        Intent home_intent=new Intent(CallAmbulanceActivity.this,MainActivity.class);
        //Clear all activities and start new task
        home_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(home_intent);
    }
    @OnClick(R.id.llInjuryALS)
    public void onclick_llInjuryALS() {
        defaultEmergencyIcon();
        ivInjuryALS.setImageDrawable(getResources().getDrawable(R.mipmap.icon_selected));
        EmergencyData = "als";
        enableCallambulanceFunction();
    }


    @OnClick(R.id.llInjuryBLS)
    public void onclick_llInjuryBLS() {
        defaultEmergencyIcon();
        ivInjuryBLS.setImageDrawable(getResources().getDrawable(R.mipmap.icon_selected));
        EmergencyData = "bls";
        enableCallambulanceFunction();
    }

    @OnClick(R.id.llTransportation)
    public void onclick_Transportation() {
        defaultEmergencyIcon();
        ivTransportation.setImageDrawable(getResources().getDrawable(R.mipmap.icon_selected));
        EmergencyData = "transport";
        enableCallambulanceFunction();
    }

    @OnClick(R.id.llOthers)
    public void onclick_Others() {
        defaultEmergencyIcon();
        ivOthers.setImageDrawable(getResources().getDrawable(R.mipmap.icon_selected));
        EmergencyData = "others";
        enableCallambulanceFunction();
    }

    @OnClick(R.id.llCallambulanceNow)
    public void onclick_CallambulanceNow() {

        if (EmergencyData.equalsIgnoreCase("null")) {
            BnBUserUtility.AlertMessage("Please select Ambulance type and try again.",this);
        } else {

            if(NetworkUtil.getConnectivityStatusBoolen(this)) {

                if(isLocationEnabled(this)){

                    if(NetworkUtil.getConnectivityStatusBoolen(CallAmbulanceActivity.this)) {
                        askForPermission(Manifest.permission.CALL_PHONE,CALL);
                        askForPermission(Manifest.permission.ACCESS_FINE_LOCATION,LOCATION);
                        getVerifiactionDialogBox();
                    }else{
                        BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",application);
                    }


                }else{

                    showTheAlertmessage();
                }

            }else{

           BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",application);
            }

        }

    }



    @OnClick(R.id.llCallambulanceLater)
    public void onclick_CallambulanceLater() {

        if (EmergencyData.equalsIgnoreCase("null")) {

            BnBUserUtility.AlertMessage("Please select type of emergency and try again.",this);
        } else {
            Log.d(TAG,"emergencydata : "+EmergencyData);

            if(isLocationEnabled(this)){

                if(NetworkUtil.getConnectivityStatusBoolen(this)) {
                    Intent booklaterintent = new Intent(CallAmbulanceActivity.this, BookAmbulanceLaterActivity.class);
                    booklaterintent.putExtra("EmergencyData", EmergencyData);
                    startActivity(booklaterintent);

                }else{

                    BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",application);
                }

            }else{

                showTheAlertmessage();
            }

        }
    }

    @OnClick(R.id.llDial108)
    public void onclick_Dial108_LL() {
        String No = "108";
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + No));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);

    }


    private void enableCallambulanceFunction() {

        tvCallambulanceNow.setTextColor(getResources().getColor(R.color.textblue));
        tvCallambulanceLater.setTextColor(getResources().getColor(R.color.textblue));

        llCallambulanceNow.setBackground(getResources().getDrawable(R.drawable.rectangular_blue));
        llCallambulanceLater.setBackground(getResources().getDrawable(R.drawable.rectangular_blue));

        ivbooklater.setImageDrawable(getResources().getDrawable(R.mipmap.booklater_after));
        ivbooknow.setImageDrawable(getResources().getDrawable(R.mipmap.booknow_after));
    }

    private void disableCallambulanceFunction() {

        tvCallambulanceNow.setTextColor(getResources().getColor(R.color.lighttext));
        tvCallambulanceLater.setTextColor(getResources().getColor(R.color.lighttext));
    }


    private void getVerificationDialog(String pickupaddress, double pickup_Latitude, double pickUp_Longitude) {
        paymnetmode="null";
        dialogCallAmbulanceNow = new Dialog(this);
        dialogCallAmbulanceNow.setContentView(R.layout.dialog_callambulance_layout);
        dialogCallAmbulanceNow.show();

        Button Done_Btn = (Button) dialogCallAmbulanceNow.findViewById(R.id.Done_Btn);
        Button cancel_Btn = (Button) dialogCallAmbulanceNow.findViewById(R.id.cancel_Btn);
        TextView tvPickupaddress=(TextView)dialogCallAmbulanceNow.findViewById(R.id.tvPickupaddress);

        CardView cashlayout=(CardView)dialogCallAmbulanceNow.findViewById(R.id.cashlayout);
        CardView cardlayout=(CardView)dialogCallAmbulanceNow.findViewById(R.id.cardlayout);

        TextView tvcash=(TextView)dialogCallAmbulanceNow.findViewById(R.id.tvcash);
        TextView tvcard=(TextView)dialogCallAmbulanceNow.findViewById(R.id.tvcard);

        tvPickupaddress.setText(pickupaddress);

        cashlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymnetmode="cash";
                cashlayout.setBackground(getResources().getDrawable(R.drawable.rounded_corner_yellow));
                cardlayout.setBackground(getResources().getDrawable(R.drawable.rectangularcorner));

                tvcash.setTextColor(getResources().getColor(R.color.white));
                tvcard.setTextColor(getResources().getColor(R.color.black));
                SharedPreferenceBnBUtility.setPaymnetmode(CallAmbulanceActivity.this,"cash");
            }
        });
        cardlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymnetmode="online";
                cardlayout.setBackground(getResources().getDrawable(R.drawable.rounded_corner_yellow));
                cashlayout.setBackground(getResources().getDrawable(R.drawable.rectangularcorner));

                SharedPreferenceBnBUtility.setPaymnetmode(CallAmbulanceActivity.this,"online");

                tvcard.setTextColor(getResources().getColor(R.color.white));
                tvcash.setTextColor(getResources().getColor(R.color.black));
            }
        });

        Done_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(paymnetmode.equals("null")){

                    BnBUserUtility.toastMessage_info("Please select the payment mode and try again",CallAmbulanceActivity.this);

                }else{

                    dialogCallAmbulanceNow.dismiss();
                    if(String.valueOf(pickup_Latitude) !=null && String.valueOf(pickUp_Longitude) !=null) {
                        updateCurrentLocationOfUser(pickupaddress, pickup_Latitude, pickUp_Longitude, paymnetmode);
                    }else{
                        BnBUserUtility.toastMessage_info("Unable to get location,Please try again.",CallAmbulanceActivity.this);
                    }
                }


            }
        });

        cancel_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCallAmbulanceNow.dismiss();
            }
        });


    }

    private void showGPSDisabledAlertToUser(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(CallAmbulanceActivity.this);
        alertDialogBuilder.setMessage("GPS is disabled in your device. Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Goto Settings Page To Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                Intent callGPSSettingIntent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(callGPSSettingIntent);
                            }
                        });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    private void updateCurrentLocationOfUser(String pickupaddress, double pickup_Latitude, double pickUp_Longitude,String paymentmode) {

        BnBUserUtility.showProgress(this,"Requesting, Please wait...");

        UpdateUserRequestToHospital(pickup_Latitude,pickUp_Longitude,pickupaddress);

    }



    private void UpdateUserRequestToHospital(double lattitude, double longitude,String pickupaddress) {


//        String token=""+sharedPrefHelper.getAuthToken();
        Log.d(TAG,"TOKEN : "+"Bearer "+sharedPrefHelper.getAuthToken());
        Log.d(TAG,"fcmtoken  : "+sharedPrefHelper.getFCMToken());
        Log.d(TAG,"id  : "+sharedPrefHelper.getUserID());
        Log.d(TAG,"pickupaddress  : "+pickupaddress);
        Log.d(TAG,"paymnet type   : "+SharedPreferenceBnBUtility.getPaymnetmode(CallAmbulanceActivity.this));
        String token="Bearer "+sharedPrefHelper.getAuthToken();

        UsercurrentLocationInput usercurrentLocationInput=new UsercurrentLocationInput();
        usercurrentLocationInput.setEmergencyType(EmergencyData);
        usercurrentLocationInput.setUserId(sharedPrefHelper.getUserID());
        usercurrentLocationInput.setUserAddress(pickupaddress);
        usercurrentLocationInput.setPaymentMode(SharedPreferenceBnBUtility.getPaymnetmode(CallAmbulanceActivity.this));

        List<String> LatLong = new ArrayList<String>();

        LatLong.add(String.valueOf(longitude));
        LatLong.add(String.valueOf(lattitude));

        usercurrentLocationInput.setUserCurrentlocation(LatLong);

        Call<BookAmbulanceNowresponce> bookAmbulanceNowresponceCall=bnBService.BookambulanceNow(token,usercurrentLocationInput);

        bookAmbulanceNowresponceCall.enqueue(new Callback<BookAmbulanceNowresponce>() {
            @Override
            public void onResponse(Call<BookAmbulanceNowresponce> call, Response<BookAmbulanceNowresponce> response) {

              if(response==null){

                  BnBUserUtility.dismissProgressDialog();
//                  Toast.makeText(application, "some problem occured during update,Please try again.", Toast.LENGTH_SHORT).show();
                  BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                          " reach our service at the moment.Please try again.",getApplicationContext());
                  Log.d(TAG,"@responce null  : ");

              }else {
                  if (response.isSuccessful()) {
                      BnBUserUtility.dismissProgressDialog();
                      bookAmbulanceNowresponce_v = response.body();

                      if (bookAmbulanceNowresponce_v.getSuccess().equals(true)) {

                          SharedPreferenceBnBUtility.setNotificationRecived(CallAmbulanceActivity.this,"false");
                          BnBUserUtility.Clear_SharedPreferenceData(CallAmbulanceActivity.this);
                           SharedPreferenceBnBUtility.setrequestedforhospital(CallAmbulanceActivity.this,"true");
                          SharedPreferenceBnBUtility.setNotificationTripID(CallAmbulanceActivity.this,bookAmbulanceNowresponce_v.getData().getId().toString());
                          Intent loadrclass = new Intent(CallAmbulanceActivity.this, DriverSearchingActivity.class);
                          loadrclass.putExtra("Latitude", String.valueOf(lattitude));
                          loadrclass.putExtra("Longitude", String.valueOf(longitude));
                          startActivity(loadrclass);
                          SharedPreferenceBnBUtility.setuserRequestedPointLatitude(CallAmbulanceActivity.this,String.valueOf(lattitude));
                          SharedPreferenceBnBUtility.setuserRequestedPointLongitude(CallAmbulanceActivity.this,String.valueOf(longitude));
                      } else if (bookAmbulanceNowresponce_v.getSuccess().equals(false)) {

//                          Toast.makeText(application, "responce success false", Toast.LENGTH_SHORT).show();
                          Log.d(TAG, "@responce false : " + bookAmbulanceNowresponce_v.getMessage());
                          BnBUserUtility.toastMessage_info(" Not able to request the hospital now, Please try again.",application);
                      } else {

                          BnBUserUtility.toastMessage_info("Not able to request the hospital now, Please try again.",application);
                      }

                  } else {
                      Log.d(TAG, "@responce not successfull : ");
                      BnBUserUtility.dismissProgressDialog();
                      BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't reach our " +
                              "service at the moment.Please try again.", CallAmbulanceActivity.this);
                  }
              }
            }

            @Override
            public void onFailure(Call<BookAmbulanceNowresponce> call, Throwable t) {
                dialogCallAmbulanceNow.dismiss();
                BnBUserUtility.dismissProgressDialog();
                Log.d(TAG," failure : "+t);
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't reach our " +
                        "service at the moment.Please try again.",CallAmbulanceActivity.this);
            }
        });
    }

    //--------------------------------------------

    private void getVerifiactionDialogBox() {

        dialogForConfirmation = new Dialog(this);
        dialogForConfirmation.setContentView(R.layout.dialog_booklater);
        dialogForConfirmation.show();

        Button Done_Btn = (Button) dialogForConfirmation.findViewById(R.id.Done_Btn);
        TextView tvTripMessage = (TextView) dialogForConfirmation.findViewById(R.id.tvTripMessage);

        tvTripMessage.setText("Please Select the pick up address.");

        Done_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogForConfirmation.dismiss();

                if(NetworkUtil.getConnectivityStatusBoolen(CallAmbulanceActivity.this)) {
                    getLocationPicker();
                }else{
                    BnBUserUtility.toastMessage_default("Please note that you are not connected to internet.",application);
                }
            }
        });
    }

    private void getLocationPicker() {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();

        try {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);

            /*    Log.d(TAG, "latlong  : " + place.getLatLng());//get place details here
                Log.d(TAG, "lat: " + place.getLatLng().latitude);//get place details here
                Log.d(TAG, "long : " + place.getLatLng().longitude);//get place details here*/


                double Pickup_Latitude=place.getLatLng().latitude;
                double PickUp_Longitude=place.getLatLng().longitude;

                String PickUpAddress=String.valueOf(place.getAddress());

                SharedPreferenceBnBUtility.setNotificationPatientLatitude(CallAmbulanceActivity.this,String.valueOf(Pickup_Latitude));
                SharedPreferenceBnBUtility.setNotificationPatientLongitude(CallAmbulanceActivity.this,String.valueOf(PickUp_Longitude));

                if(PickUpAddress!=null){

                    if(PickUpAddress.length()>0){

                        getVerificationDialog(PickUpAddress,Pickup_Latitude,PickUp_Longitude);
                    }else{
                        BnBUserUtility.toastMessage_info("Please select pick up address and try again.",application);
                    }
                }else{
                    BnBUserUtility.toastMessage_info("Please select pick up address and try again.",application);
                }


            }
        }
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }


    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(CallAmbulanceActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(CallAmbulanceActivity.this, permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(CallAmbulanceActivity.this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(CallAmbulanceActivity.this, new String[]{permission}, requestCode);
            }
        } else {
//            Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }
}

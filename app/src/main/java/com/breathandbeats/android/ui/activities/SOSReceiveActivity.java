package com.breathandbeats.android.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.breathandbeats.android.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SOSReceiveActivity extends AppCompatActivity {
    private static final String TAG = "SOSReceiveActivity";
    @BindView(R.id.activity_sosreceive_title)
    TextView titleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sosreceive);
        ButterKnife.bind(this);
        Bundle extras = getIntent().getExtras();
        String nameOfSender = extras.getString("nameOfSender");
        ArrayList<Double> locationOfSender = (ArrayList<Double>) extras.get("location");
        Log.i(TAG, "onCreate: name - " + nameOfSender);
        Log.i(TAG, "onCreate: loation - " + locationOfSender);
        titleTV.setText(nameOfSender);


    }

}

package com.breathandbeats.android.ui.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;

import com.breathandbeats.android.Models.ActivityData;
import com.breathandbeats.android.adapters.RV.SpecialityCardAdapter;
import com.breathandbeats.android.Models.DiscussionModel;
import com.breathandbeats.android.Models.DiscussionModelGegerSetter;
import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;

import java.util.ArrayList;

import butterknife.ButterKnife;

/**
 * Created by Rocky on 1/3/2018.
 */


public class FragmentSpeciality extends Fragment {
    RecyclerView mRecyclerView;
    private static ArrayList<DiscussionModelGegerSetter> arraylist;
    Activity mActivity;
    private RecyclerView recyclerView;
    private static RecyclerView.Adapter activityAdapter;
    BnBApp application;
    BnBService apiSerivice;
    SharedPrefHelper sharedPrefHelper;
    BnBService bnBService;
    static String authToken;
    TabHost host;
    private static ArrayList<String> array;
    RecyclerView rv;
    LinearLayoutManager linearLayoutManager;
    RecyclerView.Adapter adapter;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private final int count = 7;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();

        arraylist = new ArrayList<DiscussionModelGegerSetter>();

        for (int i = 0; i < DiscussionModel.id.length; i++) {
            arraylist.add(new DiscussionModelGegerSetter(
                    DiscussionModel.id[i],
                    DiscussionModel.userProfileImage[i].toString(),
                    DiscussionModel.headermsg.toString(),
                    DiscussionModel.middletext[i].toString(),
                    DiscussionModel.subject[i].toString()

            ));
        }

        //mRecyclerView = new RecyclerView(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);

        final View view = inflater.inflate(R.layout.fragment_specielity, container, false);
        ButterKnife.bind(this, view);
        application = (BnBApp) getActivity().getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Call
        sharedPrefHelper = application.getSharedPrefHelper();
        authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();

        mActivity = getActivity();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerSpecielity);

        SpecialityCardAdapter adapter = new SpecialityCardAdapter(mActivity, arraylist);
        mRecyclerView.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mActivity, OrientationHelper.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());


        return view;

    }

}

package com.breathandbeats.android.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.Constants;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

public class AmbulanceTrackingActivity extends AppCompatActivity {
    private static final String TAG = "AmbulanceTrackingActivi";
    BnBApp application;
    Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance_tracking);
        application = (BnBApp)getApplication();
        mSocket = application.getSocket();

        registerListeners();
        mSocket.connect();
    }

    private void registerListeners() {
        deRegisterListeners();
    }

    private void deRegisterListeners() {
        mSocket.on(Socket.EVENT_CONNECT,onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT,onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(Constants.CURRENT_LOCATION, onGettingLocation);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mSocket.disconnect();

        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off(Constants.CURRENT_LOCATION, onGettingLocation);

    }

    private Emitter.Listener onGettingLocation = args -> {
        Log.i(TAG, "call: ongettinglocation");
        Log.i(TAG, "call: ");
    };

    private Emitter.Listener onConnect = args -> {
        Log.i(TAG, "call: onconnect");
        Log.i(TAG, "call: ");
    };

    private Emitter.Listener onDisconnect = args -> {
        Log.i(TAG, "call: ondisconnect");
        Log.i(TAG, "call: ");
    };

    private Emitter.Listener onConnectError = args -> {
        Log.i(TAG, "call: onconnecterror");
        Log.i(TAG, "call: ");
    };
}

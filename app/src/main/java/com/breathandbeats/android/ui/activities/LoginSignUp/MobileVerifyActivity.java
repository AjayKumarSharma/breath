package com.breathandbeats.android.ui.activities.LoginSignUp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.api.GenericResponse;
import com.breathandbeats.android.pojos.api.InitiateOTPRequest;
import com.breathandbeats.android.pojos.api.VerifyOTPRequest;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.MainActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MobileVerifyActivity extends AppCompatActivity {
    private static final String TAG = "MobileVerifyActivity";

    @BindView(R.id.activity_mobile_verify_pinET)
    EditText activityMobileVerifyPinET;
    @BindView(R.id.activity_mobile_verify_sendOTP_btn)
    Button activityMobileVerifySendOTPBtn;
    @BindView(R.id.activity_mobile_verify_phoneNum_TV)
    TextView activityMobileVerifyPhoneNumTV;
    @BindView(R.id.activity_mobile_verify_resendOTP_TV)
    TextView activityMobileVerifyResendOTPBtn;

    private BnBApp application;
    private BnBService apiService;
    private SharedPrefHelper sharedPrefHelper;
    private String authToken;
    GenericResponse genericResponse_v=new GenericResponse();
    String phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_verify);
        ButterKnife.bind(this);
        application = (BnBApp) getApplication();
        apiService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();
        phoneNumber = sharedPrefHelper.getUserData().getPhoneNumber();
        activityMobileVerifyPhoneNumTV.setText(phoneNumber);
        sendInitiateOTPRequest("false");
    }

    @OnClick({R.id.activity_mobile_verify_sendOTP_btn, R.id.activity_mobile_verify_resendOTP_TV})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.activity_mobile_verify_sendOTP_btn:
                sendOTP();
                break;
            case R.id.activity_mobile_verify_resendOTP_TV:
                if(NetworkUtil.getConnectivityStatusBoolen(MobileVerifyActivity.this)) {
                    sendInitiateOTPRequest("true");
                }else {
                    BnBUserUtility.toastMessage_error("Please note that you are not connected to internet.",MobileVerifyActivity.this);
                }
                break;
        }
    }

    private void sendInitiateOTPRequest(String isResend) {
        BnBUserUtility.showProgress(MobileVerifyActivity.this,"Please wait...");
        authToken = "Bearer " + sharedPrefHelper.getAuthToken();
        Log.d(TAG," auth token : "+authToken);
        apiService.initiateOTP(authToken, getOtpRequest(isResend)).enqueue(new Callback<GenericResponse>() {
            @Override
            public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                Log.d(TAG, "onResponse: " + response.body().getSuccess());
                Log.d(TAG, "onResponse message : " + response.body().getMessage());

                BnBUserUtility.dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<GenericResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                BnBUserUtility.dismissProgressDialog();
            }
        });
    }

    private InitiateOTPRequest getOtpRequest(String isResend) {
        InitiateOTPRequest initiateOTPRequest = new InitiateOTPRequest();
        initiateOTPRequest.setResend(isResend);
        return initiateOTPRequest;
    }

    public void sendOTP() {
        Log.i(TAG, "sendOTP: ");
        String verificationCode = activityMobileVerifyPinET.getText().toString();
        authToken = "Bearer " + sharedPrefHelper.getAuthToken();
        Log.d(TAG," auth token v : "+authToken);
        boolean isCredentialValid = validateCredentials(verificationCode);
        if (isCredentialValid) {
            apiService.verifyOTP(authToken, getVerifyOTPRequest(verificationCode)).enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {

                    if(response!=null) {
                        if (response.isSuccessful()) {
                            Log.d(TAG, "success ");

                            genericResponse_v=response.body();
                            if(genericResponse_v.getSuccess().toString().equals("true")){

                                sharedPrefHelper.setLoggedIn(true);
                                sharedPrefHelper.setPhoneVerified(true);
                                Intent mainIntent = new Intent(MobileVerifyActivity.this, MainActivity.class);
                                mainIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(mainIntent);
                                finish();

                            }else if(genericResponse_v.getSuccess().toString().equals("false")){

                                BnBUserUtility.toastMessage_warning(genericResponse_v.getMessage().toString(),MobileVerifyActivity.this);
                            }

                        } else {
                            Toast.makeText(MobileVerifyActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }else{
                        BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                " reach our service at the moment.Please try again.",getApplicationContext());
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure: ");
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                            " reach our service at the moment.Please try again.",getApplicationContext());
                }
            });
        }

    }

    private VerifyOTPRequest getVerifyOTPRequest(String verificationCode) {
        VerifyOTPRequest verifyOTPRequest = new VerifyOTPRequest(verificationCode);
        return verifyOTPRequest;
    }

    private boolean validateCredentials(String verificationCode) {
        if (verificationCode.isEmpty()) {
            activityMobileVerifyPinET.setError("PIN Cannot be empty");
            return false;
        }
        return true;

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}

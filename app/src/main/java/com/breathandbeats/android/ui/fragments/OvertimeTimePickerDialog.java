package com.breathandbeats.android.ui.fragments;

import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Build;
import android.widget.TimePicker;

/**
 * Created by innovative on 6/24/13.
 */
public class OvertimeTimePickerDialog extends TimePickerDialog
{
    private int TIME_PICKER_INTERVAL = 1;

    public OvertimeTimePickerDialog(Context context, TimePickerDialog.OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView)
    {
        super(context, callBack, hourOfDay, minute, is24HourView);
        TIME_PICKER_INTERVAL = 5;
    }

    public OvertimeTimePickerDialog(Context context, int theme, TimePickerDialog.OnTimeSetListener callBack, int hourOfDay, int minute, boolean is24HourView)
    {
        super(context, theme, callBack, hourOfDay, minute, is24HourView);
        TIME_PICKER_INTERVAL = 5;
    }

    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute)
    {
        if (minute % TIME_PICKER_INTERVAL != 0)
        {
            int minuteFloor = minute - (minute % TIME_PICKER_INTERVAL);
            minute = minuteFloor + (minute == minuteFloor + 1 ? TIME_PICKER_INTERVAL : 0);
            if (minute == 60)
                minute = 0;
        }
        if (Build.VERSION.SDK_INT > 10)
        {
            updateTime(hourOfDay, minute);
        }

        // super.onTimeChanged(view, hourOfDay, minute);

    }
}

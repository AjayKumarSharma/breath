package com.breathandbeats.android.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.RV.RecieveRequestLogAdapter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.RV.RecieveRequestLogItem;
import com.breathandbeats.android.pojos.events.AcceptedEvent;
import com.breathandbeats.android.pojos.orm.RecieveRequest;
import com.breathandbeats.android.utils.Mappers;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReceivedRequestLogFragment extends Fragment {
    private static final String TAG = "ReceivedRequestLogFragm";
    RecyclerView rv;
    RecieveRequestLogAdapter recieveRequestLogAdapter;
    DBHelper dbHelper;
    BnBApp app;
    String userId;
    SharedPrefHelper sharedPrefHelper;
    private EventBus bus = EventBus.getDefault();


    public ReceivedRequestLogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_received_request_log, container, false);
        app = (BnBApp) getActivity().getApplication();
        dbHelper = app.getDBHelper();
        sharedPrefHelper = app.getSharedPrefHelper();
        userId = sharedPrefHelper.getUserData().getId();
        rv = (RecyclerView) v.findViewById(R.id.fragment_recieved_reqeuest_log_RV);
        recieveRequestLogAdapter = new RecieveRequestLogAdapter(getActivity(), getRecievedRequestLogs());
        Log.i(TAG, "onCreateView: " + getRecievedRequestLogs());
        rv.setAdapter(recieveRequestLogAdapter);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        Log.i(TAG, "onCreateView: ");
        return v;
    }

    private ArrayList<RecieveRequestLogItem> getRecievedRequestLogs() {
        List<RecieveRequest> recieveRequests = dbHelper.getRecieveRequests();
        Log.i(TAG, "getRecievedRequestLogs: " + recieveRequests);
        return Mappers.getRecievedRequestLogItems(recieveRequests);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume: ");
//        ArrayList<RecieveRequestLogItem> recievedRequestLogs = getRecievedRequestLogs();
        Log.i(TAG, "onResume: " + getRecievedRequestLogs());
        setUpRecievedRequestLogs();
    }

    private void setUpRecievedRequestLogs() {
        recieveRequestLogAdapter = new RecieveRequestLogAdapter(getActivity(), getRecievedRequestLogs());
        rv.setAdapter(recieveRequestLogAdapter);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        recieveRequestLogAdapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAcceptedEvent(AcceptedEvent event) {
        Log.i(TAG, "onChatSelectedEvent: ");
        setUpRecievedRequestLogs();
    }


    @Override
    public void onStart() {
        super.onStart();
        bus.register(this);
    }


    @Override
    public void onStop() {
        bus.unregister(this);
        super.onStop();
    }

}

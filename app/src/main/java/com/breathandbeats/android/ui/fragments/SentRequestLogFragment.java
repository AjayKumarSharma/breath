package com.breathandbeats.android.ui.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.breathandbeats.android.R;
import com.breathandbeats.android.adapters.SentRequestLogAdapter;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.RV.SentRequestLogItem;
import com.breathandbeats.android.pojos.orm.SentRequest;
import com.breathandbeats.android.utils.Mappers;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class SentRequestLogFragment extends Fragment {
    private static final String TAG = "SentRequestLogFragment";

    @BindView(R.id.fragment_sent_request_log_RV)
    RecyclerView fragmentSentRequestLogRV;
    SentRequestLogAdapter sentRequestAdapter;
    SharedPrefHelper sharedPrefHelper;
    DBHelper dbHelper;
    BnBApp app;
    private String name;

    public SentRequestLogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sent_request_log, container, false);
        ButterKnife.bind(this, view);
        app = (BnBApp) getActivity().getApplication();
        sharedPrefHelper = app.getSharedPrefHelper();
        dbHelper = app.getDBHelper();
        name = sharedPrefHelper.getName();
        return view;
    }

    private ArrayList<SentRequestLogItem> getSentRequestLogs() {
        List<SentRequest> sentRequests = dbHelper.getSentRequests();
        Log.i(TAG, "getSentRequestLogs: sent reqs" + sentRequests);//dbdata
        ArrayList<SentRequestLogItem> sentRequestLogItems = Mappers.getSentRequestLogItems(sentRequests, name);
        Log.i(TAG, "getSentRequestLogs: " + sentRequestLogItems);
        return sentRequestLogItems;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupSentRequests();
    }

    private void setupSentRequests() {
        sentRequestAdapter = new SentRequestLogAdapter(getActivity(), getSentRequestLogs());
        fragmentSentRequestLogRV.setAdapter(sentRequestAdapter);
        fragmentSentRequestLogRV.setLayoutManager(new LinearLayoutManager(getActivity()));
        sentRequestAdapter.notifyDataSetChanged();
    }

}

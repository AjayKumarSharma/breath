package com.breathandbeats.android.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.HistoryTripListPojo.CompletedTripListData;
import com.breathandbeats.android.ui.Adapter.PendingPaymentAdapter;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.google.gson.Gson;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by lavanya on 03-11-2017.
 */

public class PendingPaymentTripFragment extends Fragment implements Serializable  {

    Context context;
    private  String TAG="PendingPayment_Trip";
    @BindView(R.id.rvpaymentTrip)
    RecyclerView rvpaymentTrip;
    Gson gson;


    CompletedTripListData completedTripListData_v=new CompletedTripListData();

    public  PendingPaymentTripFragment(){
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.payment_layout, container, false);
        context = getActivity();
        ButterKnife.bind((Activity)context);

        rvpaymentTrip=(RecyclerView)rootView.findViewById(R.id.rvpaymentTrip);

        rvpaymentTrip.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        rvpaymentTrip.setLayoutManager(layoutManager);

        if(!SharedPreferenceBnBUtility.getTriphistory(context).equals("null")) {
            gson = new Gson();
            String notifData = SharedPreferenceBnBUtility.getTriphistory(context);
            CompletedTripListData completedtripdata = gson.fromJson(notifData, CompletedTripListData.class);


            if (completedtripdata.getData() != null) {

                if (completedtripdata.getData().size() > 0) {
                    PendingPaymentAdapter PendingPaymentAdapter = new PendingPaymentAdapter(context, completedtripdata);
                    rvpaymentTrip.setAdapter(PendingPaymentAdapter);
                }
            }

        }


       /* application = (BnBApp) getActivity().getApplication();
        bnBService = application.getRestClient().getApiSerivice();// for API Cal

        sharedPrefHelper = application.getSharedPrefHelper();
        authToken = "Bearer " + sharedPrefHelper.getUserData().getToken();
        Log.d(TAG,"authtoken : "+authToken);*/


        /*if(NetworkUtil.getConnectivityStatusBoolen(context)){

            BnBUserUtility.showProgress(context,"Please wait, getting data...");
            getPendingPaymentTripdData(authToken);
        }else{
            Toast.makeText(context, "Please note that you are not connected to internet.", Toast.LENGTH_SHORT).show();
        }*/
//



        return  rootView;
    }

  /*  private void getPendingPaymentTripdData(String authToken) {

        if(NetworkUtil.getConnectivityStatusBoolen(context)){

            Call<CompletedTripListData> completedTripListDataCall=bnBService.CompletedTripListData(authToken);

            completedTripListDataCall.enqueue(new Callback<CompletedTripListData>() {
                @Override
                public void onResponse(Call<CompletedTripListData> call, Response<CompletedTripListData> response) {

                    BnBUserUtility.dismissProgressDialog();
                    if(response!=null){
                        BnBUserUtility.dismissProgressDialog();
                        if(response.isSuccessful()){
                            Log.d(TAG,"responce success : ");
                            rejectedtripdata=response.body();

                            if(rejectedtripdata!=null) {
                                if (rejectedtripdata.getSuccess().equals(true)) {

                                    if (rejectedtripdata.getData() != null) {

                                        if (rejectedtripdata.getData().size() > 0) {

                                            PendingPaymentAdapter PendingPaymentAdapter = new PendingPaymentAdapter(context, rejectedtripdata);
                                            rvpaymentTrip.setAdapter(PendingPaymentAdapter);
                                        }
                                    }


                                } else if (rejectedtripdata.getSuccess().equals(false)) {

                                }
                            }
                        }else{
                            BnBUserUtility.dismissProgressDialog();
                        }

                    }else{
                        BnBUserUtility.dismissProgressDialog();
                        Log.d(TAG,"responce null : ");
                        Toast.makeText(context, "Sorry,something went wrong,we can't reach our " +
                                "service at the moment.Please try again.", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<CompletedTripListData> call, Throwable t) {
                    Log.d(TAG,"failure : "+t);
                    Toast.makeText(context, "Sorry,something went wrong,we can't reach our " +
                            "service at the moment.Please try again.", Toast.LENGTH_SHORT).show();
                }
            });


        }else{
            Toast.makeText(context, "Please note that you are not connected to internet.", Toast.LENGTH_SHORT).show();
        }

    }*/
}

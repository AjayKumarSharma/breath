package com.breathandbeats.android.ui.activities.BookAmbulance.BookAmbulanceNow;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.breathandbeats.android.DBHelper.TripCollection;
import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.CancelTheTripRequested;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Canceltrip;
import com.breathandbeats.android.pojos.DriverCordinate.DriverCoOrdinateResponce;
import com.breathandbeats.android.services.BnBFirebaseMessagingService;
import com.breathandbeats.android.ui.activities.BnBUserUtility;
import com.breathandbeats.android.ui.activities.MainActivity;
import com.breathandbeats.android.ui.activities.NetworkUtil;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Rakesh on 12-10-2017.
 */

public class DriverUserMapViewActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private String TAG = "DriverUserMap_View";
    BnBService bnBService;
    SharedPrefHelper sharedPrefHelper;

    @BindView(R.id.tvlocation)
    TextView tvlocation;
    @BindView(R.id.tvCancelRequest)
    TextView tvCancelRequest;
    @BindView(R.id.rlBackbarrow)
    RelativeLayout rlBackbarrow;

    @BindView(R.id.tvhospiatladdress)
     TextView tvhospiatladdress;

    BnBApp application;
    Dialog dialogTripAlert;
    CancelTheTripRequested cancelTheTripRequested_V = new CancelTheTripRequested();
    DriverCoOrdinateResponce driverCoOrdinateResponce_v=new DriverCoOrdinateResponce();
    private Realm myRealm;
    double Driver_Latitude;
    double Driver_Longitude;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance_user_locationview);

        ButterKnife.bind(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapview);

        mapFragment.getMapAsync(DriverUserMapViewActivity.this);
        application = (BnBApp) getApplication();
        bnBService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();

        myRealm = Realm.getInstance(DriverUserMapViewActivity.this);
        if(!SharedPreferenceBnBUtility.getNotificationTripID(DriverUserMapViewActivity.this).equals("null")){
            getAllUsers(SharedPreferenceBnBUtility.getNotificationTripID(DriverUserMapViewActivity.this));
        }
        if(SharedPreferenceBnBUtility.getAdvanceTrip(DriverUserMapViewActivity.this).equals("true")){
            tvCancelRequest.setVisibility(View.GONE);
            getAllUsers(SharedPreferenceBnBUtility.getNotificationTripID(DriverUserMapViewActivity.this));
        }else if(SharedPreferenceBnBUtility.getAdvanceTrip(DriverUserMapViewActivity.this).equals("false")){
            tvCancelRequest.setVisibility(View.VISIBLE);
        }else{
            tvCancelRequest.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        LatLng Driver_LatLong = null;

        if(SharedPreferenceBnBUtility.getDriverLatitude(this)!=null) {
            if(!SharedPreferenceBnBUtility.getDriverLatitude(this).equals("null")) {
                Driver_Latitude = Double.parseDouble(SharedPreferenceBnBUtility.getDriverLatitude(this));
            }
        }
        if(SharedPreferenceBnBUtility.getDriverLongitude(this)!=null) {
            if(!SharedPreferenceBnBUtility.getDriverLongitude(this).equals("null")) {
                Driver_Longitude = Double.parseDouble(SharedPreferenceBnBUtility.getDriverLongitude(this));
                Driver_LatLong = new LatLng(Driver_Latitude, Driver_Longitude);
            }
        }
//        LatLng Patient_Latlong = new LatLng(Patient_Latitude, Patient_Longitude);


        Log.d(TAG," lat long : "+Driver_Latitude+ " :  "+Driver_Longitude);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        if(Driver_LatLong!=null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Driver_LatLong, 13));
        }

        if(Driver_LatLong!=null) {
            setUpMap(Driver_LatLong, mMap, "source");
        }
//        setUpMap(Patient_Latlong, mMap, "destination");

        String Address = (SharedPreferenceBnBUtility.getNotificationHospitalName(DriverUserMapViewActivity.this)) ;
        tvlocation.setText(Address);
        tvhospiatladdress.setText(SharedPreferenceBnBUtility.getNotificationHospitalAddress(DriverUserMapViewActivity.this));

        myRealm = Realm.getInstance(DriverUserMapViewActivity.this);
        if (SharedPreferenceBnBUtility.getNotificationTripCompleted(this).equals("true")) {
            getVerifiactionDialogBox();

        } else if (SharedPreferenceBnBUtility.getNotificationTripCompleted(this).equals("false")) {
            //do nothing
        } else {
            //do nothing
        }

        if(SharedPreferenceBnBUtility.getAdvanceTrip(DriverUserMapViewActivity.this).equals("true")){
            tvCancelRequest.setVisibility(View.GONE);
            getAllUsers(SharedPreferenceBnBUtility.getNotificationTripID(DriverUserMapViewActivity.this));
        }else if(SharedPreferenceBnBUtility.getAdvanceTrip(DriverUserMapViewActivity.this).equals("false")){
            tvCancelRequest.setVisibility(View.VISIBLE);
        }else{
            tvCancelRequest.setVisibility(View.VISIBLE);
        }


    }


    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(broadcastReceiver, new IntentFilter(BnBFirebaseMessagingService.BROADCAST_ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        registerReceiver(broadcastReceiver, new IntentFilter(BnBFirebaseMessagingService.BROADCAST_ACTION));
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (SharedPreferenceBnBUtility.getNotificationTripCompleted(this).equals("true")) {
            getVerifiactionDialogBox();

        } else if (SharedPreferenceBnBUtility.getNotificationTripCompleted(this).equals("false")) {
            //do nothing
        } else {
            //do nothing
        }
    }

    private void setUpMap(final LatLng markerLatLng, final GoogleMap mMap, String target) {

        View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);

        ImageView ivPlaceholder = (ImageView) marker.findViewById(R.id.ivPlaceholder);

        if (target.equals("source")) {
            ivPlaceholder.setImageDrawable(getResources().getDrawable(R.mipmap.placeholder_greencolor));
        } else if (target.equals("destination")) {
            ivPlaceholder.setImageDrawable(getResources().getDrawable(R.mipmap.placeholder_redcolor));
        }

        mMap.addMarker(new MarkerOptions()
                .position(markerLatLng)
                .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));

        final View mapView = getSupportFragmentManager().findFragmentById(R.id.mapview).getView();
        if (mapView.getViewTreeObserver().isAlive()) {
            mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressLint("NewApi")
                // We check which build version we are using.
                @Override
                public void onGlobalLayout() {
                    LatLngBounds bounds = new LatLngBounds.Builder().include(markerLatLng).build();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
//                   mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 13));
                }
            });
        }
    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    //-----------------------------------------------

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            intent.getAction();

            if (intent.hasExtra("tripcompleted")) {
                intent.getStringExtra("tripcompleted");

                Log.d(TAG, "tripcompleted: " + intent.getStringExtra("tripcompleted"));

                if (intent.getStringExtra("tripcompleted").equals("true")) {
                    tvCancelRequest.setVisibility(View.GONE);
                    getVerifiactionDialogBox();

                }
            }
        }
    };

    @OnClick(R.id.llHospitalNumber)
    public void onllHospitalNumberClicked() {

        String HospitalNo = SharedPreferenceBnBUtility.getNotificationHospitalNumber(this);
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + HospitalNo));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }

    @OnClick(R.id.llCallDriver)
    public void onllCallDriverClicked() {


        String DriverNo = SharedPreferenceBnBUtility.getDriverNumber(this);
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + DriverNo));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(intent);
    }
    @OnClick(R.id.btnRefreshMap)
    public  void onbtnRefreshMapClicked(){

//        Toast.makeText(application, "updating...", Toast.LENGTH_SHORT).show();
        if(NetworkUtil.getConnectivityStatusBoolen(this)){
            BnBUserUtility.showProgress(this,"Please wait, updating the location...");
            getTheLocationUpdate();
        }else{
            BnBUserUtility.toastMessage_info("Please note that you are not connected to internet.",this);
        }

    }

    private void getTheLocationUpdate() {


        String HospitalId=SharedPreferenceBnBUtility.getDriverHospitalID(this);
        Call<DriverCoOrdinateResponce> driverCoOrdinateResponceCall=bnBService.DriverCoOrdinate(HospitalId);

        driverCoOrdinateResponceCall.enqueue(new Callback<DriverCoOrdinateResponce>() {
            @Override
            public void onResponse(Call<DriverCoOrdinateResponce> call, Response<DriverCoOrdinateResponce> response) {
                if(response!=null){
                    BnBUserUtility.dismissProgressDialog();
                    if(response.isSuccessful()){

                     driverCoOrdinateResponce_v=response.body();
                    if(driverCoOrdinateResponce_v!=null) {

                        if(driverCoOrdinateResponce_v.getSuccess().toString().equals("true")) {
                            double Driver_Latitude = Double.parseDouble(driverCoOrdinateResponce_v.getData().getDriverCurrentCoordinates().get(1).toString());
                            double Driver_Longitude = Double.parseDouble(driverCoOrdinateResponce_v.getData().getDriverCurrentCoordinates().get(0).toString());
                            LatLng Driver_LatLong = new LatLng(Driver_Latitude, Driver_Longitude);
                            updateLocationOfDriverAndCustomerOnMap(Driver_LatLong);

                            Log.d(TAG, " responce success : " + driverCoOrdinateResponce_v.getSuccess());
                            Log.d(TAG, " lat " + driverCoOrdinateResponce_v.getData().getDriverCurrentCoordinates().get(0).toString());
                        }else if(driverCoOrdinateResponce_v.getSuccess().toString().equals("false")){
                            if(driverCoOrdinateResponce_v.getMessage()!=null) {
                                BnBUserUtility.toastMessage_error(driverCoOrdinateResponce_v.getMessage().toString(), DriverUserMapViewActivity.this);
                            }
                            }
                    }else{
                        BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                " reach our service at the moment.Please try again.",getApplicationContext());
                    }
                    }else{
                        BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                " reach our service at the moment.Please try again.",getApplicationContext());
                    }
                }else{
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                            " reach our service at the moment.Please try again.",getApplicationContext());
                }
            }

            @Override
            public void onFailure(Call<DriverCoOrdinateResponce> call, Throwable t) {
                Log.d(TAG," @failure : "+t);
                BnBUserUtility.dismissProgressDialog();
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                        " reach our service at the moment.Please try again.",getApplicationContext());
            }
        });

    }

    private void getVerifiactionDialogBox() {

        dialogTripAlert = new Dialog(this);
        dialogTripAlert.setContentView(R.layout.dialog_tripalert);
        dialogTripAlert.show();

        dialogTripAlert.setCancelable(false);

        Button Done_Btn = (Button) dialogTripAlert.findViewById(R.id.Done_Btn);
        TextView tvTripMessage = (TextView) dialogTripAlert.findViewById(R.id.tvTripMessage);

        if(SharedPreferenceBnBUtility.getAdvanceTrip(DriverUserMapViewActivity.this).equals("true")){

            tvTripMessage.setText("Your trip has been completed successfully, Thank you for choosing Breath and Beats.");

        }else if(SharedPreferenceBnBUtility.getAdvanceTrip(DriverUserMapViewActivity.this).equals("false")){
            tvTripMessage.setText("Your trip has been completed successfully, Please make a payment of trip.");
        }else{
            tvTripMessage.setText("Your trip has been completed successfully, Please make a payment of trip.");
        }


        Done_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogTripAlert.dismiss();

                BnBUserUtility.Clear_notification(DriverUserMapViewActivity.this);

                if(SharedPreferenceBnBUtility.getAdvanceTrip(DriverUserMapViewActivity.this).equals("true")){


                    updateTripDataTODB(SharedPreferenceBnBUtility.getNotificationTripID(DriverUserMapViewActivity.this));

                    BnBUserUtility.Clear_notification(DriverUserMapViewActivity.this);
                    BnBUserUtility.Clear_SharedPreferenceData(DriverUserMapViewActivity.this);

                    SharedPreferenceBnBUtility.setAdvanceTrip(DriverUserMapViewActivity.this,"false");

                    BnBUserUtility.toastMessage_success("Please wait...",DriverUserMapViewActivity.this);


                    Intent driverUserIntent=new Intent(DriverUserMapViewActivity.this,MainActivity.class);
                    //Clear all activities and start new task
                    driverUserIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(driverUserIntent);

                }else if(SharedPreferenceBnBUtility.getAdvanceTrip(DriverUserMapViewActivity.this).equals("false")){
                    Intent driverUserIntent=new Intent(DriverUserMapViewActivity.this,PaymentActivity.class);
                    startActivity(driverUserIntent);
                }else{
                    Intent driverUserIntent=new Intent(DriverUserMapViewActivity.this,PaymentActivity.class);
                    startActivity(driverUserIntent);
                }

            }
        });
    }

    @OnClick(R.id.rlBackbarrow)
    public  void  onrlBackbarrowClicked(){

        BnBUserUtility.Clear_SharedPreferenceData(this);
        Intent intent=new Intent(DriverUserMapViewActivity.this,MainActivity.class);
        //Clear all activities and start new task
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.tvCancelRequest)
    public  void ontvCancelRequestClicked(){

        if (SharedPreferenceBnBUtility.getNotificationTripCompleted(this).equals("true")) {
            getVerifiactionDialogBox();
            BnBUserUtility.toastMessage_info("Please make a payment.",DriverUserMapViewActivity.this);

        } else if (SharedPreferenceBnBUtility.getNotificationTripCompleted(this).equals("false")) {
            //do nothing
            getTheCancelRequestFunction();
        } else {
            //do nothing
            getTheCancelRequestFunction();
        }

    }

    private void getTheCancelRequestFunction() {

        if(NetworkUtil.getConnectivityStatusBoolen(this)) {

            updateTheRequestToHospital();
        }else{
            BnBUserUtility.toastMessage_default("Please note that you are not connected to ineternet.",application);
        }
    }

    private void updateTheRequestToHospital() {
        BnBUserUtility.showProgress(this,"Requesting, Please wait...");

        String token="Bearer "+sharedPrefHelper.getAuthToken();

        Canceltrip canceltrip=new Canceltrip();

        canceltrip.setTripId(SharedPreferenceBnBUtility.getNotificationTripID(this));
        Call<CancelTheTripRequested> cancelTheTripRequestedCall=bnBService.CancelTripRequested(token,canceltrip);

        cancelTheTripRequestedCall.enqueue(new Callback<CancelTheTripRequested>() {
            @Override
            public void onResponse(Call<CancelTheTripRequested> call, Response<CancelTheTripRequested> response) {

                if(response!=null) {
                    BnBUserUtility.dismissProgressDialog();
                    if (response.isSuccessful()) {
                        Log.d(TAG, "Responce: success " + response.body());

                        cancelTheTripRequested_V=response.body();

                        if(cancelTheTripRequested_V.getSuccess().equals(true)){

                            BnBUserUtility.toastMessage_success("Trip request cancelled successfully.",DriverUserMapViewActivity.this);

                            SharedPreferenceBnBUtility.setNotificationTripStarted(DriverUserMapViewActivity.this,"false");
                            SharedPreferenceBnBUtility.setNotificationTripCompleted(DriverUserMapViewActivity.this,"false");
                            SharedPreferenceBnBUtility.setNotificationTripStatus(DriverUserMapViewActivity.this,"false");
                            SharedPreferenceBnBUtility.setNotificationTripvalue(DriverUserMapViewActivity.this,"Noting to show!");
                            SharedPreferenceBnBUtility.setNotificationRecived(DriverUserMapViewActivity .this,"false");
                            BnBUserUtility.Clear_notification(DriverUserMapViewActivity.this);
                            BnBUserUtility.Clear_SharedPreferenceData(DriverUserMapViewActivity.this);
                            Intent intent=new Intent(DriverUserMapViewActivity.this,MainActivity.class);
                            //Clear all activities and start new task
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        } else if (cancelTheTripRequested_V.getSuccess().equals(false)) {

                            BnBUserUtility.toastMessage_info("Unable to cancel Trip request ,Please try again.",DriverUserMapViewActivity.this);
                        }

                    } else {
                        Log.d(TAG, "responce not successfull : ");
                        BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                                " reach our service at the moment.Please try again.", getApplicationContext());
                    }
                }else{
                    BnBUserUtility.dismissProgressDialog();
                    Log.d(TAG, "Responce: null" );
                    BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                            " reach our service at the moment.Please try again.", getApplicationContext());
                }
            }

            @Override
            public void onFailure(Call<CancelTheTripRequested> call, Throwable t) {
                Log.d(TAG,"FAILURE : "+ t);
                BnBUserUtility.dismissProgressDialog();
                BnBUserUtility.toastMessage_error("Sorry,something went wrong,we can't" +
                        " reach our service at the moment.Please try again.",getApplicationContext());
            }
        });
    }


    private void updateLocationOfDriverAndCustomerOnMap(LatLng Source_address) {

        mMap.clear();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Source_address, 11));

        setUpMap(Source_address,mMap,"source");
//        setUpMap(Destination_Address,mMap,"destination");
    }


    public void updateTripDataTODB(String personID) {
        Log.d(TAG," id : "+personID);
        RealmResults<TripCollection> results = myRealm.where(TripCollection.class).findAll();
        myRealm.beginTransaction();
        if(results.size()>0){
            TripCollection editPersonDetails = myRealm.where(TripCollection.class).equalTo("trip_id", personID).findFirst();
           if(editPersonDetails!=null) {
               editPersonDetails.setStatus("completed");
           }
        }
        myRealm.commitTransaction();
    }

    private void getAllUsers(String Trip_ID) {
        RealmResults<TripCollection> results = myRealm.where(TripCollection.class).findAll();
        myRealm.beginTransaction();
        for (int i = 0; i < results.size(); i++) {
            if(Trip_ID.equals(results.get(i).getTrip_id())){
                if(results.get(i).getStatus().equals("completed")){
                    getVerifiactionDialogBox();
                }
            }
            Log.d(TAG," status : "+results.get(i).getStatus());
            Log.d(TAG," trip id : "+results.get(i).getTrip_id());

        }
        if(results.size()>0) {
            int var_v = myRealm.where(TripCollection.class).max("id").intValue() + 1;
            Log.d(TAG," var v: : "+var_v);
        }
        myRealm.commitTransaction();
    }
}

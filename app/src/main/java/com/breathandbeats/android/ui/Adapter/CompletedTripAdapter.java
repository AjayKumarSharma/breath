package com.breathandbeats.android.ui.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.HistoryTripListPojo.CompletedTripListData;
import com.breathandbeats.android.ui.fragments.TripData;

import java.util.ArrayList;

/**
 * Created by Rakesh on 02-11-2017.
 */

public class CompletedTripAdapter extends RecyclerView.Adapter<CompletedTripAdapter.ViewHolder>{


    Context context;
    String TAG="CompletedTripAdapter";
    ArrayList<TripData> tripDatas;
    CompletedTripListData completedTripListData=new CompletedTripListData();

    public CompletedTripAdapter(Context context, CompletedTripListData historyTripData) {

        this.completedTripListData=historyTripData;
        this.context=context;
    }

    @Override
    public CompletedTripAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.completedtripadapter_layout, viewGroup, false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {


        if (position % 2 == 1) {
            holder.rlMainlayout.setBackgroundColor(context.getResources().getColor(R.color.backgroundcolor));
        } else {
            holder.rlMainlayout.setBackgroundColor(context.getResources().getColor(R.color.white));
        }

//        Log.d(TAG," trip fare : "+completedTripListData.getData().get(position).getPay().getTotalFare());
        if(completedTripListData.getData().get(position).getTripStatus().equals("completed")){


            String datevalue=completedTripListData.getData().get(position).getAppTripStartTime();
//            holder.tvDate.setText(BnBUserUtility.getTheCorrectFormateDate(datevalue));
            holder.tvDate.setText(datevalue);
            if(completedTripListData.getData().get(position).getHospital()!=null){

                if(completedTripListData.getData().get(position).getHospital().getName()!=null){
                    holder.tvhospitalname.setText(completedTripListData.getData().get(position).getHospital().getName().toString());
                }
            }

            if(completedTripListData.getData().get(position).getAmbulance()!=null){

                if(completedTripListData.getData().get(position).getAmbulance().getAmbulanceCategory()!=null){
                    holder.tvambulancetype.setText(completedTripListData.getData().get(position).getAmbulance().getAmbulanceCategory().toString());
                }
            }

            if(completedTripListData.getData().get(position).getTripStatus()!=null) {
                holder.tvtripstatus.setText(completedTripListData.getData().get(position).getTripStatus().toString());
            }
//            holder.tvpaymnetstatus.setText(advancebookdata.getData().get(position).);

            if(completedTripListData.getData().get(position).getPay()!=null) {
                if (String.valueOf(completedTripListData.getData().get(position).getPay().getTotalFare())!=null) {
                    holder.tvtotalfare.setText(String.valueOf(completedTripListData.getData().get(position).getPay().getTotalFare())+" Rs");
                }
            }

        }else{
            //do nothing
        }


    }


    @Override
    public int getItemCount() {
        return completedTripListData.getData().size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvDate;
        TextView tvDeatils;
        TextView tvhospitalname,tvambulancetype,tvtripstatus,tvpaymnetstatus,tvtotalfare;
        RelativeLayout rlMainlayout;

        public ViewHolder(View itemView) {
            super(itemView);

//            tvDeatils=(TextView)itemView.findViewById(R.id.tvDeatils);
            tvDate=(TextView)itemView.findViewById(R.id.tvDate);
            tvhospitalname=(TextView)itemView.findViewById(R.id.tvhospitalname);
            tvambulancetype=(TextView)itemView.findViewById(R.id.tvambulancetype);
            tvtripstatus=(TextView)itemView.findViewById(R.id.tvtripstatus);
            rlMainlayout=(RelativeLayout)itemView.findViewById(R.id.rlMainlayout);
            tvtotalfare=(TextView)itemView.findViewById(R.id.tvtotalfare);
//            tvpaymnetstatus=(TextView)itemView.findViewById(R.id.tvpaymnetstatus);

        }
    }
}

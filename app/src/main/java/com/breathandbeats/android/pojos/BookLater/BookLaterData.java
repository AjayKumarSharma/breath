package com.breathandbeats.android.pojos.BookLater;

import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Locationdata;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lavanya on 21-11-2017.
 */

public class BookLaterData {
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("userAddress")
    @Expose
    private String userAddress;
    @SerializedName("user")
    @Expose
    private String user;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("appTotalTimeCovered")
    @Expose
    private Object appTotalTimeCovered;
    @SerializedName("appTripEndTime")
    @Expose
    private Object appTripEndTime;
    @SerializedName("appTripStartTime")
    @Expose
    private Object appTripStartTime;
    @SerializedName("driverDropPointAddress")
    @Expose
    private Object driverDropPointAddress;
    @SerializedName("driverPickupPointAddress")
    @Expose
    private Object driverPickupPointAddress;
    @SerializedName("isPaid")
    @Expose
    private Boolean isPaid;
    @SerializedName("isAccepted")
    @Expose
    private Boolean isAccepted;
    @SerializedName("arrayOfTripCoordinates")
    @Expose
    private List<Object> arrayOfTripCoordinates = null;
    @SerializedName("emergencyType")
    @Expose
    private String emergencyType;
    @SerializedName("bookLaterDate")
    @Expose
    private Object bookLaterDate;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("tripStatus")
    @Expose
    private String tripStatus;
    @SerializedName("scheduledOnServerTime")
    @Expose
    private String scheduledOnServerTime;
    @SerializedName("scheduledOnAppTime")
    @Expose
    private String scheduledOnAppTime;
    @SerializedName("isScheduledBooking")
    @Expose
    private Boolean isScheduledBooking;
    @SerializedName("location")
    @Expose
    private Locationdata location;

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getAppTotalTimeCovered() {
        return appTotalTimeCovered;
    }

    public void setAppTotalTimeCovered(Object appTotalTimeCovered) {
        this.appTotalTimeCovered = appTotalTimeCovered;
    }

    public Object getAppTripEndTime() {
        return appTripEndTime;
    }

    public void setAppTripEndTime(Object appTripEndTime) {
        this.appTripEndTime = appTripEndTime;
    }

    public Object getAppTripStartTime() {
        return appTripStartTime;
    }

    public void setAppTripStartTime(Object appTripStartTime) {
        this.appTripStartTime = appTripStartTime;
    }

    public Object getDriverDropPointAddress() {
        return driverDropPointAddress;
    }

    public void setDriverDropPointAddress(Object driverDropPointAddress) {
        this.driverDropPointAddress = driverDropPointAddress;
    }

    public Object getDriverPickupPointAddress() {
        return driverPickupPointAddress;
    }

    public void setDriverPickupPointAddress(Object driverPickupPointAddress) {
        this.driverPickupPointAddress = driverPickupPointAddress;
    }

    public Boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public List<Object> getArrayOfTripCoordinates() {
        return arrayOfTripCoordinates;
    }

    public void setArrayOfTripCoordinates(List<Object> arrayOfTripCoordinates) {
        this.arrayOfTripCoordinates = arrayOfTripCoordinates;
    }

    public String getEmergencyType() {
        return emergencyType;
    }

    public void setEmergencyType(String emergencyType) {
        this.emergencyType = emergencyType;
    }

    public Object getBookLaterDate() {
        return bookLaterDate;
    }

    public void setBookLaterDate(Object bookLaterDate) {
        this.bookLaterDate = bookLaterDate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getScheduledOnServerTime() {
        return scheduledOnServerTime;
    }

    public void setScheduledOnServerTime(String scheduledOnServerTime) {
        this.scheduledOnServerTime = scheduledOnServerTime;
    }

    public String getScheduledOnAppTime() {
        return scheduledOnAppTime;
    }

    public void setScheduledOnAppTime(String scheduledOnAppTime) {
        this.scheduledOnAppTime = scheduledOnAppTime;
    }

    public Boolean getIsScheduledBooking() {
        return isScheduledBooking;
    }

    public void setIsScheduledBooking(Boolean isScheduledBooking) {
        this.isScheduledBooking = isScheduledBooking;
    }

    public Locationdata getLocation() {
        return location;
    }

    public void setLocation(Locationdata location) {
        this.location = location;
    }

}

package com.breathandbeats.android.pojos;

/**
 * Created by Rakesh on 20-09-2017.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BookAmbulanceNowresponce implements Serializable
{

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private BookambulanceData data;

    private final static long serialVersionUID = 2875971673435746889L;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public BookambulanceData getData ()
    {
        return data;
    }

    public void setData (BookambulanceData data)
    {
        this.data = data;
    }

}
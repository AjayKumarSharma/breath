package com.breathandbeats.android.pojos.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Rocky on 3/6/2018.
 */

public class DoctorSearchListResponse {

    @SerializedName("addressLine1")
    @Expose
    private String addressLine1;

    @SerializedName("addressLine2")
    @Expose
    private String addressLine2;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("firstName")
    @Expose
    private String firstName;

    @SerializedName("lastName")
    @Expose
    private String lastName;

    @SerializedName("qualification")
    @Expose
    private String qualification;

    @SerializedName("specialityName")
    @Expose
    private String specialityName;

    @SerializedName("uuid")
    @Expose
    private String uuid;

    @SerializedName("zipCode")
    @Expose
    private String zipCode;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("like")
    @Expose
    private String like;

    @SerializedName("dislike")
    @Expose
    private String dislike;

    @SerializedName("rating")
    @Expose
    private String rating;
}

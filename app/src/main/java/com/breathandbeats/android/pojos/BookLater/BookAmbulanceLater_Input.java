package com.breathandbeats.android.pojos.BookLater;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lavanya on 14-11-2017.
 */

public class BookAmbulanceLater_Input {


    String emergencyType;
    String userId;
    String userAddress;
    String scheduledOnAppTime;

    @SerializedName("userCurrentlocation")
    @Expose
    private List<String> userCurrentlocation = null;

    public String getEmergencyType() {
        return emergencyType;
    }

    public void setEmergencyType(String emergencyType) {
        this.emergencyType = emergencyType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getScheduledOnAppTime() {
        return scheduledOnAppTime;
    }

    public void setScheduledOnAppTime(String scheduledOnAppTime) {
        this.scheduledOnAppTime = scheduledOnAppTime;
    }

    public List<String> getUserCurrentlocation() {
        return userCurrentlocation;
    }

    public void setUserCurrentlocation(List<String> userCurrentlocation) {
        this.userCurrentlocation = userCurrentlocation;
    }
}

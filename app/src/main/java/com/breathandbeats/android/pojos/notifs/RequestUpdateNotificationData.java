package com.breathandbeats.android.pojos.notifs;


public class RequestUpdateNotificationData {
    String status;
    String senderName;
    String requestId;
    String senderNumber;

    public RequestUpdateNotificationData(String status, String senderName, String requestId, String senderNumber) {
        this.status = status;
        this.senderName = senderName;
        this.requestId = requestId;
        this.senderNumber = senderNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getSenderNumber() {
        return senderNumber;
    }

    public void setSenderNumber(String senderNumber) {
        this.senderNumber = senderNumber;
    }
}

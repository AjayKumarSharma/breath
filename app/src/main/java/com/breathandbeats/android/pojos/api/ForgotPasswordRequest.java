package com.breathandbeats.android.pojos.api;

public class ForgotPasswordRequest {
    String phoneNumber;

    public ForgotPasswordRequest(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

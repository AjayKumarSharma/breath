package com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lavanya on 31-10-2017.
 */

public class NotificationBasePojo implements Serializable
{

    @SerializedName("trip")
    @Expose
    private Trip trip;
    private final static long serialVersionUID = -3908426541364521913L;

    public Trip getTrip() {
        return trip;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

}
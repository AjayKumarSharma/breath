package com.breathandbeats.android.pojos.api;

public class MakeANoiseData {
    String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}

package com.breathandbeats.android.pojos.DriverCordinate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lavanya on 21-11-2017.
 */

public class DriverCoOrdinateResponce {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private LocationData data;

    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public LocationData getData() {
        return data;
    }

    public void setData(LocationData data) {
        this.data = data;
    }

}


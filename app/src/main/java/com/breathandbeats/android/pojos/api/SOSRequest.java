package com.breathandbeats.android.pojos.api;

import java.util.ArrayList;

public class SOSRequest {
    String sosType;
    ArrayList<String> emergencyContacts;
    ArrayList<Double> location;

    public String getSosType() {
        return sosType;
    }

    public void setSosType(String sosType) {
        this.sosType = sosType;
    }

    public ArrayList<String> getEmergencyContacts() {
        return emergencyContacts;
    }

    public void setEmergencyContacts(ArrayList<String> emergencyContacts) {
        this.emergencyContacts = emergencyContacts;
    }

    public ArrayList<Double> getLocation() {
        return location;
    }

    public void setLocation(ArrayList<Double> location) {
        this.location = location;
    }
}

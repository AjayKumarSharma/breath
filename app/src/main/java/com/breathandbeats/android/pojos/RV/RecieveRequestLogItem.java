package com.breathandbeats.android.pojos.RV;

import java.util.Date;

public class RecieveRequestLogItem {
    String name;
    Date dateTime;
    String status;
    String subject;
    String requestId;
    String receiverId;
    Boolean canChat;
    String chatId;
    boolean accepted;

    public RecieveRequestLogItem(String name, Date dateTime, String status, String subject, String requestId, String receiverId, Boolean canChat, String chatId, boolean accepted) {
        this.name = name;
        this.dateTime = dateTime;
        this.status = status;
        this.subject = subject;
        this.requestId = requestId;
        this.receiverId = receiverId;
        this.canChat = canChat;
        this.chatId = chatId;
        this.accepted = accepted;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Boolean getCanChat() {
        return canChat;
    }

    public void setCanChat(Boolean canChat) {
        this.canChat = canChat;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }
}

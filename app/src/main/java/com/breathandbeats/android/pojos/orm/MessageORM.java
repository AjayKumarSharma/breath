package com.breathandbeats.android.pojos.orm;


import com.orm.SugarRecord;

import java.util.Date;

public class MessageORM extends SugarRecord {
    String messageId;
    String message;
    String chatId;
    Date createdAt;
    Integer status;
    boolean isCreatedBySelf;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public MessageORM(String message, String chatId, Date createdAt, Integer status, boolean isCreatedBySelf) {
        this.message = message;
        this.chatId = chatId;
        this.createdAt = createdAt;
        this.status = status;
        this.isCreatedBySelf = isCreatedBySelf;
    }

    public boolean isCreatedBySelf() {
        return isCreatedBySelf;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public void setCreatedBySelf(boolean createdBySelf) {
        isCreatedBySelf = createdBySelf;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public MessageORM() {
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

package com.breathandbeats.android.pojos.api;

import java.util.List;

/**
 * Created by Rocky on 3/6/2018.
 */

public class DoctorSearchList {

    private List<DoctorSearchListResponse> DoctorSearchListData = null;

    public List<DoctorSearchListResponse> getDoctorSearchListData() {
        return DoctorSearchListData;
    }

    public void setDoctorSearchListData(List<DoctorSearchListResponse> doctorSearchListData) {
        DoctorSearchListData = doctorSearchListData;
    }
}

package com.breathandbeats.android.pojos.api;

public class ForgotPasswordResponse {
   Boolean success;
    Data data;
    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

/*    public ForgotPasswordResponse(boolean success, Data data) {
        this.success = success;
        this.data = data;
    }*/

    public boolean getSuccess() {
        return success;
    }


    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

     public class Data{
        String email;

        public Data(String email) {
            this.email = email;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }
    }
}


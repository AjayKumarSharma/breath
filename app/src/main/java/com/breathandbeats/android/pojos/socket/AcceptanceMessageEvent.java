package com.breathandbeats.android.pojos.socket;

public class AcceptanceMessageEvent {
    private String message;
    private String requestId;
    private String receiverId;

    public AcceptanceMessageEvent(String message, String requestId, String receiverId) {
        this.message = message;
        this.requestId = requestId;
        this.receiverId = receiverId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

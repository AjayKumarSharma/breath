package com.breathandbeats.android.pojos.api;

import java.util.ArrayList;

public class PulseRequest {
    private String appVersion;
    private String platform;
    private String registrationId;
    private ArrayList<Double> lastLocation;

    public ArrayList<Double> getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(ArrayList<Double> lastLocation) {
        this.lastLocation = lastLocation;
    }


    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
}

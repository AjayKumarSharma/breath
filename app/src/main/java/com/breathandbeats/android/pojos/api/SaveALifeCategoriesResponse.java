package com.breathandbeats.android.pojos.api;

import com.google.gson.annotations.SerializedName;

public class SaveALifeCategoriesResponse {
    Boolean success;
    @SerializedName("data")
    SaveALifeCategoryData saveALifeCategoryData;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public SaveALifeCategoryData getSaveALifeCategoryData() {
        return saveALifeCategoryData;
    }

    public void setSaveALifeCategoryData(SaveALifeCategoryData saveALifeCategoryData) {
        this.saveALifeCategoryData = saveALifeCategoryData;
    }
}
package com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lavanya on 31-10-2017.
 */
public class DeviceInfo_ implements Serializable
{

    @SerializedName("registrationId")
    @Expose
    private String registrationId;
    private final static long serialVersionUID = -7964815794389358377L;

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

}
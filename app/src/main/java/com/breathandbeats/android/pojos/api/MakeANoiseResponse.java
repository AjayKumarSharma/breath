package com.breathandbeats.android.pojos.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MakeANoiseResponse {
    private Boolean success;
    @Expose
    @SerializedName("data")
    private MakeANoiseData makeANoiseData;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public MakeANoiseData getMakeANoiseData() {
        return makeANoiseData;
    }

    public void setMakeANoiseData(MakeANoiseData makeANoiseData) {
        this.makeANoiseData = makeANoiseData;
    }
}
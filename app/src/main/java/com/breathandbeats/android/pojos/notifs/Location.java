package com.breathandbeats.android.pojos.notifs;

import java.util.ArrayList;
import java.util.List;

public class Location {


    private ArrayList<Double> coordinates = null;

    private String type;

    public ArrayList<Double> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Double> coordinates) {
        this.coordinates = coordinates;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
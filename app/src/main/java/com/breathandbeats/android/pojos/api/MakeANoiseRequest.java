package com.breathandbeats.android.pojos.api;

import java.util.ArrayList;

public class MakeANoiseRequest {
        String subject;
        String reason;
        String message;
        int distance;
        ArrayList<Double> location;

        public int getDistance() {
            return distance;
        }

        public void setDistance(int distance) {
            this.distance = distance;
        }

        public ArrayList<Double> getCurrentLocation() {
            return location;
        }

        public void setCurrentLocation(ArrayList<Double> currentLocation) {
            this.location = currentLocation;
        }

        public MakeANoiseRequest() {
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }


    }

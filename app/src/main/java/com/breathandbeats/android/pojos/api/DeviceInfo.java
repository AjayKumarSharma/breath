package com.breathandbeats.android.pojos.api;

public class DeviceInfo {
    String deviceId;
    String IMEINumber;
    String registrationId;

    public DeviceInfo(String deviceId, String IMEINumber,String registrationId) {
        this.deviceId = deviceId;
        this.IMEINumber = IMEINumber;
        this.registrationId=registrationId;
    }

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIMEINumber() {
        return IMEINumber;
    }

    public void setIMEINumber(String IMEINumber) {
        this.IMEINumber = IMEINumber;
    }

}

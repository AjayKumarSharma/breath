package com.breathandbeats.android.pojos.socket;

public class MessageEvent {

    Payload payload;
    Message message;

    public Payload getPayload() {
        return payload;
    }

    public void setPayload(Payload payload) {
        this.payload = payload;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}

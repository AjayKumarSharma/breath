package com.breathandbeats.android.pojos.socket;


public class FirstTimeChatMessage {
    Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}

package com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lavanya on 31-10-2017.
 */
public class Ambulance implements Serializable
{

    @SerializedName("vehicleNumber")
    @Expose
    private String vehicleNumber;
    @SerializedName("ambulanceCategory")
    @Expose
    private String ambulanceCategory;
    @SerializedName("_id")
    @Expose
    private String id;
    private final static long serialVersionUID = 796740214096625124L;

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getAmbulanceCategory() {
        return ambulanceCategory;
    }

    public void setAmbulanceCategory(String ambulanceCategory) {
        this.ambulanceCategory = ambulanceCategory;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
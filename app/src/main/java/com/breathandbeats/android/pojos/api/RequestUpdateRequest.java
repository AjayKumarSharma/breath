package com.breathandbeats.android.pojos.api;

public class RequestUpdateRequest {
    String status;

    public RequestUpdateRequest(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

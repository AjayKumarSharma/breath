package com.breathandbeats.android.pojos.api;

import com.breathandbeats.android.pojos.notifs.Location;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SosLogResponseData {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("sosType")
    @Expose
    private String sosType;
    @SerializedName("emergencyContacts")
    @Expose
    private List<String> emergencyContacts = null;
    @SerializedName("location")
    @Expose
    private Location location = null;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getSosType() {
        return sosType;
    }

    public void setSosType(String sosType) {
        this.sosType = sosType;
    }


    public List<String> getEmergencyContacts() {
        return emergencyContacts;
    }

    public void setEmergencyContacts(List<String> emergencyContacts) {
        this.emergencyContacts = emergencyContacts;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
package com.breathandbeats.android.pojos.notifs;


import com.breathandbeats.android.pojos.socket.MessageData;

public class MakeANoiseChatMessageNotificationData {
    String createdAt;
    String chatId;
    MessageData messageData;
    String sentById;
    String requestId;
    String sentByName;
    String _id;

    public MakeANoiseChatMessageNotificationData(String createdAt, String chatId, MessageData messageData, String sentById, String requestId, String sentByName, String _id) {
        this.createdAt = createdAt;
        this.chatId = chatId;
        this.messageData = messageData;
        this.sentById = sentById;
        this.requestId = requestId;
        this.sentByName = sentByName;
        this._id = _id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public MessageData getMessageData() {
        return messageData;
    }

    public void setMessageData(MessageData messageData) {
        this.messageData = messageData;
    }

    public String getSentById() {
        return sentById;
    }

    public void setSentById(String sentById) {
        this.sentById = sentById;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getSentByName() {
        return sentByName;
    }

    public void setSentByName(String sentByName) {
        this.sentByName = sentByName;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    @Override
    public String toString() {
        return "MakeANoiseChatMessageNotificationData{" +
                "createdAt='" + createdAt + '\'' +
                ", chatId='" + chatId + '\'' +
                ", messageData=" + messageData +
                ", sentById='" + sentById + '\'' +
                ", requestId='" + requestId + '\'' +
                ", sentByName='" + sentByName + '\'' +
                ", _id='" + _id + '\'' +
                '}';
    }
}

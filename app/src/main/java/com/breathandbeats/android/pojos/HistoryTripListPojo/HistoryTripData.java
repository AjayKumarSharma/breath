package com.breathandbeats.android.pojos.HistoryTripListPojo;

/**
 * Created by lavanya on 03-11-2017.
 */

import com.breathandbeats.android.pojos.Advancebookpaymentdata.Pay;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Ambulance;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.DriverData;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Hospital;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class HistoryTripData implements Serializable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("userAddress")
    @Expose
    private String userAddress;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("hospital")
    @Expose
    private Hospital hospital;
    @SerializedName("ambulance")
    @Expose
    private Ambulance ambulance;
    @SerializedName("driver")
    @Expose
    private DriverData driver;
    @SerializedName("driverPickupPointCoordinates")
    @Expose
    private List<Double> driverPickupPointCoordinates = null;
    @SerializedName("driverDropPointCoordinates")
    @Expose
    private List<Double> driverDropPointCoordinates = null;
    @SerializedName("appTotalTimeCovered")
    @Expose
    private String appTotalTimeCovered;
    @SerializedName("appTripEndTime")
    @Expose
    private String appTripEndTime;
    @SerializedName("appTripStartTime")
    @Expose
    private String appTripStartTime;
    @SerializedName("driverDropPointAddress")
    @Expose
    private String driverDropPointAddress;
    @SerializedName("driverPickupPointAddress")
    @Expose
    private String driverPickupPointAddress;
    @SerializedName("isAccepted")
    @Expose
    private Boolean isAccepted;
    @SerializedName("arrayOfTripCoordinates")
    @Expose
    private List<List<String>> arrayOfTripCoordinates = null;
    @SerializedName("emergencyType")
    @Expose
    private String emergencyType;
    @SerializedName("tripStatus")
    @Expose
    private String tripStatus;
    @SerializedName("location")
    @Expose
    private Location_ location;
    @SerializedName("bookLaterDate")
    @Expose
    private Object bookLaterDate;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("scheduledOnServerTime")
    @Expose
    private Object scheduledOnServerTime;
    @SerializedName("scheduledOnAppTime")
    @Expose
    private Object scheduledOnAppTime;
    @SerializedName("isScheduledBooking")
    @Expose
    private Boolean isScheduledBooking;
    @SerializedName("pay")
    @Expose
    private Pay pay;

    public Pay getPay() {
        return pay;
    }

    public void setPay(Pay pay) {
        this.pay = pay;
    }

    public Boolean getAccepted() {
        return isAccepted;
    }

    public void setAccepted(Boolean accepted) {
        isAccepted = accepted;
    }

    public Object getBookLaterDate() {
        return bookLaterDate;
    }

    public void setBookLaterDate(Object bookLaterDate) {
        this.bookLaterDate = bookLaterDate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Object getScheduledOnServerTime() {
        return scheduledOnServerTime;
    }

    public void setScheduledOnServerTime(Object scheduledOnServerTime) {
        this.scheduledOnServerTime = scheduledOnServerTime;
    }

    public Object getScheduledOnAppTime() {
        return scheduledOnAppTime;
    }

    public void setScheduledOnAppTime(Object scheduledOnAppTime) {
        this.scheduledOnAppTime = scheduledOnAppTime;
    }

    public Boolean getScheduledBooking() {
        return isScheduledBooking;
    }

    public void setScheduledBooking(Boolean scheduledBooking) {
        isScheduledBooking = scheduledBooking;
    }

    private final static long serialVersionUID = 4729439335849932262L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Ambulance getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(Ambulance ambulance) {
        this.ambulance = ambulance;
    }

    public DriverData getDriver() {
        return driver;
    }

    public void setDriver(DriverData driver) {
        this.driver = driver;
    }

    public List<Double> getDriverPickupPointCoordinates() {
        return driverPickupPointCoordinates;
    }

    public void setDriverPickupPointCoordinates(List<Double> driverPickupPointCoordinates) {
        this.driverPickupPointCoordinates = driverPickupPointCoordinates;
    }

    public List<Double> getDriverDropPointCoordinates() {
        return driverDropPointCoordinates;
    }

    public void setDriverDropPointCoordinates(List<Double> driverDropPointCoordinates) {
        this.driverDropPointCoordinates = driverDropPointCoordinates;
    }

    public String getAppTotalTimeCovered() {
        return appTotalTimeCovered;
    }

    public void setAppTotalTimeCovered(String appTotalTimeCovered) {
        this.appTotalTimeCovered = appTotalTimeCovered;
    }

    public String getAppTripEndTime() {
        return appTripEndTime;
    }

    public void setAppTripEndTime(String appTripEndTime) {
        this.appTripEndTime = appTripEndTime;
    }

    public String getAppTripStartTime() {
        return appTripStartTime;
    }

    public void setAppTripStartTime(String appTripStartTime) {
        this.appTripStartTime = appTripStartTime;
    }

    public String getDriverDropPointAddress() {
        return driverDropPointAddress;
    }

    public void setDriverDropPointAddress(String driverDropPointAddress) {
        this.driverDropPointAddress = driverDropPointAddress;
    }

    public String getDriverPickupPointAddress() {
        return driverPickupPointAddress;
    }

    public void setDriverPickupPointAddress(String driverPickupPointAddress) {
        this.driverPickupPointAddress = driverPickupPointAddress;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public List<List<String>> getArrayOfTripCoordinates() {
        return arrayOfTripCoordinates;
    }

    public void setArrayOfTripCoordinates(List<List<String>> arrayOfTripCoordinates) {
        this.arrayOfTripCoordinates = arrayOfTripCoordinates;
    }

    public String getEmergencyType() {
        return emergencyType;
    }

    public void setEmergencyType(String emergencyType) {
        this.emergencyType = emergencyType;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public Location_ getLocation() {
        return location;
    }

    public void setLocation(Location_ location) {
        this.location = location;
    }

}
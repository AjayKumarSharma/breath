package com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo;

/**
 * Created by lavanya on 31-10-2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Trip implements Serializable
{

    @SerializedName("userAddress")
    @Expose
    private String userAddress;
    @SerializedName("driver")
    @Expose
    private DriverData driver;
    @SerializedName("location")
    @Expose
    private Locationdata location;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("tripStatus")
    @Expose
    private String tripStatus ;
    @SerializedName("ambulance")
    @Expose
    private Ambulance ambulance;
    @SerializedName("hospital")
    @Expose
    private Hospital hospital;
    @SerializedName("user")
    @Expose
    private User user;
    private final static long serialVersionUID = 2114373416240828481L;


    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("totalFare")
    @Expose
    private Float totalFare;
   /* @SerializedName("_id")
    @Expose
    private String id;*/
    @SerializedName("range")
    @Expose
    private String range;
    @SerializedName("estimatedFare")
    @Expose
    private Float estimatedFare;
    @SerializedName("totalDistance")
    @Expose
    private Float totalDistance;
    @SerializedName("paymentStatus")
    @Expose
    private String paymentStatus;
    @SerializedName("paymentGateway")
    @Expose
    private String paymentGateway;
    @SerializedName("gstPercentage")
    @Expose
    private Float gstPercentage;
    @SerializedName("razorPay")
    @Expose
    private RazorPay razorPay;
    @SerializedName("gstCost")
    @Expose
    private Float gstCost;
//---------------------------
    @SerializedName("emergencyType")
    @Expose
    private String emergencyType;
    @SerializedName("scheduledOnAppTime")
    @Expose
    private String scheduledOnAppTime;
    @SerializedName("appTripEndTime")
    @Expose
    private Object appTripEndTime;
    @SerializedName("arrayOfTripCoordinates")
    @Expose
    private List<Object> arrayOfTripCoordinates = null;
    @SerializedName("driverDropPointAddress")
    @Expose
    private Object driverDropPointAddress;
    @SerializedName("scheduledOnServerTime")
    @Expose
    private String scheduledOnServerTime;
    @SerializedName("__v")
    @Expose
    private Float v;
    @SerializedName("isPaid")
    @Expose
    private Boolean isPaid;
    @SerializedName("isScheduledBooking")
    @Expose
    private Boolean isScheduledBooking;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("appTripStartTime")
    @Expose
    private Object appTripStartTime;
    @SerializedName("driverPickupPointAddress")
    @Expose
    private Object driverPickupPointAddress;
    @SerializedName("bookLaterDate")
    @Expose
    private Object bookLaterDate;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("appTotalTimeCovered")
    @Expose
    private Object appTotalTimeCovered;
    @SerializedName("isAccepted")
    @Expose
    private Boolean isAccepted;
    //-----------------------
    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Float getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(Float totalFare) {
        this.totalFare = totalFare;
    }

/*    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }*/

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public Float getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(Float estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public Float getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Float totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public Float getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(Float gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public RazorPay getRazorPay() {
        return razorPay;
    }

    public void setRazorPay(RazorPay razorPay) {
        this.razorPay = razorPay;
    }

    public Float getGstCost() {
        return gstCost;
    }

    public void setGstCost(Float gstCost) {
        this.gstCost = gstCost;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public DriverData getDriver() {
        return driver;
    }

    public void setDriver(DriverData driver) {
        this.driver = driver;
    }

    public Locationdata getLocation() {
        return location;
    }

    public void setLocation(Locationdata location) {
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Ambulance getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(Ambulance ambulance) {
        this.ambulance = ambulance;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
//----------------
public String getEmergencyType() {
    return emergencyType;
}

    public void setEmergencyType(String emergencyType) {
        this.emergencyType = emergencyType;
    }

    public String getScheduledOnAppTime() {
        return scheduledOnAppTime;
    }

    public void setScheduledOnAppTime(String scheduledOnAppTime) {
        this.scheduledOnAppTime = scheduledOnAppTime;
    }

    public void setAppTripEndTime(Object appTripEndTime) {
        this.appTripEndTime = appTripEndTime;
    }

    public List<Object> getArrayOfTripCoordinates() {
        return arrayOfTripCoordinates;
    }

    public void setArrayOfTripCoordinates(List<Object> arrayOfTripCoordinates) {
        this.arrayOfTripCoordinates = arrayOfTripCoordinates;
    }

    public Object getDriverDropPointAddress() {
        return driverDropPointAddress;
    }

    public void setDriverDropPointAddress(Object driverDropPointAddress) {
        this.driverDropPointAddress = driverDropPointAddress;
    }

    public String getScheduledOnServerTime() {
        return scheduledOnServerTime;
    }

    public void setScheduledOnServerTime(String scheduledOnServerTime) {
        this.scheduledOnServerTime = scheduledOnServerTime;
    }

    public Float getV() {
        return v;
    }

    public void setV(Float v) {
        this.v = v;
    }

    public Boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public Boolean getIsScheduledBooking() {
        return isScheduledBooking;
    }

    public void setIsScheduledBooking(Boolean isScheduledBooking) {
        this.isScheduledBooking = isScheduledBooking;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getAppTripStartTime() {
        return appTripStartTime;
    }

    public void setAppTripStartTime(Object appTripStartTime) {
        this.appTripStartTime = appTripStartTime;
    }

    public Object getDriverPickupPointAddress() {
        return driverPickupPointAddress;
    }

    public void setDriverPickupPointAddress(Object driverPickupPointAddress) {
        this.driverPickupPointAddress = driverPickupPointAddress;
    }
    public Object getBookLaterDate() {
        return bookLaterDate;
    }

    public void setBookLaterDate(Object bookLaterDate) {
        this.bookLaterDate = bookLaterDate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Object getAppTotalTimeCovered() {
        return appTotalTimeCovered;
    }

    public void setAppTotalTimeCovered(Object appTotalTimeCovered) {
        this.appTotalTimeCovered = appTotalTimeCovered;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }
    //--------------------
}

package com.breathandbeats.android.pojos.Advancebookpaymentdata;

import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Ambulance;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.DriverData;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Hospital;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Locationdata;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lavanya on 28-11-2017.
 */

public class PaymentData {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("userAddress")
    @Expose
    private String userAddress;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("hospital")
    @Expose
    private Hospital hospital;
    @SerializedName("ambulance")
    @Expose
    private Ambulance ambulance;
    @SerializedName("driver")
    @Expose
    private DriverData driver;
    @SerializedName("pay")
    @Expose
    private Pay pay;
    @SerializedName("driverPickupPointCoordinates")
    @Expose
    private List<Float> driverPickupPointCoordinates = null;
    @SerializedName("driverDropPointCoordinates")
    @Expose
    private List<Float> driverDropPointCoordinates = null;
    @SerializedName("appTotalTimeCovered")
    @Expose
    private String appTotalTimeCovered;
    @SerializedName("appTripEndTime")
    @Expose
    private String appTripEndTime;
    @SerializedName("appTripStartTime")
    @Expose
    private String appTripStartTime;
    @SerializedName("driverDropPointAddress")
    @Expose
    private String driverDropPointAddress;
    @SerializedName("driverPickupPointAddress")
    @Expose
    private String driverPickupPointAddress;
    @SerializedName("isPaid")
    @Expose
    private Boolean isPaid;
    @SerializedName("isAccepted")
    @Expose
    private Boolean isAccepted;
    @SerializedName("arrayOfTripCoordinates")
    @Expose
    private List<Object> arrayOfTripCoordinates = null;
    @SerializedName("emergencyType")
    @Expose
    private String emergencyType;
    @SerializedName("bookLaterDate")
    @Expose
    private Object bookLaterDate;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("tripStatus")
    @Expose
    private String tripStatus;
    @SerializedName("scheduledOnServerTime")
    @Expose
    private String scheduledOnServerTime;
    @SerializedName("scheduledOnAppTime")
    @Expose
    private String scheduledOnAppTime;
    @SerializedName("isScheduledBooking")
    @Expose
    private Boolean isScheduledBooking;
    @SerializedName("location")
    @Expose
    private Locationdata location;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Ambulance getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(Ambulance ambulance) {
        this.ambulance = ambulance;
    }

    public DriverData getDriver() {
        return driver;
    }

    public void setDriver(DriverData driver) {
        this.driver = driver;
    }

    public Pay getPay() {
        return pay;
    }

    public void setPay(Pay pay) {
        this.pay = pay;
    }

    public List<Float> getDriverPickupPointCoordinates() {
        return driverPickupPointCoordinates;
    }

    public void setDriverPickupPointCoordinates(List<Float> driverPickupPointCoordinates) {
        this.driverPickupPointCoordinates = driverPickupPointCoordinates;
    }

    public List<Float> getDriverDropPointCoordinates() {
        return driverDropPointCoordinates;
    }

    public void setDriverDropPointCoordinates(List<Float> driverDropPointCoordinates) {
        this.driverDropPointCoordinates = driverDropPointCoordinates;
    }

    public String getAppTotalTimeCovered() {
        return appTotalTimeCovered;
    }

    public void setAppTotalTimeCovered(String appTotalTimeCovered) {
        this.appTotalTimeCovered = appTotalTimeCovered;
    }

    public String getAppTripEndTime() {
        return appTripEndTime;
    }

    public void setAppTripEndTime(String appTripEndTime) {
        this.appTripEndTime = appTripEndTime;
    }

    public String getAppTripStartTime() {
        return appTripStartTime;
    }

    public void setAppTripStartTime(String appTripStartTime) {
        this.appTripStartTime = appTripStartTime;
    }

    public String getDriverDropPointAddress() {
        return driverDropPointAddress;
    }

    public void setDriverDropPointAddress(String driverDropPointAddress) {
        this.driverDropPointAddress = driverDropPointAddress;
    }

    public String getDriverPickupPointAddress() {
        return driverPickupPointAddress;
    }

    public void setDriverPickupPointAddress(String driverPickupPointAddress) {
        this.driverPickupPointAddress = driverPickupPointAddress;
    }

    public Boolean getIsPaid() {
        return isPaid;
    }

    public void setIsPaid(Boolean isPaid) {
        this.isPaid = isPaid;
    }

    public Boolean getIsAccepted() {
        return isAccepted;
    }

    public void setIsAccepted(Boolean isAccepted) {
        this.isAccepted = isAccepted;
    }

    public List<Object> getArrayOfTripCoordinates() {
        return arrayOfTripCoordinates;
    }

    public void setArrayOfTripCoordinates(List<Object> arrayOfTripCoordinates) {
        this.arrayOfTripCoordinates = arrayOfTripCoordinates;
    }

    public String getEmergencyType() {
        return emergencyType;
    }

    public void setEmergencyType(String emergencyType) {
        this.emergencyType = emergencyType;
    }

    public Object getBookLaterDate() {
        return bookLaterDate;
    }

    public void setBookLaterDate(Object bookLaterDate) {
        this.bookLaterDate = bookLaterDate;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public void setTripStatus(String tripStatus) {
        this.tripStatus = tripStatus;
    }

    public String getScheduledOnServerTime() {
        return scheduledOnServerTime;
    }

    public void setScheduledOnServerTime(String scheduledOnServerTime) {
        this.scheduledOnServerTime = scheduledOnServerTime;
    }

    public String getScheduledOnAppTime() {
        return scheduledOnAppTime;
    }

    public void setScheduledOnAppTime(String scheduledOnAppTime) {
        this.scheduledOnAppTime = scheduledOnAppTime;
    }

    public Boolean getIsScheduledBooking() {
        return isScheduledBooking;
    }

    public void setIsScheduledBooking(Boolean isScheduledBooking) {
        this.isScheduledBooking = isScheduledBooking;
    }

    public Locationdata getLocation() {
        return location;
    }

    public void setLocation(Locationdata location) {
        this.location = location;
    }

}

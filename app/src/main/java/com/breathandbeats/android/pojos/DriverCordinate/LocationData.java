package com.breathandbeats.android.pojos.DriverCordinate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by lavanya on 21-11-2017.
 */

public class LocationData {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("driverCurrentCoordinates")
    @Expose
    private List<Float> driverCurrentCoordinates = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Float> getDriverCurrentCoordinates() {
        return driverCurrentCoordinates;
    }

    public void setDriverCurrentCoordinates(List<Float> driverCurrentCoordinates) {
        this.driverCurrentCoordinates = driverCurrentCoordinates;
    }

}

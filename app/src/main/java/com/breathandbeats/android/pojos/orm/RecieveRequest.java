package com.breathandbeats.android.pojos.orm;


import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.ArrayList;
import java.util.Date;

public class RecieveRequest extends SugarRecord {
    boolean accepted;
    String firstMessage;
    @Unique
    String requestID;
    String status;
    String subject;
    String reason;
    String message;
    Integer distance;
    ArrayList<Double> location;
    Date createdAt;
    String senderId;
    String chatId;
    Boolean canChat;
    String senderName;

    public RecieveRequest(boolean accepted, String firstMessage, String requestID, String status, String subject, String reason, String message, Integer distance, ArrayList<Double> location, Date createdAt, String senderId, String chatId, Boolean canChat, String senderName) {
        this.accepted = accepted;
        this.firstMessage = firstMessage;
        this.requestID = requestID;
        this.status = status;
        this.subject = subject;
        this.reason = reason;
        this.message = message;
        this.distance = distance;
        this.location = location;
        this.createdAt = createdAt;
        this.senderId = senderId;
        this.chatId = chatId;
        this.canChat = canChat;
        this.senderName = senderName;
    }

    public Boolean getCanChat() {
        return canChat;
    }

    public void setCanChat(Boolean canChat) {
        this.canChat = canChat;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public ArrayList<Double> getLocation() {
        return location;
    }

    public void setLocation(ArrayList<Double> location) {
        this.location = location;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public RecieveRequest() {
    }


    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public String getFirstMessage() {
        return firstMessage;
    }

    public void setFirstMessage(String firstMessage) {
        this.firstMessage = firstMessage;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getChatId() {
        return chatId;
    }


    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    @Override
    public String toString() {
        return "RecieveRequest{" +
                "accepted=" + accepted +
                ", firstMessage='" + firstMessage + '\'' +
                ", requestID='" + requestID + '\'' +
                ", status='" + status + '\'' +
                ", subject='" + subject + '\'' +
                ", reason='" + reason + '\'' +
                ", message='" + message + '\'' +
                ", distance=" + distance +
                ", location=" + location +
                ", createdAt=" + createdAt +
                ", senderId='" + senderId + '\'' +
                ", chatId='" + chatId + '\'' +
                '}';
    }


}

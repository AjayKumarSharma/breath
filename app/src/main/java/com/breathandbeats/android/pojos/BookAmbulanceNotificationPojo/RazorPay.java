package com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lavanya on 21-11-2017.
 */

public class RazorPay {
    @SerializedName("captured")
    @Expose
    private Boolean captured;
    @SerializedName("razorpayPaymentId")
    @Expose
    private Object razorpayPaymentId;

    public Boolean getCaptured() {
        return captured;
    }

    public void setCaptured(Boolean captured) {
        this.captured = captured;
    }

    public Object getRazorpayPaymentId() {
        return razorpayPaymentId;
    }

    public void setRazorpayPaymentId(Object razorpayPaymentId) {
        this.razorpayPaymentId = razorpayPaymentId;
    }

}

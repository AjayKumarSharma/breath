package com.breathandbeats.android.pojos.BookLater;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lavanya on 14-11-2017.
 */

public class BookAmbulanceLaterResponce {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private BookLaterData data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public BookLaterData getData() {
        return data;
    }

    public void setData(BookLaterData data) {
        this.data = data;
    }

}

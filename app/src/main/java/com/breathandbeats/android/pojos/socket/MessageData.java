package com.breathandbeats.android.pojos.socket;


public class MessageData {
    String text;

    public MessageData(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

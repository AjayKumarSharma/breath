package com.breathandbeats.android.pojos.socket;

public class Payload {

    String clientMessageId;

    String clientChatId;

    public String getClientMessageId() {
        return clientMessageId;
    }

    public void setClientMessageId(String clientMessageId) {
        this.clientMessageId = clientMessageId;
    }

    public String getClientChatId() {
        return clientChatId;
    }

    public void setClientChatId(String clientChatId) {
        this.clientChatId = clientChatId;
    }
}

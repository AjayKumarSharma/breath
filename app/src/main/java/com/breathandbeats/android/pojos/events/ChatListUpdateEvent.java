package com.breathandbeats.android.pojos.events;

public class ChatListUpdateEvent {
    String chatId;

    public ChatListUpdateEvent(String chatId) {
        this.chatId = chatId;
    }

    public String getChatId() {

        return chatId;
    }
//
    public void setChatId(String chatId) {
        this.chatId = chatId;
    }
}

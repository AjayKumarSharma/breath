package com.breathandbeats.android.pojos.socket;

public class OnMessageSuccessEvent {
    Object obj;

    public OnMessageSuccessEvent(Object obj) {
        this.obj = obj;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
}
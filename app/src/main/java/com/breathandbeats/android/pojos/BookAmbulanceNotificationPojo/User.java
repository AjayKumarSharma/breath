package com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lavanya on 31-10-2017.
 */
public class User implements Serializable
{

    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("deviceInfo")
    @Expose
    private DeviceInfo_ deviceInfo;
    private final static long serialVersionUID = -1667169631513185886L;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DeviceInfo_ getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo_ deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

}
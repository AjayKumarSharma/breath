package com.breathandbeats.android.pojos.api;

public class VerifyOTPRequest {
    String verificationCode;

    public VerifyOTPRequest(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }
}

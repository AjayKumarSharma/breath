package com.breathandbeats.android.pojos.RV;


import java.util.Date;

public class SentRequestLogItem {
    String name;
    Date dateTime;
    String status;
    String subject;
    String requestId;

    public SentRequestLogItem(String name, Date dateTime, String status, String subject, String requestId) {
        this.name = name;
        this.dateTime = dateTime;
        this.status = status;
        this.subject = subject;
        this.requestId = requestId;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @Override
    public String
    toString() {
        return "SentRequestLogItem{" +
                "name='" + name + '\'' +
                ", dateTime=" + dateTime +
                ", status='" + status + '\'' +
                ", subject='" + subject + '\'' +
                ", requestId='" + requestId + '\'' +
                '}';
    }
}

package com.breathandbeats.android.pojos.orm;


import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.ArrayList;
import java.util.Date;

public class SentRequest extends SugarRecord {
    @Unique
    String requestID;
    String status;
    String subject;
    String reason;
    String message;
    Integer distance;
    ArrayList<Double> location;
    Date createdAt;

    public SentRequest(String requestID, String status, String subject, String reason, String message, Integer distance, ArrayList<Double> location, Date createdAt) {
        this.requestID = requestID;
        this.status = status;
        this.subject = subject;
        this.reason = reason;
        this.message = message;
        this.distance = distance;
        this.location = location;
        this.createdAt = createdAt;
    }

    public Date getCreatedAt() {

        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public SentRequest() {
    }


    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String  getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public ArrayList<Double> getLocation() {
        return location;
    }

    public void setLocation(ArrayList<Double> location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "SentRequest{" +
                "requestID='" + requestID + '\'' +
                ", status='" + status + '\'' +
                ", subject='" + subject + '\'' +
                ", reason='" + reason + '\'' +
                ", message='" + message + '\'' +
                ", distance=" + distance +
                ", location=" + location +
                ", createdAt=" + createdAt +
                '}';
    }
}


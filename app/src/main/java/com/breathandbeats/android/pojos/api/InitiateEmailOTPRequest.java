package com.breathandbeats.android.pojos.api;

public class InitiateEmailOTPRequest {
    String resend;

    public String getResend() {
        return resend;
    }

    public void setResend(String resend) {
        this.resend = resend;
    }
}

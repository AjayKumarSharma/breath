package com.breathandbeats.android.pojos.notifs;

public class MakeANoiseNotificationData {
    private String senderId;
    private String subject;
    private String message;
    private String metaData;
    private Location location;
    private Integer distance;
    private String requestId;
    private String senderName;
    private String reason;




    public MakeANoiseNotificationData(String senderId, String subject, String message, String metaData, Location location, Integer distance, String requestId, String senderName,String reason) {
        this.senderId = senderId;
        this.subject = subject;
        this.message = message;
        this.metaData = metaData;
        this.location = location;
        this.distance = distance;
        this.requestId = requestId;
        this.senderName = senderName;
        this.reason=reason;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getRequestID() {
        return requestId;
    }

    public void setRequestID(String requestId) {
        this.requestId = requestId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "MakeANoiseNotificationData{" +
                "senderId='" + senderId + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", metaData='" + metaData + '\'' +
                ", location=" + location +
                ", distance=" + distance +
                ", requestID='" + requestId + '\'' +
                '}';
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }
}


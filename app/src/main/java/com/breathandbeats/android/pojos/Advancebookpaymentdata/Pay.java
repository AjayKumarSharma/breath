package com.breathandbeats.android.pojos.Advancebookpaymentdata;

import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.RazorPay;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lavanya on 28-11-2017.
 */

public class Pay {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("trip")
    @Expose
    private String trip;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("capturedOnApp")
    @Expose
    private String capturedOnApp;
    @SerializedName("razorPay")
    @Expose
    private RazorPay razorPay;
    @SerializedName("paymentGateway")
    @Expose
    private String paymentGateway;
    @SerializedName("gstPercentage")
    @Expose
    private Integer gstPercentage;
    @SerializedName("gstCost")
    @Expose
    private float gstCost;
    @SerializedName("totalFare")
    @Expose
    private float totalFare;
    @SerializedName("estimatedFare")
    @Expose
    private Integer estimatedFare;
    @SerializedName("range")
    @Expose
    private String range;
    @SerializedName("totalDistance")
    @Expose
    private Float totalDistance;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("paymentStatus")
    @Expose
    private String paymentStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getTrip() {
        return trip;
    }

    public void setTrip(String trip) {
        this.trip = trip;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public String getCapturedOnApp() {
        return capturedOnApp;
    }

    public void setCapturedOnApp(String capturedOnApp) {
        this.capturedOnApp = capturedOnApp;
    }

    public RazorPay getRazorPay() {
        return razorPay;
    }

    public void setRazorPay(RazorPay razorPay) {
        this.razorPay = razorPay;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public Integer getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(Integer gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public float getGstCost() {
        return gstCost;
    }

    public void setGstCost(float gstCost) {
        this.gstCost = gstCost;
    }

    public float getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(float totalFare) {
        this.totalFare = totalFare;
    }

    public Integer getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(Integer estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public Float getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Float totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

}

package com.breathandbeats.android.pojos.socket;


import com.google.gson.annotations.SerializedName;

public class GeneralChatMessage {
    @SerializedName("message")
    ChatMessage chatMessage;

    public ChatMessage getChatMessage() {
        return chatMessage;
    }

    public void setChatMessage(ChatMessage chatMessage) {
        this.chatMessage = chatMessage;
    }
}

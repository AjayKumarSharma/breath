package com.breathandbeats.android.pojos.HistoryTripListPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by rakesh on 02-11-2017.
 */

public class AdvannceBookedTripData {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<HistoryTripData> data = null;
    private final static long serialVersionUID = -1851907735460734751L;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<HistoryTripData> getData() {
        return data;
    }

    public void setData(List<HistoryTripData> data) {
        this.data = data;
    }
}

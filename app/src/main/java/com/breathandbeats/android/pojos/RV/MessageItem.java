package com.breathandbeats.android.pojos.RV;


import java.util.Date;

public class MessageItem {
    String text;
    boolean isCreatedBySelf;
    Date dateTime;

    public MessageItem(String text, boolean isCreatedBySelf, Date dateTime) {
        this.text = text;
        this.isCreatedBySelf = isCreatedBySelf;
        this.dateTime = dateTime;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public boolean isCreatedBySelf() {

        return isCreatedBySelf;
    }

    public void setCreatedBySelf(boolean createdBySelf) {
        isCreatedBySelf = createdBySelf;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

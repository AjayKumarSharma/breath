package com.breathandbeats.android.pojos.notifs;

//sosType: 'emergencyContact',
//        sosId: 59024e1f490cd83b3d2fc4be,
//        userId:'1234'
//        senderName: 'pappu',
//        location:
//        {
//        coordinates: []
//        }
public class SOSNotificationData {
    String createdAt;
    String userId;
    String senderName;
    String sosId;
    String sosType;
    Location location;

    public SOSNotificationData(String createdAt, String userId, String senderName, String sosId, String sosType, Location location) {
        this.createdAt = createdAt;
        this.userId = userId;
        this.senderName = senderName;
        this.sosId = sosId;
        this.sosType = sosType;
        this.location = location;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSosId() {
        return sosId;
    }

    public void setSosId(String sosId) {
        this.sosId = sosId;
    }

    public String getSosType() {
        return sosType;
    }

    public void setSosType(String sosType) {
        this.sosType = sosType;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}

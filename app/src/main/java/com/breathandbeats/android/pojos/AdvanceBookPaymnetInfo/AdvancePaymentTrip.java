package com.breathandbeats.android.pojos.AdvanceBookPaymnetInfo;

import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.RazorPay;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lavanya on 23-11-2017.
 */

public class AdvancePaymentTrip {
    @SerializedName("totalFare")
    @Expose
    private Integer totalFare;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;
    @SerializedName("gstPercentage")
    @Expose
    private Integer gstPercentage;
    @SerializedName("estimatedFare")
    @Expose
    private Integer estimatedFare;
    @SerializedName("range")
    @Expose
    private String range;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("trip")
    @Expose
    private String trip;
    @SerializedName("gstCost")
    @Expose
    private Integer gstCost;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("razorPay")
    @Expose
    private RazorPay razorPay;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("totalDistance")
    @Expose
    private Integer totalDistance;
    @SerializedName("paymentStatus")
    @Expose
    private String paymentStatus;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("paymentGateway")
    @Expose
    private String paymentGateway;

    public Integer getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(Integer totalFare) {
        this.totalFare = totalFare;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Integer getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(Integer gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public Integer getEstimatedFare() {
        return estimatedFare;
    }

    public void setEstimatedFare(Integer estimatedFare) {
        this.estimatedFare = estimatedFare;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getTrip() {
        return trip;
    }

    public void setTrip(String trip) {
        this.trip = trip;
    }

    public Integer getGstCost() {
        return gstCost;
    }

    public void setGstCost(Integer gstCost) {
        this.gstCost = gstCost;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public RazorPay getRazorPay() {
        return razorPay;
    }

    public void setRazorPay(RazorPay razorPay) {
        this.razorPay = razorPay;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(Integer totalDistance) {
        this.totalDistance = totalDistance;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

}

package com.breathandbeats.android.pojos.RV;


public class SOSLogItem {
    String date;
    String typeOfSOS;

    public SOSLogItem(String date, String typeOfSOS) {
        this.date = date;
        this.typeOfSOS = typeOfSOS;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTypeOfSOS() {
        return typeOfSOS;
    }

    public void setTypeOfSOS(String typeOfSOS) {
        this.typeOfSOS = typeOfSOS;
    }

    @Override
    public String toString() {
        return "SOSLogItem{" +
                "date='" + date + '\'' +
                ", typeOfSOS='" + typeOfSOS + '\'' +
                '}';
    }
}
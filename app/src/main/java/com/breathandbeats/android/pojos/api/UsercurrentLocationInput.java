package com.breathandbeats.android.pojos.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Rakesh on 04-10-2017.
 */

public class UsercurrentLocationInput implements Serializable {

    @SerializedName("emergencyType")
    @Expose
    private String emergencyType;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("userAddress")
    @Expose
    private String userAddress;

    @SerializedName("userCurrentlocation")
    @Expose
    private List<String> userCurrentlocation = null;
    @SerializedName("paymentMode")
    @Expose
    private String paymentMode;

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    private final static long serialVersionUID = -6022931133323159036L;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getEmergencyType() {
        return emergencyType;
    }

    public void setEmergencyType(String emergencyType) {
        this.emergencyType = emergencyType;
    }

    public List<String> getUserCurrentlocation() {
        return userCurrentlocation;
    }

    public void setUserCurrentlocation(List<String> userCurrentlocation) {
        this.userCurrentlocation = userCurrentlocation;
    }

}

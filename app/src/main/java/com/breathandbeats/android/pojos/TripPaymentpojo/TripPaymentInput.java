package com.breathandbeats.android.pojos.TripPaymentpojo;

/**
 * Created by lavanya on 18-11-2017.
 */

public class TripPaymentInput {

    String tripId;
    boolean isPaid;
    String paymentStatus;
    String razorpayPaymentId;
    boolean captured;
    String capturedOnApp;

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getRazorpayPaymentId() {
        return razorpayPaymentId;
    }

    public void setRazorpayPaymentId(String razorpayPaymentId) {
        this.razorpayPaymentId = razorpayPaymentId;
    }

    public boolean isCaptured() {
        return captured;
    }

    public void setCaptured(boolean captured) {
        this.captured = captured;
    }

    public String getCapturedOnApp() {
        return capturedOnApp;
    }

    public void setCapturedOnApp(String capturedOnApp) {
        this.capturedOnApp = capturedOnApp;
    }
}

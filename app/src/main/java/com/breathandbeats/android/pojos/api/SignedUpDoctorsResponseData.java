package com.breathandbeats.android.pojos.api;


import com.breathandbeats.android.pojos.notifs.Location;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SignedUpDoctorsResponseData {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("emailId")
    @Expose
    private String emailId;
    @SerializedName("primaryContact")
    @Expose
    private String primaryContact;
    @SerializedName("cost")
    @Expose
    private String cost;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("appVersion")
    @Expose
    private String appVersion;
    @SerializedName("platform")
    @Expose
    private String platform;
    @SerializedName("experience")
    @Expose
    private String experience;
    @SerializedName("specialization")
    @Expose
    private String specialization;
    @SerializedName("deviceInfo")
    @Expose
    private DeviceInfo deviceInfo;
    @SerializedName("lastLocation")
    @Expose
    private Location lastLocation;
    @SerializedName("servicesOffered")
    @Expose
    private List<String> servicesOffered = null;
    @SerializedName("workingTime")
    @Expose
    private List<String> workingTime = null;
    @SerializedName("workingDays")
    @Expose
    private List<String> workingDays = null;
    @SerializedName("homeHealthCare")
    @Expose
    private Boolean homeHealthCare;
    @SerializedName("otherContactNumber")
    @Expose
    private List<String> otherContactNumber = null;
    @SerializedName("category")
    @Expose
    private String category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public DeviceInfo getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfo deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public Location getLastLocation() {
        return lastLocation;
    }

    public void setLastLocation(Location lastLocation) {
        this.lastLocation = lastLocation;
    }

    public List<String> getServicesOffered() {
        return servicesOffered;
    }

    public void setServicesOffered(List<String> servicesOffered) {
        this.servicesOffered = servicesOffered;
    }

    public List<String> getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(List<String> workingTime) {
        this.workingTime = workingTime;
    }

    public List<String> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(List<String> workingDays) {
        this.workingDays = workingDays;
    }

    public Boolean getHomeHealthCare() {
        return homeHealthCare;
    }

    public void setHomeHealthCare(Boolean homeHealthCare) {
        this.homeHealthCare = homeHealthCare;
    }

    public List<String> getOtherContactNumber() {
        return otherContactNumber;
    }

    public void setOtherContactNumber(List<String> otherContactNumber) {
        this.otherContactNumber = otherContactNumber;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}

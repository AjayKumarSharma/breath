package com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by lavanya on 10-11-2017.
 */

public class CancelTheTripRequested implements Serializable
{

    @SerializedName("success")
    @Expose
    private Boolean success;
//    @SerializedName("data")
//    @Expose
////    private Data data;
    private final static long serialVersionUID = 6000314287062124362L;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

   /* public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }*/

}
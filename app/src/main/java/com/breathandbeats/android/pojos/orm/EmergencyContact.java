package com.breathandbeats.android.pojos.orm;


import com.orm.SugarRecord;

public class EmergencyContact extends SugarRecord {
    String name;
    String relationship;
    String contactNumber;
    String email;

    public EmergencyContact() {
    }

    public EmergencyContact(String name, String relationship, String contactNumber, String email) {
        this.name = name;
        this.relationship = relationship;
        this.contactNumber = contactNumber;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

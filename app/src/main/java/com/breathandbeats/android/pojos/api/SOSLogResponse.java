package com.breathandbeats.android.pojos.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SOSLogResponse {
    private String message;

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("data")
    @Expose
    private List<SosLogResponseData> sosLogResponseDataList = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<SosLogResponseData> getSosLogResponseDataList() {
        return sosLogResponseDataList;
    }

    public void setSosLogResponseDataList(List<SosLogResponseData> sosLogResponseDataList) {
        this.sosLogResponseDataList = sosLogResponseDataList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}


package com.breathandbeats.android.pojos.api;

public class InitiateOTPRequest {
    String resend;

    public String getResend() {
        return resend;
    }

    public void setResend(String resend) {
        this.resend = resend;
    }
}

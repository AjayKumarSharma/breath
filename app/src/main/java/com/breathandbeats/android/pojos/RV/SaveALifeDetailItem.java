package com.breathandbeats.android.pojos.RV;


public class SaveALifeDetailItem {

    String id;
    String title;
    String category;
    String videoUrl;
    String description;

    public SaveALifeDetailItem(String id, String title, String category, String videoUrl, String description) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.videoUrl = videoUrl;
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public String getDescription() {
        return description;
    }
}

package com.breathandbeats.android.pojos.RV;

import java.util.Date;

public class RepliesItem {
    String name;
    String msg;
    Date dateTime;
    String chatID;

    public RepliesItem(String name, String msg, Date dateTime, String chatID) {

        this.name = name;
        this.msg = msg;
        this.dateTime = dateTime;
        this.chatID = chatID;
    }

    public String getChatID() {
        return chatID;
    }

    public void setChatID(String chatID) {
        this.chatID = chatID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }
}

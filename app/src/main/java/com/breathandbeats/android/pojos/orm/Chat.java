package com.breathandbeats.android.pojos.orm;


import com.orm.SugarRecord;
import com.orm.dsl.Unique;

import java.util.Date;

public class Chat extends SugarRecord {
    String requestId;
    @Unique
    String chatID;
    String requestType;
    String otherUser;
    boolean canChat;
    Date createdAt;

    public Chat(String requestId, String chatID, String requestType, String otherUser, boolean canChat, Date createdAt) {
        this.requestId = requestId;
        this.chatID = chatID;
        this.requestType = requestType;
        this.otherUser = otherUser;
        this.canChat = canChat;
        this.createdAt = createdAt;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public boolean isCanChat() {
        return canChat;
    }

    public void setCanChat(boolean canChat) {
        this.canChat = canChat;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getOtherUser() {
        return otherUser;
    }

    public void setOtherUser(String otherUser) {
        this.otherUser = otherUser;
    }

    public Chat() {
    }


    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getChatID() {
        return chatID;
    }

    public void setChatID(String chatID) {
        this.chatID = chatID;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "requestId='" + requestId + '\'' +
                ", chatID='" + chatID + '\'' +
                ", requestType='" + requestType + '\'' +
                ", otherUser='" + otherUser + '\'' +
                ", canChat=" + canChat +
                ", createdAt=" + createdAt +
                '}';
    }
}

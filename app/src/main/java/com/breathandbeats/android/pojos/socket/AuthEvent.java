package com.breathandbeats.android.pojos.socket;

public class AuthEvent {
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

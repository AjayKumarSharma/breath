package com.breathandbeats.android.pojos.AdvanceBookPaymnetInfo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by lavanya on 23-11-2017.
 */

public class AdvancePaymnetInfo {
    @SerializedName("trip")
    @Expose
    private AdvancePaymentTrip trip;
    @SerializedName("advanceBooking")
    @Expose
    private Boolean advanceBooking;

    public AdvancePaymentTrip getTrip() {
        return trip;
    }

    public void setTrip(AdvancePaymentTrip trip) {
        this.trip = trip;
    }

    public Boolean getAdvanceBooking() {
        return advanceBooking;
    }

    public void setAdvanceBooking(Boolean advanceBooking) {
        this.advanceBooking = advanceBooking;
    }

}

package com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by lavanya on 31-10-2017.
 */
public class DriverData implements Serializable
{

    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("driverCurrentCoordinates")
    @Expose
    private List<Double> driverCurrentCoordinates = null;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("deviceInfo")
    @Expose
    private DeviceInfoData deviceInfo;
    @SerializedName("username")
    @Expose
    private String username;
    private final static long serialVersionUID = 23210381836669729L;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Double> getDriverCurrentCoordinates() {
        return driverCurrentCoordinates;
    }

    public void setDriverCurrentCoordinates(List<Double> driverCurrentCoordinates) {
        this.driverCurrentCoordinates = driverCurrentCoordinates;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public DeviceInfoData getDeviceInfo() {
        return deviceInfo;
    }

    public void setDeviceInfo(DeviceInfoData deviceInfo) {
        this.deviceInfo = deviceInfo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
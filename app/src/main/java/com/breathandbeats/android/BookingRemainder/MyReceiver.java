package com.breathandbeats.android.BookingRemainder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;

/**
 * Created by lavanya on 16-11-2017.
 */

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent)
    {
      /*Intent service1 = new Intent(context, MyAlarmService.class);
        context.startService(service1);*/

        Log.d("my_receiver", "called receiver method");

        if(intent.hasExtra("trip_id")){

            Log.d("my_receiver"," trip id @alaram : "+intent.getStringExtra("trip_id").toString());
            Log.d("my_receiver"," driver_name @alaram : "+intent.getStringExtra("driver_name").toString());
            Log.d("my_receiver"," driver_number @alaram : "+intent.getStringExtra("driver_number").toString());

           SharedPreferenceBnBUtility.setNotificationTripID(context,intent.getStringExtra("trip_id").toString());

            SharedPreferenceBnBUtility.setDriverName(context,intent.getStringExtra("driver_name").toString());
            SharedPreferenceBnBUtility.setDriverNumber(context,intent.getStringExtra("driver_number").toString());

            SharedPreferenceBnBUtility.setNotificationHospitalAddress(context,intent.getStringExtra("hospital_address").toString());
            SharedPreferenceBnBUtility.setNotificationHospitalName(context,intent.getStringExtra("hospital_name").toString());
            SharedPreferenceBnBUtility.setNotificationHospitalNumber(context,intent.getStringExtra("hospital_number").toString());
           SharedPreferenceBnBUtility.setDriverHospitalID(context,intent.getStringExtra("hospital_ID").toString());

            SharedPreferenceBnBUtility.setNotificationPatientLatitude(context,intent.getStringExtra("patient_lat").toString());
            SharedPreferenceBnBUtility.setNotificationPatientLongitude(context,intent.getStringExtra("patient_long").toString());

            SharedPreferenceBnBUtility.setDriverLatitude(context,intent.getStringExtra("driver_lat").toString());
            SharedPreferenceBnBUtility.setDriverLongitude(context,intent.getStringExtra("driver_long").toString());

            SharedPreferenceBnBUtility.setNotificationRecived(context,"true");
            SharedPreferenceBnBUtility.setTripPaymentRecived(context,"false");
            SharedPreferenceBnBUtility.setAdvanceTrip(context,"true");

        }
        try{
            RemainderBooking.generateNotification(context);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
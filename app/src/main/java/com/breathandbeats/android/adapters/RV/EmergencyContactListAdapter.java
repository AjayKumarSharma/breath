package com.breathandbeats.android.adapters.RV;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.pojos.events.DeleteContactEvent;
import com.breathandbeats.android.pojos.orm.EmergencyContact;
import com.breathandbeats.android.ui.activities.UpdateEmergencyContactActivity;
import com.breathandbeats.android.viewholders.EmergencyContactListVH;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class EmergencyContactListAdapter extends RecyclerView.Adapter<EmergencyContactListVH> {
    ArrayList<EmergencyContact> emergencyContacts;
    Context context;
    BnBApp bnBApp;
    DBHelper dbHelper;
    EventBus bus;

    public EmergencyContactListAdapter(ArrayList<EmergencyContact> emergencyContacts, Context context) {
        this.emergencyContacts = emergencyContacts;
        this.context = context;
        bnBApp = BnBApp.getInstance();
        dbHelper = bnBApp.getDBHelper();
        bus = EventBus.getDefault();
    }

    @Override
    public EmergencyContactListVH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.item_emergency_contact, parent, false);
        return new EmergencyContactListVH(v);
    }

    @Override
    public void onBindViewHolder(EmergencyContactListVH holder, int position) {
        EmergencyContact emergencyContact = emergencyContacts.get(position);
        holder.getNameTV().setText(emergencyContact.getName());
        holder.getEmailTV().setText(emergencyContact.getEmail());
        holder.getNumTV().setText(emergencyContact.getContactNumber());
        holder.getRelationShipTV().setText(emergencyContact.getRelationship());
        holder.getDeleteContactBtn().setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage("Are you sure?")
                    .setPositiveButton("Yes", (dialogInterface, i) -> deleteContact(emergencyContact))
                    .setNegativeButton("No", (dialogInterface, i) -> {
                    }).show();
        });

        holder.getEditContactBtn().setOnClickListener(view -> {
            Intent emergencyContactIntent = new Intent(context, UpdateEmergencyContactActivity.class);
            emergencyContactIntent.putExtra("name", emergencyContact.getName());
            emergencyContactIntent.putExtra("email", emergencyContact.getEmail());
            emergencyContactIntent.putExtra("number", emergencyContact.getContactNumber());
            emergencyContactIntent.putExtra("relationship", emergencyContact.getRelationship());
            context.startActivity(emergencyContactIntent);

        });

    }

    private void deleteContact(EmergencyContact emergencyContact) {
        dbHelper.deleteContact(emergencyContact.getContactNumber());
        bus.post(new DeleteContactEvent());
    }

    @Override
    public int getItemCount() {
        return emergencyContacts.size();
    }


}

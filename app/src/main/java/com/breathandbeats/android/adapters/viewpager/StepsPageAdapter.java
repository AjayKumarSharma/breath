package com.breathandbeats.android.adapters.viewpager;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.view_pager.StepItem;

import java.util.ArrayList;

public class StepsPageAdapter extends PagerAdapter{


    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private final ArrayList<StepItem> stepItems;

    public StepsPageAdapter(ArrayList<StepItem> stepItems, Context context) {
        this.stepItems  = stepItems;
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        return stepItems.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (View)object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = mLayoutInflater.inflate(R.layout.step_layout, container, false);
        TextView detailTV = (TextView) view.findViewById(R.id.step_layout_detail_TV);
        TextView titleTV = (TextView) view.findViewById(R.id.step_layout_Title_TV);
        StepItem stepItem = stepItems.get(position);
        detailTV.setText(stepItem.getDetail());
        titleTV.setText(stepItem.getTitle());
        container.addView(view);
        return view;
    }



//    @Override
//    public int getItemPosition(Object object) {
//         StepItem stepItem = (StepItem) ((View)object).getTag();
//        int position = stepItems.indexOf(stepItem);
//        return position;
//    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}


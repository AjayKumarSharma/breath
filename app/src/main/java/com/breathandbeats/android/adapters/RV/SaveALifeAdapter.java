package com.breathandbeats.android.adapters.RV;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.RV.SaveALifeItem;
import com.breathandbeats.android.viewholders.SaveALifeViewHolder;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SaveALifeAdapter extends RecyclerView.Adapter<SaveALifeViewHolder> {
    private ArrayList<SaveALifeItem> saveALifeItemList;
    private Context context;

    public SaveALifeAdapter(ArrayList<SaveALifeItem> saveALifeItemList, Context context) {
        this.saveALifeItemList = saveALifeItemList;
        this.context = context;
    }

    @Override
    public SaveALifeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View saveALifeView = inflater.inflate(R.layout.item_save_a_life, parent, false);
        SaveALifeViewHolder viewHolder = new SaveALifeViewHolder(saveALifeView, context, saveALifeItemList);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SaveALifeViewHolder holder, int position) {
        SaveALifeItem saveALifeItem = saveALifeItemList.get(position);
        TextView textView = holder.titleTV;
        textView.setText(saveALifeItem.getTitle());
        CircleImageView circleImageView = holder.itemImageView;
    }

    @Override
    public int getItemCount() {
        return saveALifeItemList.size();
    }

}

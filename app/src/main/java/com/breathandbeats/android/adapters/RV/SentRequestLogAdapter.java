package com.breathandbeats.android.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.BnBService;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.RV.SentRequestLogItem;
import com.breathandbeats.android.pojos.api.GenericResponse;
import com.breathandbeats.android.pojos.api.RequestUpdateRequest;
import com.breathandbeats.android.ui.activities.RepliesActivity;
import com.breathandbeats.android.utils.Shared;
import com.breathandbeats.android.viewholders.SentRequestLogViewHolder;

import java.util.ArrayList;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SentRequestLogAdapter extends RecyclerView.Adapter<SentRequestLogViewHolder> {
    private static final String TAG = "SentRequestLogAdapter";

    Context context;
    ArrayList<SentRequestLogItem> sentRequestLogItemArrayList;
    BnBApp application;
    DBHelper dbHelper;
    String requestId;
    SharedPrefHelper sharedPrefHelper;
    BnBService bnBService;
    String authToken;

    public SentRequestLogAdapter(Context context, ArrayList<SentRequestLogItem> sentRequestLogItemArrayList) {
        this.context = context;
        this.sentRequestLogItemArrayList = sentRequestLogItemArrayList;
        application = BnBApp.getInstance();
        dbHelper = application.getDBHelper();
        bnBService = application.getRestClient().getApiSerivice();
        sharedPrefHelper = application.getSharedPrefHelper();
        authToken = sharedPrefHelper.getAuthToken();
    }
    @Override
    public SentRequestLogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_sent_request, parent, false);
        return new SentRequestLogViewHolder(view);
    }

    @Override

    public void onBindViewHolder(SentRequestLogViewHolder holder, int position) {
        SentRequestLogItem sentRequestLogItem = sentRequestLogItemArrayList.get(position);
        requestId = sentRequestLogItem.getRequestId();
        String status = sentRequestLogItem.getStatus();
        int chatCount = dbHelper.getChatCount(requestId);
        int length = String.valueOf(chatCount).length();
        Date dateTime = sentRequestLogItem.getDateTime();
        holder.getDateTimeTV().setText(Shared.getFormattedDate(dateTime));
        holder.getStatusTV().setText(sentRequestLogItem.getStatus());
        holder.getSubjectTV().setText(sentRequestLogItem.getSubject());
        String buttonText = chatCount + context.getString(R.string.chatCount);
        Log.d(TAG,"chatCount : "+chatCount);
        SpannableString text = new SpannableString(buttonText);
        int greenColor = ContextCompat.getColor(context, R.color.green);
        text.setSpan(greenColor, 0, length, 0);

        holder.getReplyBtn().setText(text);
        if (status.equals("closed")) {
            holder.getCloseRequestBtn().setText("Closed");
            holder.getCloseRequestBtn().setEnabled(false);
        }
        holder.getReplyBtn().setOnClickListener(view -> {
            if (!status.equals("closed")) {
                Intent intent = new Intent(context, RepliesActivity.class);
                intent.putExtra("requestId", requestId);
                context.startActivity(intent);
            } else {
                Toast.makeText(context, "This request has been closed", Toast.LENGTH_SHORT).show();
            }
        });
        holder.getCloseRequestBtn().setOnClickListener(view -> {
            RequestUpdateRequest requestUpdateRequest = new RequestUpdateRequest("closed");
            bnBService.sendCloseRequest("Bearer " + authToken, requestId, requestUpdateRequest).enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                    Log.i(TAG, "onResponse: ");
                    dbHelper.updateSentRequest(requestId);
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    Log.i(TAG, "onFailure: ");
                }
            });
        });
    }

    @Override
    public int getItemCount() {
        return sentRequestLogItemArrayList.size();
    }
}
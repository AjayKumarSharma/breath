package com.breathandbeats.android.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.RV.MessageItem;
import com.breathandbeats.android.utils.Shared;
import com.breathandbeats.android.viewholders.OtherMessageViewHolder;
import com.breathandbeats.android.viewholders.SelfMessageViewHolder;

import java.util.ArrayList;
import java.util.Date;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "ChatAdapter";

    Context context;
    ArrayList<MessageItem> messageItems;
    int messageType;
    public static final int SELF = 1;
    public static final int OTHER = 2;

    public ChatAdapter(Context context, ArrayList<MessageItem> messageItems) {
        this.context = context;
        this.messageItems = messageItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case SELF:
                View v1 = inflater.inflate(R.layout.item_message_self, parent, false);
                viewHolder = new SelfMessageViewHolder(v1);
                break;
            case OTHER:
                View v2 = inflater.inflate(R.layout.item_message_other, parent, false);
                viewHolder = new OtherMessageViewHolder(v2);
                break;
            default:
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case SELF:
                SelfMessageViewHolder selfMessageViewHolder = (SelfMessageViewHolder) holder;
                configureSelfMessageViewHolder(selfMessageViewHolder, position);
                break;
            case OTHER:
                OtherMessageViewHolder otherMessageViewHolder = (OtherMessageViewHolder) holder;
                configureOtherMessageViewHolder(otherMessageViewHolder, position);
                break;
            default:
                break;
        }
    }

    private void configureOtherMessageViewHolder(OtherMessageViewHolder otherMessageViewHolder, int position) {
        MessageItem messageItem = messageItems.get(position);
        otherMessageViewHolder.getMessageTV().setText(messageItem.getText());
        Date dateTime = messageItem.getDateTime();
        String formattedDate = Shared.getFormattedDate(dateTime);
        otherMessageViewHolder.getCreatedAtTV().setText(formattedDate);
    }

    private void configureSelfMessageViewHolder(SelfMessageViewHolder selfMessageViewHolder, int position) {
        MessageItem messageItem = messageItems.get(position);
        selfMessageViewHolder.getMessageTV().setText(messageItem.getText());
        Date dateTime = messageItem.getDateTime();
        String formattedDate = Shared.getFormattedDate(dateTime);
        selfMessageViewHolder.getCreatedAtTV().setText(formattedDate);
    }

    @Override
    public int getItemCount() {
        return messageItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        MessageItem messageItem = messageItems.get(position);
        if (messageItem.isCreatedBySelf()) {
            messageType = SELF;
        } else {
            messageType = OTHER;
        }
        return messageType;

    }
}

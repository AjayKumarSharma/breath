package com.breathandbeats.android.adapters.RV;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.RV.SOSLogItem;
import com.breathandbeats.android.viewholders.SOSLogViewHolder;

import java.util.ArrayList;

public class SOSLogAdapter extends RecyclerView.Adapter<SOSLogViewHolder> {
    private ArrayList<SOSLogItem> sosLogItems;
    private Context context;

    public SOSLogAdapter(ArrayList<SOSLogItem> sosLogItems, Context context) {
        this.sosLogItems = sosLogItems;
        this.context = context;
    }

    @Override
    public SOSLogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View sosView = inflater.inflate(R.layout.item_sos_log, parent, false);

        // Return a new holder instance
        SOSLogViewHolder sosLogViewHolder = new SOSLogViewHolder(sosView);
        return sosLogViewHolder;
    }

    @Override
    public void onBindViewHolder(SOSLogViewHolder holder, int position) {
        SOSLogItem sosLogItem = sosLogItems.get(position);
        TextView typeOfSOSTV = holder.typeOfSOSTV;
        TextView dateTV = holder.dateTV;
        typeOfSOSTV.setText(sosLogItem.getTypeOfSOS());
        dateTV.setText(sosLogItem.getDate());

    }

    @Override
    public int getItemCount() {
        return sosLogItems.size();
    }
}

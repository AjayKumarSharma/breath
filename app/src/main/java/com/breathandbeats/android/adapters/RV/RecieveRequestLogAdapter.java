package com.breathandbeats.android.adapters.RV;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.infrastructure.Constants;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.RV.RecieveRequestLogItem;
import com.breathandbeats.android.pojos.events.AcceptedEvent;
import com.breathandbeats.android.pojos.socket.AuthEvent;
import com.breathandbeats.android.pojos.socket.FirstTimeChatMessage;
import com.breathandbeats.android.pojos.socket.Message;
import com.breathandbeats.android.pojos.socket.MessageData;
import com.breathandbeats.android.pojos.socket.OnMessageSuccessEvent;
import com.breathandbeats.android.ui.activities.ChatActivity;
import com.breathandbeats.android.utils.Shared;
import com.breathandbeats.android.viewholders.RecieveRequestLogViewHolder;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;

public class RecieveRequestLogAdapter extends RecyclerView.Adapter<RecieveRequestLogViewHolder> {
    private static final String TAG = "RecieveRequestLogAdapte";
    Context context;
    ArrayList<RecieveRequestLogItem> recieveRequestLogItems;
    Dialog acceptanceMessageDialog;
    BnBApp app;
    Socket mSocket;
    EventBus bus;
    String token;
    SharedPrefHelper sharedPrefHelper;
    DBHelper dbHelper;

    private static final String EMIT_EVENT_AUTHENTICATION = "authentication";
    private static final String EVENT_AUTHENTICATED = "authenticated";
    private static final String EVENT_UNAUTHORIZED = "unauthorized";
    private static final String EVENT_MSG_SUCCESS = "success";
    private static final String EVENT_MSG_ERROR = "error";
    private static final String EMIT_EVENT_MESSAGE = "message";

    public RecieveRequestLogAdapter(Context context, ArrayList<RecieveRequestLogItem> recieveRequestLogItems) {
        this.context = context;
        this.recieveRequestLogItems = recieveRequestLogItems;
        bus = EventBus.getDefault();
        app = BnBApp.getInstance();
//        mSocket = app.getSocket();
        sharedPrefHelper = app.getSharedPrefHelper();
        dbHelper = app.getDBHelper();
        token = sharedPrefHelper.getAuthToken();
        try {
            mSocket = IO.socket(Constants.BASE_URL_STRING);
            BnBApp.getInstance().setSocket(mSocket);
        } catch (URISyntaxException e) {
            Log.i(TAG, "RecieveRequestLogAdapter: " + e.toString());
            throw new RuntimeException(e);
        }

        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(EVENT_AUTHENTICATED, onAuthenticated);
        mSocket.on(EVENT_UNAUTHORIZED, onUnauthorized);
        mSocket.on(EVENT_MSG_SUCCESS, onMsgSuccess);
        mSocket.on(EVENT_MSG_ERROR, onMsgError);
        mSocket.connect();
        Log.i(TAG, "RecieveRequestLogAdapter: " + mSocket.connected());
    }

    @Override
    public RecieveRequestLogViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.item_recieved_request, parent, false);
        return new RecieveRequestLogViewHolder(v);

    }

    @Override
    public void onBindViewHolder(RecieveRequestLogViewHolder holder, int position) {
        RecieveRequestLogItem recieveRequestLogItem = recieveRequestLogItems.get(position);
        String requestId = recieveRequestLogItem.getRequestId();
        String receiverId = recieveRequestLogItem.getReceiverId();
        Boolean canChat = recieveRequestLogItem.getCanChat();
        String chatId = recieveRequestLogItem.getChatId();
        boolean accepted = recieveRequestLogItem.isAccepted();
        holder.getSubjectTV().setText(recieveRequestLogItem.getSubject());
        holder.getStatusTV().setText(recieveRequestLogItem.getStatus());
        Date dateTime = recieveRequestLogItem.getDateTime();
        String formattedDate = Shared.getFormattedDate(dateTime);
        holder.getDateTV().setText(formattedDate);
        holder.getSenderNameTV().setText(recieveRequestLogItem.getName());
        boolean isRequestClosed = recieveRequestLogItem.getStatus().equals("closed");
        Log.d(TAG, "canChat: " + canChat);
        Log.d(TAG, "isRequestClosed: " + isRequestClosed);
        Log.d(TAG, "requestId: " + requestId);

        if (isRequestClosed) {
            holder.getAcceptBtn().setEnabled(false);
            holder.getAcceptBtn().setText("closed");
        }
        if (accepted) {
            holder.getAcceptBtn().setText("Accepted");
        }
        if (canChat) {
            holder.getAcceptBtn().setText("Chat");
        }
        holder.getAcceptBtn().setOnClickListener(view -> {
            if (!canChat) {
                if (!accepted) {
                    acceptanceMessageDialog = new Dialog(context);
                    acceptanceMessageDialog.setContentView(R.layout.fragment_acceptance_dialog);
                    EditText acceptanceMsgET = (EditText) acceptanceMessageDialog.findViewById(R.id.fragment_acceptance_ET);
                    Button submitBtn = (Button) acceptanceMessageDialog.findViewById(R.id.fragment_acceptance_btn);
                    submitBtn.setOnClickListener(btn -> {
                    sendMessage(acceptanceMsgET.getText().toString(), requestId, receiverId);

//                AcceptanceMessageEvent acceptanceMessageEvent = new AcceptanceMessageEvent(acceptanceMsgET.getText().toString(), requestId, receiverId);
//                bus.post(acceptanceMessageEvent);

                    acceptanceMessageDialog.dismiss();
                    });
                    acceptanceMessageDialog.show();
                } else {
                    Toast.makeText(context, "You have accepted the request. We will notify you once the user replies", Toast.LENGTH_SHORT).show();
                }

            } else {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("chatId", chatId);
                context.startActivity(intent);

            }
        });
    }

    private void sendMessage(String textMessage, String requestId, String receiverId) {
        Gson gson = new Gson();
        MessageData messageData = new MessageData(textMessage);
        FirstTimeChatMessage firstTimeChatMessage = new FirstTimeChatMessage();
        Message message = new Message();
        message.setMessageData(messageData);
        message.setReceiverId(receiverId);
        message.setRequestId(requestId);
        firstTimeChatMessage.setMessage(message);
        mSocket.emit(EMIT_EVENT_MESSAGE, gson.toJson(firstTimeChatMessage));
    }

    @Override
    public int getItemCount() {
        return recieveRequestLogItems.size();
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            AuthEvent event = new AuthEvent();
            event.setToken(token);
            Gson gson = new Gson();
            mSocket.emit(EMIT_EVENT_AUTHENTICATION, gson.toJson(event));
        }
    };

    private Emitter.Listener onAuthenticated = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (args.length > 0) {
//                bus.post(new OnAuthEvent(args[0]));
            }
        }
    };

    private Emitter.Listener onMsgSuccess = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            if (args.length > 0) {
                JSONObject data = (JSONObject) args[0];
                bus.post(new OnMessageSuccessEvent(args[0]));
                String requestId = "";
                String chatID = "";
                try {
                    requestId = (String) data.get("requestId");
                    chatID = (String) data.get("chatId");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                dbHelper.addChatIdToRecieveRequest(requestId, chatID);
                dbHelper.setAcceptedOnReceiveReauest(requestId);
                bus.post(new AcceptedEvent());


            }
        }
    };

    private Emitter.Listener onMsgError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (args.length > 0) {
//                bus.post(new OnMessageErrorEvent(args[0]));
            }
        }
    };

    private Emitter.Listener onUnauthorized = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (args.length > 0) {
//                bus.post(new OnUnAuthEvent(args[0]));
            }
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (args.length > 0) {
//                bus.post(new OnDisconnectEvent(args[0]));
            }
        }
    };

    private Emitter.Listener onConnectError = args -> {
        if (args.length > 0) {
//                bus.post(new OnConnectErrorEvent(args[0]));
            for (Object obj : args) {
            }
        }
    };
}
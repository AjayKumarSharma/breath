package com.breathandbeats.android.adapters.viewpager;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.breathandbeats.android.ui.fragments.FragmentSearchDoctor;
import com.breathandbeats.android.ui.fragments.FragmentSpeciality;
import com.breathandbeats.android.ui.fragments.HomeFragment;
import com.breathandbeats.android.ui.fragments.RequestFragment;

public class MainAdapter extends FragmentPagerAdapter {
    private Context context;
    private Fragment fragment = null;

    public MainAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                break;
            case 1:
                fragment = new RequestFragment();
                break;
            case 2:
                fragment = new FragmentSearchDoctor();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        int PAGE_COUNT = 2;
        return PAGE_COUNT;
    }


}

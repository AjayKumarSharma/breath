package com.breathandbeats.android.adapters.RV;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.breathandbeats.android.Models.DiscussionModelGegerSetter;
import com.breathandbeats.android.Models.DoctorSearchModel;
import com.breathandbeats.android.R;

import java.util.ArrayList;

/**
 * Created by Rocky on 3/3/2018.
 */

public class DoctorSearchCardAdpter extends RecyclerView.Adapter<DoctorSearchCardAdpter.ViewHolder> {
    private static ArrayList<DoctorSearchModel> dataSource;
    private Activity mActivity;


    public DoctorSearchCardAdpter(Context context, ArrayList<DoctorSearchModel> dataArgs){
        dataSource = dataArgs;
        mActivity = (Activity) context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        protected TextView textView;
        protected ImageView imageView;
        protected CardView cardView;
        DoctorSearchModel doctorSearchModel;
        public ViewHolder(View itemView) {
            super(itemView);
            textView =  (TextView) itemView.findViewById(R.id.msgDoctSearchFrom);
            imageView =  (ImageView) itemView.findViewById(R.id.ImgCheckUncheck);
            cardView =  (CardView) itemView.findViewById(R.id.card_DocSearch);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(imageView.getVisibility()==View.VISIBLE){
                        imageView.setVisibility(View.GONE);
                        String speciality= dataSource.get(getAdapterPosition()).getTxtSpeciality();
                        dataSource.set(getAdapterPosition(),new DoctorSearchModel(getAdapterPosition(),speciality,false));

                    }else{
                        imageView.setVisibility(View.VISIBLE);
                        String speciality= dataSource.get(getAdapterPosition()).getTxtSpeciality();
                        dataSource.set(getAdapterPosition(),new DoctorSearchModel(getAdapterPosition(),speciality,true));
                    }
                }
            });
        }


    }

    @Override
    public DoctorSearchCardAdpter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_searchdoctor, parent, false);

        DoctorSearchCardAdpter.ViewHolder viewHolder = new DoctorSearchCardAdpter.ViewHolder(view);
        return viewHolder;


    }

    @Override
    public void onBindViewHolder(DoctorSearchCardAdpter.ViewHolder holder, int position) {
        //holder.textView.setText(dataSource[position]);
        final DoctorSearchModel melody = dataSource.get(position);
        holder.textView.setText(melody.getTxtSpeciality());
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }


}

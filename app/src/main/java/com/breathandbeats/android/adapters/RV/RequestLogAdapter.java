package com.breathandbeats.android.adapters.RV;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.breathandbeats.android.ui.fragments.ReceivedRequestLogFragment;
import com.breathandbeats.android.ui.fragments.SentRequestLogFragment;

public class RequestLogAdapter extends FragmentStatePagerAdapter {
    private static int NUM_ITEMS = 2;
    private String tabTitles[] = new String[] { "Recieved Request", "Sent Request"};

    public RequestLogAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return new ReceivedRequestLogFragment();
            case 1: // Fragment # 0 - This will show FirstFragment different title
                return new SentRequestLogFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];

    }
}

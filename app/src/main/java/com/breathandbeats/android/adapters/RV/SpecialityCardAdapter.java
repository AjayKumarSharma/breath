package com.breathandbeats.android.adapters.RV;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.breathandbeats.android.Models.DiscussionModelGegerSetter;
import com.breathandbeats.android.R;

import java.util.ArrayList;

/**
 * Created by Rocky on 1/3/2018.
 */


    public class SpecialityCardAdapter extends RecyclerView.Adapter<SpecialityCardAdapter.ViewHolder> {
        private ArrayList<DiscussionModelGegerSetter> dataSource;
        private Activity mActivity;

        public SpecialityCardAdapter(Context context, ArrayList<DiscussionModelGegerSetter> dataArgs){
            dataSource = dataArgs;
            mActivity = (Activity) context;
        }


        @Override
        public SpecialityCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // create a new view
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_specialitybasedq, parent, false);

            SpecialityCardAdapter.ViewHolder viewHolder = new SpecialityCardAdapter.ViewHolder(view);
            return viewHolder;


        }

        @Override
        public void onBindViewHolder(SpecialityCardAdapter.ViewHolder holder, int position) {
            //holder.textView.setText(dataSource[position]);
        }

        @Override
        public int getItemCount() {
            return dataSource.size();
        }

        public static class ViewHolder extends RecyclerView.ViewHolder{
            protected TextView textView;
            public ViewHolder(View itemView) {
                super(itemView);
//                textView =  (TextView) itemView.findViewById(R.id.list_item);
//                textView =  (TextView) itemView.findViewById(R.id.list_item);
//                textView =  (TextView) itemView.findViewById(R.id.list_item);
//                textView =  (TextView) itemView.findViewById(R.id.list_item);

            }


        }
    }

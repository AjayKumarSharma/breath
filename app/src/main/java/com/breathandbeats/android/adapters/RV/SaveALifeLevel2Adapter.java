package com.breathandbeats.android.adapters.RV;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.RV.SaveALifeDetailItem;
import com.breathandbeats.android.viewholders.SaveALifeLevel2ViewHolder;
import com.breathandbeats.android.ui.activities.SaveaLife.SaveALifeDetailActivity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class SaveALifeLevel2Adapter extends RecyclerView.Adapter<SaveALifeLevel2ViewHolder> {
    Context context;
    ArrayList<SaveALifeDetailItem> saveALifeDetailItemArrayList;

    public SaveALifeLevel2Adapter(ArrayList<SaveALifeDetailItem> SaveALifeDetailItemList, Context context) {
        this.saveALifeDetailItemArrayList = SaveALifeDetailItemList;
        this.context = context;
    }

    @Override
    public SaveALifeLevel2ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        View saveALifeView = inflater.inflate(R.layout.item_save_a_life, parent, false);
        SaveALifeLevel2ViewHolder viewHolder = new SaveALifeLevel2ViewHolder(saveALifeView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SaveALifeLevel2ViewHolder holder, int position) {
        SaveALifeDetailItem saveALifeDetailItem = saveALifeDetailItemArrayList.get(position);
        TextView textView = holder.titleTV;
        textView.setText(saveALifeDetailItem.getTitle());
        CircleImageView circleImageView = holder.itemImageView;
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, SaveALifeDetailActivity.class);
                intent.putExtra("title", saveALifeDetailItem.getTitle());
                intent.putExtra("description", saveALifeDetailItem.getDescription());
                intent.putExtra("videoId", saveALifeDetailItem.getVideoUrl());
                context.startActivity(intent);
            }
        });

    }


    @Override
    public int getItemCount() {
        return saveALifeDetailItemArrayList.size();
    }
}

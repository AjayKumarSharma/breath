package com.breathandbeats.android.adapters;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.RV.RepliesItem;
import com.breathandbeats.android.ui.activities.ChatActivity;
import com.breathandbeats.android.utils.Shared;
import com.breathandbeats.android.viewholders.RepliesViewHolder;

import java.util.ArrayList;
import java.util.Date;

public class RepliesAdapter extends RecyclerView.Adapter<RepliesViewHolder> {
    Context context;
    ArrayList<RepliesItem> repliesItems;
    String TAG="Replies_Adapter";
    public RepliesAdapter(Context context, ArrayList<RepliesItem> repliesItems) {
        this.context = context;
        this.repliesItems = repliesItems;
    }

    @Override
    public RepliesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View v = layoutInflater.inflate(R.layout.item_reply, parent, false);
        Log.d(TAG," size : "+repliesItems.size());
        return new RepliesViewHolder(v, context);
    }

    @Override
    public void onBindViewHolder(RepliesViewHolder holder, int position) {
        Log.d(TAG,"position: "+position);
        RepliesItem repliesItem = repliesItems.get(position);
        String chatID = repliesItem.getChatID();
        Date dateTime = repliesItem.getDateTime();
        String name = repliesItem.getName();
        String formattedDate = Shared.getFormattedDate(dateTime);
        holder.getDateTimeTV().setText(formattedDate);
        holder.getMsgTV().setText(repliesItem.getMsg());
        holder.getNameTV().setText(repliesItem.getName());
        holder.getChatBtn().setOnClickListener(view -> {
            Intent intent = new Intent(context, ChatActivity.class);
            intent.putExtra("chatId", chatID);
            intent.putExtra("name",name);
            context.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return repliesItems.size();
    }
}

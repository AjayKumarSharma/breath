package com.breathandbeats.android.services;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.breathandbeats.android.R;
import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.NotificationBasePojo;
import com.breathandbeats.android.pojos.events.ChatListUpdateEvent;
import com.breathandbeats.android.pojos.notifs.Location;
import com.breathandbeats.android.pojos.notifs.MakeANoiseChatMessageNotificationData;
import com.breathandbeats.android.pojos.notifs.MakeANoiseNotificationData;
import com.breathandbeats.android.pojos.notifs.RequestUpdateNotificationData;
import com.breathandbeats.android.pojos.notifs.SOSNotificationData;
import com.breathandbeats.android.pojos.socket.MessageData;
import com.breathandbeats.android.ui.activities.ChatActivity;
import com.breathandbeats.android.ui.activities.MainActivity;
import com.breathandbeats.android.ui.activities.RequestLogActivity;
import com.breathandbeats.android.ui.activities.SOSReceiveActivity;
import com.breathandbeats.android.ui.activities.SharedPreferenceBnBUtility;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

public class BnBFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FireBaseMessage_Service";
    public static final String BROADCAST_ACTION = "MessagingService";
    String notificationType;
    Gson gson;
    private String notificationMessage;
    BnBApp app;
    Intent intent;
    SharedPrefHelper sharedPrefHelper;
    String notifData;
    DBHelper dbHelper;
    private  String tripstatus_="null";
    String Datavalue="null";
    String AlertMessage="Breath and Beats alert message.";
    private EventBus bus = EventBus.getDefault();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        app = BnBApp.getInstance();
        intent = new Intent(BROADCAST_ACTION);
        sharedPrefHelper = app.getSharedPrefHelper();
        dbHelper = app.getDBHelper();


        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) { // notofication for trip

            String value = entry.getValue();
            Log.d(TAG, " value " + value);
            gson = new Gson();
            JSONObject object = null;

            try {
                object = new JSONObject(value);
                if(object.has("data")) {

                    JSONObject newjsonobj = new JSONObject();
                    newjsonobj = object.getJSONObject("data");
                    JSONObject secobj = new JSONObject();
                    secobj = newjsonobj.getJSONObject("data");
                    Log.d(TAG, " secobj " + secobj);
                    JSONObject thirdobj = new JSONObject();
                    thirdobj = secobj.getJSONObject("trip");


                     if (secobj.has("trip")) {

                         if(secobj.has("advanceBooking")){  // checking related to advance book or not ?
                        //do nothing
                         }else{

                             if(thirdobj.has("paymentGateway")){ // Current trip payment recieved confirmation

                                SharedPreferenceBnBUtility.setNotificationTripCompleted(BnBFirebaseMessagingService.this,"true");
                                SharedPreferenceBnBUtility.setPaymentInfo(BnBFirebaseMessagingService.this,String.valueOf(secobj));
                                AlertMessage="Breath and Beats Ambulance trip has been completed successfully.";
                                intent.putExtra("tripcompleted", "true");
                                sendBroadcast(intent);

                             }
                         }
                         if(thirdobj.has("paymentGateway")){
                             // do nothing
                         }

                        if(thirdobj.has("paymentGateway")){
                            // do nothing
                        }else{
                            //-------------------------------------
                            if (secobj.has("trip")) {
                                if (secobj != null) {  // complete details of trip.
                                    notifData = String.valueOf(secobj);
                                    if (notifData != null && notifData.length() > 0) {

                                        NotificationBasePojo NotificationBasePojo = gson.fromJson(notifData, NotificationBasePojo.class);
                                        Log.d(TAG," trip id  : "+NotificationBasePojo.getTrip().getId());
                                        SharedPreferenceBnBUtility.setNotificationTripID(BnBFirebaseMessagingService.this,NotificationBasePojo.getTrip().getId());
                                        SharedPreferenceBnBUtility.setDriverName(BnBFirebaseMessagingService.this,NotificationBasePojo.getTrip().getDriver().getUsername());
                                        SharedPreferenceBnBUtility.setDriverNumber(this,NotificationBasePojo.getTrip().getDriver().getPhoneNumber());

                                        SharedPreferenceBnBUtility.setNotificationHospitalAddress(this,NotificationBasePojo.getTrip().getHospital().getAddress());
                                        SharedPreferenceBnBUtility.setNotificationHospitalName(this,NotificationBasePojo.getTrip().getHospital().getName());
                                        SharedPreferenceBnBUtility.setNotificationHospitalNumber(this,NotificationBasePojo.getTrip().getHospital().getPhoneNumber());
                                        SharedPreferenceBnBUtility.setDriverHospitalID(this,NotificationBasePojo.getTrip().getHospital().getId());

                                        SharedPreferenceBnBUtility.setNotificationPatientLatitude(this,NotificationBasePojo.getTrip().getLocation().getCoordinates().get(1).toString());
                                        SharedPreferenceBnBUtility.setNotificationPatientLongitude(this,NotificationBasePojo.getTrip().getLocation().getCoordinates().get(0).toString());

                                        if(NotificationBasePojo.getTrip().getDriver().getDriverCurrentCoordinates()!=null) {
                                            if(NotificationBasePojo.getTrip().getDriver().getDriverCurrentCoordinates().get(1).toString()!=null) {
                                                SharedPreferenceBnBUtility.setDriverLatitude(this, NotificationBasePojo.getTrip().getDriver().getDriverCurrentCoordinates().get(1).toString());
                                            }
                                            if(NotificationBasePojo.getTrip().getDriver().getDriverCurrentCoordinates().get(0).toString()!=null){
                                                SharedPreferenceBnBUtility.setDriverLongitude(BnBFirebaseMessagingService.this, NotificationBasePojo.getTrip().getDriver().getDriverCurrentCoordinates().get(0).toString());
                                            }
                                        }
                                        SharedPreferenceBnBUtility.setNotificationRecived(BnBFirebaseMessagingService.this,"true");

                                        Datavalue="gotnotification";
                                        if(NotificationBasePojo.getTrip().getTripStatus()!=null){

                                            if(NotificationBasePojo.getTrip().getTripStatus().equals("ongoing")){
                                                SharedPreferenceBnBUtility.setTripPaymentRecived(BnBFirebaseMessagingService.this,"false");
                                                SharedPreferenceBnBUtility.setNotificationTripStarted(BnBFirebaseMessagingService.this,"true");
                                                intent.putExtra("tripstarted", "true");
                                                AlertMessage="Breath and Beats, Ambulance trip has been started and ambulance will reach you soon.";

                                            }else if(NotificationBasePojo.getTrip().getTripStatus().equals("completed")){
                                                Log.d(TAG," tripcomplted true 0  : ");
                                                SharedPreferenceBnBUtility.setNotificationTripCompleted(BnBFirebaseMessagingService.this,"true");

                                                AlertMessage="Breath and Beats Ambulance trip has been completed successfully.";
                                                intent.putExtra("tripcompleted", "true");
                                            }else if(NotificationBasePojo.getTrip().getTripStatus().equals("assigned")){
                                                SharedPreferenceBnBUtility.setNotificationTripStatus(BnBFirebaseMessagingService.this,"true");
                                                SharedPreferenceBnBUtility.setTripPaymentRecived(BnBFirebaseMessagingService.this,"false");
                                                SharedPreferenceBnBUtility.setNotificationTripCompleted(BnBFirebaseMessagingService.this,"false");
                                                AlertMessage="Breath and Beats, Ambulance  has been assigned to pick up patient .";
                                                SharedPreferenceBnBUtility.setrequestedforhospital(BnBFirebaseMessagingService.this,"false");
                                                 intent.putExtra("AmbulanceAssigned", "true");

                                            }
                                        }
                                        if(NotificationBasePojo.getTrip().getIsScheduledBooking()!=null) {
                                            if (NotificationBasePojo.getTrip().getIsScheduledBooking().toString().equals("true")) {
                                                SharedPreferenceBnBUtility.setAdvanceTrip(getApplicationContext(), "true");
                                            } else if (NotificationBasePojo.getTrip().getIsScheduledBooking().toString().equals("false")) {
                                                SharedPreferenceBnBUtility.setAdvanceTrip(getApplicationContext(), "false");
                                            }
                                        }
                                        SharedPreferenceBnBUtility.setNotificationTripvalue(this,"");
                                        sendBroadcast(intent);
                                    }
                                }
                            }
                            //----------------------------------
                        }
                    }

                    if(secobj.has("advanceBooking")){

                        if(secobj.get("advanceBooking").equals(true)) {

                            SharedPreferenceBnBUtility.setNotificationRecived(BnBFirebaseMessagingService.this,"false");
                            SharedPreferenceBnBUtility.setNotificationTripID(BnBFirebaseMessagingService.this,thirdobj.get("trip").toString());
                            AlertMessage = "A Ambulance has been alloted for you.";
                            SharedPreferenceBnBUtility.setAdvancePaymentInfo(BnBFirebaseMessagingService.this,String.valueOf(secobj));
                            intent.putExtra("AdvanceBookConfirmed", "true");
                            SharedPreferenceBnBUtility.setAdvacneBookedReq(BnBFirebaseMessagingService.this,"true");
                            sendBroadcast(intent);
                        }
                        else if(secobj.get("advanceBooking").equals(false)){
                            // do nothing
                        }
                    }

                    sendNotification("Breath and Beats ",AlertMessage,Datavalue);

                }else{

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (remoteMessage == null)
            return;
        if (remoteMessage.getData() != null) {
           handleNotification(remoteMessage); // for other than trip notification.
        }

    }

    private void handleNotification(RemoteMessage remoteMessage) {

        for (Map.Entry<String, String> entry : remoteMessage.getData().entrySet()) {

            String value = entry.getValue();
            gson = new Gson();
            JSONObject object = null;

            try {
                object = new JSONObject(value);

                if (object.has("data")) {

                    JSONObject body = new JSONObject();
                    body = object.getJSONObject("data");
                    JSONObject secobj = new JSONObject();
                    secobj = body.getJSONObject("data");

                    if(body.has("type")) {
                        notificationType = body.get("type").toString();
                    }

                    if(body.has("data")) {
//                        notifData = body.get("data");
                        notifData=String.valueOf(secobj);
                        Log.d(TAG,"notificationType:  "+notificationType);
                        gson = new Gson();
                        switch (notificationType) {
                            case "makeANoise":
                                updateDB(notifData);
                                createMakeANoiseNotification(notifData);
                                break;
                            case "sos":
                                createSOSNotification(notifData);
                                break;
                            case "requestStatusUpdate":
                                updateRequestStatus(notifData);
                                break;
                            case "makeANoiseChat":
                                updateDBForIncomingMessage(notifData);
                                if (sharedPrefHelper.getCurrentChatId() == "") {
                                    createMakeANoiseChatMessageNotification(notifData);
                                } else {
                                    sendUpdateRVEvent(notifData);
                                }
                                break;
                        }
                    }
                    if(body.has("message")) {
                        notificationMessage = body.get("message").toString();
                        Log.d(TAG, "handleNotification: " + notificationType + "--" + notifData + "--" + notificationMessage);
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }



    }

    private void updateRequestStatus(String notifData) {
        Log.i(TAG, "updateRequestStatus: ");
        RequestUpdateNotificationData requestUpdateNotificationData = gson.fromJson(notifData,RequestUpdateNotificationData.class);
        String requestId = requestUpdateNotificationData.getRequestId();
        dbHelper.updateRecieveRequest(requestId);
    }

    private void createSOSNotification(String notifData) {
        SOSNotificationData sosNotificationData = gson.fromJson(notifData, SOSNotificationData.class);
        int requestId = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestId, getSOSNotifIntent(sosNotificationData), PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("You received a SOS")
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        int mNotificationId = 001;
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    private Intent getSOSNotifIntent(SOSNotificationData sosNotificationData) {
        Intent intent = new Intent(this, SOSReceiveActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Location location = sosNotificationData.getLocation();
        ArrayList<Double> locationOfSender = location.getCoordinates();
        String senderName = sosNotificationData.getSenderName();
        intent.putExtra("location",locationOfSender);
        intent.putExtra("nameOfSender",senderName);
        return intent;
    }

    private void sendUpdateRVEvent(String notifData) {
        MakeANoiseChatMessageNotificationData makeANoiseChatMessageNotificationData = gson.fromJson(notifData, MakeANoiseChatMessageNotificationData.class);
        MessageData messageData = makeANoiseChatMessageNotificationData.getMessageData();
        String text = messageData.getText();
        String chatId = makeANoiseChatMessageNotificationData.getChatId();
        bus.post(new ChatListUpdateEvent(chatId));
    }

    private void createMakeANoiseChatMessageNotification(String notifData) {
        MakeANoiseChatMessageNotificationData makeANoiseChatMessageNotificationData = gson.fromJson(notifData, MakeANoiseChatMessageNotificationData.class);
        int requestId = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestId, getMakeANoiseChatNotifIntent(makeANoiseChatMessageNotificationData), PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentText("You recieved a New Message")
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        int mNotificationId = 001;
        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    private Intent getMakeANoiseChatNotifIntent(MakeANoiseChatMessageNotificationData makeANoiseChatMessageNotificationData) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        String requestId = makeANoiseChatMessageNotificationData.getRequestId();
        String messageId = makeANoiseChatMessageNotificationData.get_id();
        String chatId = makeANoiseChatMessageNotificationData.getChatId();
        String createdAt = makeANoiseChatMessageNotificationData.getCreatedAt();
        MessageData messageData = makeANoiseChatMessageNotificationData.getMessageData();
        String sendByName = makeANoiseChatMessageNotificationData.getSentByName();
        String sentById = makeANoiseChatMessageNotificationData.getSentById();
        String messageDataText = messageData.getText();
        intent.putExtra("type", "makeANoiseChat");
        intent.putExtra("requestId", requestId);
        intent.putExtra("messageId", messageId);
        intent.putExtra("chatId", chatId);
        intent.putExtra("createdAt", createdAt);
        intent.putExtra("sendByName", sendByName);
        intent.putExtra("sentById", sentById);
        intent.putExtra("messageDataText", messageDataText);
        return intent;
    }

    private void updateDBForIncomingMessage(String notifData) {
        dbHelper.createChat(notifData);
    }

    private void updateDB(String notifData) {
        dbHelper.createRecievedRequest(notifData);
    }

    private void createMakeANoiseNotification(String notifData) {
        MakeANoiseNotificationData makeANoiseNotificationData = gson.fromJson(notifData, MakeANoiseNotificationData.class);
        int requestId = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestId, getMakeANoiseNotifIntent(makeANoiseNotificationData), PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.bnb_logo)
                        .setContentTitle("Breath and Beats")
                        .setContentText("You received a request")
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);

        NotificationManager mNotifyMgr =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        int mNotificationId = 001;
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
    }

    private Intent getMakeANoiseNotifIntent(MakeANoiseNotificationData makeANoiseNotificationData) {
        Intent intent = new Intent(this, RequestLogActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        String subject = makeANoiseNotificationData.getSubject();
        String senderId = makeANoiseNotificationData.getSenderId();
        String message = makeANoiseNotificationData.getMessage();
        Double lat = makeANoiseNotificationData.getLocation().getCoordinates().get(0);
        Double lng = makeANoiseNotificationData.getLocation().getCoordinates().get(1);
        String metaData = makeANoiseNotificationData.getMetaData();
        Integer distance = makeANoiseNotificationData.getDistance();
        String requestId = makeANoiseNotificationData.getRequestID();
        intent.putExtra("type", "MakeANoise");
        intent.putExtra("subject", subject);
        intent.putExtra("reason", metaData);
        intent.putExtra("message", message);
        intent.putExtra("distance", distance);
        intent.putExtra("lat", lat);
        intent.putExtra("lng", lng);
        intent.putExtra("senderID", senderId);
        intent.putExtra("requestId", requestId);
        return intent;
    }

    private void sendNotification(String title, String messageBody,String Datavalue) {

            intent = new Intent(BnBFirebaseMessagingService.this, MainActivity.class);
               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.bnb_logo)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());

    }

}

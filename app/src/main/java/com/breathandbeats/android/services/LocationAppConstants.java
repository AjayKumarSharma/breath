package com.breathandbeats.android.services;

/**
 * Created by Rakesh on 09/10/17.
 */

public class LocationAppConstants {

    // Location updates intervals
    public static int UPDATE_INTERVAL = 5000; // 3 sec
    public static int FATEST_INTERVAL = 8000; // 5 sec
    public static int DISPLACEMENT =20; // 10 meters
}

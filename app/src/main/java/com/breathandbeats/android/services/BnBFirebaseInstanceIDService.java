package com.breathandbeats.android.services;


import android.util.Log;

import com.breathandbeats.android.infrastructure.BnBApp;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class BnBFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = "BnBFirebaseInstanceIDSe";
    BnBApp application;
    SharedPrefHelper sharedPrefHelper;

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        application = (BnBApp) getApplication();
        sharedPrefHelper = application.getSharedPrefHelper();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "onTokenRefresh: " + refreshedToken);
        sharedPrefHelper.setFCMToken(refreshedToken);

        // Saving reg id to shared preferences
//        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
//        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
    }
}

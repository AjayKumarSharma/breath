package com.breathandbeats.android.utils;


public class Validators {
    public static boolean isMobileValid(String phoneNumber) {
        // TODO: 27/01/17 validate more paramters of phoneNumbers;
        return phoneNumber.length() == 10;
    }

    public static boolean isEmailValid(String email) {
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        return email.matches(emailPattern);
    }

    public static boolean isPasswordValid(String password) {
        return !password.matches("");
    }


}

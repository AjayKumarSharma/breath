package com.breathandbeats.android.utils;


import com.breathandbeats.android.pojos.RV.MessageItem;
import com.breathandbeats.android.pojos.RV.RecieveRequestLogItem;
import com.breathandbeats.android.pojos.RV.RepliesItem;
import com.breathandbeats.android.pojos.RV.SentRequestLogItem;
import com.breathandbeats.android.pojos.orm.Chat;
import com.breathandbeats.android.pojos.orm.MessageORM;
import com.breathandbeats.android.pojos.orm.RecieveRequest;
import com.breathandbeats.android.pojos.orm.SentRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Mappers {
    public static ArrayList<SentRequestLogItem> getSentRequestLogItems(List<SentRequest> sentRequests, String name) {
        ArrayList<SentRequestLogItem> sentRequestLogItems = new ArrayList<>();
        for (int i = 0; i < sentRequests.size(); i++) {
            SentRequest sentRequest = sentRequests.get(i);
            String status = sentRequest.getStatus();
            String requestID = sentRequest.getRequestID();
            String subject = sentRequest.getSubject();
            Date createdAt = sentRequest.getCreatedAt();
            SentRequestLogItem sentRequestLogItem = new SentRequestLogItem(name, createdAt, status, subject, requestID);
            sentRequestLogItems.add(sentRequestLogItem);
        }
        return sentRequestLogItems;
    }

    public static ArrayList<RecieveRequestLogItem> getRecievedRequestLogItems(List<RecieveRequest> recieveRequests) {
        ArrayList<RecieveRequestLogItem> recievedRequestLogItems = new ArrayList<>();
        for (int i = 0; i < recieveRequests.size(); i++) {
            RecieveRequest recieveRequest = recieveRequests.get(i);
            String status = recieveRequest.getStatus();
            String subject = recieveRequest.getSubject();
            Date createdAt = recieveRequest.getCreatedAt();
            String name = recieveRequest.getSenderName();
            String chatId = recieveRequest.getChatId();
            String requestId = recieveRequest.getRequestID();
            String receiverId = recieveRequest.getSenderId();
            Boolean canChat = recieveRequest.getCanChat();
            boolean accepted = recieveRequest.isAccepted();
            RecieveRequestLogItem recieveRequestLogItem = new RecieveRequestLogItem(name, createdAt, status, subject, requestId, receiverId, canChat, chatId, accepted);
            recievedRequestLogItems.add(recieveRequestLogItem);
        }

        return recievedRequestLogItems;
    }


    public static ArrayList<RepliesItem> getRepliesItems(List<Chat> replies) {
        ArrayList<RepliesItem> repliesItems = new ArrayList<>();
        for (int i = 0; i < replies.size(); i++) {
            Chat chat = replies.get(i);
            String chatId = chat.getChatID();
            String otherUser = chat.getOtherUser();
            Date createdAt = chat.getCreatedAt();
            RepliesItem repliesItem = new RepliesItem(otherUser, "I need your help", createdAt, chatId);
            repliesItems.add(repliesItem);
        }
        return repliesItems;
    }

    public static ArrayList<MessageItem> getMessageItems(List<MessageORM> messageORMs) {
        ArrayList<MessageItem> messageItems = new ArrayList<>();
        for (int i = 0; i < messageORMs.size(); i++) {
            MessageORM messageORM = messageORMs.get(i);
            MessageItem messageItem = new MessageItem(messageORM.getMessage(), messageORM.isCreatedBySelf(), messageORM.getCreatedAt());
            messageItems.add(messageItem);
        }
        return messageItems;
    }
}

package com.breathandbeats.android.utils;


import android.util.Base64;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Shared {

    public static String generateRandomString(int randomLength) {
        String allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGRHIJKLMNOPQRSTUVWXYZ";
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < randomLength; i++) {
            stringBuilder.append(allowedChars.charAt(random.nextInt(allowedChars.length())));
        }
        return stringBuilder.toString();
    }

    public static String encryptPassword(String phoneNumber,String password){
        password = phoneNumber + password + System.currentTimeMillis();
        password = new String(Base64.encode(password.getBytes(), Base64.DEFAULT)).trim();
        return password;
    }

    public static String getFormattedDate(Date date){
        SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy 'at' hh:mm a");
        return ft.format(date);
    }


}

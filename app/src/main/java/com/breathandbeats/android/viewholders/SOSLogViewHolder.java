package com.breathandbeats.android.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.breathandbeats.android.R;

public class SOSLogViewHolder extends RecyclerView.ViewHolder{
    public TextView dateTV;
    public TextView typeOfSOSTV;
    public SOSLogViewHolder(View itemView) {
        super(itemView);
        dateTV = (TextView) itemView.findViewById(R.id.item_sos_log_date_TV);
        typeOfSOSTV = (TextView) itemView.findViewById(R.id.item_sos_log_typeOfSOS_TV);
    }
}

package com.breathandbeats.android.viewholders;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.breathandbeats.android.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class SaveALifeLevel2ViewHolder extends RecyclerView.ViewHolder {
    public TextView titleTV;
    public CircleImageView itemImageView;

    public SaveALifeLevel2ViewHolder(View itemView) {
        super(itemView);
        titleTV = (TextView) itemView.findViewById(R.id.item_save_a_life_TV);
        itemImageView = (CircleImageView) itemView.findViewById(R.id.item_save_a_life_IV);

    }
}

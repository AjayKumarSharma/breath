package com.breathandbeats.android.viewholders;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.ui.activities.ChatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RepliesViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final Context context;
    @BindView(R.id.item_reply_name_TV)
    TextView nameTV;
    @BindView(R.id.item_reply_msg_TV)
    TextView msgTV;
    @BindView(R.id.item_reply_dateTime_TV)
    TextView dateTimeTV;
    @BindView(R.id.item_reply_chat_Btn)
    ImageView chatBtn;
    @BindView(R.id.item_reply_map_Btn)
    ImageView mapBtn;

    public ImageView getChatBtn() {
        return chatBtn;
    }

    public void setChatBtn(ImageView chatBtn) {
        this.chatBtn = chatBtn;
    }

    public ImageView getMapBtn() {
        return mapBtn;
    }

    public void setMapBtn(ImageView mapBtn) {
        this.mapBtn = mapBtn;
    }

    public TextView getNameTV() {
        return nameTV;
    }

    public void setNameTV(TextView nameTV) {
        this.nameTV = nameTV;
    }

    public TextView getMsgTV() {
        return msgTV;
    }

    public void setMsgTV(TextView msgTV) {
        this.msgTV = msgTV;
    }

    public TextView getDateTimeTV() {
        return dateTimeTV;
    }

    public void setDateTimeTV(TextView dateTimeTV) {
        this.dateTimeTV = dateTimeTV;
    }

    public RepliesViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;
        ButterKnife.bind(this, itemView);
    }

    @Override
    public void onClick(View view) {
        context.startActivity(new Intent(context, ChatActivity.class));
    }
}

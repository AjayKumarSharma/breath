package com.breathandbeats.android.viewholders;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.breathandbeats.android.R;
import com.breathandbeats.android.pojos.RV.SaveALifeItem;
import com.breathandbeats.android.ui.activities.SaveaLife.SaveALifeLevel2Activity;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class SaveALifeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private static final String TAG = "SaveALifeViewHolder";
    public TextView titleTV;
    public CircleImageView itemImageView;

    Dialog forgotDialog;


    private Context context;
    private ArrayList<SaveALifeItem> saveALifeItemList;

    public SaveALifeViewHolder(View itemView, Context context, ArrayList<SaveALifeItem> saveALifeItemList) {
        super(itemView);
        this.context = context;
        this.saveALifeItemList = saveALifeItemList;
        itemView.setOnClickListener(this);
        titleTV = (TextView) itemView.findViewById(R.id.item_save_a_life_TV);
        itemImageView = (CircleImageView) itemView.findViewById(R.id.item_save_a_life_IV);
    }

    @Override
    public void onClick(View view) {
        Log.i(TAG, "onClick: ");
        int pos = getAdapterPosition();
        SaveALifeItem item = this.saveALifeItemList.get(pos);
        Intent intent = new Intent(context, SaveALifeLevel2Activity.class);
        intent.putExtra("categoryTitle", item.getTitle());
        this.context.startActivity(intent);

    }


    private void markFavorite() {
        Log.i(TAG, "markFavorite: ");
    }

    private void saveVideo() {
        Log.i(TAG, "saveVideo: ");
    }
}

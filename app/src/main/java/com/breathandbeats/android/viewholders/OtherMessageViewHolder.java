package com.breathandbeats.android.viewholders;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.breathandbeats.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OtherMessageViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.item_message_other_msg_TV)
    TextView messageTV;
    @BindView(R.id.item_message_other_createdAt_TV)
    TextView createdAtTV;

    public TextView getCreatedAtTV() {
        return createdAtTV;
    }

    public void setCreatedAtTV(TextView createdAtTV) {
        this.createdAtTV = createdAtTV;
    }

    public TextView getMessageTV() {
        return messageTV;
    }

    public void setMessageTV(TextView messageTV) {
        this.messageTV = messageTV;
    }

    public OtherMessageViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}

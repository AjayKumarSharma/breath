package com.breathandbeats.android.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.breathandbeats.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class EmergencyContactListVH extends RecyclerView.ViewHolder {
    @BindView(R.id.item_emergencyContact_name)
    TextView nameTV;
    @BindView(R.id.item_emergencyContact_email)
    TextView emailTV;
    @BindView(R.id.item_emergencyContact_num)
    TextView numTV;
    @BindView(R.id.item_emergencyContact_relationship)
    TextView relationShipTV;
    @BindView(R.id.item_emergencyContact_IV)
    CircleImageView imageView;
    @BindView(R.id.item_emergencyContact_EditContactBtn)
    Button editContactBtn;
    @BindView(R.id.item_emergencyContact_DeleteContactBtn)
    Button deleteContactBtn;

    public EmergencyContactListVH(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public TextView getNameTV() {
        return nameTV;
    }

    public void setNameTV(TextView nameTV) {
        this.nameTV = nameTV;
    }

    public TextView getEmailTV() {
        return emailTV;
    }

    public void setEmailTV(TextView emailTV) {
        this.emailTV = emailTV;
    }

    public TextView getNumTV() {
        return numTV;
    }

    public void setNumTV(TextView numTV) {
        this.numTV = numTV;
    }

    public CircleImageView getImageView() {
        return imageView;
    }

    public void setImageView(CircleImageView imageView) {
        this.imageView = imageView;
    }

    public Button getEditContactBtn() {
        return editContactBtn;
    }

    public void setEditContactBtn(Button editContactBtn) {
        this.editContactBtn = editContactBtn;
    }

    public Button getDeleteContactBtn() {
        return deleteContactBtn;
    }

    public void setDeleteContactBtn(Button deleteContactBtn) {
        this.deleteContactBtn = deleteContactBtn;
    }

    public TextView getRelationShipTV() {
        return relationShipTV;
    }

    public void setRelationShipTV(TextView relationShipTV) {
        this.relationShipTV = relationShipTV;
    }
}

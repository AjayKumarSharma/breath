package com.breathandbeats.android.viewholders;


import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.breathandbeats.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

//String name;
//        Date dateTime;
//        String status;
//        String subject;
//


public class RecieveRequestLogViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.item_recieved_request_senderName_TV)
    TextView senderNameTV;
    @BindView(R.id.item_recieved_request_accept_Btn)
    Button acceptBtn;
    @BindView(R.id.item_recieved_request_status_TV)
    TextView statusTV;
    @BindView(R.id.item_recieved_request_subject_TV)
    TextView subjectTV;
    @BindView(R.id.item_recieved_request_date_TV)
    TextView dateTV;

    public TextView getDateTV() {
        return dateTV;
    }

    public void setDateTV(TextView dateTV) {
        this.dateTV = dateTV;
    }

    public TextView getSenderNameTV() {
        return senderNameTV;
    }

    public void setSenderNameTV(TextView senderNameTV) {
        this.senderNameTV = senderNameTV;
    }

    public Button getAcceptBtn() {
        return acceptBtn;
    }

    public void setAcceptBtn(Button acceptBtn) {
        this.acceptBtn = acceptBtn;
    }

    public TextView getStatusTV() {
        return statusTV;
    }

    public void setStatusTV(TextView statusTV) {
        this.statusTV = statusTV;
    }

    public TextView getSubjectTV() {
        return subjectTV;
    }

    public void setSubjectTV(TextView subjectTV) {
        this.subjectTV = subjectTV;
    }

    public RecieveRequestLogViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this,itemView);

    }
}

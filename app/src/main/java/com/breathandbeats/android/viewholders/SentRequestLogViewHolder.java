package com.breathandbeats.android.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.breathandbeats.android.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SentRequestLogViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.item_sent_request_dateTime_TV)
    TextView dateTimeTV;
    @BindView(R.id.item_sent_request_replies_Btn)
    Button replyBtn;
    @BindView(R.id.item_sent_request_replies_closeRequestBtn)
    Button closeRequestBtn;
    @BindView(R.id.item_sent_request_status_TV)
    TextView statusTV;
    @BindView(R.id.item_sent_request_subject_TV)
    TextView subjectTV;

    public Button getCloseRequestBtn() {
        return closeRequestBtn;
    }

    public void setCloseRequestBtn(Button closeRequestBtn) {
        this.closeRequestBtn = closeRequestBtn;
    }

    public TextView getDateTimeTV() {
        return dateTimeTV;
    }

    public void setDateTimeTV(TextView dateTimeTV) {
        this.dateTimeTV = dateTimeTV;
    }

    public Button getReplyBtn() {
        return replyBtn;
    }

    public void setReplyBtn(Button replyBtn) {
        this.replyBtn = replyBtn;
    }

    public TextView getStatusTV() {
        return statusTV;
    }

    public void setStatusTV(TextView statusTV) {
        this.statusTV = statusTV;
    }

    public TextView getSubjectTV() {
        return subjectTV;
    }

    public void setSubjectTV(TextView subjectTV) {
        this.subjectTV = subjectTV;
    }

    public SentRequestLogViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);

    }
}

package com.breathandbeats.android.NewUtils;

/**
 * Created by Ajay Kumar Sharma
 */

public class Const {
    // web services
    public class ServiceType {
        //http://ec2-18-220-141-119.us-east-2.compute.amazonaws.com:8080/BreathBeatsAPI/signUp/signUpDoctor
        public static final String HOST_URL = "http://ec2-18-220-141-119.us-east-2.compute.amazonaws.com:8080/BreathBeatsAPI/";

        public static final String BASE_URL = HOST_URL + "userDoctor/";//dev_api//api
        //public static final String SIGNUP = HOST_URL + "signUp/"+"signUpDoctor";
        public static final String SearchDoctorInfor = HOST_URL + "userDoctor/"+"searchDoctor";

    }


}

package com.breathandbeats.android.infrastructure;


import android.content.Context;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private Context context;
    private BnBService apiSerivice;
    private HttpLoggingInterceptor logging;
    private OkHttpClient client;
    private static RestClient instance = null;

    public RestClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL_STRING)
                .addConverterFactory(GsonConverterFactory.create())
                .client(provideOkHttpClient())
                .build();

        apiSerivice = retrofit.create(BnBService.class);
    }

    public OkHttpClient getClient() {
        logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        client = new OkHttpClient.Builder().addInterceptor(logging)
                .build();

        return client;
    }


    public BnBService getApiSerivice() {
        return apiSerivice;
    }

    public static RestClient getInstance() {
        if (instance == null) {
            instance = new RestClient();
        }
        return instance;
    }


    public OkHttpClient provideOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(new RequestInterceptor())
                .addInterceptor(logging)
                .build();

        return client;
    }
}

package com.breathandbeats.android.infrastructure;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Venkat on 02/04/2017.
 */

public class RequestInterceptor implements Interceptor {

    /*String usernmae;
    String token;
*/
    public static final String CONTENT_TYPE = "content-type";
    public static final String Accept = "Accept";
    public static final String CONTENT_APPLICATION = "application/json";
    public RequestInterceptor() {

    }

    public RequestInterceptor(String usernmae, String token) {
       /* this.usernmae=usernmae;
        this.token=token;*/
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request original = chain.request();
        Request.Builder requestBuilder = original.newBuilder()
                .addHeader(CONTENT_TYPE, CONTENT_APPLICATION)
                .addHeader(Accept, CONTENT_APPLICATION);

        Request request = requestBuilder.build();
        Response response = chain.proceed(request);
        return response;
    }
}
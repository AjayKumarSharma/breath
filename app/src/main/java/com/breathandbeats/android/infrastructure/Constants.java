package com.breathandbeats.android.infrastructure;


public class Constants {
    public static final String PLATFORM = "an";
    public static final String FCM_PUSH_ID = "FCM_PUSH_ID";
    public static final String CURRENT_LOCATION = "CURRENT_LOCATION";
    public static final String ERRORMSG = "Something went wrong! Please try again";
    public static final String LAST_LATI = "LAST_LATI";
    public static final String LAST_LONGI = "LAST_LONGI";
    public static final String MAKE_A_NOISE_NOTIFICATION_TYPE = "MAKE_A_NOISE_NOTIFICATION_TYPE";
    public static final String CURRENT_CHAT = "CURRENT_CHAT";
    public static final String DEFAULT_LOCATION = "DEFAULT_LOCATION";

//    public static String BASE_URL_STRING = "http://139.59.13.184";
//    public static String BASE_URL_STRING = "http://192.168.1.102:3000";


    public static String BASE_URL_STRING = "http://139.59.13.184";

    public static String LAST_LOCATION = "LAST_LOCATION";
    public static String AUTH_TOKEN = "AUTH_TOKEN";
    public static String IS_PHONE_VERIFIED = "IS_PHONE_VERIFIED";
    public static String IS_EMAIL_VERIFIED = "IS_EMAIL_VERIFIED";
    public static String USER_DATA = "USER_DATA";
    public static String SENT_TOKEN_TO_SERVER = "SENT_TOKEN_TO_SERVER";
    public static String IS_EMERGENCY_CONTACT_SAVED = "IS_EMERGENCY_CONTACT_SAVED";
    public static String IS_LOGGED_IN = "IS_LOGGED_IN";
    public static String AUTH_ID = "AUTH_ID";
    public static String EMERGENCY_CONTACT = "EMERGENCY_CONTACT";
    public static String DEV_KEY = "AIzaSyCIP-xNEgqkjb1OHEqMbeNtTz1RelaWuCg";
    public static String MAP_API_KEY = "AIzaSyDsufPqEPLXMCyREgRxustNbgovfouF7Ag";

}

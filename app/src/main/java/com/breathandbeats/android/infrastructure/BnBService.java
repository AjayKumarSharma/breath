package com.breathandbeats.android.infrastructure;


import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.CancelTheTripRequested;
import com.breathandbeats.android.pojos.BookAmbulanceNotificationPojo.Canceltrip;
import com.breathandbeats.android.pojos.BookAmbulanceNowresponce;
import com.breathandbeats.android.pojos.BookLater.BookAmbulanceLaterResponce;
import com.breathandbeats.android.pojos.BookLater.BookAmbulanceLater_Input;
import com.breathandbeats.android.pojos.DriverCordinate.DriverCoOrdinateResponce;
import com.breathandbeats.android.pojos.HistoryTripListPojo.AdvannceBookedTripData;
import com.breathandbeats.android.pojos.HistoryTripListPojo.CompletedTripListData;
import com.breathandbeats.android.pojos.HistoryTripListPojo.Rejectedtripdata;
import com.breathandbeats.android.pojos.Logout;
import com.breathandbeats.android.pojos.TripPaymentpojo.TripPaymentInput;
import com.breathandbeats.android.pojos.TripPaymentpojo.TripPaymentResponce;
import com.breathandbeats.android.pojos.api.ForgotPasswordRequest;
import com.breathandbeats.android.pojos.api.ForgotPasswordResponse;
import com.breathandbeats.android.pojos.api.GenericResponse;
import com.breathandbeats.android.pojos.api.HealthDirResponse;
import com.breathandbeats.android.pojos.api.InitiateEmailOTPRequest;
import com.breathandbeats.android.pojos.api.InitiateOTPRequest;
import com.breathandbeats.android.pojos.api.LoginRequest;
import com.breathandbeats.android.pojos.api.LoginResponse;
import com.breathandbeats.android.pojos.api.MakeANoiseRequest;
import com.breathandbeats.android.pojos.api.MakeANoiseResponse;
import com.breathandbeats.android.pojos.api.PulseRequest;
import com.breathandbeats.android.pojos.api.RequestUpdateRequest;
import com.breathandbeats.android.pojos.api.SOSLogResponse;
import com.breathandbeats.android.pojos.api.SOSRequest;
import com.breathandbeats.android.pojos.api.SaveALifeByCategoryResponse;
import com.breathandbeats.android.pojos.api.SaveALifeCategoriesResponse;
import com.breathandbeats.android.pojos.api.SignUpRequest;
import com.breathandbeats.android.pojos.api.SignedUpDoctorsResponse;
import com.breathandbeats.android.pojos.api.SignupResponse;
import com.breathandbeats.android.pojos.api.UsercurrentLocationInput;
import com.breathandbeats.android.pojos.api.VerifyForgotPassRequest;
import com.breathandbeats.android.pojos.api.VerifyOTPRequest;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BnBService {

    @POST("/api/v1/users/")
    Call<LoginResponse> signup(@Body SignUpRequest signUpRequest);

    @POST("/api/v1/users/login/")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @POST("/api/v1/users/make-a-noise")
    Call<MakeANoiseResponse> makeANoise(@Header("Authorization") String auth, @Body MakeANoiseRequest makeANoiseRequest);

    @POST("/api/v1/users/pulse/")
    Call<SignupResponse> pulse(@Header("Authorization") String auth, @Body PulseRequest pulseRequest);

    @POST("/api/v1/users/mobile-otp")
    Call<GenericResponse> initiateOTP(@Header("Authorization") String auth, @Body InitiateOTPRequest initiateOTPRequest);

    @POST("/api/v1/users/email-otp")
    Call<GenericResponse> initiateEmailOTP(@Header("Authorization") String auth, @Body InitiateEmailOTPRequest initiateOTPRequest);

    @POST("/api/v1/users/mobile-otp/verify")
    Call<GenericResponse> verifyOTP(@Header("Authorization") String auth, @Body VerifyOTPRequest verifyOTPRequest);

    @POST("/api/v1/users/sos")
    Call<GenericResponse> sos(@Header("Authorization") String auth, @Body SOSRequest sosRequest);

    @GET("/api/v1/users/sos/logs")
    Call<SOSLogResponse> getSOSLogs(@Header("Authorization") String authToken) ;

    @POST("/api/v1/users/forgot-password")
    Call<ForgotPasswordResponse> initiateForgotPassword(@Body ForgotPasswordRequest forgotPasswordRequest);

    @POST("/api/v1/users/forgot-password/verify")
    Call<GenericResponse> verifyForgotPass(@Body VerifyForgotPassRequest verifyForgotPassRequest);


    @GET("/api/v1/users/healthdir/{healthdiritem}")
    Call<HealthDirResponse> getHealthDirItem(@Header("Authorization") String authToken, @Path("healthdiritem") String healthDirItem);

    @GET("/api/v1/save-a-life/categories")
    Call<SaveALifeCategoriesResponse> getSaveALifeCategories(@Header("Authorization") String authToken);

    @GET("/api/v1/save-a-life")
    Call<SaveALifeByCategoryResponse> getSaveALifeItemsByCategory(@Header("Authorization") String authToken, @Query("category") String category);

    @GET("/api/v1/doctors")
    Call<SignedUpDoctorsResponse> getSignupDoctors(@Header("Authorization") String authToken, @Query("category") String category);

    @GET("/api/v1/hospitals")
    Call<SignedUpDoctorsResponse> getSignupHospitals(@Header("Authorization") String authToken, @Query("category") String category);

    @PATCH("/api/v1/users/make-a-noise/{requestId}/status")
    Call<GenericResponse> sendCloseRequest(@Header("Authorization") String authToken, @Path("requestId") String requestId,@Body RequestUpdateRequest requestUpdateRequest);

    @POST("/api/v1/trip/call-ambulance") // for book ambulance now
    Call<BookAmbulanceNowresponce>  BookambulanceNow(
                                                     @Header("Authorization") String Authorization
                                                    , @Body UsercurrentLocationInput usercurrentLocationInput);
    @GET("/api/v1/trip/usersTripListing/completed")
    Call<CompletedTripListData> CompletedTripListData(@Header("Authorization") String Authorization);

    @GET("/api/v1/trip/usersTripListing/cancelled_by_user")
    Call<Rejectedtripdata> RejectedTripListData(@Header("Authorization") String Authorization);

    @GET("/api/v1/trip/usersTripListing/ab")
    Call<AdvannceBookedTripData> AdvancebookedTripListData(@Header("Authorization") String Authorization);

   /* @GET("/api/v1/trip/userTripDetails/{_id}")
    Call<TripCompleteDetails> tripcompletetDetails(@Path("_id") String _id);*/

    @POST("/api/v1/trip/cancelTrip")
    Call<CancelTheTripRequested> CancelTripRequested(@Header("Authorization") String Authorization
                                                         ,@Body Canceltrip tripId);


    @POST("/api/v1/trip/call-ambulance-later")
    Call<BookAmbulanceLaterResponce> BookAmbulanceLater(@Header("Authorization") String Authorization
            , @Body BookAmbulanceLater_Input BooklaterInputdata);

    @POST("/api/v1/trip/finishTrip")
    Call<TripPaymentResponce> Trippayment(@Header("Authorization") String Authorization
            , @Body TripPaymentInput TripPaymentInput);

    @GET("/api/v1/hospitals/drivers/driverLocation/{id}")
    Call<DriverCoOrdinateResponce> DriverCoOrdinate(@Path("id") String _id);

    @POST("/api/v1/users/logout")
    Call<Logout> LogoutInput(@Header("Authorization") String Authorization);
}

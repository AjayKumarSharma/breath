package com.breathandbeats.android.infrastructure;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import android.util.Log;

import com.breathandbeats.android.R;
import com.breathandbeats.android.model.DBHelper;
import com.breathandbeats.android.model.SharedPrefHelper;
import com.crashlytics.android.Crashlytics;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.orm.SugarContext;

import java.net.URISyntaxException;

import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class BnBApp extends Application {
    private static final String TAG = "BnBApp";
    private RestClient restClient;
    private static BnBApp mInstance;
    private Socket mSocket;
    SharedPrefHelper sharedPrefHelper;
    DBHelper dbHelper;


    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        restClient = RestClient.getInstance();
        mInstance = this;
        sharedPrefHelper = new SharedPrefHelper(this);
        dbHelper = new DBHelper();
        mSocket = createSocket();
        SugarContext.init(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto_Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    public SharedPrefHelper getSharedPrefHelper() {
        return sharedPrefHelper;
    }


    public Socket createSocket() {
        try {
            Log.i(TAG, "getSocket: " + sharedPrefHelper.getFCMToken());
            mSocket = IO.socket(Constants.BASE_URL_STRING);
        } catch (URISyntaxException e) {
            Log.i(TAG, "instance initializer: " + e.toString());
            throw new RuntimeException(e);
        }
        return mSocket;
    }

    public void setSocket(Socket mSocket) {
        this.mSocket = mSocket;
    }


    public RestClient getRestClient() {
        return restClient;
    }

//    public Preferences getAppPreferences() {
//        return prefs;
//    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized BnBApp getInstance() {
        return mInstance;
    }

    public Socket getSocket() {
        return mSocket;
    }


    public DBHelper getDBHelper() {
        return dbHelper;
    }

    @Override
    public void onTerminate() {
        SugarContext.terminate();
        super.onTerminate();
    }


}

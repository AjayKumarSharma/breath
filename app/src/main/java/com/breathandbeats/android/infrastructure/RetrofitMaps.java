package com.breathandbeats.android.infrastructure;

import com.breathandbeats.android.pojos.api.maps.Example;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitMaps {
 
    /*
     * Retrofit get annotation with our URL
     * And our method that will return us details of student.
     */
    @GET("api/place/nearbysearch/json?sensor=true&key=AIzaSyCYwqAYGY7HvDjvG9OddRe3BoFBI4jYGHs")
    Call<Example> getNearbyPlaces(@Query("type") String type, @Query("location") String location, @Query("radius") int radius);
 
}
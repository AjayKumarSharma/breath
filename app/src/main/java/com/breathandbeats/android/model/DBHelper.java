package com.breathandbeats.android.model;


import android.util.Log;

import com.breathandbeats.android.pojos.notifs.MakeANoiseChatMessageNotificationData;
import com.breathandbeats.android.pojos.notifs.MakeANoiseNotificationData;
import com.breathandbeats.android.pojos.orm.Chat;
import com.breathandbeats.android.pojos.orm.EmergencyContact;
import com.breathandbeats.android.pojos.orm.MessageORM;
import com.breathandbeats.android.pojos.orm.RecieveRequest;
import com.breathandbeats.android.pojos.orm.SentRequest;
import com.google.gson.Gson;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DBHelper {
    private static final String TAG = "DBHelper";
    Gson gson;


    public List<SentRequest> getSentRequests() {
        List<SentRequest> sentRequests = SentRequest.listAll(SentRequest.class);
        return sentRequests;
    }

    public List<RecieveRequest> getRecieveRequests() {
        return RecieveRequest.listAll(RecieveRequest.class);
    }

    public void addSentRequest(String requestID, String status, String subject, String reason, String message, Integer distance, ArrayList<Double> location, Date createdAt) {
        SentRequest sentRequest = new SentRequest(requestID, status, subject, reason, message, distance, location, createdAt);
        sentRequest.save();
    }

    public List<Chat> getReplies(String requestId) {
        return Select.from(Chat.class).where(Condition.prop("REQUEST_ID").eq(requestId)).list();
    }

    public List<MessageORM> getMessageItems(String chatId) {
        List<MessageORM> messageORMs = Select.from(MessageORM.class).where(Condition.prop("CHAT_ID").eq(chatId)).list();
//        List<MessageORM> messageORMs = MessageORM.find(MessageORM.class, "CHAT_ID = ?", chatId);
        return messageORMs;
    }

    public void createMessage(String textMessage, String chatId, Date createdAt) {
        MessageORM messageORM = new MessageORM(textMessage, chatId, createdAt, 1, true);
        messageORM.save();
    }

    public void createRecievedRequest(String notifData) {
        gson = new Gson();
        MakeANoiseNotificationData makeANoiseNotificationData = gson.fromJson(notifData, MakeANoiseNotificationData.class);
        String subject = makeANoiseNotificationData.getSubject();
        String reason=makeANoiseNotificationData.getReason();
        String senderId = makeANoiseNotificationData.getSenderId();
        String senderName = makeANoiseNotificationData.getSenderName();
        String message = makeANoiseNotificationData.getMessage();
        Double lat = makeANoiseNotificationData.getLocation().getCoordinates().get(0);
        Double lng = makeANoiseNotificationData.getLocation().getCoordinates().get(1);
        String metaData = makeANoiseNotificationData.getMetaData();
        Integer distance = makeANoiseNotificationData.getDistance();
        String requestId = makeANoiseNotificationData.getRequestID();
        ArrayList<Double> location = new ArrayList<>();
        location.add(lat);
        location.add(lng);
        RecieveRequest recieveRequest = new RecieveRequest(false, "I want to help you", requestId, "open", reason, metaData, message, distance, location, new Date(), senderId, "", false, senderName);
        recieveRequest.save();
    }

    public void createChat(String notifData) {
        gson = new Gson();
        MakeANoiseChatMessageNotificationData makeANoiseChatMessageNotificationData = gson.fromJson(notifData, MakeANoiseChatMessageNotificationData.class);
        String requestId = makeANoiseChatMessageNotificationData.getRequestId();
        String chatId = makeANoiseChatMessageNotificationData.getChatId();
        String sentById = makeANoiseChatMessageNotificationData.getSentById();
        String sentByName = makeANoiseChatMessageNotificationData.getSentByName();
        Log.i(TAG, "createChat: " + makeANoiseChatMessageNotificationData);
        Chat chat = Select.from(Chat.class).where(Condition.prop("CHAT_ID").eq(chatId)).first();
        if (chat == null) {
            chat = new Chat(requestId, chatId, "sentRequest", sentByName, true, new Date());
            chat.save();
        }
        RecieveRequest recieveRequest = Select.from(RecieveRequest.class).where(Condition.prop("REQUEST_ID").eq(requestId)).first();
        if (recieveRequest != null) {
            recieveRequest.setCanChat(true);
            recieveRequest.save();

        }
        MessageORM messageORM = new MessageORM();
        messageORM.setChatId(chatId);
        messageORM.setCreatedBySelf(false);
        messageORM.setMessage(makeANoiseChatMessageNotificationData.getMessageData().getText());
        messageORM.setMessageId(makeANoiseChatMessageNotificationData.get_id());
        messageORM.setCreatedAt(new Date());
        messageORM.save();
    }

    public void addChatIdToRecieveRequest(String requestId, String chatID) {
        RecieveRequest recieveRequest = Select.from(RecieveRequest.class).where(Condition.prop("REQUEST_ID").eq(requestId)).first();
        if (recieveRequest != null) {
            recieveRequest.setChatId(chatID);
            Boolean canChat = recieveRequest.getCanChat();
            String senderId = recieveRequest.getSenderId();
            recieveRequest.save();
            Chat chat = Select.from(Chat.class).where(Condition.prop("REQUEST_ID").eq(requestId)).first();
            if (chat == null) {
                chat = new Chat(requestId, chatID, "recieveRequest", senderId, false, new Date());
                chat.save();
            }
        }
    }


    public int getChatCount(String requestId) {
        return Select.from(Chat.class).where(Condition.prop("REQUEST_ID").eq(requestId)).list().size();
    }

    public void setAcceptedOnReceiveReauest(String requestId) {
        RecieveRequest recieveRequest = Select.from(RecieveRequest.class).where(Condition.prop("REQUEST_ID").eq(requestId)).first();
        if (recieveRequest != null) {
            recieveRequest.setAccepted(true);
            recieveRequest.save();
        }
    }

    public void setEmergencyContact(String name, String relationship, String number, String email) {
        EmergencyContact emergencyContact = new EmergencyContact(name, relationship, number, email);
        emergencyContact.save();
    }


    public long getNumberOfEmergencyContacts() {
        return EmergencyContact.count(EmergencyContact.class);
    }

    public List<EmergencyContact> getEmergencyContacts() {
        List<EmergencyContact> emergencyContacts = EmergencyContact.listAll(EmergencyContact.class);
        return emergencyContacts;
    }

    public void updateEmergencyContact(String name, String relationship, String number, String email, String oldNumber) {
        EmergencyContact emergencyContact = Select.from(EmergencyContact.class).where(Condition.prop("CONTACT_NUMBER").eq(oldNumber)).first();
        Log.i(TAG, "updateEmergencyContact: " + emergencyContact);
        emergencyContact.delete();
        setEmergencyContact(name, relationship, number, email);
    }

    public void deleteContact(String number) {
        EmergencyContact emergencyContact = Select.from(EmergencyContact.class).where(Condition.prop("CONTACT_NUMBER").eq(number)).first();
        Log.i(TAG, "deleteContact: " + emergencyContact);
        emergencyContact.delete();

    }

    public void updateSentRequest(String requestId) {
        List<SentRequest> sentRequests = Select.from(SentRequest.class).where(Condition.prop("REQUEST_ID").eq(requestId)).list();
        Log.i(TAG, "updateSentRequest: " + sentRequests);
        SentRequest sentRequest = sentRequests.get(0);
        sentRequest.setStatus("closed");
        sentRequest.save();

    }

    public void updateRecieveRequest(String requestId) {
        Log.i(TAG, "updateRecieveRequest: req id" + requestId);
        List<RecieveRequest> recieveRequests = Select.from(RecieveRequest.class).where(Condition.prop("REQUEST_ID").eq(requestId)).list();
        Log.i(TAG, "updateRecieveRequest: ");
        Log.i(TAG, "updateRecieveRequest: " + recieveRequests);
        RecieveRequest recieveRequest = recieveRequests.get(0);
        recieveRequest.setStatus("closed");
        recieveRequest.save();
    }
}

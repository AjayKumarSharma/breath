package com.breathandbeats.android.model;


import android.content.Context;

import com.breathandbeats.android.infrastructure.Constants;
import com.breathandbeats.android.pojos.api.UserData;
import com.breathandbeats.android.utils.Preferences;

import java.util.ArrayList;

public class SharedPrefHelper {
    private static final String TAG = "SharedPrefHelper";
    Context context;
    Preferences preferences;

    public SharedPrefHelper(Context context) {
        this.context = context;
        preferences = new Preferences(context);
    }


    public void saveUserData(UserData data) {
        preferences.putObject(Constants.USER_DATA, data);

    }

    public UserData getUserData() {
        return preferences.getObject(Constants.USER_DATA, UserData.class);
    }

    public void setFCMToken(String token) {
        preferences.putString(Constants.FCM_PUSH_ID, token);
    }

    public String getFCMToken() {
        return preferences.getString(Constants.FCM_PUSH_ID);
    }

    public void setLastLocation(ArrayList<Double> lastLocation) {
        preferences.putListDouble(Constants.LAST_LOCATION, lastLocation);
    }

    public ArrayList<Double> getLastLocation() {
        return preferences.getListDouble(Constants.LAST_LOCATION);
    }

    public double getLastLatitude() {
        return preferences.getDouble(Constants.LAST_LATI, 0.0);
    }

    public double getLastLongitude() {
        return preferences.getDouble(Constants.LAST_LONGI, 0.0);
    }

    public void setLastLatitude(double lastLatitude) {
        preferences.putDouble(Constants.LAST_LATI, 0.0);
    }

    public void setLastLongitude(double lastLatitude) {
        preferences.putDouble(Constants.LAST_LONGI, 0.0);
    }


    public void setEmailVerified(Boolean isEmailVerified) {
        preferences.putBoolean(Constants.IS_EMAIL_VERIFIED, isEmailVerified);
    }

    public boolean getEmailVerified() {
        return preferences.getBoolean(Constants.IS_EMAIL_VERIFIED, false);
    }

    public void setPhoneVerified(Boolean isPhoneVerified) {
        preferences.putBoolean(Constants.IS_PHONE_VERIFIED, isPhoneVerified);
    }

    public boolean getPhoneVerified() {
        return preferences.getBoolean(Constants.IS_PHONE_VERIFIED, false);
    }


    public Boolean getLoggedIn() {
        return preferences.getBoolean(Constants.IS_LOGGED_IN, false);
    }

    public void setLoggedIn(Boolean isLoggedIn) {
        preferences.putBoolean(Constants.IS_LOGGED_IN, isLoggedIn);
    }

    public String getAuthToken() {
        return preferences.getString(Constants.AUTH_TOKEN);
    }

    public void setAuthToken(String UserID) {
        preferences.putString(Constants.AUTH_TOKEN, UserID);
    }
    public String getUserID() {
        return preferences.getString(Constants.AUTH_ID);
    }

    public void setUserID(String UserID) {
        preferences.putString(Constants.AUTH_ID, UserID);
    }
    public void setCurrentChatId(String chatId) {
        preferences.putString(Constants.CURRENT_CHAT, chatId);
    }

    public void removeCurrentChatId() {
        preferences.remove(Constants.CURRENT_CHAT);
    }

    public String getCurrentChatId() {
        return preferences.getString(Constants.CURRENT_CHAT);
    }

    public void setName(String name) {
        preferences.putString("name", name);
    }

    public String getName() {
        return preferences.getString("name");
    }

    public void setNumber(String number) {
        preferences.putString("number", number);
    }

    public String getNumber() {
        return preferences.getString("number");
    }

    public void setDefaultLocation() {
        ArrayList<Double> defaultLocation = new ArrayList<>();
        defaultLocation.add(0.0);
        defaultLocation.add(0.0);
        preferences.putListDouble(Constants.DEFAULT_LOCATION, defaultLocation);
    }


}

package com.breathandbeats.android.Models;

/**
 * Created by Rocky on 3/4/2018.
 */

public class SearchDataModel {
    String msginfo;

    public SearchDataModel(String msginfo) {
        this.msginfo = msginfo;
    }

    public String getMsginfo() {
        return msginfo;
    }

    public void setMsginfo(String msginfo) {
        this.msginfo = msginfo;
    }
}

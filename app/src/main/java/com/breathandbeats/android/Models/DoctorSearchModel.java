package com.breathandbeats.android.Models;

/**
 * Created by Rocky on 3/3/2018.
 */

public class DoctorSearchModel {
    int ids;
    String txtSpeciality;
    boolean IsActive=false;

    public DoctorSearchModel(int ids, String txtSpeciality, boolean isActive) {
        this.ids = ids;
        this.txtSpeciality = txtSpeciality;
        IsActive = isActive;
    }

    public int getIds() {
        return ids;
    }

    public void setIds(int ids) {
        this.ids = ids;
    }

    public String getTxtSpeciality() {
        return txtSpeciality;
    }

    public void setTxtSpeciality(String txtSpeciality) {
        this.txtSpeciality = txtSpeciality;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }
}

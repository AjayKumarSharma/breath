package com.breathandbeats.android.Models;

/**
 * Created by Saurabh Singh on 12/21/2016.
 */

public class ActivityModels {
    int id_ ;
    String tvmsg,userProfileImage;



    public ActivityModels(int id_, String userProfileImage, String tvmsg) {
        this.tvmsg = tvmsg;
        this.id_ = id_;
        this.userProfileImage = userProfileImage;
    }

    public String getUserProfileImage() {
        return userProfileImage;
    }

    public int getId_() {
        return id_;
    }

    public String getTvmsg() {
        return tvmsg;
    }
}

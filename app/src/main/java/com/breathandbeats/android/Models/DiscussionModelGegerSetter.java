package com.breathandbeats.android.Models;

/**
 * Created by Rocky on 1/2/2018.
 */

public class DiscussionModelGegerSetter {
    int id;
    String userimg,headertext,middletext,subject;

    public DiscussionModelGegerSetter(int id, String userimg, String headertext, String middletext, String subject) {
        this.id = id;
        this.userimg = userimg;
        this.headertext = headertext;
        this.middletext = middletext;
        this.subject = subject;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserimg() {
        return userimg;
    }

    public void setUserimg(String userimg) {
        this.userimg = userimg;
    }

    public String getHeadertext() {
        return headertext;
    }

    public void setHeadertext(String headertext) {
        this.headertext = headertext;
    }

    public String getMiddletext() {
        return middletext;
    }

    public void setMiddletext(String middletext) {
        this.middletext = middletext;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
